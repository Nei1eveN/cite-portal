package cp2.citeportalv1;

import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.presenters.splash.SplashPresenter;
import cp2.citeportalv1.presenters.splash.SplashPresenterImpl;

public class SplashScreenActivity extends AppCompatActivity implements SplashPresenter.SplashView {

    SplashPresenter splashPresenter;

    @BindView(R.id.imageIcon)
    ImageView splashIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        splashPresenter = new SplashPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        splashPresenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        splashPresenter.onDestroy();
    }

    @Override
    public void checkUser() {
        new Handler().postDelayed(() -> splashPresenter.checkUser(), 1000);
    }

    @Override
    public void setToastMessage(String message) {
        Toast.makeText(SplashScreenActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * To add user detection going to student, faculty, or guest activity.
     *
     * To fix student login module
     * **/
}

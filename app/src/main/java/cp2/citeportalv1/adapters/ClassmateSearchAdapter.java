package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.views.student.classmate.ClassmateProfileActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Student;

public class ClassmateSearchAdapter extends RecyclerView.Adapter<ClassmateSearchAdapter.ClassmateViewHolder> {
    private Context context;
    private List<Student> students;

    public ClassmateSearchAdapter(Context context, List<Student> students) {
        this.context = context;
        this.students = students;
    }

    class ClassmateViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.studentCard) CardView studentCard;
        @BindView(R.id.studentLayout) ConstraintLayout studentLayout;

        @BindView(R.id.ivStudImage) ImageView studentImage;
        @BindView(R.id.tvStudName) TextView studentName;
        @BindView(R.id.tvStudProgram) TextView studentProgram;
        @BindView(R.id.tvStudYrLvl) TextView studentYearLevel;

        ClassmateViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ClassmateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.student_classmate_list_item, parent, false);
        return new ClassmateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassmateViewHolder holder, int position) {
        Student student = students.get(position);

        Glide.with(context).load(student.getStudentImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(holder.studentImage);
        holder.studentName.setText(String.format("%s, %s %s", student.getLastName(), student.getfName(), student.getMidName()));
        holder.studentProgram.setText(student.getProgram());
        holder.studentYearLevel.setText(String.format("%s Year", student.getYearLvl()));

        holder.studentLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, ClassmateProfileActivity.class);
            intent.putExtra("studentImage", student.getStudentImageURL());
            intent.putExtra("studentName", holder.studentName.getText().toString());
            intent.putExtra("studentProg", holder.studentProgram.getText().toString());
            intent.putExtra("studentYearLvl", holder.studentYearLevel.getText().toString());
            intent.putExtra("studentId", String.valueOf(student.getStudentID()));
            intent.putExtra("studentEmail", student.getEmail());
            intent.putExtra("notificationId", student.studentUId);

            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return students.size();
    }
}

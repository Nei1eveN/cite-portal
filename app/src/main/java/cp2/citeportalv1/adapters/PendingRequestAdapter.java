package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.views.student.consultation.StudentPendingRequestsLogDetailActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.ConsultationRequest;

public class PendingRequestAdapter extends RecyclerView.Adapter<PendingRequestAdapter.ConsultRequestViewHolder> {
    private Context context;
    private List<ConsultationRequest> requests;


    public PendingRequestAdapter(Context context, List<ConsultationRequest> requests) {
        this.context = context;
        this.requests = requests;
    }

    public static class ConsultRequestViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintRequest)
        public ConstraintLayout constraintLayout;

        @BindView(R.id.ivUserImage)
        public ImageView facultyImage;
        @BindView(R.id.tvFacultyName)
        public TextView facultyName;
        @BindView(R.id.tvMessageResponse)
        public TextView messageTitle;

        public ConsultRequestViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ConsultRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.student_consultation_status_item, parent, false);
        return new ConsultRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConsultRequestViewHolder holder, int position) {
        /**
         * TO REPLACE ConsultationResponse with ConsultationRequest
         * **/
        ConsultationRequest response = requests.get(position);

        Log.d("notificationId", response.notificationId);

        Glide.with(context).load(response.getSenderImageURL()).apply(new RequestOptions().circleCrop()).into(holder.facultyImage);
        holder.facultyName.setText(String.format("%s, %s", response.getSenderLastName(), response.getSenderFirstName()));
        holder.messageTitle.setText(response.getMessageTitle());
        holder.constraintLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, StudentPendingRequestsLogDetailActivity.class);

            intent.putExtra("notificationId", response.notificationId);

            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });

    }

    @Override
    public int getItemCount() {
        return requests.size();
    }
}

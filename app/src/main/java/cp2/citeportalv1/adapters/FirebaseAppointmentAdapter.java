package cp2.citeportalv1.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import androidx.annotation.NonNull;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Appointments;

public class FirebaseAppointmentAdapter extends FirebaseRecyclerAdapter<Appointments, AppointmentAdapter.AppointmentViewHolder> {
//    /**
//     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
//     * {@link FirebaseRecyclerOptions} for configuration options.
//     *
//     * @param options
//     */

    private Context context;

    public FirebaseAppointmentAdapter(@NonNull FirebaseRecyclerOptions<Appointments> options, Context context) {
        super(options);
        this.context = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull AppointmentAdapter.AppointmentViewHolder holder, int i, @NonNull Appointments appointment) {

        holder.subject.setText(appointment.getMessageTitle());
        holder.venue.setText(appointment.getVenue());
        holder.timeStart.setText(String.format("%s", appointment.getRequestedTimeStart()));
        holder.timeEnd.setText(String.format("%s", appointment.getRequestedTimeEnd()));
        Glide.with(context).load(appointment.getSenderImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(holder.senderImage);

    }

    @NonNull
    @Override
    public AppointmentAdapter.AppointmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.faculty_appointment_list_item, parent, false);
        return new AppointmentAdapter.AppointmentViewHolder(view);
    }
}

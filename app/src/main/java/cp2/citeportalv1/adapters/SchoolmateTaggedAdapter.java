package cp2.citeportalv1.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Student;

public class SchoolmateTaggedAdapter extends RecyclerView.Adapter<SchoolmateTaggedAdapter.SchoolmateTaggedViewHolder>{
    private Context context;
    private List<Student> students;

    public SchoolmateTaggedAdapter(Context context, List<Student> students) {
        this.context = context;
        this.students = students;
    }

    class SchoolmateTaggedViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivTaggedImage) public ImageView taggedImage;
        @BindView(R.id.tvTaggedName) public TextView taggedName;
        @BindView(R.id.tvTaggedProgram) public TextView taggedProgram;

        SchoolmateTaggedViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public SchoolmateTaggedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_schoolmate_tag_item, parent, false);
        return new SchoolmateTaggedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolmateTaggedViewHolder holder, int position) {
        Student classmate = students.get(position);

        Glide.with(context).load(classmate.getStudentImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)).into(holder.taggedImage);
        holder.taggedName.setText(String.format("%s, %s", classmate.getLastName(), classmate.getfName()));
        holder.taggedProgram.setText(classmate.getProgram());
    }

    @Override
    public int getItemCount() {
        return students != null ? students.size() : 0;
    }
}

package cp2.citeportalv1.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Classmate;

public class ClassmateAutoCompleteAdapter extends ArrayAdapter<Classmate> {
    private Context mContext;
    private List<Classmate> classmateList;

    public ClassmateAutoCompleteAdapter(@NonNull Context context, @NonNull List<Classmate> classmates) {
        super(context, 0, classmates);
        this.mContext = context;
        classmateList = new ArrayList<>(classmates);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_classmate_tag_item, parent, false);
        }

        ImageView studentImage = convertView.findViewById(R.id.ivStudentProfileImage);
        TextView studentName = convertView.findViewById(R.id.tvStudentName);
        TextView studentEmail = convertView.findViewById(R.id.tvStudentEmail);

        Classmate classmate = getItem(position);

        if (classmate != null){
            Glide.with(mContext).load(classmate.getStudentImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(studentImage);
            studentName.setText(String.format("%s %s", classmate.getFirstName(), classmate.getLastName()));
            studentEmail.setText(classmate.getEmail());
        }

        return convertView;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<Classmate> classmates = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                classmates.addAll(classmateList);
            }
            else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Classmate classmate : classmateList) {
                    if (classmate.getFirstName().toLowerCase().contains(filterPattern)){
                        classmates.add(classmate);
                    }
                    else if (classmate.getLastName().toLowerCase().contains(filterPattern)){
                        classmates.add(classmate);
                    }
                    else if (classmate.getEmail().toLowerCase().contains(filterPattern)){
                        classmates.add(classmate);
                    }
                    else if (classmate.getStudentId().toString().contains(filterPattern)){
                        classmates.add(classmate);
                    }
                }
            }

            results.values = classmates;
            results.count = classmates.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Classmate) resultValue).getEmail();
        }
    };

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }
}

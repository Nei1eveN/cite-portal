package cp2.citeportalv1.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.listeners.RecyclerOnClickListener;
import cp2.citeportalv1.models.Schedule;

public class StudentConsultScheduleAdapter extends RecyclerView.Adapter<StudentConsultScheduleAdapter.ViewHolder> {
    private Context context;
    private List<Schedule> schedules;

    private RecyclerOnClickListener clickListener;

    public StudentConsultScheduleAdapter(Context context, List<Schedule> schedules, RecyclerOnClickListener listener) {
        this.context = context;
        this.schedules = schedules;
        this.clickListener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.scheduleConstraint) ConstraintLayout constraintLayout;

        @BindView(R.id.tvSchedStartTime) TextView schedStartTime;
        @BindView(R.id.tvSchedEndTime) TextView schedEndTime;
        @BindView(R.id.tvSchedVenue) TextView schedVenue;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.schedule_list_item_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Schedule schedule = schedules.get(position);

        holder.schedStartTime.setText(schedule.getTimeStart());
        holder.schedEndTime.setText(schedule.getTimeEnd());
        holder.schedVenue.setText(schedule.getVenue());

        holder.constraintLayout.setOnClickListener(v ->
                clickListener.OnDetailClick(
                        schedule.getTimeStart(),
                        schedule.getTimeEnd(),
                        schedule.getVenue(),
                        schedule.getDay()));
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }
}

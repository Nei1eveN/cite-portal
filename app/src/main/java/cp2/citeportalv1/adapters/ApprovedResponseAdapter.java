package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.views.student.consultation.StudentConsultationResponseLogDetailActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.ConsultationResponse;

public class ApprovedResponseAdapter extends RecyclerView.Adapter<ApprovedResponseAdapter.ApproveViewHolder> {
    private Context context;
    private List<ConsultationResponse> responseList;

    public ApprovedResponseAdapter(Context context, List<ConsultationResponse> responseList) {
        this.context = context;
        this.responseList = responseList;
    }

    public static class ApproveViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintRequest)
        public ConstraintLayout constraintLayout;

        @BindView(R.id.ivUserImage) public ImageView facultyImage;
        @BindView(R.id.tvFacultyName) public TextView facultyName;
        @BindView(R.id.tvMessageResponse) public TextView messageResponse;

        public ApproveViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ApproveViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.student_consultation_status_item, parent, false);
        return new ApproveViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ApproveViewHolder holder, int position) {
        ConsultationResponse response = responseList.get(position);

        Log.d("notificationId", response.notificationId);

        Glide.with(context).load(response.getSenderImageURL()).apply(new RequestOptions().circleCrop()).into(holder.facultyImage);
        holder.facultyName.setText(String.format("%s, %s", response.getSenderFirstName(), response.getSenderLastName()));
        holder.messageResponse.setText(response.getMessageRemarks());
//        switch (response.getMessageStatus()) {
//            case "REJECTED":
//                holder.chipStatus.setChipBackgroundColor(ColorStateList.valueOf(context.getResources().getColor(android.R.color.holo_red_light)));
//                holder.chipStatus.setText(response.getMessageStatus());
//                break;
//        }

        holder.constraintLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, StudentConsultationResponseLogDetailActivity.class);

            intent.putExtra("notificationId", response.notificationId);

            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return responseList.size();
    }
}

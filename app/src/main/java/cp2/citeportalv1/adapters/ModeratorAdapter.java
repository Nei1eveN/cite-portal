package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.views.admin.AccountDetailActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Accounts;

import static cp2.citeportalv1.utils.Constants.ACCOUNT_UID;

public class ModeratorAdapter extends RecyclerView.Adapter<ModeratorAdapter.ModeratorViewHolder> {
    private Context context;
    private List<Accounts> accounts;

    public ModeratorAdapter(Context context, List<Accounts> accounts) {
        this.context = context;
        this.accounts = accounts;
    }

    static class ModeratorViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintLayout) ConstraintLayout constraintLayout;
        @BindView(R.id.ivUserImage) ImageView userImage;
        @BindView(R.id.tvLastName) TextView lastName;
        @BindView(R.id.tvName) TextView name;
        @BindView(R.id.tvEmail) TextView email;
        @BindView(R.id.tvAccessLevel) TextView accessLevel;

        ModeratorViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ModeratorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ModeratorViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_moderator_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ModeratorViewHolder holder, int position) {
        Accounts account = accounts.get(position);

        holder.lastName.setText(account.getLastName());
        holder.name.setText(String.format("%s %s", account.getFirstName(), account.getMiddleName()));
        holder.email.setText(account.getEmail());
        holder.accessLevel.setText(String.format("Access Level: %s", account.getAccessLevel().toUpperCase()));

        holder.constraintLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, AccountDetailActivity.class);
            intent.putExtra(ACCOUNT_UID, account.accountsId);
            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }
}

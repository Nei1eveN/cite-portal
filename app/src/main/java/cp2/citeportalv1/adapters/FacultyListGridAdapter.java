package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.views.student.consultation.SendConcernActivity;
import cp2.citeportalv1.models.Faculty;

public class FacultyListGridAdapter extends RecyclerView.Adapter<FacultyListGridAdapter.FacultyListViewHolder> {
    private Context context;
    private List<Faculty> facultyList;

    public FacultyListGridAdapter(Context context, List<Faculty> facultyList) {
        this.context = context;
        this.facultyList = facultyList;
    }

    public static class FacultyListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardView) public CardView cardView;
        @BindView(R.id.ivUserImage) public ImageView facultyImage;
        @BindView(R.id.tvFacultyLastName) public TextView facultyLastName;
        @BindView(R.id.tvFacultyName) public TextView facultyName;

        public FacultyListViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public FacultyListGridAdapter.FacultyListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.faculty_instructor_list_item, viewGroup, false);

        return new FacultyListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FacultyListGridAdapter.FacultyListViewHolder viewHolder, int i) {
        Faculty faculty = facultyList.get(i);
        Log.d("facultyIds", faculty.facultyId);
        Glide.with(context).load(faculty.getFacultyImageURL()).apply(new RequestOptions().circleCrop()).into(viewHolder.facultyImage);
        viewHolder.facultyName.setText(String.format("%s %s", faculty.getfName(), faculty.getMidName()));
        viewHolder.cardView.setOnClickListener(v -> {
            Intent intent = new Intent(context, SendConcernActivity.class);
            intent.putExtra("facultyId", faculty.facultyId);
            intent.putExtra("facultyImage", faculty.getFacultyImageURL());
            intent.putExtra("facultyLastName", String.format("%s", faculty.getLastName()));
            intent.putExtra("facultyFirstName", faculty.getfName());
            intent.putExtra("facultyMiddleName", faculty.getMidName());
            intent.putExtra("facultyDepartment", faculty.getDepartment()+" Department");
            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return facultyList.size();
    }
}

package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.views.faculty.FacultyConsultationDetailsActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.ConsultationRequest;

public class ConsultationRequestAdapter extends RecyclerView.Adapter<ConsultationRequestAdapter.ViewHolder> {
    private Context context;
    private List<ConsultationRequest> notifications;

    public ConsultationRequestAdapter(Context context, List<ConsultationRequest> notifications) {
        this.context = context;
        this.notifications = notifications;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.notificationConstraint) public ConstraintLayout constraintLayout;

        @BindView(R.id.ivSenderImageProfile) public ImageView senderProfileImage;
        @BindView(R.id.tvSenderName) public TextView senderName;
        @BindView(R.id.tvSenderMessage) public TextView senderMessage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ConsultationRequest notification = notifications.get(i);

        Log.d("notificationId", notification.notificationId);

        Date date = new Date(Long.parseLong(notification.getTimeStamp())*1000);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("H:mm a", Locale.getDefault());
        String formattedTime = simpleDateFormat.format(date);

        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("EEE", Locale.getDefault());
        String formattedDay = simpleDateFormat1.format(date);

//        if (DateUtils.isToday(Long.parseLong(notification.getTimeStamp())*1000)){
////                            viewHolder.senderTimeStamp.setText(formattedTime);
//            viewHolder.senderTimeStamp.setText(DateUtils.getRelativeTimeSpanString(Long.parseLong(notification.getTimeStamp())*1000));
//        }
//        else {
//            viewHolder.senderTimeStamp.setText(String.format("%s, %s", formattedDay, formattedTime));
//            //            viewHolder.senderTimeStamp.setText(DateUtils.getRelativeTimeSpanString(Long.parseLong(notification.getTimeStamp())*1000));
//        }

        viewHolder.senderName.setText(String.format("%s, %s %s", notification.getSenderLastName(), notification.getSenderFirstName(), notification.getSenderMiddleName())); //notification.getSenderId()

        viewHolder.senderMessage.setText(String.format("%s%s", notification.getMessageTitle(), notification.getMessageBody()));
        Glide.with(context).load(notification.getSenderImageURL()).apply(new RequestOptions().circleCrop()).into(viewHolder.senderProfileImage);

        viewHolder.constraintLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, FacultyConsultationDetailsActivity.class);
            intent.putExtra("notificationId", notification.notificationId);
            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }
}

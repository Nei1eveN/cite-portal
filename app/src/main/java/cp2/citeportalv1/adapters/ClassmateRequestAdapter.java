package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.ClassmateRequest;
import cp2.citeportalv1.views.student.classmate.ClassmateProfileActivity;

public class ClassmateRequestAdapter extends RecyclerView.Adapter<ClassmateRequestAdapter.ClassmateRequestViewHolder>{
    private Context context;
    private List<ClassmateRequest> requestList;

    public ClassmateRequestAdapter(Context context, List<ClassmateRequest> requestList) {
        this.context = context;
        this.requestList = requestList;
    }

    class ClassmateRequestViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.studentLayout)
        ConstraintLayout constraintLayout;
        @BindView(R.id.ivStudImage)
        ImageView profileImage;
        @BindView(R.id.tvStudName)
        TextView studentName;
        @BindView(R.id.tvStudProgram) TextView studentProgram;
        @BindView(R.id.tvStudYrLvl) TextView studentYearLevel;

        ClassmateRequestViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ClassmateRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.student_classmate_list_item, parent, false);
        return new ClassmateRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassmateRequestViewHolder holder, int position) {
        ClassmateRequest request = requestList.get(position);
        Glide.with(context).load(request.getStudentImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(holder.profileImage);
        holder.studentName.setText(String.format("%s, %s %s", request.getLastName(), request.getFirstName(), request.getMiddleName()));
        holder.studentProgram.setText(request.getProgram());
        holder.studentYearLevel.setText(String.format("%s Year", request.getYearLvl()));

        holder.constraintLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, ClassmateProfileActivity.class);
            intent.putExtra("studentImage", request.getStudentImageURL());
            intent.putExtra("studentName", holder.studentName.getText().toString());
            intent.putExtra("studentProg", holder.studentProgram.getText().toString());
            intent.putExtra("studentYearLvl", holder.studentYearLevel.getText().toString());
            intent.putExtra("studentId", String.valueOf(request.getStudentId()));
            intent.putExtra("notificationId", request.classmateRequestKeyId);

            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return requestList.size();
    }


}

package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Class;
import cp2.citeportalv1.views.faculty.FacultyClassInfoActivity;

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.FacultyClassViewHolder> {

    private List<Class> classList;
    private Context context;

    public ClassAdapter(List<Class> classList, Context context) {
        this.classList = classList;
        this.context = context;
    }

    class FacultyClassViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintCourseBody)
        ConstraintLayout constraintCourseBody;
        @BindView(R.id.tvFacClassCourseCode) TextView courseCode;
        @BindView(R.id.tvFacClassCourseDesc) TextView courseDesc;
        @BindView(R.id.tvFacClassSection) TextView courseSection;
        @BindView(R.id.btnFacClassInfo) Button courseClassInfo;

        View view;

        FacultyClassViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, view);
        }
    }

    @NonNull
    @Override
    public FacultyClassViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.faculty_class_list, viewGroup, false);

        return new FacultyClassViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassAdapter.FacultyClassViewHolder viewHolder, int position) {
        /**
         *get a random color.
         * int randomColor = MaterialColorPalette.getRandomColor("700");
         **/

        /**
         * create your own material color palette
         * MaterialColorPalette myCustomPalette = new MaterialColorPalette(Color.CYAN);
         * int my200Color = myCustomPalette.getColor("200");
         * **/

        Log.d("documentIds", classList.get(position).facultyClassId);
        viewHolder.courseCode.setText(classList.get(position).getCourseCode());
        viewHolder.courseDesc.setText(classList.get(position).getCourseDescription());
        viewHolder.courseSection.setText(classList.get(position).getSection());
        viewHolder.courseClassInfo.setOnClickListener(v -> {
            Intent intent = new Intent(context, FacultyClassInfoActivity.class);
            intent.putExtra("courseTitle", classList.get(position).getCourseDescription());
            intent.putExtra("courseSection", classList.get(position).getSection());
            intent.putExtra("courseCode", classList.get(position).getCourseCode());
            intent.putExtra("courseDescription", classList.get(position).getCourseDescription());
            intent.putExtra("createdByUid", classList.get(position).getCreatedBy());
            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return classList.size();
    }
}

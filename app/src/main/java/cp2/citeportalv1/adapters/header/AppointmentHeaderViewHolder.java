package cp2.citeportalv1.adapters.header;

import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;

public class AppointmentHeaderViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvApptDay) public TextView dayHeader;
    @BindView(R.id.btnViewAll) public MaterialButton btnViewAll;

    public AppointmentHeaderViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

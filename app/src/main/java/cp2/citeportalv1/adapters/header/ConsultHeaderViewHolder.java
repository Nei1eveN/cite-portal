package cp2.citeportalv1.adapters.header;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;

public class ConsultHeaderViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvHeaderDay) public TextView day;
    @BindView(R.id.tvHeaderDate) public TextView date;

    public ConsultHeaderViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

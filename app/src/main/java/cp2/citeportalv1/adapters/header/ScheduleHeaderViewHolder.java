package cp2.citeportalv1.adapters.header;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;

public class ScheduleHeaderViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvScheduleDayHeader) public TextView dayHeader;

    public ScheduleHeaderViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

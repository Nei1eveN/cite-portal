package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.views.secretary.DepartmentConsultationLogDetailActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.ConsultationLog;

import static cp2.citeportalv1.utils.Constants.FACULTY_UID;
import static cp2.citeportalv1.utils.Constants.NOTIFICATION_ID;

public class DepartmentInstructorLogAdapter extends RecyclerView.Adapter<DepartmentInstructorLogAdapter.InstructorLogViewHolder>{
    private Context context;
    private List<ConsultationLog> consultationLogs;

    public DepartmentInstructorLogAdapter(Context context, List<ConsultationLog> consultationLogs) {
        this.context = context;
        this.consultationLogs = consultationLogs;
    }

    static class InstructorLogViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintApptItem) public ConstraintLayout apptItem;

        @BindView(R.id.tvTitle) public TextView subject;
        @BindView(R.id.tvVenue) public TextView venue;
        @BindView(R.id.tvTimeStart) public TextView timeStart;
        @BindView(R.id.tvTimeEnd) public TextView timeEnd;
        @BindView(R.id.ivSenderImage) public ImageView senderImage;

        InstructorLogViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public InstructorLogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new InstructorLogViewHolder(LayoutInflater.from(context).inflate(R.layout.faculty_appointment_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull InstructorLogViewHolder holder, int position) {
        ConsultationLog consultationLog = consultationLogs.get(position);

        holder.subject.setText(consultationLog.getMessageTitle());
        holder.venue.setText(consultationLog.getVenue());
        holder.timeStart.setText(String.format("%s", consultationLog.getRequestedTimeStart()));
        holder.timeEnd.setText(String.format("%s", consultationLog.getRequestedTimeEnd()));
        Glide.with(context).load(consultationLog.getSenderImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(holder.senderImage);
        holder.apptItem.setOnClickListener(v -> {
            Intent intent = new Intent(context, DepartmentConsultationLogDetailActivity.class);
            intent.putExtra(NOTIFICATION_ID, consultationLog.consultationLogId);
            intent.putExtra(FACULTY_UID, consultationLog.getReceiverId());
            context.startActivity(intent);
            ((Activity) context).finish();
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return consultationLogs.size();
    }
}

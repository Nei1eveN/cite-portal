package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Classmate;
import cp2.citeportalv1.views.student.classmate.ClassmateProfileActivity;

public class ClassmateListAdapter extends RecyclerView.Adapter<ClassmateListAdapter.ClassmateViewHolder> {
    private Context context;
    private List<Classmate> classmates;

    public ClassmateListAdapter(Context context, List<Classmate> classmates) {
        this.context = context;
        this.classmates = classmates;
    }

    class ClassmateViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.studentLayout) ConstraintLayout constraintLayout;
        @BindView(R.id.ivStudImage) ImageView profileImage;
        @BindView(R.id.tvStudName) TextView studentName;
        @BindView(R.id.tvStudProgram) TextView studentProgram;
        @BindView(R.id.tvStudYrLvl) TextView studentYearLevel;

        ClassmateViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ClassmateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.student_classmate_list_item, parent, false);
        return new ClassmateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassmateViewHolder holder, int position) {
        Classmate classmate = classmates.get(position);

        Glide.with(context).load(classmate.getStudentImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(holder.profileImage);
        holder.studentName.setText(String.format("%s, %s %s", classmate.getLastName(), classmate.getFirstName(), classmate.getMiddleName()));
        holder.studentProgram.setText(classmate.getProgram());
        holder.studentYearLevel.setText(String.format("%s Year", classmate.getYearLvl()));

        holder.constraintLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, ClassmateProfileActivity.class);
            intent.putExtra("studentImage", classmate.getStudentImageURL());
            intent.putExtra("studentName", holder.studentName.getText().toString());
            intent.putExtra("studentProg", holder.studentProgram.getText().toString());
            intent.putExtra("studentYearLvl", holder.studentYearLevel.getText().toString());
            intent.putExtra("studentId", String.valueOf(classmate.getStudentId()));
            intent.putExtra("studentEmail", classmate.getEmail());
            intent.putExtra("notificationId", classmate.classmateUId);

            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return classmates.size();
    }
}

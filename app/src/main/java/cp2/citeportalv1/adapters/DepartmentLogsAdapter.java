package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.views.secretary.InstructorConsultationLogsActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Faculty;

import static cp2.citeportalv1.utils.Constants.FACULTY_UID;

public class DepartmentLogsAdapter extends RecyclerView.Adapter<DepartmentLogsAdapter.DepartmentLogViewHolder> {
    private Context context;
    private List<Faculty> facultyList;

    public DepartmentLogsAdapter(Context context, List<Faculty> facultyList) {
        this.context = context;
        this.facultyList = facultyList;
    }

    static class DepartmentLogViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintLayout) ConstraintLayout constraintLayout;
        @BindView(R.id.ivUserImage) ImageView facultyImage;
        @BindView(R.id.tvLastName) TextView lastName;
        @BindView(R.id.tvName) TextView name;
        @BindView(R.id.tvEmail) TextView email;

        DepartmentLogViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public DepartmentLogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DepartmentLogViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_faculty_member_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DepartmentLogViewHolder holder, int position) {
        Faculty faculty = facultyList.get(position);

        Glide.with(context).load(faculty.getFacultyImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(holder.facultyImage);
        holder.lastName.setText(faculty.getLastName());
        holder.name.setText(String.format("%s %s", faculty.getfName(), faculty.getMidName()));
        holder.email.setText(faculty.getEmail());

        holder.constraintLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, InstructorConsultationLogsActivity.class);
            intent.putExtra(FACULTY_UID, faculty.facultyId);
            context.startActivity(intent);
//            ((FacultyLoginActivity) context).finish();
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return facultyList.size();
    }
}

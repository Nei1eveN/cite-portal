package cp2.citeportalv1.adapters.grouplist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import cp2.citeportalv1.views.faculty.FacultyAppointmentDetailsActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.AppointmentAdapter;
import cp2.citeportalv1.adapters.header.AppointmentHeaderViewHolder;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.views.faculty.FacultyAppointmentsActivity;

public class FacultyAppointmentDayGroupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ListItem> itemList;

    public FacultyAppointmentDayGroupAdapter(Context context, List<ListItem> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ListItem.TYPE_DATE) {
//            View view = LayoutInflater.from(context).inflate(R.layout.appointment_header_day_item, parent, false);
            return new AppointmentHeaderViewHolder(LayoutInflater.from(context).inflate(R.layout.appointment_header_day_item, parent, false));
        } else if (viewType == ListItem.TYPE_GENERAL) {
//            View view = LayoutInflater.from(context).inflate(R.layout.faculty_appointment_list_item, parent, false);
            return new AppointmentAdapter.AppointmentViewHolder(LayoutInflater.from(context).inflate(R.layout.faculty_appointment_list_item, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == ListItem.TYPE_DATE) {
            DateItem dateItem = (DateItem) itemList.get(position);
            AppointmentHeaderViewHolder viewHolder = (AppointmentHeaderViewHolder) holder;
            viewHolder.dayHeader.setText(dateItem.getDate());
            viewHolder.btnViewAll.setOnClickListener(v
                    -> context.startActivity(new Intent(context, FacultyAppointmentsActivity.class)));
        } else if (holder.getItemViewType() == ListItem.TYPE_GENERAL) {
            GeneralItem generalItem = (GeneralItem) itemList.get(position);
            AppointmentAdapter.AppointmentViewHolder viewHolder = (AppointmentAdapter.AppointmentViewHolder) holder;

            viewHolder.subject.setText(generalItem.getAppointments().getMessageTitle());
            viewHolder.venue.setText(generalItem.getAppointments().getVenue());
            viewHolder.timeStart.setText(generalItem.getAppointments().getRequestedTimeStart());
            viewHolder.timeEnd.setText(generalItem.getAppointments().getRequestedTimeEnd());
            Glide.with(context).load(generalItem.getAppointments().getSenderImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px).diskCacheStrategy(DiskCacheStrategy.RESOURCE)).into(viewHolder.senderImage);
            viewHolder.apptItem.setOnClickListener(v -> {
                Intent intent = new Intent(context, FacultyAppointmentDetailsActivity.class);
                intent.putExtra("notificationId", generalItem.getAppointments().appointmentId);
                context.startActivity(intent);
//                ((FacultyLoginActivity) context).finish();
                ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
            });
        }
    }

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType();
    }
}

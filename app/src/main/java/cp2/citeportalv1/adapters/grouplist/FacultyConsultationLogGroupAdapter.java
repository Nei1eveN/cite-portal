package cp2.citeportalv1.adapters.grouplist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import cp2.citeportalv1.views.faculty.FacultyConsultationLogDetailActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.AppointmentAdapter;
import cp2.citeportalv1.adapters.header.ScheduleHeaderViewHolder;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;

public class FacultyConsultationLogGroupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<ListItem> itemList;

    public FacultyConsultationLogGroupAdapter(Context context, List<ListItem> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ListItem.TYPE_DATE) {
            View view = LayoutInflater.from(context).inflate(R.layout.header_day_item, parent, false);
            return new ScheduleHeaderViewHolder(view);
        } else if (viewType == ListItem.TYPE_GENERAL) {
            View view = LayoutInflater.from(context).inflate(R.layout.faculty_appointment_list_item, parent, false);
            return new AppointmentAdapter.AppointmentViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == ListItem.TYPE_DATE) {
            DateItem dateItem = (DateItem) itemList.get(position);
            ScheduleHeaderViewHolder viewHolder = (ScheduleHeaderViewHolder) holder;
            viewHolder.dayHeader.setText(dateItem.getDate());
        } else if (holder.getItemViewType() == ListItem.TYPE_GENERAL) {
            GeneralItem generalItem = (GeneralItem) itemList.get(position);
            AppointmentAdapter.AppointmentViewHolder viewHolder = (AppointmentAdapter.AppointmentViewHolder) holder;

            viewHolder.subject.setText(generalItem.getConsultationLog().getMessageTitle());
            viewHolder.venue.setText(generalItem.getConsultationLog().getVenue());
            viewHolder.timeStart.setText(generalItem.getConsultationLog().getRequestedTimeStart());
            viewHolder.timeEnd.setText(generalItem.getConsultationLog().getRequestedTimeEnd());
            Glide.with(context).load(generalItem.getConsultationLog().getSenderImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px).diskCacheStrategy(DiskCacheStrategy.ALL)).into(viewHolder.senderImage);

            viewHolder.apptItem.setOnClickListener(v -> {
                Intent intent = new Intent(context, FacultyConsultationLogDetailActivity.class);
                intent.putExtra("notificationId", generalItem.getConsultationLog().consultationLogId);
                context.startActivity(intent);
//                ((FacultyLoginActivity) context).finish();
                ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
            });
        }
    }

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType();
    }

}

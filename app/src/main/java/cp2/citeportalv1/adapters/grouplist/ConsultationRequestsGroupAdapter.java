package cp2.citeportalv1.adapters.grouplist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ConsultationRequestAdapter;
import cp2.citeportalv1.adapters.header.ConsultHeaderViewHolder;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.views.faculty.FacultyConsultationDetailsActivity;

public class ConsultationRequestsGroupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ListItem> itemList;
    private Context context;

    public ConsultationRequestsGroupAdapter(List<ListItem> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ListItem.TYPE_DATE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_day_date_item, parent, false);
            return new ConsultHeaderViewHolder(view);
        }
        else if (viewType == ListItem.TYPE_GENERAL){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item, parent, false);
            return new ConsultationRequestAdapter.ViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == ListItem.TYPE_DATE) {
            DateItem dateItem = (DateItem) itemList.get(position);
            ConsultHeaderViewHolder viewHolder = (ConsultHeaderViewHolder) holder;
            viewHolder.day.setText(dateItem.getDay());
            viewHolder.date.setText(dateItem.getDate());
        }else if (holder.getItemViewType() == ListItem.TYPE_GENERAL){
            GeneralItem generalItem = (GeneralItem) itemList.get(position);
            ConsultationRequestAdapter.ViewHolder viewHolder = (ConsultationRequestAdapter.ViewHolder) holder;
            Glide.with(context)
                    .load(generalItem.getRequest().getSenderImageURL())
                    .apply(new RequestOptions()
                            .circleCrop()
                            .placeholder(R.drawable.cite_logo300px)
                            .error(R.drawable.cite_logo300px)
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(viewHolder.senderProfileImage);
            viewHolder.senderName.setText(
                    String.format("%s, %s",
                            generalItem.getRequest().getSenderLastName().toUpperCase(),
                            generalItem.getRequest().getSenderFirstName()));
            viewHolder.senderMessage.setText(generalItem.getRequest().getMessageTitle());
            viewHolder.constraintLayout.setOnClickListener(v -> {
                Intent intent = new Intent(context, FacultyConsultationDetailsActivity.class);
                intent.putExtra("notificationId", generalItem.getRequest().notificationId);
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
            });

        }
    }

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0;
    }
}

package cp2.citeportalv1.adapters.grouplist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ScheduleAdapter;
import cp2.citeportalv1.adapters.header.ScheduleHeaderViewHolder;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.utils.Constants;

public class ScheduleGroupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ListItem> itemList;

    public ScheduleGroupAdapter(Context context, List<ListItem> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ListItem.TYPE_DATE) {
            View view = LayoutInflater.from(context).inflate(R.layout.header_day_item, parent, false);
            return new ScheduleHeaderViewHolder(view);
        } else if (viewType == ListItem.TYPE_GENERAL) {
            View view = LayoutInflater.from(context).inflate(R.layout.faculty_schedule_list_item, parent, false);
            return new ScheduleAdapter.ViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == ListItem.TYPE_DATE) {
            DateItem dateItem = (DateItem) itemList.get(position);
            ScheduleHeaderViewHolder viewHolder = (ScheduleHeaderViewHolder) holder;
            viewHolder.dayHeader.setText(dateItem.getDay());
        } else if (holder.getItemViewType() == ListItem.TYPE_GENERAL) {
            GeneralItem generalItem = (GeneralItem) itemList.get(position);
            ScheduleAdapter.ViewHolder viewHolder = (ScheduleAdapter.ViewHolder) holder;
            viewHolder.timeStart.setText(generalItem.getSchedule().getTimeStart());
            viewHolder.timeEnd.setText(generalItem.getSchedule().getTimeEnd());
            viewHolder.venue.setText(generalItem.getSchedule().getVenue());

            try {
                Date startTime = Constants.timeFormat12hr.parse(generalItem.getSchedule().getTimeStart());
                Date endTime = Constants.timeFormat12hr.parse(generalItem.getSchedule().getTimeEnd());
                long difference = endTime.getTime() - startTime.getTime();
                long diffHours = difference / (60 * 60 * 1000) % 24;
                long diffMinutes = difference / (60 * 1000) % 60;

                if (diffHours == 0) {
                    viewHolder.hours.setText(String.format(Locale.getDefault(), "Duration: %d minutes", diffMinutes));
                }
                if (diffMinutes == 1) {
                    viewHolder.hours.setText(String.format(Locale.getDefault(), "Duration: %d minute", diffMinutes));
                }
                if (diffHours == 1 && diffMinutes == 1) {
                    viewHolder.hours.setText(String.format(Locale.getDefault(), "Duration: %d hour and %d minute", diffHours, diffMinutes));
                }
                if (diffHours == 1 && diffMinutes > 1) {
                    viewHolder.hours.setText(String.format(Locale.getDefault(), "Duration: %d hour and %d minutes", diffHours, diffMinutes));
                }
                if (diffHours > 1 && diffMinutes > 1){
                    viewHolder.hours.setText(String.format(Locale.getDefault(), "Duration: %d hours and %d minutes", diffHours, diffMinutes));
                }
                if (diffMinutes == 0 && diffHours == 1) {
                    viewHolder.hours.setText(String.format(Locale.getDefault(), "Duration: %d hour", diffHours));
                }
                if (diffMinutes == 0 && diffHours > 1){
                    viewHolder.hours.setText(String.format(Locale.getDefault(), "Duration: %d hours", diffHours));
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType();
    }
}

package cp2.citeportalv1.adapters.grouplist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.FacultyListGridAdapter;
import cp2.citeportalv1.adapters.header.DepartmentHeaderViewHolder;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.views.student.consultation.SendConcernActivity;

public class InstructorGroupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<ListItem> itemList;

    public InstructorGroupAdapter(Context context, List<ListItem> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType();
    }

    public boolean isHeader(int position) {
        return position == 0;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ListItem.TYPE_DATE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_department_item, parent, false);
            return new DepartmentHeaderViewHolder(view);
        } else if (viewType == ListItem.TYPE_GENERAL) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.faculty_instructor_list_item, parent, false);
            return new FacultyListGridAdapter.FacultyListViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == ListItem.TYPE_DATE) {
            DateItem dateItem = (DateItem) itemList.get(position);
            DepartmentHeaderViewHolder viewHolder = (DepartmentHeaderViewHolder) holder;
            viewHolder.departmentHeader.setText(dateItem.getDepartment());
        } else if (holder.getItemViewType() == ListItem.TYPE_GENERAL) {
            GeneralItem generalItem = (GeneralItem) itemList.get(position);
            FacultyListGridAdapter.FacultyListViewHolder viewHolder = (FacultyListGridAdapter.FacultyListViewHolder) holder;
            Glide.with(context).load(generalItem.getFaculty().getFacultyImageURL()).apply(new RequestOptions().circleCrop().placeholder(context.getResources().getDrawable(R.drawable.cite_logo300px)).error(context.getResources().getDrawable(R.drawable.cite_logo300px)).diskCacheStrategy(DiskCacheStrategy.ALL)).into(viewHolder.facultyImage);
            viewHolder.facultyLastName.setText(generalItem.getFaculty().getLastName().toUpperCase());
            viewHolder.facultyName.setText(String.format("%s %s", generalItem.getFaculty().getfName(), generalItem.getFaculty().getMidName()));
            viewHolder.cardView.setOnClickListener(v -> {
                Intent intent = new Intent(context, SendConcernActivity.class);
                intent.putExtra("facultyId", generalItem.getFaculty().facultyId);
                intent.putExtra("facultyImage", generalItem.getFaculty().getFacultyImageURL());
                intent.putExtra("facultyLastName", String.format("%s", generalItem.getFaculty().getLastName()));
                intent.putExtra("facultyFirstName", generalItem.getFaculty().getfName());
                intent.putExtra("facultyMiddleName", generalItem.getFaculty().getMidName());
                intent.putExtra("facultyDepartment", generalItem.getFaculty().getDepartment()+" Department");
                context.startActivity(intent);
//                ((FacultyLoginActivity)context).finish();
                ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
            });
        }
    }

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0;
    }
}

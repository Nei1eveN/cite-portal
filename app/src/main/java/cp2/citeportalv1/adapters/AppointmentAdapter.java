package cp2.citeportalv1.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Appointments;

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.AppointmentViewHolder> {
    private Context context;
    private List<Appointments> appointmentList;

    public AppointmentAdapter(Context context, List<Appointments> appointmentList) {
        this.context = context;
        this.appointmentList = appointmentList;
    }

    public static class AppointmentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintApptItem) public ConstraintLayout apptItem;

        @BindView(R.id.tvTitle) public TextView subject;
        @BindView(R.id.tvVenue) public TextView venue;
        @BindView(R.id.tvTimeStart) public TextView timeStart;
        @BindView(R.id.tvTimeEnd) public TextView timeEnd;
        @BindView(R.id.ivSenderImage) public ImageView senderImage;

        public AppointmentViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public AppointmentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.faculty_appointment_list_item, viewGroup, false);
        return new AppointmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AppointmentViewHolder holder, int i) {
        Appointments appointment = appointmentList.get(i);
        holder.subject.setText(appointment.getMessageTitle());
        holder.venue.setText(appointment.getVenue());
        holder.timeStart.setText(String.format("%s", appointment.getRequestedTimeStart()));
        holder.timeEnd.setText(String.format("%s", appointment.getRequestedTimeEnd()));
        Glide.with(context).load(appointment.getSenderImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(holder.senderImage);

    }

    @Override
    public int getItemCount() {
        return appointmentList.size();
    }


}

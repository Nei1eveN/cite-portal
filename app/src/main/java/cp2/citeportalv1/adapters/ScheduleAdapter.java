package cp2.citeportalv1.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Schedule;

import static cp2.citeportalv1.utils.Constants.timeFormat12hr;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {
    private Context context;
    private List<Schedule> list;

    public ScheduleAdapter(Context context, List<Schedule> list) {
        this.context = context;
        this.list = list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintSchedule) public ConstraintLayout constraintLayout;

//        @BindView(R.id.tvDay) TextView day;
        @BindView(R.id.tvSchedStartTime) public TextView timeStart;
        @BindView(R.id.tvEndingTime) public TextView timeEnd;
        @BindView(R.id.tvVenue) public TextView venue;
        @BindView(R.id.tvHours) public TextView hours;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.faculty_schedule_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Schedule schedule = list.get(i);

        Date startTime = new Date(Long.parseLong(schedule.getTimeStart()));
        Date endTime = new Date(Long.parseLong(schedule.getTimeEnd()));

        long difference = endTime.getTime() - startTime.getTime();
        long diffHours = difference / (60 * 60 * 1000) % 24;
        long diffMinutes = difference / (60 * 1000) % 60;

//        viewHolder.day.setText(schedule.getDay());
        viewHolder.timeStart.setText(timeFormat12hr.format(startTime));
        viewHolder.timeEnd.setText(timeFormat12hr.format(endTime));
        viewHolder.venue.setText(schedule.getVenue());
        viewHolder.hours.setText(String.format(Locale.getDefault(),"%d hour(s) %d minutes", diffHours, diffMinutes));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

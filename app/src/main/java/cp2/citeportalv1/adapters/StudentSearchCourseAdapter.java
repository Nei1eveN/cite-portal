package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Class;
import cp2.citeportalv1.views.student.section.ClassSectionActivity;

public class StudentSearchCourseAdapter extends RecyclerView.Adapter<StudentSearchCourseAdapter.SearchCourseViewHolder> {
    private Context context;
    private List<Class> classList;

    public StudentSearchCourseAdapter(Context context, List<Class> classList) {
        this.context = context;
        this.classList = classList;
    }

    class SearchCourseViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvSection)
        TextView courseSection;
        @BindView(R.id.tvCourseCode) TextView courseCode;
        @BindView(R.id.tvCourseDescription) TextView courseDescription;
        @BindView(R.id.btnClassInfo)
        Button btnClassInfo;

        SearchCourseViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public SearchCourseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.student_search_class_section_item, viewGroup, false);
        return new SearchCourseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchCourseViewHolder searchCourseViewHolder, int i) {
        Class aClass = classList.get(i);

        Log.d("classId", aClass.facultyClassId);

        searchCourseViewHolder.courseSection.setText(aClass.getSection());
        searchCourseViewHolder.courseCode.setText(aClass.getCourseCode());
        searchCourseViewHolder.courseDescription.setText(aClass.getCourseDescription());
        searchCourseViewHolder.btnClassInfo.setOnClickListener(v -> {
            Intent intent = new Intent(context, ClassSectionActivity.class);
            intent.putExtra("courseSection", aClass.getSection());
            intent.putExtra("courseCode", aClass.getCourseCode());
            intent.putExtra("courseDesc", aClass.getCourseDescription());
            intent.putExtra("courseInstructor", aClass.getCreatedBy());
            intent.putExtra("classId", aClass.facultyClassId);
            intent.putExtra("createdByUid", aClass.getCreatedBy());
            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

}

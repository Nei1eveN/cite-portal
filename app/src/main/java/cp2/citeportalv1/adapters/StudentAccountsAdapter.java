package cp2.citeportalv1.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.views.secretary.StudentUserDetailActivity;
import cp2.citeportalv1.models.Student;

import static cp2.citeportalv1.utils.Constants.STUDENT_UID;

public class StudentAccountsAdapter extends RecyclerView.Adapter<StudentAccountsAdapter.StudentAccountViewHolder> {
    private Context context;
    private List<Student> studentList;

    public StudentAccountsAdapter(Context context, List<Student> studentList) {
        this.context = context;
        this.studentList = studentList;
    }

    static class StudentAccountViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintLayout) ConstraintLayout constraintLayout;
        @BindView(R.id.ivUserImage) ImageView facultyImage;
        @BindView(R.id.tvLastName) TextView lastName;
        @BindView(R.id.tvName) TextView name;
        @BindView(R.id.tvProgram) TextView program;
        @BindView(R.id.tvYearLevel) TextView yearLevel;
        @BindView(R.id.tvEmail) TextView email;

        StudentAccountViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public StudentAccountViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StudentAccountViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_student_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StudentAccountViewHolder holder, int position) {
        Student student = studentList.get(position);

        Glide.with(context).load(student.getStudentImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(holder.facultyImage);
        holder.lastName.setText(student.getLastName());
        holder.name.setText(String.format("%s %s", student.getfName(), student.getMidName()));
        holder.program.setText(student.getProgram());
        holder.yearLevel.setText(String.format(Locale.getDefault(),"%s | %d", student.getYearLvl(), student.getStudentID()));
        holder.email.setText(student.getEmail());

        holder.constraintLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, StudentUserDetailActivity.class);
            intent.putExtra(STUDENT_UID, student.studentUId);
            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }
}

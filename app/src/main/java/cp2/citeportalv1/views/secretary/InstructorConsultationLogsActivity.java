package cp2.citeportalv1.views.secretary;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.DepartmentInstructorLogAdapter;
import cp2.citeportalv1.presenters.reports.faculty_report_logs.InstructorLogPresenter;
import cp2.citeportalv1.presenters.reports.faculty_report_logs.InstructorLogPresenterImpl;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import static cp2.citeportalv1.utils.Constants.FACULTY_UID;

public class InstructorConsultationLogsActivity extends AppCompatActivity implements InstructorLogPresenter.View, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.coorApproved) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.swipeApproved) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ivEmptyApproved) ImageView emptyApprovedImage;
    @BindView(R.id.tvEmptyApproved) TextView emptyApprovedText;
    @BindView(R.id.rvApproved) RecyclerView recyclerView;

    Intent intent;

    String facultyUid;

    InstructorLogPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructor_consultation_logs);
        ButterKnife.bind(this);

        intent = getIntent();
        facultyUid = intent.getStringExtra(FACULTY_UID);

        presenter = new InstructorLogPresenterImpl(this, this);
        swipeRefreshLayout.setOnRefreshListener(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Instructor Logs");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(facultyUid);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setAppointments(DepartmentInstructorLogAdapter adapter) {
        emptyApprovedImage.setVisibility(View.GONE);
        emptyApprovedText.setVisibility(View.GONE);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyAppointment(String emptyMessage) {
        recyclerView.setVisibility(View.GONE);

        emptyApprovedImage.setVisibility(View.VISIBLE);
        emptyApprovedText.setVisibility(View.VISIBLE);

        emptyApprovedText.setText(emptyMessage);
    }

    @Override
    public void onRefresh() {
        presenter.onStart(facultyUid);
    }
}

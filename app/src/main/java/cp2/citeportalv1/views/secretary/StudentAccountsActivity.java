package cp2.citeportalv1.views.secretary;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.StudentAccountsAdapter;
import cp2.citeportalv1.base.SecretaryBaseActivity;
import cp2.citeportalv1.presenters.account_verification.student.StudentListPresenter;
import cp2.citeportalv1.presenters.account_verification.student.StudentListPresenterImpl;

public class StudentAccountsActivity extends SecretaryBaseActivity implements StudentListPresenter.View, SwipeRefreshLayout.OnRefreshListener {

    CoordinatorLayout coordinatorLayout;
    SwipeRefreshLayout swipeRefreshLayout;
    ImageView ivEmptyListImage;
    TextView tvEmptyListText;
    RecyclerView recyclerView;

    ProgressDialog progressDialog;

    StudentListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_accounts);

        navigationView.setCheckedItem(R.id.student_accounts);
        toolbar.setTitle("For Verification");
        toolbar.setSubtitle("Student Accounts");

        progressDialog = new ProgressDialog(this);

        coordinatorLayout = findViewById(R.id.coorSecretaryHome);
        swipeRefreshLayout = findViewById(R.id.swipeList);
        ivEmptyListImage = findViewById(R.id.ivEmptyList);
        tvEmptyListText = findViewById(R.id.tvEmptyList);
        recyclerView = findViewById(R.id.rvListAccounts);

        presenter = new StudentListPresenterImpl(this, this);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }


    @Override
    public void setProgress() {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setEmptyState(String emptyStateMessage) {
        recyclerView.setVisibility(View.GONE);

        ivEmptyListImage.setVisibility(View.VISIBLE);
        tvEmptyListText.setVisibility(View.VISIBLE);
        tvEmptyListText.setText(emptyStateMessage);
    }

    @Override
    public void setStudentList(StudentAccountsAdapter adapter) {
        ivEmptyListImage.setVisibility(View.GONE);
        tvEmptyListText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        presenter.requestStudentList();
    }
}

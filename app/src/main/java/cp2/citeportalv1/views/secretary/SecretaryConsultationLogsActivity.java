package cp2.citeportalv1.views.secretary;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.DepartmentLogsAdapter;
import cp2.citeportalv1.base.SecretaryBaseActivity;
import cp2.citeportalv1.presenters.reports.DepartmentLogsPresenter;
import cp2.citeportalv1.presenters.reports.DepartmentLogsPresenterImpl;

public class SecretaryConsultationLogsActivity extends SecretaryBaseActivity implements DepartmentLogsPresenter.View {

    CoordinatorLayout coordinatorLayout;
    ImageView ivEmptyListImage;
    TextView tvEmptyListText;
    RecyclerView recyclerView;

    ProgressDialog progressDialog;

    DepartmentLogsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secretary_consultation_logs);

        navigationView.setCheckedItem(R.id.consultation_logs);

        toolbar.setTitle("Consultation Logs");

        progressDialog = new ProgressDialog(this);

        coordinatorLayout = findViewById(R.id.coorSecretaryHome);
        ivEmptyListImage = findViewById(R.id.ivEmptyList);
        tvEmptyListText = findViewById(R.id.tvEmptyList);
        recyclerView = findViewById(R.id.rvListAccounts);

        presenter = new DepartmentLogsPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void setProgress() {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setEmptyState(String emptyStateMessage) {
        recyclerView.setVisibility(View.GONE);

        ivEmptyListImage.setVisibility(View.VISIBLE);
        tvEmptyListText.setVisibility(View.VISIBLE);
        tvEmptyListText.setText(emptyStateMessage);
    }

    @Override
    public void setFacultyEmployees(DepartmentLogsAdapter adapter) {
        ivEmptyListImage.setVisibility(View.GONE);
        tvEmptyListText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }
}

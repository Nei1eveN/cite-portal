package cp2.citeportalv1.views.secretary;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.ConsultationLog;
import cp2.citeportalv1.presenters.reports.faculty_report_logs.log_detail.LogDetailPresenter;
import cp2.citeportalv1.presenters.reports.faculty_report_logs.log_detail.LogDetailPresenterImpl;

import static cp2.citeportalv1.utils.Constants.FACULTY_UID;
import static cp2.citeportalv1.utils.Constants.NOTIFICATION_ID;

public class DepartmentConsultationLogDetailActivity extends AppCompatActivity implements LogDetailPresenter.View {

    @BindView(R.id.facLogDetailCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.ivSenderImage) ImageView senderImage;
    @BindView(R.id.tvSenderName) TextView senderName;
    @BindView(R.id.tvSenderDepartment) TextView senderDepartment;
    @BindView(R.id.tvApptDay) TextView apptDay;
    @BindView(R.id.tvApptDate) TextView apptDate;
    @BindView(R.id.tvTimeStart) TextView timeStart;
    @BindView(R.id.tvTimeEnd) TextView timeEnd;
    @BindView(R.id.tvVenue) TextView venue;
    @BindView(R.id.tvMessageTitle) TextView messageTitle;
    @BindView(R.id.tvMessageBody) TextView messageBody;
    @BindView(R.id.tvStatus) TextView messageStatus;
    @BindView(R.id.tvMessageRemarks) TextView messageRemarks;
    @BindView(R.id.tvSideNote) TextView sideNote;

    @BindView(R.id.tvAppointmentFeedback) TextView appointmentFeedback;

    ProgressDialog progressDialog;

    String notificationId, facultyUid;

    Intent intent;

    LogDetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department_consultation_log_detail);
        ButterKnife.bind(this);

        intent = getIntent();
        notificationId = intent.getStringExtra(NOTIFICATION_ID);
        facultyUid = intent.getStringExtra(FACULTY_UID);

        progressDialog = new ProgressDialog(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Details");

        presenter = new LogDetailPresenterImpl(this, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, InstructorConsultationLogsActivity.class);
                intent.putExtra(FACULTY_UID, facultyUid);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, InstructorConsultationLogsActivity.class);
        intent.putExtra(FACULTY_UID, facultyUid);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(facultyUid, notificationId);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }


    @Override
    public void showProgress(String title, String caption) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(caption);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setApprovedDetail(ConsultationLog consultationLog) {
        Glide.with(this).load(consultationLog.getSenderImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px).diskCacheStrategy(DiskCacheStrategy.ALL)).into(senderImage);
        senderName.setText(String.format("%s, %s %s", consultationLog.getSenderLastName(), consultationLog.getSenderFirstName(), consultationLog.getSenderMiddleName()));
        senderDepartment.setText(consultationLog.getSenderProgram());
        apptDay.setText(String.format("(%s)", consultationLog.getRequestedDay()));
        apptDate.setText(consultationLog.getRequestedDate());
        timeStart.setText(consultationLog.getRequestedTimeStart());
        timeEnd.setText(consultationLog.getRequestedTimeEnd());
        venue.setText(consultationLog.getVenue());
        messageTitle.setText(consultationLog.getMessageTitle());
        messageBody.setText(consultationLog.getMessageBody());
        messageStatus.setText(consultationLog.getMessageStatus());
        messageStatus.setTextColor(getResources().getColor(R.color.green_positive));
        messageRemarks.setText(consultationLog.getMessageRemarks());
        sideNote.setText(consultationLog.getMessageSideNote());
        appointmentFeedback.setText(consultationLog.getAppointmentFeedback());


        Objects.requireNonNull(getSupportActionBar()).setSubtitle(consultationLog.getRequestedDay()+" "+consultationLog.getRequestedDate());
    }

    @Override
    public void showExitDialog(String title, String message) {
        AlertDialog.Builder noDetailBuilder = new AlertDialog.Builder(this).setCancelable(false).setIcon(R.drawable.ic_insert_invitation).setTitle(title).setMessage(message)
                .setPositiveButton("EXIT", (dialog, which) -> {
                    Intent intent = new Intent(this, InstructorConsultationLogsActivity.class);
                    intent.putExtra(FACULTY_UID, facultyUid);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                });

        noDetailBuilder.create().show();
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }
}

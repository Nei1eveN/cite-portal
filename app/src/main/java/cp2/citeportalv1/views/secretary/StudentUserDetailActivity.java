package cp2.citeportalv1.views.secretary;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.presenters.account_verification.student.detail.StudentVerificationPresenter;
import cp2.citeportalv1.presenters.account_verification.student.detail.StudentVerificationPresenterImpl;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import static cp2.citeportalv1.utils.Constants.STUDENT_UID;

public class StudentUserDetailActivity extends AppCompatActivity implements StudentVerificationPresenter.View {

    @BindView(R.id.coorUserDetail) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.ivUserImage) ImageView userImage;
    @BindView(R.id.ivUserName) TextView userName;
    @BindView(R.id.tvUserProgDept) TextView userProgramDept;
    @BindView(R.id.tvUserID) TextView userID;
    @BindView(R.id.tvUserEmail) TextView userEmail;

    @BindView(R.id.btnValidate) MaterialButton btnValidate;
    @BindView(R.id.btnCancel) MaterialButton btnCancel;

    Intent intent;
    String userUid;

    ProgressDialog progressDialog;

    StudentVerificationPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_user_detail);
        ButterKnife.bind(this);

        intent = getIntent();
        userUid = intent.getStringExtra(STUDENT_UID);

        progressDialog = new ProgressDialog(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("User Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new StudentVerificationPresenterImpl(this, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(userUid);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @OnClick(R.id.btnValidate)
    void validateUser() {
        new AlertDialog.Builder(this).setTitle("Validating User Account").setMessage("Do you want to proceed?").setPositiveButton("VALIDATE", (dialog, which) -> presenter.setUserValid(userUid)).setNegativeButton("CANCEL", null).setCancelable(false).create().show();
    }

    @OnClick(R.id.btnCancel)
    void cancelUser() {
        new AlertDialog.Builder(this).setTitle("Removing User Account").setMessage("Do you want to proceed?").setPositiveButton("VALIDATE", (dialog, which) -> presenter.setUserRemoved(userUid)).setNegativeButton("CANCEL", null).setCancelable(false).create().show();
    }


    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton("EXIT", (dialog, which) -> {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }).setCancelable(false).create().show();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title).setMessage(message)
                .setPositiveButton("DISMISS", (dialog, which)
                        -> dialog.dismiss()).create().show();
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setUserDetail(Student student) {
        Glide.with(this).load(student.getStudentImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(userImage);
        userName.setText(String.format("%s, %s %s", student.getLastName(), student.getfName(), student.getMidName()));
        userProgramDept.setText(String.format("%s", student.getProgram()));
        userID.setText(String.valueOf(student.getStudentID()));
        userEmail.setText(student.getEmail());
    }
}

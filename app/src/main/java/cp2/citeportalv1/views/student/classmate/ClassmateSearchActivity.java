package cp2.citeportalv1.views.student.classmate;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ClassmateSearchAdapter;
import cp2.citeportalv1.base.StudentBaseActivity;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.presenters.classes.student.search.classmate.ClassmateSearchPresenter;
import cp2.citeportalv1.presenters.classes.student.search.classmate.ClassmateSearchPresenterImpl;

public class ClassmateSearchActivity extends StudentBaseActivity
        implements ClassmateSearchPresenter.View, SearchView.OnQueryTextListener {

    SearchView searchClassmate;
    ImageView emptyClassmateImage;
    TextView emptyClassmateText;
    RecyclerView recyclerView;

    ClassmateSearchPresenter presenter;
    ClassmateSearchAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_classmate);

        toolbar.setTitle("Schoolmate Search");
        navigationView.setCheckedItem(R.id.searchClassmate);

        presenter = new ClassmateSearchPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void initViews() {
        searchClassmate = findViewById(R.id.searchClassmate);
        searchClassmate.setOnQueryTextListener(this);

        emptyClassmateImage = findViewById(R.id.ivEmptyClassmate);
        emptyClassmateText = findViewById(R.id.tvEmptyClassmate);
        recyclerView = findViewById(R.id.recyclerClassmate);

        emptyClassmateImage.setVisibility(View.GONE);
        emptyClassmateText.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void setEmptyState(String emptyStateMessage) {
        recyclerView.setVisibility(View.GONE);
        emptyClassmateImage.setVisibility(View.VISIBLE);
        emptyClassmateText.setVisibility(View.VISIBLE);
        emptyClassmateText.setText(emptyStateMessage);
    }

    @Override
    public void setClassmateFound(List<Student> studentList) {
        emptyClassmateImage.setVisibility(View.GONE);
        emptyClassmateText.setVisibility(View.GONE);

        adapter = new ClassmateSearchAdapter(this, studentList);
        adapter.notifyDataSetChanged();

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setNoClassmateFound(String noClassmateFoundMessage) {
        recyclerView.setVisibility(View.GONE);

        emptyClassmateImage.setVisibility(View.VISIBLE);
        emptyClassmateText.setVisibility(View.VISIBLE);

        emptyClassmateText.setText(noClassmateFoundMessage);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        presenter.submitQuery(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        presenter.submitQuery(newText);
        return false;
    }
}

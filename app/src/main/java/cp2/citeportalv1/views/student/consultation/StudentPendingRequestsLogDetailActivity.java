package cp2.citeportalv1.views.student.consultation;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Objects;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.presenters.consult.student.pending.detail.PendingDetailPresenter;
import cp2.citeportalv1.presenters.consult.student.pending.detail.PendingDetailPresenterImpl;

public class StudentPendingRequestsLogDetailActivity extends AppCompatActivity implements PendingDetailPresenter.View {

    Intent intent;
    Bundle data;

    @BindView(R.id.pendingCoor) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.ivSenderImage) ImageView senderImage;
    @BindView(R.id.tvSenderDepartment) TextView senderDepartment;
    @BindView(R.id.tvFacultyName) TextView facultyName;
    @BindView(R.id.tvRequestedDay) TextView requestedDay;
    @BindView(R.id.tvRequestedDate) TextView requestedDate;
    @BindView(R.id.tvRequestedTimeStart) TextView requestedTimeStart;
    @BindView(R.id.tvRequestedTimeEnd) TextView requestedTimeEnd;
    @BindView(R.id.tvRequestedVenue) TextView requestedVenue;
    @BindView(R.id.tvMessageTitle) TextView messageTitle;
    @BindView(R.id.tvMessageBody) TextView messageBody;
    @BindView(R.id.tvStatus) TextView messageStatus;

    /**
     * HEADERS
     * **/
    @BindView(R.id.tvRequestedSchedHeader) TextView requestSchedHeader;
    @BindView(R.id.tvConcernHeader) TextView concernHeader;
    @BindView(R.id.tvStatusHeader) TextView statusHeader;

    PendingDetailPresenter detailPresenter;

    AlertDialog.Builder noDetailBuilder;
    AlertDialog noDetailDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_pending_requests_log_detail);
        ButterKnife.bind(this);

        intent = getIntent();
        data = getIntent().getExtras();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        detailPresenter = new PendingDetailPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        String notificationId = intent.getStringExtra("notificationId");
        detailPresenter.onStart(notificationId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detailPresenter.onDestroy();
    }

    @Override
    public void setPendingDetail(ConsultationRequest response) {
        Glide.with(this).load(response.getSenderImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(senderImage);
        facultyName.setText(String.format("%s %s", response.getSenderFirstName(), response.getSenderLastName()));
        senderDepartment.setText(response.getSenderProgram());
        requestedDay.setText(String.format("(%s)", response.getRequestedDay()));
        requestedDate.setText(response.getRequestedDate());
        requestedTimeStart.setText(response.getRequestedTimeStart());
        requestedTimeEnd.setText(response.getRequestedTimeEnd());
        requestedVenue.setText(response.getVenue());
        messageTitle.setText(response.getMessageTitle());
        messageBody.setText(response.getMessageBody());
        messageStatus.setText(response.getMessageStatus());
        messageStatus.setTextColor(getResources().getColor(android.R.color.holo_orange_dark));

        Objects.requireNonNull(getSupportActionBar()).setTitle(response.getMessageTitle());
        getSupportActionBar().setSubtitle("Consultation Request");
    }

    @Override
    public void setPendingNotExist(String notExistMessage) {
        noDetailBuilder = new AlertDialog.Builder(this);
        noDetailBuilder.setCancelable(false);
        noDetailBuilder.setIcon(R.drawable.ic_insert_invitation);
        noDetailBuilder.setTitle("Not Existing Request");
        noDetailBuilder.setMessage(notExistMessage);
        noDetailBuilder.setPositiveButton("EXIT", (dialog, which) -> {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        });

        noDetailDialog = noDetailBuilder.create();
        noDetailDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}

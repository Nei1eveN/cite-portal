package cp2.citeportalv1.views.student.consultation.logs;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.ConsultationResponseGroupAdapter;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.presenters.consult.student.rejected.RejectedResponsePresenter;
import cp2.citeportalv1.presenters.consult.student.rejected.RejectedResponsePresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class StudentConsultationResponseLogFragment extends Fragment implements RejectedResponsePresenter.View {

    private Unbinder unbinder;
    @BindView(R.id.rejectedCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.rejectedSwipe) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.rvRejected) RecyclerView recyclerView;

    @BindView(R.id.ivEmptyRejected) ImageView emptyRejectedImage;
    @BindView(R.id.tvEmptyRejected) TextView emptyRejectedText;

    private RejectedResponsePresenter presenter;

    public StudentConsultationResponseLogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_consultation_response_log, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new RejectedResponsePresenterImpl(this, getActivity());
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.requestRejectRequests());
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void loadRejectedResponses(List<ListItem> list) {
        ConsultationResponseGroupAdapter adapter = new ConsultationResponseGroupAdapter(getActivity(), list);
        adapter.notifyDataSetChanged();

        emptyRejectedImage.setVisibility(View.GONE);
        emptyRejectedText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyState(String emptyMessage) {
        recyclerView.setVisibility(View.GONE);

        emptyRejectedImage.setVisibility(View.VISIBLE);
        emptyRejectedText.setVisibility(View.VISIBLE);

        emptyRejectedText.setText(emptyMessage);
    }
}

package cp2.citeportalv1.views.student.profile;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.views.student.StudentUserEditSignatureActivity;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.presenters.profile.student.detail.StudentDetailPresenter;
import cp2.citeportalv1.presenters.profile.student.detail.StudentDetailPresenterImpl;
import cp2.citeportalv1.views.student.StudentSignatureActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class StudentUserDetailFragment extends Fragment implements StudentDetailPresenter.View, SwipeRefreshLayout.OnRefreshListener, FloatingActionButton.OnClickListener {

    private Unbinder unbinder;
    @BindView(R.id.studentDetailCoor) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.studentDetailSwipe) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.studName) TextView studentName;
    @BindView(R.id.studContactNumber) TextView contactNumber;
    @BindView(R.id.tvEmail) TextView email;

    @BindView(R.id.tvUserSignatureHeader) TextView userSignatureHeader;
    @BindView(R.id.ivUserSignature) ImageView userSignatureImage;
    @BindView(R.id.tvEmptyUserSignature) TextView emptyUserSignatureText;

    @BindView(R.id.tvProgram) TextView program;
    @BindView(R.id.tvYearLevel) TextView yearLevel;
    @BindView(R.id.tvStudentId) TextView studentId;

    @BindView(R.id.fabProfileEdit) FloatingActionButton editProfile;

    private StudentDetailPresenter presenter;

    private boolean hasLoadedOnce = false;

    public StudentUserDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new StudentDetailPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_student_user_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        swipeRefreshLayout.setOnRefreshListener(this);
        editProfile.setOnClickListener(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser && !hasLoadedOnce) {
                presenter.requestStudentUserInfo();
                hasLoadedOnce = true;
            }
        }
    }

    @OnClick(R.id.ivUserSignature)
    void goToStudentSignaturePage() {
        Objects.requireNonNull(getActivity()).startActivity(new Intent(getActivity(), StudentSignatureActivity.class));
        Objects.requireNonNull(getActivity()).finish();
    }

    @OnClick(R.id.tvEmptyUserSignature)
    void createSignaturePage() {
        Objects.requireNonNull(getActivity()).startActivity(new Intent(getActivity(), StudentUserEditSignatureActivity.class));
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.requestStudentUserInfo();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setUserInfo(Student student) {
        studentName.setText(String.format("%s, %s %s", student.getLastName(), student.getfName(), student.getMidName()));
        contactNumber.setText(student.getContactNumber());
        program.setText(student.getProgram());
        yearLevel.setText(String.format("%s Year", student.getYearLvl()));
        email.setText(student.getEmail());
        studentId.setText(String.valueOf(student.getStudentID()));
    }

    @Override
    public void setUserSignature(String userSignature) {
        emptyUserSignatureText.setVisibility(View.GONE);

        userSignatureImage.setVisibility(View.VISIBLE);
        userSignatureHeader.setVisibility(View.VISIBLE);

        Glide.with(Objects.requireNonNull(getActivity())).load(userSignature).apply(new RequestOptions().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(userSignatureImage);
    }

    @Override
    public void setNoSignatureFound(String message) {
        userSignatureImage.setVisibility(View.GONE);
        userSignatureHeader.setVisibility(View.GONE);

        emptyUserSignatureText.setVisibility(View.VISIBLE);

        emptyUserSignatureText.setText(message);
    }

    @Override
    public void setExitDialog(String title, String message) {
        AlertDialog.Builder exitBuilder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        exitBuilder.setCancelable(false);
        exitBuilder.setTitle(title);
        exitBuilder.setMessage(message);
        exitBuilder.setPositiveButton("EXIT", (dialog, which) -> getActivity().finish());

        AlertDialog exitDialog = exitBuilder.create();
        exitDialog.show();
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        presenter.requestStudentUserInfo();
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(getActivity(), StudentEditProfileActivity.class));
        Objects.requireNonNull(getActivity()).finish();
    }
}

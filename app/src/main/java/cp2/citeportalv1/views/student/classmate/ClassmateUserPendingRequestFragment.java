package cp2.citeportalv1.views.student.classmate;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ClassmateRequestAdapter;
import cp2.citeportalv1.models.ClassmateRequest;
import cp2.citeportalv1.presenters.classes.student.user_pending_classmate_request.PendingClassmateRequestPresenter;
import cp2.citeportalv1.presenters.classes.student.user_pending_classmate_request.PendingClassmateRequestPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClassmateUserPendingRequestFragment extends Fragment implements PendingClassmateRequestPresenter.View, SwipeRefreshLayout.OnRefreshListener {

    private Unbinder unbinder;
    @BindView(R.id.userPendingCoor)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.userPendingSwipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.rvUserPending)
    RecyclerView recyclerView;

    @BindView(R.id.ivUserPending)
    ImageView userPendingImage;
    @BindView(R.id.tvUserPending)
    TextView userPendingText;

    private PendingClassmateRequestPresenter presenter;

    private boolean hasLoadedOnce = false;

    public ClassmateUserPendingRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PendingClassmateRequestPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_pending_classmate_request, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser && !hasLoadedOnce) {
                presenter.findUserPendingRequests();
                hasLoadedOnce = true;
            }
        }
    }

    @OnClick(R.id.ivUserPending)
    void searchClassmate(){
        startActivity(new Intent(getContext(), ClassmateSearchActivity.class));
        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setUserPendingRequests(List<ClassmateRequest> classmateRequests) {
        userPendingImage.setVisibility(View.GONE);
        userPendingText.setVisibility(View.GONE);

        ClassmateRequestAdapter adapter = new ClassmateRequestAdapter(getActivity(), classmateRequests);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyState(String emptyStateMessage) {
        recyclerView.setVisibility(View.GONE);

        userPendingImage.setVisibility(View.VISIBLE);
        userPendingText.setVisibility(View.VISIBLE);

        userPendingText.setText(emptyStateMessage);
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        presenter.findUserPendingRequests();
    }
}

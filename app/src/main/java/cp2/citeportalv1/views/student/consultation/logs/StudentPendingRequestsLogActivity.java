package cp2.citeportalv1.views.student.consultation.logs;

import android.os.Bundle;
import android.widget.FrameLayout;

import cp2.citeportalv1.R;
import cp2.citeportalv1.base.StudentBaseActivity;

public class StudentPendingRequestsLogActivity extends StudentBaseActivity {

    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_pending_requests_log);

        toolbar.setTitle("Pending Requests");
        toolbar.setSubtitle("Current Requests");

        navigationView.setCheckedItem(R.id.pending_requests);

        frameLayout = findViewById(R.id.content_holder);
    }
}

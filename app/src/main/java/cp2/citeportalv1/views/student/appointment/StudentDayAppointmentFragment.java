package cp2.citeportalv1.views.student.appointment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.StudentAppointmentDayGroupAdapter;
import cp2.citeportalv1.presenters.consult.student.appointment.day_appointment.StudentDayAppointmentPresenter;
import cp2.citeportalv1.presenters.consult.student.appointment.day_appointment.StudentDayAppointmentPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class StudentDayAppointmentFragment extends Fragment implements StudentDayAppointmentPresenter.View, SwipeRefreshLayout.OnRefreshListener {

    private Unbinder unbinder;
    @BindView(R.id.apptCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.apptSwipe) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ivEmptyAppointment) ImageView emptyAppointmentImage;
    @BindView(R.id.tvEmptyAppointment) TextView emptyAppointmentText;
    @BindView(R.id.rvAppointments) RecyclerView recyclerView;

    private StudentDayAppointmentPresenter presenter;

//    private boolean hasLoadedOnce = false;

    public StudentDayAppointmentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new StudentDayAppointmentPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_student_day_appointment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (this.isVisible()) {
//            if (isVisibleToUser && !hasLoadedOnce) {
//                presenter.findAppointments();
//                hasLoadedOnce = true;
//            }
//        }
//    }

    @OnClick(R.id.ivEmptyAppointment)
    void showAllAppointments() {
        startActivity(new Intent(getActivity(), StudentAppointmentsActivity.class));
        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.findAppointments();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setAppointments(StudentAppointmentDayGroupAdapter adapter) {
        emptyAppointmentImage.setVisibility(View.GONE);
        emptyAppointmentText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyAppointment(String emptyMessage) {
        recyclerView.setVisibility(View.GONE);
        emptyAppointmentImage.setVisibility(View.VISIBLE);
        emptyAppointmentText.setVisibility(View.VISIBLE);

        emptyAppointmentText.setText(emptyMessage);
    }

    @Override
    public void onRefresh() {
        presenter.findAppointments();
    }
}

package cp2.citeportalv1.views.student.consultation.logs;

import android.os.Bundle;

import cp2.citeportalv1.R;
import cp2.citeportalv1.base.StudentBaseActivity;

public class StudentConsultationResponseLogActivity extends StudentBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_consultation_response_log);

        toolbar.setTitle("Declined Requests");
        toolbar.setSubtitle("Instructor Responses");

        navigationView.setCheckedItem(R.id.consultation_responses);
    }
}

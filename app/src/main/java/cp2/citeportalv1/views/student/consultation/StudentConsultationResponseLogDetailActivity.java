package cp2.citeportalv1.views.student.consultation;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Objects;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.ConsultationResponse;
import cp2.citeportalv1.presenters.consult.student.rejected.rejected_detail.RejectedDetailPresenter;
import cp2.citeportalv1.presenters.consult.student.rejected.rejected_detail.RejectedDetailPresenterImpl;

public class StudentConsultationResponseLogDetailActivity extends AppCompatActivity implements RejectedDetailPresenter.View {

    Intent intent;
    Bundle data;

    @BindView(R.id.responseCoor) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.ivSenderImage) ImageView senderImage;
    @BindView(R.id.tvSenderDepartment) TextView senderDepartment;
    @BindView(R.id.tvFacultyName) TextView facultyName;
    @BindView(R.id.tvRequestedDay) TextView requestedDay;
    @BindView(R.id.tvRequestedDate) TextView requestedDate;
    @BindView(R.id.tvRequestedTimeStart) TextView requestedTimeStart;
    @BindView(R.id.tvRequestedTimeEnd) TextView requestedTimeEnd;
    @BindView(R.id.tvRequestedVenue) TextView requestedVenue;
    @BindView(R.id.tvMessageTitle) TextView messageTitle;
    @BindView(R.id.tvMessageBody) TextView messageBody;
    @BindView(R.id.tvStatus) TextView messageStatus;
    @BindView(R.id.tvMessageRemarks) TextView messageRemarks;
    @BindView(R.id.tvMessageSideNote) TextView messageSideNote;

    /**
     * HEADERS
     * **/
    @BindView(R.id.tvRequestedSchedHeader) TextView requestSchedHeader;
    @BindView(R.id.tvConcernHeader) TextView concernHeader;
    @BindView(R.id.tvStatusHeader) TextView statusHeader;
    @BindView(R.id.tvRemarkHeader) TextView remarkHeader;

    RejectedDetailPresenter detailPresenter;

    AlertDialog.Builder noDetailBuilder;
    AlertDialog noDetailDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_consultation_response_log_detail);
        ButterKnife.bind(this);

        intent = getIntent();
        data = getIntent().getExtras();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        detailPresenter = new RejectedDetailPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        String notificationId = intent.getStringExtra("notificationId");
        detailPresenter.onStart(notificationId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detailPresenter.onDestroy();
    }

    @Override
    public void setRejectedDetail(ConsultationResponse response) {
        Objects.requireNonNull(getSupportActionBar()).setTitle("Response");
        getSupportActionBar().setSubtitle(response.getMessageTitle());

        Glide.with(this).load(response.getSenderImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(senderImage);
        facultyName.setText(String.format("%s %s", response.getSenderFirstName(), response.getSenderLastName()));
        senderDepartment.setText(response.getSenderDepartment());
        requestedDay.setText(String.format("(%s)", response.getRequestedDay()));
        requestedDate.setText(response.getRequestedDate());
        requestedTimeStart.setText(response.getRequestedTimeStart());
        requestedTimeEnd.setText(response.getRequestedTimeEnd());
        requestedVenue.setText(response.getVenue());
        messageTitle.setText(response.getMessageTitle());
        messageBody.setText(response.getMessageBody());
        messageStatus.setText(response.getMessageStatus());
        messageStatus.setTextColor(getResources().getColor(android.R.color.holo_red_light));
        messageRemarks.setText(response.getMessageRemarks());
        messageSideNote.setText(response.getMessageSideNote());
    }

    @Override
    public void setApprovedNotExist(String notExistMessage) {
        noDetailBuilder = new AlertDialog.Builder(this);
        noDetailBuilder.setCancelable(false);
        noDetailBuilder.setIcon(R.drawable.ic_insert_invitation);
        noDetailBuilder.setTitle("Not Existing Details");
        noDetailBuilder.setMessage(notExistMessage);
        noDetailBuilder.setPositiveButton("EXIT", (dialog, which) -> {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        });

        noDetailDialog = noDetailBuilder.create();
        noDetailDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}

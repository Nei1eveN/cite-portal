package cp2.citeportalv1.views.student.home;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import java.util.Date;

import androidx.viewpager.widget.ViewPager;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.base.StudentBaseActivity;
import cp2.citeportalv1.presenters.home.student.StudentHomePresenter;
import cp2.citeportalv1.presenters.home.student.StudentHomePresenterImpl;

import static cp2.citeportalv1.utils.Constants.dayFormat;

public class StudHomeActivity extends StudentBaseActivity implements StudentHomePresenter.View {

    ViewPager viewPager;
    TabLayout tabLayout;

    StudentHomePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_main);
        toolbar.setTitle(R.string.title_activity_student_main);
        toolbar.setSubtitle("Today is "+dayFormat.format(new Date()));

        viewPager = findViewById(R.id.viewPager2);
        tabLayout = findViewById(R.id.tabLayout);

        navigationView.setCheckedItem(R.id.studHomeMenu);

        presenter = new StudentHomePresenterImpl(this, this);
    }

    public void initializeTabs(){
//        StudNewsFeedFragment newsFeedFragment = new StudNewsFeedFragment();
//        StudLectureFragment lectureFragment = new StudLectureFragment();

//        viewPagerAdapter.addFragment(newsFeedFragment, "News Feed");
//        viewPagerAdapter.addFragment(lectureFragment, "Notes");

//        viewPagerAdapter.addFragment(new ClassmateFragment(), "Classmates");

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void loadPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(numberOfPages);
        viewPager.setCurrentItem(0, false);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.student_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


}

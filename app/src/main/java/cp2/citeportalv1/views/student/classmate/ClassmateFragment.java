package cp2.citeportalv1.views.student.classmate;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ClassmateListAdapter;
import cp2.citeportalv1.models.Classmate;
import cp2.citeportalv1.presenters.classes.student.classmates.ClassmateListPresenter;
import cp2.citeportalv1.presenters.classes.student.classmates.ClassmateListPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClassmateFragment extends Fragment implements ClassmateListPresenter.View {

    private Unbinder unbinder;
    @BindView(R.id.classmateCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.classmateSwipe) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.fabAddClassmate) FloatingActionButton floatingActionButton;
    @BindView(R.id.rvClassmate) RecyclerView recyclerView;

    @BindView(R.id.ivEmptyClassmate) ImageView emptyClassmateImage;
    @BindView(R.id.tvEmptyClassmate) TextView emptyClassmateText;

    private ClassmateListPresenter presenter;

    private boolean hasLoadedOnce = false;

    public ClassmateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ClassmateListPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_classmates, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        swipeRefreshLayout.setOnRefreshListener(() -> presenter.findClassmates());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser && !hasLoadedOnce) {
                presenter.findClassmates();
                hasLoadedOnce = true;
            }
        }
    }

    @OnClick(R.id.ivEmptyClassmate)
    protected void searchClassmate(){
        startActivity(new Intent(getContext(), ClassmateSearchActivity.class));
        Objects.requireNonNull(getActivity()).finish();
    }

    @OnClick(R.id.fabAddClassmate)
    void searchClassmateUsingFloatingButton(){
        startActivity(new Intent(getContext(), ClassmateSearchActivity.class));
        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setClassmateFound(List<Classmate> classmates) {
        emptyClassmateImage.setVisibility(View.GONE);
        emptyClassmateText.setVisibility(View.GONE);

        ClassmateListAdapter adapter = new ClassmateListAdapter(getActivity(), classmates);
        adapter.notifyDataSetChanged();

        recyclerView.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        floatingActionButton.show();
    }

    @Override
    public void setNoClassmateFound(String noClassmatesMessage) {
        recyclerView.setVisibility(View.GONE);
        floatingActionButton.hide();

        emptyClassmateImage.setVisibility(View.VISIBLE);
        emptyClassmateText.setVisibility(View.VISIBLE);

        emptyClassmateText.setText(noClassmatesMessage);
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }
}

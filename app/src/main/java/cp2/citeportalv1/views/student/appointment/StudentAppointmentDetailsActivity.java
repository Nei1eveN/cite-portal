package cp2.citeportalv1.views.student.appointment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.presenters.consult.student.appointment.day_appointment.detail.StudentAppointmentDetailPresenterImpl;
import cp2.citeportalv1.presenters.consult.student.appointment.day_appointment.detail.StudentAppointmentDetailPresenter;

public class StudentAppointmentDetailsActivity extends AppCompatActivity implements StudentAppointmentDetailPresenter.View {

    @BindView(R.id.studApptDetailCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.ivSenderImage) ImageView senderImage;
    @BindView(R.id.tvSenderName) TextView senderName;
    @BindView(R.id.tvSenderDepartment) TextView senderDepartment;
    @BindView(R.id.tvApptDay) TextView apptDay;
    @BindView(R.id.tvApptDate) TextView apptDate;
    @BindView(R.id.tvTimeStart) TextView timeStart;
    @BindView(R.id.tvTimeEnd) TextView timeEnd;
    @BindView(R.id.tvVenue) TextView venue;
    @BindView(R.id.tvMessageTitle) TextView messageTitle;
    @BindView(R.id.tvMessageBody) TextView messageBody;
    @BindView(R.id.tvStatus) TextView messageStatus;
    @BindView(R.id.tvMessageRemarks) TextView messageRemarks;
    @BindView(R.id.tvSideNote) TextView sideNote;

    ProgressDialog progressDialog;

    String notificationId, appointmentId;

    Intent intent;

    StudentAppointmentDetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_appointment_detail);
        ButterKnife.bind(this);

        intent = getIntent();
        notificationId = intent.getStringExtra("notificationId");

        progressDialog = new ProgressDialog(this);
        presenter = new StudentAppointmentDetailPresenterImpl(this, this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Appointment Details");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                startActivity(new Intent(this, StudentAppointmentsActivity.class));
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        startActivity(new Intent(this, StudentAppointmentsActivity.class));
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(notificationId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String caption) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(caption);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setApprovedDetail(Appointments appointment) {
        Glide.with(this).load(appointment.getSenderImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px).diskCacheStrategy(DiskCacheStrategy.ALL)).into(senderImage);
        senderName.setText(String.format("%s, %s %s", appointment.getSenderLastName(), appointment.getSenderFirstName(), appointment.getSenderMiddleName()));
        senderDepartment.setText(appointment.getSenderDepartment());
        apptDay.setText(String.format("(%s)", appointment.getRequestedDay()));
        apptDate.setText(appointment.getRequestedDate());
        timeStart.setText(appointment.getRequestedTimeStart());
        timeEnd.setText(appointment.getRequestedTimeEnd());
        venue.setText(appointment.getVenue());
        messageTitle.setText(appointment.getMessageTitle());
        messageBody.setText(appointment.getMessageBody());
        messageStatus.setText(appointment.getMessageStatus());
        messageStatus.setTextColor(getResources().getColor(R.color.green_positive));
        messageRemarks.setText(appointment.getMessageRemarks());
        sideNote.setText(appointment.getMessageSideNote());

        appointmentId = appointment.appointmentId;

        Objects.requireNonNull(getSupportActionBar()).setSubtitle(appointment.getRequestedDay()+" "+appointment.getRequestedDate());
    }

    @Override
    public void setApprovedNotExist(String notExistTitle, String notExistMessage) {
        AlertDialog.Builder noDetailBuilder = new AlertDialog.Builder(this).setCancelable(false).setIcon(R.drawable.ic_insert_invitation).setTitle(notExistTitle).setMessage(notExistMessage)
                .setPositiveButton("EXIT", (dialog, which) -> {
//                    startActivity(new Intent(this, StudentAppointmentsActivity.class));
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                });

        noDetailBuilder.create().show();
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }
}

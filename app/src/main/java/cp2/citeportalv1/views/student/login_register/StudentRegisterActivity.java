package cp2.citeportalv1.views.student.login_register;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.presenters.register.student.StudRegPresenter;
import cp2.citeportalv1.presenters.register.student.StudRegPresenterImpl;
import cp2.citeportalv1.utils.FileUtil;
import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cp2.citeportalv1.utils.Constants.PICK_IMAGE_REQUEST;
import static cp2.citeportalv1.utils.FileUtil.fullyReadFileToBytes;
import static cp2.citeportalv1.utils.FileUtil.getFileName;
import static cp2.citeportalv1.utils.FileUtil.getReadableFileSize;

public class StudentRegisterActivity extends AppCompatActivity implements StudRegPresenter.View {

    @BindView(R.id.studImgProfile) ImageButton studImage;

    @BindView(R.id.tvStudPic) TextView studPic;

    @BindView(R.id.etStudentId) TextInputEditText studId;
    @BindView(R.id.etFName) TextInputEditText fName;
    @BindView(R.id.etMidName) TextInputEditText midName;
    @BindView(R.id.etLName) TextInputEditText lName;
    @BindView(R.id.etEmail) TextInputEditText email;
    @BindView(R.id.etPassword) TextInputEditText pass;
    @BindView(R.id.spinProg) Spinner programs;
    @BindView(R.id.spinYearLvl) Spinner yearLevels;

    @BindView(R.id.txStudId) TextInputLayout studentIdWrapper;
    @BindView(R.id.txFName) TextInputLayout fNameWrapper;
    @BindView(R.id.txMidName) TextInputLayout midNameWrapper;
    @BindView(R.id.txLName) TextInputLayout lNameWrapper;
    @BindView(R.id.txEmail) TextInputLayout emailWrapper;
    @BindView(R.id.txPassword) TextInputLayout passWrapper;
    @BindView(R.id.btnReg) Button register;

    @BindView(R.id.regCoorLayout) CoordinatorLayout coordinatorLayout;

    ArrayAdapter<CharSequence> progAdapter;
    ArrayAdapter<CharSequence> yearLvlAdapter;

    FirebaseAuth firebaseAuth;

    StudRegPresenter presenter;

    ProgressDialog progressDialog;

    protected Uri mainImageURI = null;
    protected File actualImage;
    protected File compressedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_reg);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        firebaseAuth = FirebaseAuth.getInstance();
        presenter = new StudRegPresenterImpl(this, this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Register as Student");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, StudentLoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, StudentLoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @OnClick(R.id.studImgProfile)
    public void selectImage() {
//        studImage.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
        Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.tvStudPic)
    public void selectStudentImage() {
        selectImage();
    }

    @OnClick(R.id.btnReg)
    public void registerCredentials() {
        submit();
    }

    public void submit() {
        try {
            presenter.submitCredentials(fullyReadFileToBytes(compressedImage), mainImageURI,
                    Objects.requireNonNull(studId.getText()).toString(),
                    Objects.requireNonNull(fName.getText()).toString(), Objects.requireNonNull(midName.getText()).toString(), Objects.requireNonNull(lName.getText()).toString(),
                    Objects.requireNonNull(email.getText()).toString(), Objects.requireNonNull(pass.getText()).toString(),
                    programs.getSelectedItem().toString(), yearLevels.getSelectedItem().toString(),
                    studentIdWrapper, fNameWrapper, midNameWrapper, lNameWrapper, emailWrapper, passWrapper);
        }catch (NullPointerException e){
            e.printStackTrace();
            Snackbar.make(coordinatorLayout, "Please select an image", Snackbar.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showProgress(String title, String caption) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(caption);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void initSpinnerItems() {
        progAdapter = ArrayAdapter.createFromResource(this, R.array.progList, android.R.layout.simple_spinner_item);
        progAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        programs.setAdapter(progAdapter);

        yearLvlAdapter = ArrayAdapter.createFromResource(this, R.array.yearLvls, android.R.layout.simple_spinner_item);
        yearLvlAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearLevels.setAdapter(yearLvlAdapter);

        studId.setFilters(new InputFilter[]{new InputFilter.LengthFilter(7)});
    }

    @Override
    public void errorWrapper(TextInputLayout textInputLayout, String message) {
        textInputLayout.setError(message);
    }

    @Override
    public void nullWrapper(TextInputLayout textInputLayout) {
        textInputLayout.setError(null);
    }

    @Override
    public void setSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAlertDialog(String title, String message) {
        new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss()).create().show();
    }

    @SuppressLint("CheckResult")
    public void compressImage(View view) {
        if (actualImage == null) {
            Toast.makeText(this, "Please choose an image!", Toast.LENGTH_SHORT).show();
        } else {
            new Compressor(this)
                    .compressToFileAsFlowable(actualImage)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(file -> {
                        compressedImage = file;
                        setCompressedImage();
                    }, Throwable::printStackTrace);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data == null) {
                Toast.makeText(this, "Failed to open picture!", Toast.LENGTH_SHORT).show();
                return;
            }
            try {
                mainImageURI = data.getData();
                actualImage = FileUtil.from(this, data.getData());
                compressImage(studImage);
                studPic.setVisibility(View.GONE);
            } catch (IOException e) {
                setSnackMessage("Failed to read picture data!");
                e.printStackTrace();
            }
        }
    }

    public void setCompressedImage() {
//        Glide.with(this).asBitmap().load(BitmapFactory.decodeFile(compressedImage.getAbsolutePath())).into(studImage);
        studImage.setImageBitmap(BitmapFactory.decodeFile(compressedImage.getAbsolutePath()));
        Log.d("URI String", getFileName(this, mainImageURI) + "\n Size: " + getReadableFileSize(compressedImage.length()));
    }
}
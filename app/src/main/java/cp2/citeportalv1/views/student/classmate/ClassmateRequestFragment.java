package cp2.citeportalv1.views.student.classmate;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ClassmateRequestAdapter;
import cp2.citeportalv1.models.ClassmateRequest;
import cp2.citeportalv1.presenters.classes.student.classmate_request.ClassmateRequestPresenter;
import cp2.citeportalv1.presenters.classes.student.classmate_request.ClassmateRequestPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClassmateRequestFragment extends Fragment implements ClassmateRequestPresenter.View {

    private Unbinder unbinder;
    @BindView(R.id.classmateRequestsCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.classmateReqSwipe) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.rvClassmateRequest) RecyclerView recyclerView;

    @BindView(R.id.ivEmptyClassmateRequest) ImageView emptyClassmateReqImage;

    @BindView(R.id.tvEmptyClassmateRequest) TextView emptyClassmateReqText;

    private ClassmateRequestPresenter presenter;

    private boolean hasLoadedOnce = false;

    public ClassmateRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ClassmateRequestPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_classmate_requests, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.findRequests());

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser && !hasLoadedOnce) {
                presenter.findRequests();
                hasLoadedOnce = true;
            }
        }
    }

    @OnClick(R.id.ivEmptyClassmateRequest)
    void searchClassmate() {
        startActivity(new Intent(getActivity(), ClassmateSearchActivity.class));
        Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setClassmateRequests(List<ClassmateRequest> classmateRequests) {
        emptyClassmateReqImage.setVisibility(View.GONE);
        emptyClassmateReqText.setVisibility(View.GONE);

        ClassmateRequestAdapter adapter = new ClassmateRequestAdapter(getActivity(), classmateRequests);
        adapter.notifyDataSetChanged();

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyState(String emptyStateMessage) {
        recyclerView.setVisibility(View.GONE);

        emptyClassmateReqImage.setVisibility(View.VISIBLE);
        emptyClassmateReqText.setVisibility(View.VISIBLE);

        emptyClassmateReqText.setText(emptyStateMessage);
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }
}

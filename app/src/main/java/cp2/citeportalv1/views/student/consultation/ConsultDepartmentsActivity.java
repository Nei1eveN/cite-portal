package cp2.citeportalv1.views.student.consultation;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.InstructorGroupAdapter;
import cp2.citeportalv1.base.StudentBaseActivity;
import cp2.citeportalv1.presenters.consult.student.loadpages.InstructorConsultPresenter;
import cp2.citeportalv1.presenters.consult.student.loadpages.InstructorConsultPresenterImpl;

public class ConsultDepartmentsActivity extends StudentBaseActivity implements InstructorConsultPresenter.View, SearchView.OnQueryTextListener {

    SearchView searchClassmate;
    ImageView emptyClassmateImage;
    TextView emptyClassmateText;
    RecyclerView recyclerView;

    InstructorConsultPresenter consultPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_consult);

        toolbar.setTitle(R.string.title_activity_student_consult);
        navigationView.setCheckedItem(R.id.consult);

        consultPresenter = new InstructorConsultPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        consultPresenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        consultPresenter.onDestroy();
    }

    @Override
    public void initViews() {
        searchClassmate = findViewById(R.id.searchInstructor);
        searchClassmate.setOnQueryTextListener(this);

        emptyClassmateImage = findViewById(R.id.ivEmptyInstructor);
        emptyClassmateText = findViewById(R.id.tvEmptyInstructor);
        recyclerView = findViewById(R.id.recyclerInstructor);

        emptyClassmateImage.setVisibility(View.GONE);
        emptyClassmateText.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void setEmptyState(String emptyStateMessage) {
        recyclerView.setVisibility(View.GONE);
        emptyClassmateImage.setVisibility(View.VISIBLE);
        emptyClassmateText.setVisibility(View.VISIBLE);
        emptyClassmateText.setText(emptyStateMessage);
    }

    @Override
    public void setInstructorFound(InstructorGroupAdapter adapter) {
        emptyClassmateImage.setVisibility(View.GONE);
        emptyClassmateText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setNoInstructorFound(String noInstructorFoundMessage) {
        recyclerView.setVisibility(View.GONE);

        emptyClassmateImage.setVisibility(View.VISIBLE);
        emptyClassmateText.setVisibility(View.VISIBLE);

        emptyClassmateText.setText(noInstructorFoundMessage);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        consultPresenter.submitQuery(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        consultPresenter.submitQuery(newText);
        return false;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.student_consult, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}

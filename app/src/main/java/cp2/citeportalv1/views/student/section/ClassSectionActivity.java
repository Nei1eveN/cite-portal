package cp2.citeportalv1.views.student.section;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import cp2.citeportalv1.R;

public class ClassSectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_section_info);
    }
}

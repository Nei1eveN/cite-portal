package cp2.citeportalv1.views.student.consultation.logs;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.PendingRequestGroupAdapter;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.presenters.consult.student.pending.PendingRequestPresenter;
import cp2.citeportalv1.presenters.consult.student.pending.PendingRequestPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class StudentPendingRequestsLogFragment extends Fragment implements PendingRequestPresenter.View {

    private Unbinder unbinder;

    @BindView(R.id.coorPending) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.swipePending) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ivEmptyPending) ImageView emptyPendingImage;
    @BindView(R.id.tvEmptyPending) TextView emptyPendingText;
    @BindView(R.id.rvPending) RecyclerView recyclerView;

    private PendingRequestPresenter responsePresenter;

    public StudentPendingRequestsLogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_pending_requests_log, container, false);
        unbinder = ButterKnife.bind(this, view);
        responsePresenter = new PendingRequestPresenterImpl(this, getActivity());

        swipeRefreshLayout.setOnRefreshListener(() -> responsePresenter.requestPendingList());
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        responsePresenter.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        responsePresenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void loadPendingResponses(List<ListItem> models) {
        PendingRequestGroupAdapter requestAdapter = new PendingRequestGroupAdapter(models, getActivity());

        emptyPendingImage.setVisibility(View.GONE);
        emptyPendingText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(requestAdapter);
    }

    @Override
    public void setEmptyState(String emptyMessage) {
        recyclerView.setVisibility(View.GONE);

        emptyPendingImage.setVisibility(View.VISIBLE);
        emptyPendingText.setVisibility(View.VISIBLE);

        emptyPendingText.setText(emptyMessage);
    }
}

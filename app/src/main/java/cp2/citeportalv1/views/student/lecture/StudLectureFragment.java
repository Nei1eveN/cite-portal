package cp2.citeportalv1.views.student.lecture;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cp2.citeportalv1.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class StudLectureFragment extends Fragment {


    public StudLectureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lecture, container, false);
    }

}

package cp2.citeportalv1.views.student.appointment;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.base.StudentBaseActivity;
import cp2.citeportalv1.presenters.consult.student.appointment.appointment_pages.StudentAppointmentPagePresenter;
import cp2.citeportalv1.presenters.consult.student.appointment.appointment_pages.StudentAppointmentPagePresenterImpl;

public class StudentAppointmentsActivity extends StudentBaseActivity implements StudentAppointmentPagePresenter.View {

    TabLayout tabLayout;
    ViewPager viewPager;

    StudentAppointmentPagePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_appointments);

        tabLayout = findViewById(R.id.tabDays);
        viewPager = findViewById(R.id.vpDay);

        toolbar.setTitle("Appointments");
        navigationView.setCheckedItem(R.id.appointments);
        presenter = new StudentAppointmentPagePresenterImpl(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void loadDayPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(numberOfPages);
        viewPager.setCurrentItem(0, false);
    }
}

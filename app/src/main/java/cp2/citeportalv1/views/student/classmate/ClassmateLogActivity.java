package cp2.citeportalv1.views.student.classmate;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.base.StudentBaseActivity;
import cp2.citeportalv1.presenters.classes.student.classmates.log_pages.ClassmateLogPresenter;
import cp2.citeportalv1.presenters.classes.student.classmates.log_pages.ClassmateLogPresenterImpl;

public class ClassmateLogActivity extends StudentBaseActivity implements ClassmateLogPresenter.View, ViewPager.OnPageChangeListener {

    TabLayout tabLayout;
    ViewPager viewPager;

    Intent intent;
    Bundle data;

    ClassmateLogPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_classmate_log);

        intent = getIntent();
        data = intent.getExtras();

        toolbar.setTitle("Schoolmates");

        navigationView.setCheckedItem(R.id.classmates);

        tabLayout = findViewById(R.id.tabClassmateLog);
        viewPager = findViewById(R.id.vpClassmateLog);

        presenter = new ClassmateLogPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void initializeTabs(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(numberOfPages);

        if (data != null){
            int position = data.getInt("tabNumber");
            viewPager.setCurrentItem(position, false);
        }

        viewPager.addOnPageChangeListener(this);
    }

    /**
     * View Pager OnPageChangeListener
     * **/

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                toolbar.setTitle("Schoolmates");
                break;
            case 1:
                toolbar.setTitle("Received Requests");
                break;
            case 2:
                toolbar.setTitle("Sent Requests");
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

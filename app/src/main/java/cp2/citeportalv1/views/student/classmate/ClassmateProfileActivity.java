package cp2.citeportalv1.views.student.classmate;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.presenters.classes.student.profile.ClassmateProfilePresenter;
import cp2.citeportalv1.presenters.classes.student.profile.ClassmateProfilePresenterImpl;

public class ClassmateProfileActivity extends AppCompatActivity implements ClassmateProfilePresenter.View {

    Intent intent;

    @BindView(R.id.classmateProfileCoor) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.profileSwipe) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.studentImage) ImageView studentImage;
    @BindView(R.id.ivEmailVerified) ImageView studentEmailVerified;
    @BindView(R.id.studentName) TextView studentName;
    @BindView(R.id.studentProg) TextView studentProg;
    @BindView(R.id.studentYrLvl) TextView studentYrLvl;
    @BindView(R.id.studentIDNum) TextView studentIDNum;
    @BindView(R.id.studentEmail) TextView studentEmail;
    @BindView(R.id.btnAddClassmate) Button addClassmate;

    /**
     * Headers
     **/
    @BindView(R.id.tvStudentIDHeader) TextView studentIDHeader;
    @BindView(R.id.tvStudentEmailHeader) TextView studentEmailHeader;

    ClassmateProfilePresenter profilePresenter;
    ProgressDialog progressDialog;

    AlertDialog.Builder emailVerifiedBuilder;
    AlertDialog emailVerifiedDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classmate_profile);
        ButterKnife.bind(this);

        intent = getIntent();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);


        progressDialog = new ProgressDialog(ClassmateProfileActivity.this);

        profilePresenter = new ClassmateProfilePresenterImpl(this, this);

        swipeRefreshLayout.setOnRefreshListener(() -> profilePresenter.refreshStudentInfo(intent.getStringExtra("notificationId")));
    }

    @Override
    protected void onStart() {
        super.onStart();
        profilePresenter.onStart(intent.getStringExtra("notificationId"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        profilePresenter.onDestroy();
        progressDialog.dismiss();
    }

    @OnClick(R.id.ivEmailVerified)
    protected void setStudentEmailVerified() {
        emailVerifiedBuilder = new AlertDialog.Builder(this);
        emailVerifiedBuilder.setIcon(R.drawable.ic_check_circle);
        emailVerifiedBuilder.setTitle("Email Verified");
        emailVerifiedBuilder.setMessage("This student has email verified.");
        emailVerifiedBuilder.setPositiveButton("Got it", (dialog, which) -> dialog.dismiss());

        emailVerifiedDialog = emailVerifiedBuilder.create();
        emailVerifiedDialog.show();
    }

    @OnClick(R.id.btnAddClassmate)
    protected void sendRequest() {
        profilePresenter.submitClassmateRequest(intent.getStringExtra("notificationId"));
    }

    @Override
    public void showProgress() {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
        progressDialog.hide();
    }

    @Override
    public void setToastMessage(String toastMessage) {
        Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setStudentProfile(Student student) {

        Glide.with(this).load(student.getStudentImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(studentImage);
        studentName.setText(String.format("%s, %s %s", student.getLastName(), student.getfName(), student.getMidName()));
        studentProg.setText(student.getProgram());
        studentYrLvl.setText(String.format("%s Year", student.getYearLvl()));
        studentIDNum.setText(String.valueOf(student.getStudentID()));
        studentEmail.setText(student.getEmail());

        Objects.requireNonNull(getSupportActionBar()).setTitle(studentName.getText().toString());
    }

    @Override
    public void setButtonStatus(String buttonStatus, Drawable buttonDrawable) {
        addClassmate.setText(buttonStatus);
        addClassmate.setCompoundDrawablesWithIntrinsicBounds(null, buttonDrawable, null, null);
    }

    @Override
    public void setStudentVerifiedStatus() {
        studentEmailVerified.setVisibility(View.VISIBLE);
    }

    @Override
    public void setStudentNotVerifiedStatus() {
        studentEmailVerified.setVisibility(View.GONE);
    }

    @Override
    public void hideAddButtonToSameOwner() {
        addClassmate.setVisibility(View.GONE);
    }

    @Override
    public void hideConfidentialOptionsToOtherUser() {
        studentIDHeader.setVisibility(View.GONE);
        studentIDNum.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}

package cp2.citeportalv1.views.student.profile;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.presenters.profile.student.StudentUserPresenter;
import cp2.citeportalv1.presenters.profile.student.StudentUserPresenterImpl;

public class StudentProfileActivity extends AppCompatActivity implements
        AppBarLayout.OnOffsetChangedListener, StudentUserPresenter.View {

    @BindView(R.id.studProfCoor) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.studProfAppBar) AppBarLayout appBarLayout;
    @BindView(R.id.collapsingToolbarLayout) CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.studProfToolbar) Toolbar toolbar;

    @BindView(R.id.studentProfileImage) ImageView studentProfileImage;
    @BindView(R.id.tvStudentProfileName) TextView studentProfileName;
    @BindView(R.id.tvStudentProfileIDProg) TextView studentProfileIDProg;

    @BindView(R.id.studTabLayout) TabLayout tabLayout;
    @BindView(R.id.vpStudProf) ViewPager viewPager;

    StudentUserPresenter presenter;

    AlertDialog.Builder exitBuilder;
    AlertDialog exitDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_profile);
        ButterKnife.bind(this);

        presenter = new StudentUserPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0)
        {
            //  Collapsed
            Objects.requireNonNull(getSupportActionBar()).setTitle("Profile");
        }
        else
        {
            //Expanded
            Objects.requireNonNull(getSupportActionBar()).setTitle(null);
        }
    }

    @Override
    public void setPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(numberOfPages);
//        viewPager.setCurrentItem(0, false);

        tabLayout.setupWithViewPager(viewPager);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back);
        upArrow.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    public void setStudentUserInfo(Student studentUserInfo) {
        Glide.with(this).load(studentUserInfo.getStudentImageURL()).apply(new RequestOptions().circleCrop()).into(studentProfileImage);
        studentProfileName.setText(String.format("%s, %s %s", studentUserInfo.getLastName(), studentUserInfo.getfName(), studentUserInfo.getMidName()));
        studentProfileIDProg.setText(String.format("%s", String.valueOf(studentUserInfo.getStudentID())));
    }

    @Override
    public void setExitDialog(String title, String message) {
        exitBuilder = new AlertDialog.Builder(this);
        exitBuilder.setCancelable(false);
        exitBuilder.setTitle(title);
        exitBuilder.setMessage(message);
        exitBuilder.setPositiveButton("EXIT", (dialog, which) -> finish());

        exitDialog = exitBuilder.create();
        exitDialog.show();
    }
}

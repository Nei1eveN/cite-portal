package cp2.citeportalv1.views.student.section;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.List;

import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.StudentSearchCourseAdapter;
import cp2.citeportalv1.base.StudentBaseActivity;
import cp2.citeportalv1.models.Class;
import cp2.citeportalv1.presenters.classes.student.search.StudClassSearchPresenter;
import cp2.citeportalv1.presenters.classes.student.search.StudClassSearchPresenterImpl;

public class SearchClassActivity extends StudentBaseActivity implements
        StudClassSearchPresenter.SearchView,
        SearchView.OnQueryTextListener {

    SearchView searchView;
    ImageView emptyStateImage;
    TextView emptyStateText;
    RecyclerView recyclerView;
    TextView existingClasses;

    StudClassSearchPresenter searchPresenter;
    StudentSearchCourseAdapter courseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_search_class);

        toolbar.setTitle("Search Class");
//        navigationView.setCheckedItem(R.id.search);

        searchPresenter = new StudClassSearchPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        searchPresenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        searchPresenter.onDestroy();
    }

    @Override
    public void initViews() {
        searchView = findViewById(R.id.searchCourseCode);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);

        emptyStateImage = findViewById(R.id.ivSearchEmptyState);
        emptyStateText = findViewById(R.id.tvSearchNotFound);
        recyclerView = findViewById(R.id.rvClassSection);
        existingClasses = findViewById(R.id.tvExistingClasses);

        emptyStateImage.setVisibility(View.GONE);
        emptyStateText.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        existingClasses.setVisibility(View.GONE);
    }

    @Override
    public void setEmptyState(String emptyCaption) {
        recyclerView.setVisibility(View.GONE);
        existingClasses.setVisibility(View.GONE);
        emptyStateImage.setVisibility(View.VISIBLE);
        emptyStateText.setVisibility(View.VISIBLE);
        emptyStateText.setText(emptyCaption);
    }

    @Override
    public void setClassResult(List<Class> classList) {
        emptyStateImage.setVisibility(View.GONE);
        emptyStateText.setVisibility(View.GONE);

        courseAdapter = new StudentSearchCourseAdapter(this, classList);
        courseAdapter.notifyDataSetChanged();

        existingClasses.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(courseAdapter);
    }

    @Override
    public void setClassNotFound(String notFoundMessage) {
        existingClasses.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        emptyStateImage.setVisibility(View.VISIBLE);
        emptyStateText.setVisibility(View.VISIBLE);
        emptyStateText.setText(notFoundMessage);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        searchPresenter.submitQuery(s);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        searchPresenter.submitQuery(s);
        return true;
    }
}

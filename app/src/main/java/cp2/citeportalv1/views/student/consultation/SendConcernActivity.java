package cp2.citeportalv1.views.student.consultation;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ClassmateAutoCompleteAdapter;
import cp2.citeportalv1.adapters.StudentConsultScheduleAdapter;
import cp2.citeportalv1.adapters.listeners.RecyclerOnClickListener;
import cp2.citeportalv1.models.Classmate;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.presenters.consult.student.sendmessage.StudSendMessagePresenter;
import cp2.citeportalv1.presenters.consult.student.sendmessage.StudSendMessagePresenterImpl;
import cp2.citeportalv1.utils.EventDecorator;
import cp2.citeportalv1.utils.OneDayDecorator;

import static cp2.citeportalv1.utils.Constants.calendar;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;
import static cp2.citeportalv1.utils.Constants.fixedFormat;
import static cp2.citeportalv1.utils.Constants.generateTimepoints;
import static cp2.citeportalv1.utils.Constants.hourFormat;
import static cp2.citeportalv1.utils.Constants.minuteFormat;
import static cp2.citeportalv1.utils.Constants.secondsFormat;
import static cp2.citeportalv1.utils.Constants.timeFormat12hr;

public class SendConcernActivity extends AppCompatActivity implements StudSendMessagePresenter.View,
        RecyclerOnClickListener, OnDateSelectedListener{

    @BindView(R.id.studConsultCoor) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.tvSenderName) TextView senderName;
    @BindView(R.id.ivStudentProfileImage) ImageView senderImage;
    @BindView(R.id.tvStudentYearLevel) TextView senderYearLevel;

    @BindView(R.id.rvDaySched) RecyclerView recyclerView;
    @BindView(R.id.tvSessionHint) TextView sessionHint;

    @BindView(R.id.ivEmptySchedule) ImageView emptyScheduleImage;
    @BindView(R.id.tvEmptySchedule) TextView emptyScheduleText;

    @BindView(R.id.tvConSchedHeader) TextView spinnerSchedHeader;
    @BindView(R.id.tvCCHeader) TextView ccHeader;
    @BindView(R.id.tvRequestedSchedHeader) TextView reqSchedHeader;

    @BindView(R.id.macTvClassmate) MultiAutoCompleteTextView classmateName;

    @BindView(R.id.ivFacultyProfileImage) ImageView facultyProfileImage;
    @BindView(R.id.tvFacultyName) TextView facultyName;
    @BindView(R.id.tvFacultyDepartment) TextView facultyDepartment;

    @BindView(R.id.spinnerCourses) Spinner spinnerCourses;
    @BindView(R.id.spinnerConcern) Spinner spinnerConcernType;

    @BindView(R.id.txMessage) TextInputLayout messageWrapper;
    @BindView(R.id.etMessage) TextInputEditText consultationMessage;

    @BindView(R.id.tvReqStart) TextView reqStart;
    @BindView(R.id.tvUpto) TextView upTo;
    @BindView(R.id.tvReqEnd) TextView reqEnd;
    @BindView(R.id.tvReqVenue) TextView reqVenue;
    @BindView(R.id.tvRequestedDay) TextView reqDay;
    @BindView(R.id.tvReqDayDateUpto) TextView reqDayDateUpTo;
    @BindView(R.id.tvRequestedDate) TextView reqDate;

    @BindView(R.id.calendarView) MaterialCalendarView calendarView;

    ProgressDialog progressDialog;

    Intent intent;

    StudSendMessagePresenter sendMessagePresenter;

    AlertDialog.Builder builder, schedSetBuilder;
    AlertDialog dialog;
    LayoutInflater schedInflater;
    View schedView;
    TextView timeFrom, timeTo;
    MaterialButton startFrom, endTo;

    String timeStart, timeEnd, venue, requestedDay, reqTimeStart, reqTimeEnd;
    String facultyId;

    Date selectedStartTime, selectedDate;

    TimePickerDialog pickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_send_message);
        ButterKnife.bind(this);

        intent = getIntent();
        facultyId = intent.getStringExtra("facultyId");

        progressDialog = new ProgressDialog(this);

        sendMessagePresenter = new StudSendMessagePresenterImpl(this, this);
    }

    @OnItemSelected(R.id.spinnerConcern)
    void setSpinnerConcernType(int position) {
        String concernType = spinnerConcernType.getItemAtPosition(position).toString();

        switch (concernType) {
            case "Others":
                messageWrapper.setVisibility(View.VISIBLE);
                consultationMessage.setVisibility(View.VISIBLE);
                break;
                default:
                    messageWrapper.setVisibility(View.GONE);
                    consultationMessage.setVisibility(View.GONE);
                    break;
        }
    }

    @OnClick(R.id.btnSubmit)
    protected void submitConcern() {
        sendMessagePresenter.submitConsultationDetailsWithCc(classmateName.getText().toString(), spinnerCourses.getSelectedItem().toString(), spinnerConcernType.getSelectedItem().toString(), Objects.requireNonNull(consultationMessage.getText()).toString(), reqDay.getText().toString(), reqDate.getText().toString(), reqTimeStart, reqTimeEnd, reqVenue.getText().toString(), facultyId);
    }

    @OnClick(R.id.btnCancel)
    protected void cancel() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStart() {
        super.onStart();
        sendMessagePresenter.onStart();
        sendMessagePresenter.findSchedules(facultyId);
        sendMessagePresenter.findScheduleFromDate(facultyId, calendarView.getSelectedDate().getDate());
    }

    @Override
    protected void onResume() {
        super.onResume();
        sendMessagePresenter.initiateCalendar();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sendMessagePresenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                startActivity(new Intent(this, ConsultDepartmentsActivity.class));
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        startActivity(new Intent(this, ConsultDepartmentsActivity.class));
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void setSenderDetails(Student student) {
        senderName.setText(String.format("%s, %s %s", student.getLastName(), student.getfName(), student.getMidName()));
        senderYearLevel.setText(String.format("%s Year", student.getYearLvl()));
        Glide.with(this).load(student.getStudentImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px).diskCacheStrategy(DiskCacheStrategy.ALL)).into(senderImage);
    }

    @Override
    public void setNoAvailableSchedules(String emptyMessage) {
        spinnerSchedHeader.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        sessionHint.setVisibility(View.GONE);

        reqStart.setText(null);
        reqEnd.setText(null);
        reqVenue.setText(null);
        reqDay.setText(null);
        reqDate.setText(null);

        reqSchedHeader.setVisibility(View.GONE);
        reqStart.setVisibility(View.GONE);
        upTo.setVisibility(View.GONE);
        reqEnd.setVisibility(View.GONE);
        reqVenue.setVisibility(View.GONE);
        reqDay.setVisibility(View.GONE);
        reqDayDateUpTo.setVisibility(View.GONE);
        reqDate.setVisibility(View.GONE);

        emptyScheduleImage.setVisibility(View.VISIBLE);
        emptyScheduleText.setVisibility(View.VISIBLE);

        emptyScheduleText.setText(emptyMessage);
    }

    @Override
    public void setScheduleDetails(List<Schedule> schedules) {
        StudentConsultScheduleAdapter scheduleAdapter = new StudentConsultScheduleAdapter(this, schedules, this);
        emptyScheduleImage.setVisibility(View.GONE);
        emptyScheduleText.setVisibility(View.GONE);

        spinnerSchedHeader.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        sessionHint.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(scheduleAdapter);
    }

    @Override
    public void initViews(ArrayList<String> courses) {
        String imageURL = intent.getStringExtra("facultyImage");
        String lastName = intent.getStringExtra("facultyLastName");
        String firstName = intent.getStringExtra("facultyFirstName");
        String middleName = intent.getStringExtra("facultyMiddleName");
        String department = intent.getStringExtra("facultyDepartment");

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Send Me A Message");
        getSupportActionBar().setSubtitle(lastName + ", " + firstName);

        Glide.with(this).load(imageURL).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px).diskCacheStrategy(DiskCacheStrategy.ALL)).into(facultyProfileImage);
        facultyName.setText(String.format("%s, %s %s", lastName.toUpperCase(), firstName, middleName));
        facultyDepartment.setText(department);

        Log.d("facultyDepartment", facultyDepartment.getText().toString());

        ArrayAdapter<String> coursesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, courses);
        coursesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCourses.setAdapter(coursesAdapter);
        spinnerCourses.setPrompt("Course");

        spinnerConcernType.setPrompt("Type of Concern");

//        spinnerCourses.setThreshold(1);

        reqSchedHeader.setVisibility(View.GONE);
        reqStart.setVisibility(View.GONE);
        upTo.setVisibility(View.GONE);
        reqEnd.setVisibility(View.GONE);
        reqVenue.setVisibility(View.GONE);
        reqDay.setVisibility(View.GONE);
        reqDayDateUpTo.setVisibility(View.GONE);
        reqDate.setVisibility(View.GONE);
    }

    @Override
    public void showProgress(String caption) {
        progressDialog.setMessage(caption);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setEmptyWrapper() {
        messageWrapper.setError("Required Field");
    }

    @Override
    public void setNullWrapper() {
        messageWrapper.setError(null);
    }

    @Override
    public void setSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorMessage(String title, String errorMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(errorMessage).setPositiveButton("Exit", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    @Override
    public void setExitMessage(String title, String exitMessage) {
        builder = new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(exitMessage).setPositiveButton("Exit", (dialog, which) ->
        {
            startActivity(new Intent(this, ConsultDepartmentsActivity.class));
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void loadClassmates(List<Classmate> classmates) {
        ClassmateAutoCompleteAdapter adapter = new ClassmateAutoCompleteAdapter(this, classmates);

        classmateName.setEnabled(true);
        classmateName.setThreshold(1);
        classmateName.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        classmateName.setAdapter(adapter);
    }

    @Override
    public void setEmptyClassmates(String emptyMessage) {
        classmateName.setEnabled(false);
        classmateName.setHint(emptyMessage);
    }

    @Override
    public void setUpCalendar() {
        calendarView.setVisibility(View.VISIBLE);
        calendarView.addDecorator(new OneDayDecorator());
        calendarView.setTileSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 44, getResources().getDisplayMetrics()));
        calendarView.setSelectedDate(new Date());
        calendarView.newState().setMinimumDate(Calendar.getInstance()).setCalendarDisplayMode(CalendarMode.MONTHS).commit();
        calendarView.setOnDateChangedListener(this);

        sendMessagePresenter.findAppointments(facultyId);
    }

    @Override
    public void setUpDotsToCalendar(EventDecorator eventDecorator) {
        calendarView.addDecorator(eventDecorator);
    }

    @Override
    public void setUpApprovedTimeSlotsToTimePicker(Timepoint[] existingTimes) {
        pickerDialog.setDisabledTimes(existingTimes);
    }

    @SuppressLint("InflateParams")
    @Override
    public void OnDetailClick(String startTime, String endTime, String location, String day) {
        timeStart = startTime;
        timeEnd = endTime;
        venue = location;
        requestedDay = day;

        schedInflater = LayoutInflater.from(SendConcernActivity.this);
        schedView = schedInflater.inflate(R.layout.faculty_request_time, null);
        timeFrom = schedView.findViewById(R.id.tvTimeFrom);
        timeTo = schedView.findViewById(R.id.tvTimeTo);
        startFrom = schedView.findViewById(R.id.btnStartFrom);
        endTo = schedView.findViewById(R.id.btnEndTo);

        endTo.setVisibility(View.GONE);

        try {
            Date startingTime = timeFormat12hr.parse(timeStart);
            Date endingTime = timeFormat12hr.parse(timeEnd);
            Log.d("getStartingTime", timeFormat12hr.format(startingTime));
            Log.d("getStartingTimeInDate", String.valueOf(startingTime));

            startFrom.setOnClickListener(v -> {

                sendMessagePresenter.findExistingTimeSlotAppointmentsFromDate(facultyId, calendarView.getSelectedDate().getDate());

                TimePickerDialog.OnTimeSetListener setListener = (view, hourOfDay, minute, second) -> {
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    calendar.set(Calendar.MINUTE, minute);

                    timeFrom.setText(timeFormat12hr.format(calendar.getTime()));
                    Log.d("getReqTimeStart", fixedFormat.format(calendar.getTime()));
                    Log.d("getReqTimeStartInDate", String.valueOf(calendar.getTime()));
                    reqTimeStart = timeFormat12hr.format(calendar.getTime());
                    try {
                        selectedStartTime = timeFormat12hr.parse(reqTimeStart);
                        Log.d("selectedStartTime", String.valueOf(selectedStartTime.getTime()));
                        calendar.setTime(selectedStartTime);
                        calendar.add(Calendar.MINUTE, 15);
                        reqTimeEnd = timeFormat12hr.format(calendar.getTime());
                        timeTo.setText(reqTimeEnd);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                };

                pickerDialog = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(setListener, Integer.valueOf(hourFormat.format(startingTime)), Integer.valueOf(minuteFormat.format(startingTime)), false);
                pickerDialog.setMinTime(Integer.valueOf(hourFormat.format(startingTime)), Integer.valueOf(minuteFormat.format(startingTime)), Integer.valueOf(secondsFormat.format(startingTime)));
                pickerDialog.setMaxTime(Integer.valueOf(hourFormat.format(endingTime)), Integer.valueOf(minuteFormat.format(endingTime)), Integer.valueOf(secondsFormat.format(endingTime)));
                pickerDialog.setSelectableTimes(generateTimepoints(15));

                pickerDialog.show(getSupportFragmentManager(), "TimePickerDialogStartTime");
            });

            endTo.setOnClickListener(v -> {
                TimePickerDialog.OnTimeSetListener setListener = (view, hourOfDay, minute, second) -> {
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    calendar.set(Calendar.MINUTE, minute);

                    timeTo.setText(timeFormat12hr.format(calendar.getTime()));
                    Log.d("getReqTimeEnd", fixedFormat.format(calendar.getTime()));
                    Log.d("getReqTimeEndInDate", String.valueOf(calendar.getTime()));
                    reqTimeEnd = timeFormat12hr.format(calendar.getTime());
                };

                if (selectedStartTime == null) {
                    androidx.appcompat.app.AlertDialog.Builder emptyStartTimeBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
                    emptyStartTimeBuilder.setTitle("No Starting Time").setMessage("Please select a starting time.");
                    emptyStartTimeBuilder.setPositiveButton("GOT IT", (dialog, which) -> dialog.dismiss());

                    androidx.appcompat.app.AlertDialog emptyStartTimeDialog = emptyStartTimeBuilder.create();
                    emptyStartTimeDialog.show();
                } else {
                    TimePickerDialog pickerDialog = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(setListener, Integer.valueOf(hourFormat.format(startingTime)), Integer.valueOf(minuteFormat.format(startingTime)), false);
                    pickerDialog.setMinTime(Integer.valueOf(hourFormat.format(selectedStartTime)), Integer.valueOf(minuteFormat.format(selectedStartTime)), Integer.valueOf(secondsFormat.format(selectedStartTime)));
                    pickerDialog.setMaxTime(Integer.valueOf(hourFormat.format(endingTime)), Integer.valueOf(minuteFormat.format(endingTime)), Integer.valueOf(secondsFormat.format(endingTime)));
                    pickerDialog.setSelectableTimes(generateTimepoints(15));

                    pickerDialog.show(getSupportFragmentManager(), "TimePickerDialogStartTime");
                }

            });

        } catch (ParseException e) {
            e.printStackTrace();
        }

        timeFrom.setText(startTime);
        timeTo.setText(endTime);

        schedSetBuilder = new AlertDialog.Builder(this);
        schedSetBuilder.setView(schedView).setTitle(String.format("%s - %s", timeStart, timeEnd)).setIcon(R.drawable.ic_timer)
                .setPositiveButton("SET", (dialog, which) ->
                {
                    reqSchedHeader.setVisibility(View.VISIBLE);
                    reqStart.setVisibility(View.VISIBLE);
                    upTo.setVisibility(View.VISIBLE);
                    reqEnd.setVisibility(View.VISIBLE);
                    reqVenue.setVisibility(View.VISIBLE);
                    reqDayDateUpTo.setVisibility(View.VISIBLE);
                    reqDay.setVisibility(View.VISIBLE);
                    reqDate.setVisibility(View.VISIBLE);

                    if (selectedDate != null) {
                        reqStart.setText(reqTimeStart);
                        reqEnd.setText(reqTimeEnd);
                        reqVenue.setText(venue);
                        reqDay.setText(dayFormat.format(selectedDate));
                        reqDate.setText(dateFormat.format(selectedDate));
                    } else {
                        reqStart.setText(reqTimeStart);
                        reqEnd.setText(reqTimeEnd);
                        reqVenue.setText(venue);
                        reqDay.setText(dayFormat.format(calendarView.getSelectedDate().getDate()));
                        reqDate.setText(dateFormat.format(new Date()));
                    }

                    dialog.dismiss();
                }).setNegativeButton("CANCEL", null);

        schedSetBuilder.create().show();
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        calendarView.setDateSelected(date.getDate(), selected);
        sendMessagePresenter.findScheduleFromDate(facultyId, date.getDate());
        selectedDate = calendarView.getSelectedDate().getDate();
    }
}
package cp2.citeportalv1.views.student.consultation.logs;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.StudentConsultationLogGroupAdapter;
import cp2.citeportalv1.presenters.consult.student.consultation_log.StudentConsultationLogPresenter;
import cp2.citeportalv1.presenters.consult.student.consultation_log.StudentConsultationLogPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class StudentConsultationLogFragment extends Fragment implements StudentConsultationLogPresenter.View, SwipeRefreshLayout.OnRefreshListener {

    protected Unbinder unbinder;

    @BindView(R.id.coorStudentLog) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.swipeStudentLog) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ivEmptyLog) ImageView emptyApprovedImage;
    @BindView(R.id.tvEmptyLog) TextView emptyApprovedText;
    @BindView(R.id.rvStudentLog) RecyclerView recyclerView;

    private StudentConsultationLogPresenter presenter;

    public StudentConsultationLogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new StudentConsultationLogPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_student_consultation_log, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void loadConsultationLogs(StudentConsultationLogGroupAdapter adapter) {
        emptyApprovedImage.setVisibility(View.GONE);
        emptyApprovedText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyState(String emptyMessage) {
        recyclerView.setVisibility(View.GONE);

        emptyApprovedImage.setVisibility(View.VISIBLE);
        emptyApprovedText.setVisibility(View.VISIBLE);

        emptyApprovedText.setText(emptyMessage);
    }

    @Override
    public void onRefresh() {
        presenter.requestConsultationLogs();
    }
}

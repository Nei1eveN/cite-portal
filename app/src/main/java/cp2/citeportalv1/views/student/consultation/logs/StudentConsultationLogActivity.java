package cp2.citeportalv1.views.student.consultation.logs;

import android.os.Bundle;

import cp2.citeportalv1.R;
import cp2.citeportalv1.base.StudentBaseActivity;

public class StudentConsultationLogActivity extends StudentBaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_consultation_log);

        toolbar.setTitle("Consultation Log");
        toolbar.setSubtitle("Finished Requests");
        navigationView.setCheckedItem(R.id.stud_consultation_history);
    }
}

package cp2.citeportalv1.views.student.login_register;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.presenters.login.student.StudentLoginPresenter;
import cp2.citeportalv1.presenters.login.student.StudentLoginPresenterImpl;
import cp2.citeportalv1.views.faculty.FacultyLoginActivity;
import cp2.citeportalv1.views.guest.GuestViewActivity;

import static cp2.citeportalv1.utils.Constants.showExitDialog;

public class StudentLoginActivity extends AppCompatActivity implements StudentLoginPresenter.StudentLoginView {

    @BindView(R.id.userLayout) TextInputLayout userIdWrapper;
    @BindView(R.id.passLayout) TextInputLayout passWrapper;

    @BindView(R.id.txUserId) TextInputEditText userID;
    @BindView(R.id.txPass) TextInputEditText password;
    @BindView(R.id.btnLogin) Button btnLogin;
    @BindView(R.id.btnReg) Button btnReg;
    @BindView(R.id.tvForgot) TextView forgotPassword;
    @BindView(R.id.tvVerifyEmail) TextView verifyEmail;

    StudentLoginPresenter presenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_login);
        ButterKnife.bind(this);
        presenter = new StudentLoginPresenterImpl(this, this);
        progressDialog = new ProgressDialog(StudentLoginActivity.this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, GuestViewActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showExitDialog(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @OnClick(R.id.btnLogin)
    public void submitCredentials() {
        presenter.requestLogin(Objects.requireNonNull(userID.getText()).toString(), Objects.requireNonNull(password.getText()).toString());
    }

    @OnClick(R.id.btnReg)
    public void register() {
        Intent intent = new Intent(this, StudentRegisterActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
    }

    @OnClick(R.id.tvForgot)
    public void setForgotPassword() {
        LayoutInflater loginFormInflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View loginForm = loginFormInflater.inflate(R.layout.layout_send_email, null);

        TextInputEditText email = loginForm.findViewById(R.id.etEmail);

        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle("Send Email For Password Reset");
        dialogBuilder.setIcon(R.drawable.ic_info);
        dialogBuilder.setView(loginForm);

        dialogBuilder.setPositiveButton("SUBMIT", (dialog, which) -> {
            dialog.dismiss();
            presenter.sendPasswordResetLink(Objects.requireNonNull(email.getText()).toString());
        }).setNegativeButton("CANCEL", null).create().show();
    }

    @OnClick(R.id.tvVerifyEmail)
    void sendEmailVerificationLink() {
        LayoutInflater loginFormInflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View loginForm = loginFormInflater.inflate(R.layout.layout_send_email, null);

        TextInputEditText email = loginForm.findViewById(R.id.etEmail);

        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle("Send Email For Verification");
        dialogBuilder.setIcon(R.drawable.ic_info);
        dialogBuilder.setView(loginForm);

        dialogBuilder.setPositiveButton("SUBMIT", (dialog, which) -> {
            dialog.dismiss();
            presenter.sendEmailVerificationLink(Objects.requireNonNull(email.getText()).toString());
        }).setNegativeButton("CANCEL", null).create().show();
    }

    @OnClick(R.id.btnLogAsFac)
    protected void goToFacultyLogin(){
        Intent intent = new Intent(this, FacultyLoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showProgress() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setNullWrapper() {
        if (!Objects.requireNonNull(userID.getText()).toString().isEmpty()) {
            userIdWrapper.setError(null);
        }
        else if (!Objects.requireNonNull(password.getText()).toString().isEmpty()) {
            passWrapper.setError(null);
        }
    }

    @Override
    public void showAlertDialog(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setCancelable(false).setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss()).create().show();
    }

    @Override
    public void setEmptyUserId() {
        userIdWrapper.setError("Required field");
    }

    @Override
    public void setEmptyPass() {
        passWrapper.setError("Required field");
    }
}
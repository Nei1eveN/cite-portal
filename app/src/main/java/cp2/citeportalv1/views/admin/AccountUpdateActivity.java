package cp2.citeportalv1.views.admin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Accounts;
import cp2.citeportalv1.presenters.accounts.detail.edit.AccountUpdatePresenter;
import cp2.citeportalv1.presenters.accounts.detail.edit.AccountUpdatePresenterImpl;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.MenuItem;
import android.widget.Spinner;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import static cp2.citeportalv1.utils.Constants.ACCOUNT_UID;

public class AccountUpdateActivity extends AppCompatActivity implements AccountUpdatePresenter.View {

    @BindView(R.id.coorUpdate) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.etID) TextInputEditText employeeID;
    @BindView(R.id.etFirstName) TextInputEditText firstName;
    @BindView(R.id.etMiddleName) TextInputEditText middleName;
    @BindView(R.id.etLastName) TextInputEditText lastName;
    @BindView(R.id.spinnerDepartment) Spinner spinnerDepartment;
    @BindView(R.id.spinnerAccessLevel) Spinner spinnerAccessLevel;

    @BindView(R.id.btnUpdate) MaterialButton btnUpdate;
    @BindView(R.id.btnCancel) MaterialButton btnCancel;

    Intent intent;

    String userUid;

    ProgressDialog progressDialog;

    AccountUpdatePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_update);
        ButterKnife.bind(this);

        intent = getIntent();
        userUid = intent.getStringExtra(ACCOUNT_UID);

        progressDialog = new ProgressDialog(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Update Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new AccountUpdatePresenterImpl(this, this);

        employeeID.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
    }

    @OnClick(R.id.btnUpdate)
    void updateAccount() {
        presenter.submitCredentials(userUid, Objects.requireNonNull(employeeID.getText()).toString(), Objects.requireNonNull(firstName.getText()).toString(), Objects.requireNonNull(middleName.getText()).toString(), Objects.requireNonNull(lastName.getText()).toString(), spinnerDepartment.getSelectedItem().toString(), spinnerAccessLevel.getSelectedItem().toString());
    }

    @OnClick(R.id.btnCancel)
    void cancelUpdate() {
        Intent intent = new Intent(this, AccountDetailActivity.class);
        intent.putExtra(ACCOUNT_UID, userUid);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, AccountDetailActivity.class);
                intent.putExtra(ACCOUNT_UID, userUid);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, AccountDetailActivity.class);
        intent.putExtra(ACCOUNT_UID, userUid);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(userUid);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }


    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton("EXIT", (dialog, which) -> {
            Intent intent = new Intent(this, AccountDetailActivity.class);
            intent.putExtra(ACCOUNT_UID, userUid);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }).setCancelable(false).create().show();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss()).create().show();
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setUserDetail(Accounts account) {
        employeeID.setText(account.getAccountID());
        firstName.setText(account.getFirstName());
        middleName.setText(account.getMiddleName());
        lastName.setText(account.getLastName());
        spinnerDepartment.setPrompt("Department");
        spinnerAccessLevel.setPrompt("Access Level");
    }
}

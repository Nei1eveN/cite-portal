package cp2.citeportalv1.views.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.base.AdminBaseActivity;

public class AccountsActivity extends AdminBaseActivity implements Toolbar.OnMenuItemClickListener {

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_panel);

        navigationView.setCheckedItem(R.id.accounts);
        toolbar.setTitle("Accounts");
        toolbar.setSubtitle("Moderator & Admin Accounts");

        toolbar.inflateMenu(R.menu.admin_create_new_account_menu);
        toolbar.setOnMenuItemClickListener(this);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AdminListFragment(), "Admins");
        adapter.addFragment(new SecretaryListFragment(), "Department Secretaries");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setCurrentItem(0, false);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.create_new:
                startActivity(new Intent(this, AccountCreateActivity.class));
                overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                break;
        }
        return false;
    }
}

package cp2.citeportalv1.views.admin;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ModeratorAdapter;
import cp2.citeportalv1.presenters.accounts.secretary.SecretaryAccountsPresenter;
import cp2.citeportalv1.presenters.accounts.secretary.SecretaryAccountsPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecretaryListFragment extends Fragment implements SecretaryAccountsPresenter.View, SwipeRefreshLayout.OnRefreshListener {

    private Unbinder unbinder;
    @BindView(R.id.coorList)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.swipeList)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ivEmptyList)
    ImageView emptyImage;
    @BindView(R.id.tvEmptyList)
    TextView emptyText;
    @BindView(R.id.rvList)
    RecyclerView recyclerView;

    private SecretaryAccountsPresenter presenter;

    public SecretaryListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_secretary_list, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SecretaryAccountsPresenterImpl(this, getActivity());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setEmptyState(String emptyStateMessage) {
        recyclerView.setVisibility(View.GONE);

        emptyImage.setVisibility(View.VISIBLE);
        emptyText.setVisibility(View.VISIBLE);

        emptyText.setText(emptyStateMessage);
    }

    @Override
    public void setList(ModeratorAdapter adapter) {
        emptyImage.setVisibility(View.GONE);
        emptyText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        presenter.requestList();
    }
}

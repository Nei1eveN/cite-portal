package cp2.citeportalv1.views.admin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Accounts;
import cp2.citeportalv1.presenters.accounts.detail.AccountDetailPresenter;
import cp2.citeportalv1.presenters.accounts.detail.AccountDetailPresenterImpl;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import static cp2.citeportalv1.utils.Constants.ACCOUNT_UID;

public class AccountDetailActivity extends AppCompatActivity implements AccountDetailPresenter.View {

    @BindView(R.id.coorUserDetail) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.ivUserImage) ImageView userImage;
    @BindView(R.id.ivUserName) TextView userName;
    @BindView(R.id.tvUserProgDept) TextView userProgramDept;
    @BindView(R.id.tvUserID) TextView userID;
    @BindView(R.id.tvUserEmail) TextView userEmail;

    @BindView(R.id.btnSendEmailLink) MaterialButton btnSendEmail;
    @BindView(R.id.btnRemove) MaterialButton btnRemove;
    @BindView(R.id.btnPasswordReset) MaterialButton btnPasswordReset;
    @BindView(R.id.btnEdit) MaterialButton btnEditProfile;

    Intent intent;

    String userUid;

    ProgressDialog progressDialog;

    AccountDetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_detail);
        ButterKnife.bind(this);

        intent = getIntent();
        userUid = intent.getStringExtra(ACCOUNT_UID);

        progressDialog = new ProgressDialog(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("User Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new AccountDetailPresenterImpl(this, this);
    }

    @OnClick(R.id.btnSendEmailLink)
    void sendVerificationLink() {
        new AlertDialog.Builder(this).setTitle("Sending Email Verification Link").setMessage("Do you want to proceed?").setPositiveButton("SEND", (dialog, which) -> presenter.sendEmailVerification(userUid)).setNegativeButton("CANCEL", null).setCancelable(false).create().show();
    }

    @OnClick(R.id.btnRemove)
    void removeUser() {
        new AlertDialog.Builder(this).setTitle("Removing User Account").setMessage("Do you want to proceed?").setPositiveButton("PROCEED", (dialog, which) -> presenter.setUserRemoved(userUid)).setNegativeButton("CANCEL", null).setCancelable(false).create().show();
    }

    @OnClick(R.id.btnPasswordReset)
    void sendPasswordReset() {
        new AlertDialog.Builder(this).setTitle("Sending Password Reset Link").setMessage("Do you want to proceed?").setPositiveButton("PROCEED", (dialog, which) -> presenter.sendPasswordReset(userUid)).setNegativeButton("CANCEL", null).setCancelable(false).create().show();
    }

    @OnClick(R.id.btnEdit)
    void editUserProfile() {
        Intent intent = new Intent(this, AccountUpdateActivity.class);
        intent.putExtra(ACCOUNT_UID, userUid);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(userUid);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }


    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton("EXIT", (dialog, which) -> {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }).setCancelable(false).create().show();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss()).create().show();
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setUserDetail(Accounts account) {
        userName.setText(String.format("%s, %s %s", account.getLastName(), account.getFirstName(), account.getMiddleName()));
        userProgramDept.setText(String.format("%s | %s", account.getDepartment(), account.getAccessLevel()));
        userEmail.setText(account.getEmail());
        userID.setText(account.getAccountID());
    }
}

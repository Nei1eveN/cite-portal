package cp2.citeportalv1.views.admin;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.presenters.accounts.detail.create.AccountCreatePresenter;
import cp2.citeportalv1.presenters.accounts.detail.create.AccountCreatePresenterImpl;
import cp2.citeportalv1.views.faculty.FacultyLoginActivity;

public class AccountCreateActivity extends AppCompatActivity implements AccountCreatePresenter.View {

    @BindView(R.id.coorCreate) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.etID) TextInputEditText employeeID;
    @BindView(R.id.etFirstName) TextInputEditText firstName;
    @BindView(R.id.etMiddleName) TextInputEditText middleName;
    @BindView(R.id.etLastName) TextInputEditText lastName;
    @BindView(R.id.spinnerDepartment) Spinner spinnerDepartment;
    @BindView(R.id.spinnerAccessLevel) Spinner spinnerAccessLevel;

    @BindView(R.id.etEmail) TextInputEditText email;
    @BindView(R.id.etPassword) TextInputEditText password;

    @BindView(R.id.btnCreate) MaterialButton btnCreate;
    @BindView(R.id.btnCancel) MaterialButton btnCancel;

    ProgressDialog progressDialog;

    AccountCreatePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_create);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Create Moderator Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new AccountCreatePresenterImpl(this, this);

        employeeID.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
    }

    @OnClick(R.id.btnCreate)
    void createAccount() {
        presenter.submitCredentials(Objects.requireNonNull(employeeID.getText()).toString(), Objects.requireNonNull(firstName.getText()).toString(), Objects.requireNonNull(middleName.getText()).toString(), Objects.requireNonNull(lastName.getText()).toString(), spinnerDepartment.getSelectedItem().toString(), Objects.requireNonNull(email.getText()).toString(), Objects.requireNonNull(password.getText()).toString(), spinnerAccessLevel.getSelectedItem().toString());
    }

    @OnClick(R.id.btnCancel)
    void cancelUpdate() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showExitDialog(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton("EXIT", (dialog, which) -> {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }).setCancelable(false).create().show();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss()).create().show();
    }

    @Override
    public void showLoginPageDialog(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton("EXIT", (dialog, which) -> {
            startActivity(new Intent(this, FacultyLoginActivity.class));
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }).setCancelable(false).create().show();
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setUpReAuthenticationDialog(String title, String message) {
        LayoutInflater loginFormInflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View loginForm = loginFormInflater.inflate(R.layout.login_form, null);

        TextInputEditText email = loginForm.findViewById(R.id.etEmail);
        TextInputEditText password = loginForm.findViewById(R.id.etPass);
        TextView sideNote = loginForm.findViewById(R.id.tvSideNote);
        TextView captionHeader = loginForm.findViewById(R.id.tvCaptionHeader);

        captionHeader.setText("Save changes by entering your Login Credentials");

        sideNote.setText(String.format("%s\n\n%s\n\nPlease reconnect to database by logging in your credentials.", title, message));

        email.setEnabled(true);
        password.setEnabled(true);

        email.requestFocus();

        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle("Re-Authentication Form");
        dialogBuilder.setIcon(R.drawable.ic_info);
        dialogBuilder.setView(loginForm);

        dialogBuilder.setPositiveButton("SUBMIT", (dialog, which) -> presenter.submitLoginCredentials(Objects.requireNonNull(email.getText()).toString(), Objects.requireNonNull(password.getText()).toString())).setNegativeButton("LOG OUT", (dialog, which) -> presenter.userLogOut()).create();

        dialogBuilder.show();
    }
}

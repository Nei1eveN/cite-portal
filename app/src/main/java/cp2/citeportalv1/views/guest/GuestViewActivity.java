package cp2.citeportalv1.views.guest;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.views.guest.about.GuestAboutDepartmentFragment;

import static cp2.citeportalv1.utils.Constants.showExitDialog;

public class GuestViewActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener,
        ViewPager.OnPageChangeListener{

    @BindView(R.id.botGuestNav) BottomNavigationView bottomNavigationView;
    @BindView(R.id.viewPagerGuest) ViewPager viewPager;

    ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
    MenuItem prevMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_view);
        ButterKnife.bind(this);

        bottomNavigationView.setOnNavigationItemSelectedListener(this);
//        viewPagerAdapter.addFragment(new GuestAnnouncementFragment(), "CITE Department Announcements for Guests");
//        viewPagerAdapter.addFragment(new GuestUpcomingEventsFragment(), "CITE Upcoming Events / Competitions / Org. Related");
//        viewPagerAdapter.addFragment(new GuestAboutDepartmentFragment(), "About CITE Department Wholesome");
        viewPagerAdapter.addFragment(new GuestLoginFragment(), "Login for Student or Faculty");
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
//            case R.id.guest_announce:
//                viewPager.setCurrentItem(0);
//                return true;
//            case R.id.guest_events:
//                viewPager.setCurrentItem(1);
//                return true;
//            case R.id.guest_about:
//                viewPager.setCurrentItem(2);
//                return true;
//            case R.id.guest_login:
//                viewPager.setCurrentItem(3);
//                return true;
            case R.id.guest_login:
                viewPager.setCurrentItem(0);
                return true;
        }
        return false;
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int position) {
        if (prevMenuItem != null){
            prevMenuItem.setChecked(false);
        }
        else {
            bottomNavigationView.getMenu().getItem(0).setChecked(false);
        }
        bottomNavigationView.getMenu().getItem(position).setChecked(true);
        prevMenuItem = bottomNavigationView.getMenu().getItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onBackPressed() {
        showExitDialog(this);
    }
}

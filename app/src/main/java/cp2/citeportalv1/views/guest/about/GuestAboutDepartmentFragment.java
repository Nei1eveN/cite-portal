package cp2.citeportalv1.views.guest.about;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cp2.citeportalv1.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class GuestAboutDepartmentFragment extends Fragment {


    public GuestAboutDepartmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_guest_about_department, container, false);
    }

}

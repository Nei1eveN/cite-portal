package cp2.citeportalv1.views.guest;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.views.faculty.FacultyLoginActivity;
import cp2.citeportalv1.views.student.login_register.StudentLoginActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class GuestLoginFragment extends Fragment {

    private Unbinder unbinder;

    public GuestLoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_guest_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.ivStudent)
    void goToStudentLogin(){
        Objects.requireNonNull(getActivity()).startActivity(new Intent(getActivity(), StudentLoginActivity.class));
        getActivity().finish();
    }

    @OnClick(R.id.ivInstructor)
    void goToFacultyLogin(){
        Objects.requireNonNull(getActivity()).startActivity(new Intent(getActivity(), FacultyLoginActivity.class));
        getActivity().finish();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

package cp2.citeportalv1.views.guest.about;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.InstructorGroupAdapter;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.presenters.consult.student.loadpages.infotech.InfoTechInstructorListPresenter;
import cp2.citeportalv1.presenters.consult.student.loadpages.infotech.InfoTechInstructorListPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class InformationTechnologyInstructorsFragment extends Fragment implements InfoTechInstructorListPresenter.InfoTechView, SwipeRefreshLayout.OnRefreshListener {

    private Unbinder unbinder;
    @BindView(R.id.infoTechListCoordinator)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.ivInstructor)
    ImageView facultyImageEmptyState;

    @BindView(R.id.tvInstructor)
    TextView facultyCaptionEmptyState;

    @BindView(R.id._dynamicITFaculty)
    RecyclerView facultyRecyclerView;

    @BindView(R.id.infoTechSwipe) SwipeRefreshLayout swipeRefreshLayout;

    private InfoTechInstructorListPresenter instructorListPresenter;

    public InformationTechnologyInstructorsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_info_tech_instructors, container, false);
        unbinder = ButterKnife.bind(this, view);
        instructorListPresenter = new InfoTechInstructorListPresenterImpl(this, getActivity());
        swipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        instructorListPresenter.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        instructorListPresenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setEmptyState(String emptyStateMessage) {
        facultyRecyclerView.setVisibility(View.GONE);
        facultyImageEmptyState.setVisibility(View.VISIBLE);
        facultyCaptionEmptyState.setVisibility(View.VISIBLE);
        facultyCaptionEmptyState.setText(emptyStateMessage);
    }

    @Override
    public void setFacultyEmployees(List<ListItem> facultyEmployees) {
        facultyImageEmptyState.setVisibility(View.GONE);
        facultyCaptionEmptyState.setVisibility(View.GONE);
        facultyRecyclerView.setVisibility(View.VISIBLE);
        InstructorGroupAdapter adapter = new InstructorGroupAdapter(getActivity(), facultyEmployees);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return adapter.isHeader(position) ? gridLayoutManager.getSpanCount() : 1;
            }
        });
        facultyRecyclerView.setHasFixedSize(true);
        facultyRecyclerView.setNestedScrollingEnabled(false);
        facultyRecyclerView.setLayoutManager(gridLayoutManager);
        facultyRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        instructorListPresenter.requestFacultyEmployees();
    }
}

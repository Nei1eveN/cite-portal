package cp2.citeportalv1.views.guest;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.citeportalv1.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class GuestAnnouncementFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.rvAnnouncements) protected RecyclerView recyclerViewGuestAnnouncements;
    @BindView(R.id.ivAnnouncements) protected ImageView imageViewGuestAnnouncements;
    @BindView(R.id.tvAnnouncements) protected TextView textViewGuestAnnouncements;

    public GuestAnnouncementFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_guest_announcement, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

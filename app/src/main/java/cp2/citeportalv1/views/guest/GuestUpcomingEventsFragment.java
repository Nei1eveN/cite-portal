package cp2.citeportalv1.views.guest;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cp2.citeportalv1.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class GuestUpcomingEventsFragment extends Fragment {


    public GuestUpcomingEventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_guest_upcoming_events, container, false);
    }

}

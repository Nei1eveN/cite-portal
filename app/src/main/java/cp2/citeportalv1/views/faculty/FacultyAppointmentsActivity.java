package cp2.citeportalv1.views.faculty;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.base.FacultyBaseActivity;
import cp2.citeportalv1.presenters.consult.faculty.appointment.appointment_pages.FacultyAppointmentPagePresenter;
import cp2.citeportalv1.presenters.consult.faculty.appointment.appointment_pages.FacultyAppointmentPagePresenterImpl;

public class FacultyAppointmentsActivity extends FacultyBaseActivity implements FacultyAppointmentPagePresenter.View {

    TabLayout tabLayout;
    ViewPager viewPager;

    FacultyAppointmentPagePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_appointments);

        tabLayout = findViewById(R.id.tabDays);
        viewPager = findViewById(R.id.vpDay);

        toolbar.setTitle("Appointments");
        navigationView.setCheckedItem(R.id.appointments);
        presenter = new FacultyAppointmentPagePresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void loadDayPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(numberOfPages);
        viewPager.setCurrentItem(0, false);
    }
}

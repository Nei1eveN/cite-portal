package cp2.citeportalv1.views.faculty;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.ConsultationRequestsGroupAdapter;
import cp2.citeportalv1.base.FacultyBaseActivity;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.presenters.consult.faculty.request.ConsultRequestPresenter;
import cp2.citeportalv1.presenters.consult.faculty.request.ConsultRequestPresenterImpl;

public class FacultyConsultationRequestsLogActivity extends FacultyBaseActivity implements ConsultRequestPresenter.View {

    CoordinatorLayout coordinatorLayout;
    SwipeRefreshLayout swipeRefreshLayout;

    RecyclerView recyclerView;
    ImageView emptyNotificationsImage;
    TextView emptyCaptionText;

    ConsultRequestPresenter consultRequestPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_consultation_requests_log);
        toolbar.setTitle("Consultation Requests");
        navigationView.setCheckedItem(R.id.consultation_requests);

        consultRequestPresenter = new ConsultRequestPresenterImpl(this, this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        consultRequestPresenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        consultRequestPresenter.onDestroy();
    }

    @Override
    public void initViews() {
        coordinatorLayout = findViewById(R.id.requestCoor);
        swipeRefreshLayout = findViewById(R.id.swipeCoor);
        recyclerView = findViewById(R.id.recyclerView);
        emptyNotificationsImage = findViewById(R.id.ivEmptyRequestsState);
        emptyCaptionText = findViewById(R.id.tvEmptyRequestsState);

        swipeRefreshLayout.setOnRefreshListener(() -> consultRequestPresenter.requestNotificationList());
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void hideViewsState() {
        recyclerView.setVisibility(View.GONE);
        emptyNotificationsImage.setVisibility(View.GONE);
        emptyCaptionText.setVisibility(View.GONE);
    }

    @Override
    public void setEmptyState(String message) {
        recyclerView.setVisibility(View.GONE);
        emptyNotificationsImage.setVisibility(View.VISIBLE);
        emptyCaptionText.setVisibility(View.VISIBLE);
        emptyCaptionText.setText(message);
    }

    @Override
    public void setNotifications(List<ListItem> notificationsList) {
        emptyNotificationsImage.setVisibility(View.GONE);
        emptyCaptionText.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        ConsultationRequestsGroupAdapter groupAdapter = new ConsultationRequestsGroupAdapter(notificationsList, this);
        groupAdapter.notifyDataSetChanged();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(groupAdapter);
    }
}

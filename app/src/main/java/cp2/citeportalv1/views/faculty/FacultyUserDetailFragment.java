package cp2.citeportalv1.views.faculty;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.presenters.profile.faculty.detail.FacultyDetailPresenter;
import cp2.citeportalv1.presenters.profile.faculty.detail.FacultyDetailPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class FacultyUserDetailFragment extends Fragment implements FacultyDetailPresenter.View, SwipeRefreshLayout.OnRefreshListener, FloatingActionButton.OnClickListener {

    private Unbinder unbinder;

    @BindView(R.id.facultyDetailCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.facultyDetailSwipe) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.facName) TextView facultyName;
    @BindView(R.id.facContactNumber) TextView contactNumber;
    @BindView(R.id.tvFacEmail) TextView email;

    @BindView(R.id.tvUserSignatureHeader) TextView userSignatureHeader;
    @BindView(R.id.ivUserSignature) ImageView userSignatureImage;
    @BindView(R.id.tvEmptyUserSignature) TextView emptyUserSignatureText;

    @BindView(R.id.tvFacDept) TextView department;
    @BindView(R.id.tvFacEmpId) TextView employeeId;
    @BindView(R.id.fabProfileEdit) FloatingActionButton editProfile;

    private FacultyDetailPresenter presenter;

    private boolean hasLoadedOnce = false;

    public FacultyUserDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new FacultyDetailPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_faculty_user_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(this);
        editProfile.setOnClickListener(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser && !hasLoadedOnce) {
                presenter.requestUserInfo();
                hasLoadedOnce = true;
            }
        }
    }

    @OnClick(R.id.ivUserSignature)
    void goToStudentSignaturePage() {
        Objects.requireNonNull(getActivity()).startActivity(new Intent(getActivity(), FacultySignatureActivity.class));
    }

    @OnClick(R.id.tvEmptyUserSignature)
    void createSignaturePage() {
        Objects.requireNonNull(getActivity()).startActivity(new Intent(getActivity(), FacultyUserEditSignatureActivity.class));
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setUserInfo(Faculty faculty) {
        facultyName.setText(String.format("%s, %s %s", faculty.getLastName(), faculty.getfName(), faculty.getMidName()));
        contactNumber.setText(faculty.getContactNumber());
        email.setText(faculty.getEmail());
        department.setText(faculty.getDepartment());
        employeeId.setText(faculty.getEmployeeID());
    }

    @Override
    public void setUserSignature(String userSignature) {
        emptyUserSignatureText.setVisibility(View.GONE);

        userSignatureImage.setVisibility(View.VISIBLE);
        userSignatureHeader.setVisibility(View.VISIBLE);

        Glide.with(Objects.requireNonNull(getActivity())).load(userSignature).apply(new RequestOptions().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(userSignatureImage);
    }

    @Override
    public void setNoSignatureFound(String message) {
        userSignatureImage.setVisibility(View.GONE);
        userSignatureHeader.setVisibility(View.GONE);

        emptyUserSignatureText.setVisibility(View.VISIBLE);

        emptyUserSignatureText.setText(message);
    }

    @Override
    public void setExitDialog(String title, String message) {
        AlertDialog.Builder exitBuilder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        exitBuilder.setCancelable(false);
        exitBuilder.setTitle(title);
        exitBuilder.setMessage(message);
        exitBuilder.setPositiveButton("EXIT", (dialog, which) -> getActivity().finish());

        AlertDialog exitDialog = exitBuilder.create();
        exitDialog.show();
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        presenter.requestUserInfo();
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(getActivity(), FacultyEditProfileActivity.class));
    }
}

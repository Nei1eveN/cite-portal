package cp2.citeportalv1.views.faculty;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.ConsultationRequestsGroupAdapter;
import cp2.citeportalv1.presenters.consult.faculty.request.consultation_request.ConsultationRequestPresenter;
import cp2.citeportalv1.presenters.consult.faculty.request.consultation_request.ConsultationRequestPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class FacultyConsultationRequestLogFragment extends Fragment implements ConsultationRequestPresenter.View, SwipeRefreshLayout.OnRefreshListener {

    private Unbinder unbinder;

    @BindView(R.id.coorPending) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.swipePending) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ivEmptyPending) ImageView emptyPendingImage;
    @BindView(R.id.tvEmptyPending) TextView emptyPendingText;
    @BindView(R.id.rvPending) RecyclerView recyclerView;

    private ConsultationRequestPresenter presenter;

    private boolean hasLoadedOnce = false;

    public FacultyConsultationRequestLogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ConsultationRequestPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_faculty_consultation_request_log, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.requestPendingList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser && !hasLoadedOnce) {
                presenter.requestPendingList();
                hasLoadedOnce = true;
            }
        }
    }

    @Override
    public void onRefresh() {
        presenter.requestPendingList();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void loadPendingResponses(ConsultationRequestsGroupAdapter adapter) {
        emptyPendingImage.setVisibility(View.GONE);
        emptyPendingText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyState(String emptyMessage) {
        recyclerView.setVisibility(View.GONE);

        emptyPendingImage.setVisibility(View.VISIBLE);
        emptyPendingText.setVisibility(View.VISIBLE);

        emptyPendingText.setText(emptyMessage);
    }
}

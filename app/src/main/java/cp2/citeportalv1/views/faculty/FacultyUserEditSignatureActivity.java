package cp2.citeportalv1.views.faculty;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.presenters.profile.faculty.signature.edit_signature.FacultyUserEditSignaturePresenter;
import cp2.citeportalv1.presenters.profile.faculty.signature.edit_signature.FacultyUserEditSignaturePresenterImpl;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

public class FacultyUserEditSignatureActivity extends AppCompatActivity implements FacultyUserEditSignaturePresenter.View {

    @BindView(R.id.coorUserSignEdit)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.signature_pad) SignaturePad signaturePad;
    @BindView(R.id.ivUserSignature) ImageView userSignatureImage;

    ProgressDialog progressDialog;

    FacultyUserEditSignaturePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_user_edit_signature);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        presenter = new FacultyUserEditSignaturePresenterImpl(this, this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Edit Signature");

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.signature_edit_menu, menu);
        return true;
    }

    @SuppressLint("CheckResult")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
            case R.id.save_signature:
                presenter.submitUserSignature(signaturePad);
                break;
            case R.id.clear_canvas:
                signaturePad.clear();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void initViews() {
        userSignatureImage.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("Exit", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    @Override
    public void showExitMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("Exit", (dialog, which) -> {
            dialog.dismiss();
            finish();
        });
        builder.create().show();
    }
}

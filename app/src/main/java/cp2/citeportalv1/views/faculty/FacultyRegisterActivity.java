package cp2.citeportalv1.views.faculty;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.presenters.register.faculty.FacRegPresenter;
import cp2.citeportalv1.presenters.register.faculty.FacRegPresenterImpl;
import cp2.citeportalv1.utils.FileUtil;
import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cp2.citeportalv1.utils.Constants.PICK_IMAGE_REQUEST;
import static cp2.citeportalv1.utils.FileUtil.fullyReadFileToBytes;
import static cp2.citeportalv1.utils.FileUtil.getFileName;
import static cp2.citeportalv1.utils.FileUtil.getReadableFileSize;

public class FacultyRegisterActivity extends AppCompatActivity implements FacRegPresenter.FacultyRegisterView {

    @BindView(R.id.facRegCoorLayout) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.facImgProfile) ImageButton profileImage;
    @BindView(R.id.tvFacPic) TextView profileImageText;

    @BindView(R.id.txFacId) TextInputLayout employeeIdWrapper;
    @BindView(R.id.txFacFName) TextInputLayout firstNameWrapper;
    @BindView(R.id.txFacMidName) TextInputLayout middleNameWrapper;
    @BindView(R.id.txFacLName) TextInputLayout lastNameWrapper;
    @BindView(R.id.txFacEmail) TextInputLayout emailWrapper;
    @BindView(R.id.txFacPassword) TextInputLayout passwordWrapper;

    @BindView(R.id.etFacId) TextInputEditText employeeId;
    @BindView(R.id.etFacFName) TextInputEditText firstName;
    @BindView(R.id.etFacMidName) TextInputEditText middleName;
    @BindView(R.id.etFacLName) TextInputEditText lastName;
    @BindView(R.id.etFacEmail) TextInputEditText email;
    @BindView(R.id.etFacPassword) TextInputEditText facPassword;

    @BindView(R.id.spinDepartment) Spinner spinnerDepartment;
    ArrayAdapter<CharSequence> deptAdapter;

    ProgressDialog progressDialog;

    protected Uri mainImageURI = null;
    protected File actualImage;
    protected File compressedImage;

    FacRegPresenter facRegPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fac_register);
        ButterKnife.bind(this);

        facRegPresenter = new FacRegPresenterImpl(this, this);
        progressDialog = new ProgressDialog(FacultyRegisterActivity.this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Register as Instructor");

        employeeId.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, FacultyLoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, FacultyLoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @OnClick(R.id.facImgProfile)
    protected void selectImage(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @OnClick(R.id.tvFacPic)
    protected void selectImageFromTextView(){
        selectImage();
    }

    @OnClick(R.id.btnRegFacultySubmit)
    protected void setSubmitFacultyCredentials(){
        if (mainImageURI != null){
            try {
                facRegPresenter.submitCredentials(fullyReadFileToBytes(compressedImage), mainImageURI,
                        Objects.requireNonNull(employeeId.getText()).toString(),
                        Objects.requireNonNull(firstName.getText()).toString(),
                        Objects.requireNonNull(middleName.getText()).toString(),
                        Objects.requireNonNull(lastName.getText()).toString(),
                        Objects.requireNonNull(email.getText()).toString(),
                        Objects.requireNonNull(facPassword.getText()).toString(),
                        spinnerDepartment.getSelectedItem().toString(), employeeIdWrapper, firstNameWrapper, middleNameWrapper, lastNameWrapper, emailWrapper, passwordWrapper);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            Snackbar.make(coordinatorLayout, "Select an image", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        facRegPresenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        facRegPresenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void snackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void initSpinnerItems() {
        deptAdapter = ArrayAdapter.createFromResource(this, R.array.deptList, android.R.layout.simple_spinner_item);
        deptAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDepartment.setAdapter(deptAdapter);
    }

    @Override
    public void errorWrapper(TextInputLayout textInputLayout, String message) {
        textInputLayout.setError(message);
    }

    @Override
    public void nullWrapper(TextInputLayout textInputLayout) {
        textInputLayout.setError(null);
    }

    @Override
    public void showAlertDialog(String title, String message) {
        new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss()).create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data == null) {
                Toast.makeText(this, "Failed to open picture!", Toast.LENGTH_SHORT).show();
                return;
            }
            try {
                mainImageURI = data.getData();
                actualImage = FileUtil.from(this, data.getData());
                compressImage(profileImage);
                profileImageText.setVisibility(View.GONE);
            } catch (IOException e) {
                snackMessage("Failed to read picture data!");
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("CheckResult")
    public void compressImage(View view) {
        if (actualImage == null) {
            Toast.makeText(this, "Please choose an image!", Toast.LENGTH_SHORT).show();
        } else {
            new Compressor(this)
                    .compressToFileAsFlowable(actualImage)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(file -> {
                        compressedImage = file;
                        setCompressedImage();
                    }, Throwable::printStackTrace);
        }
    }

    public void setCompressedImage() {
        profileImage.setImageBitmap(BitmapFactory.decodeFile(compressedImage.getAbsolutePath()));
        Log.d("URI String", getFileName(this, mainImageURI) + "\n Size: " + getReadableFileSize(compressedImage.length()));
    }
}

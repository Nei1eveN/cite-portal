package cp2.citeportalv1.views.faculty;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.ScheduleGroupAdapter;
import cp2.citeportalv1.presenters.consult.faculty.schedule.SchedulePresenter;
import cp2.citeportalv1.presenters.consult.faculty.schedule.SchedulePresenterImpl;

import static cp2.citeportalv1.utils.Constants.SCHEDULE_DAY;
import static cp2.citeportalv1.utils.Constants.calendar;
import static cp2.citeportalv1.utils.Constants.fixedFormat;
import static cp2.citeportalv1.utils.Constants.generateTimepoints;
import static cp2.citeportalv1.utils.Constants.hourFormat;
import static cp2.citeportalv1.utils.Constants.minuteFormat;
import static cp2.citeportalv1.utils.Constants.timeFormat12hr;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConsultationScheduleFragment extends Fragment implements SchedulePresenter.View { //implements SchedulePresenter.View

    private Unbinder unbinder;
    @BindView(R.id.schedCoor)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.ivEmptySchedule)
    ImageView emptySchedImage;
    @BindView(R.id.tvEmptySchedule)
    TextView emptySchedText;
    @BindView(R.id.rvScheds)
    RecyclerView recyclerView;
    @BindView(R.id.fabAddSched)
    FloatingActionButton floatingActionButton;

    private ProgressDialog progressDialog;

    private SchedulePresenter presenter;

    private AlertDialog dialogSched;

    private Spinner days;
    private TextView startTime, endTime;
    private Spinner venue;

    private Date selectedStartTime;

    private String toPassStartTime = null, toPassEndTime = null;

    public ConsultationScheduleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getActivity());
        presenter = new SchedulePresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_consultation_schedule, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        unbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle bundle = this.getArguments();
        String day = Objects.requireNonNull(bundle).getString(SCHEDULE_DAY);
        presenter.requestScheduleAccordingToDay(day);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();

        Bundle bundle = this.getArguments();
        String day = Objects.requireNonNull(bundle).getString(SCHEDULE_DAY);
        presenter.requestScheduleAccordingToDay(day);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading Schedules. Please wait...");
        progressDialog.show();

        floatingActionButton.hide();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setSchedules(ScheduleGroupAdapter adapter) {
        emptySchedImage.setVisibility(View.INVISIBLE);
        emptySchedText.setVisibility(View.INVISIBLE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(animation);

        floatingActionButton.show();
    }

    @Override
    public void setEmptySchedule(String emptyMessage) {
        floatingActionButton.hide();
        recyclerView.setVisibility(View.INVISIBLE);

        emptySchedImage.setVisibility(View.VISIBLE);
        emptySchedText.setVisibility(View.VISIBLE);

        emptySchedText.setText(emptyMessage);
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @Override
    public void setUpCreateScheduleDialog() {
        LayoutInflater layoutInflaterSched = LayoutInflater.from(getActivity());
        View promptViewSched = layoutInflaterSched.inflate(R.layout.faculty_create_schedule, null);

        days = promptViewSched.findViewById(R.id.spinnerDays);
        startTime = promptViewSched.findViewById(R.id.tvStartingTime);
        endTime = promptViewSched.findViewById(R.id.tvEndingTime);
        venue = promptViewSched.findViewById(R.id.etVenue);
        Button timeStart = promptViewSched.findViewById(R.id.btnTimeStart);
        Button timeEnd = promptViewSched.findViewById(R.id.btnTimeEnd);

        startTime.setText("7:30 AM");
        endTime.setText("8:00 AM");

        ArrayAdapter<CharSequence> venueAdapter = ArrayAdapter.createFromResource(Objects.requireNonNull(getActivity()), R.array.venue, android.R.layout.simple_spinner_item);
        venueAdapter.setDropDownViewResource(android.R.layout.select_dialog_item);
        venue.setAdapter(venueAdapter);
        venue.setPrompt("Consultation Venue");

        timeStart.setOnClickListener(v -> {
            TimePickerDialog.OnTimeSetListener setListener = (view, hourOfDay, minute, second) -> {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                calendar.set(Calendar.SECOND, second);

                startTime.setText(timeFormat12hr.format(calendar.getTime()));
                Log.d("getTimeFromStart", fixedFormat.format(calendar.getTime()));
                toPassStartTime = startTime.getText().toString(); //fixedFormat.format(calendar.getTime())
                Log.d("passStartTime", toPassStartTime);
                try {
                    selectedStartTime = timeFormat12hr.parse(toPassStartTime);
                    Log.d("selectedStartTime", String.valueOf(selectedStartTime.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            };

            TimePickerDialog pickerDialog = TimePickerDialog.newInstance(setListener, 7, 30, false);
            pickerDialog.setSelectableTimes(generateTimepoints(15));
            pickerDialog.setMinTime(7, 30, 0);
            pickerDialog.show(Objects.requireNonNull(getFragmentManager()), "TimePickerDialogStartTime");
        });

        timeEnd.setOnClickListener(v -> {
            TimePickerDialog.OnTimeSetListener setListener = (view, hourOfDay, minute, second) -> {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                calendar.set(Calendar.SECOND, second);

                endTime.setText(timeFormat12hr.format(calendar.getTime()));
                Log.d("getTimeFromEnd", fixedFormat.format(calendar.getTime()));
                toPassEndTime = endTime.getText().toString(); //fixedFormat.format(calendar.getTime())
                Log.d("passEndTime", toPassEndTime);
            };

            if (selectedStartTime == null) {
                AlertDialog.Builder emptyStartTimeBuilder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
                emptyStartTimeBuilder.setTitle("No Starting Time");
                emptyStartTimeBuilder.setMessage("Please select a starting time.");
                emptyStartTimeBuilder.setPositiveButton("GOT IT", (dialog, which) -> dialog.dismiss());

                androidx.appcompat.app.AlertDialog emptyStartTimeDialog = emptyStartTimeBuilder.create();
                emptyStartTimeDialog.show();
            } else {
                TimePickerDialog pickerDialog = TimePickerDialog.newInstance(setListener, Integer.valueOf(hourFormat.format(selectedStartTime)), Integer.valueOf(minuteFormat.format(selectedStartTime)), false);
                pickerDialog.setSelectableTimes(generateTimepoints(15));
                pickerDialog.setMinTime(Integer.valueOf(hourFormat.format(selectedStartTime)), Integer.valueOf(minuteFormat.format(selectedStartTime)), 0);
                pickerDialog.show(Objects.requireNonNull(getFragmentManager()), "TimePickerDialogStartTime");
            }
        });

        AlertDialog.Builder builderSched = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        builderSched.setView(promptViewSched);

        builderSched.setCancelable(false)
                .setIcon(R.drawable.ic_insert_invitation)
                .setTitle("Create a Schedule")
                .setPositiveButton("Submit",
                        (DialogInterface dialog, int which) ->
                        {
                            if (toPassStartTime != null && toPassEndTime != null) {
                                //startTime.getText().toString()
                                //endTime.getText().toString()
                                presenter.submitCreatingSchedule(
                                        days.getSelectedItem().toString(), toPassStartTime, toPassEndTime,
                                        venue.getSelectedItem().toString());
                            }
                            dialog.dismiss();

                        })
                .setNegativeButton("Cancel", null);

        dialogSched = builderSched.create();
    }

    @Override
    public void setUpConflictScheduleDialog(String conflictMessage) {
        AlertDialog.Builder conflictBuilderSched = new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setCancelable(false)
                .setIcon(R.drawable.ic_insert_invitation)
                .setTitle("Schedule Conflict")
                .setMessage(conflictMessage)
                .setPositiveButton("OK", null);

        conflictBuilderSched.create().show();
    }

    @OnClick(R.id.ivEmptySchedule)
    void createSchedule() {
        dialogSched.show();
    }

    @OnClick(R.id.fabAddSched)
    void addSchedule() {
        dialogSched.show();
    }
}

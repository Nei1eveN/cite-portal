package cp2.citeportalv1.views.faculty;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.presenters.login.faculty.FacultyLoginPresenter;
import cp2.citeportalv1.presenters.login.faculty.FacultyLoginPresenterImpl;
import cp2.citeportalv1.views.guest.GuestViewActivity;
import cp2.citeportalv1.views.student.login_register.StudentLoginActivity;

import static cp2.citeportalv1.utils.Constants.showExitDialog;

public class FacultyLoginActivity extends AppCompatActivity implements FacultyLoginPresenter.View {
    @BindView(R.id.facUserLayout) TextInputLayout facUserIDWrapper;
    @BindView(R.id.facPassLayout) TextInputLayout facPassWrapper;

    @BindView(R.id.facTxUserId) TextInputEditText userID;
    @BindView(R.id.facTxPass) TextInputEditText password;

    @BindView(R.id.btnFacLogin) Button facLogin;
    @BindView(R.id.btnFacReg) Button facReg;
    @BindView(R.id.tvFacForgot) TextView facForgot;
    @BindView(R.id.tvVerifyEmail) TextView verifyEmail;

    ProgressDialog progressDialog;

    FacultyLoginPresenter facultyLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_login);
        ButterKnife.bind(this);

        facultyLoginPresenter = new FacultyLoginPresenterImpl(this, this);
        progressDialog = new ProgressDialog(FacultyLoginActivity.this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
    }

    @OnClick(R.id.tvFacForgot)
    void sendPasswordLink() {
        LayoutInflater loginFormInflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View loginForm = loginFormInflater.inflate(R.layout.layout_send_email, null);

        TextInputEditText email = loginForm.findViewById(R.id.etEmail);

        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle("Send Email For Password Reset");
        dialogBuilder.setIcon(R.drawable.ic_info);
        dialogBuilder.setView(loginForm);

        dialogBuilder.setPositiveButton("SUBMIT", (dialog, which) -> {
            dialog.dismiss();
            facultyLoginPresenter.sendPasswordResetLink(Objects.requireNonNull(email.getText()).toString());
        }).setNegativeButton("CANCEL", null).create().show();
    }

    @OnClick(R.id.tvVerifyEmail)
    void sendEmailVerificationLink() {
        LayoutInflater loginFormInflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View loginForm = loginFormInflater.inflate(R.layout.layout_send_email, null);

        TextInputEditText email = loginForm.findViewById(R.id.etEmail);

        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle("Send Email For Verification");
        dialogBuilder.setIcon(R.drawable.ic_info);
        dialogBuilder.setView(loginForm);

        dialogBuilder.setPositiveButton("SUBMIT", (dialog, which) -> {
            dialog.dismiss();
            facultyLoginPresenter.sendEmailVerificationLink(Objects.requireNonNull(email.getText()).toString());
        }).setNegativeButton("CANCEL", null).create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, GuestViewActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        facultyLoginPresenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        facultyLoginPresenter.onDestroy();
        progressDialog.dismiss();
    }

    @OnClick(R.id.btnLogAsStud)
    protected void goToStudentLogin() {
        startActivity(new Intent(this, StudentLoginActivity.class));
        finish();
    }

    @OnClick(R.id.btnFacLogin)
    protected void submitCredentials(){
        facultyLoginPresenter.requestLogin(userID.getText().toString(), password.getText().toString());
    }

    @OnClick(R.id.btnFacReg)
    protected void goToRegisterFaculty(){
        startActivity(new Intent(this, FacultyRegisterActivity.class));
        finish();
        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
    }


    @Override
    public void showProgress() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setEmptyUserId() {
        facUserIDWrapper.setError("Required field");
    }

    @Override
    public void setEmptyPass() {
        facPassWrapper.setError("Required field");
    }

    @Override
    public void setNullWrapper() {
        if (!userID.getText().toString().isEmpty()) {
            facUserIDWrapper.setError(null);
        }
        else if (!password.getText().toString().isEmpty()) {
            facPassWrapper.setError(null);
        }
    }

    @Override
    public void showAlertDialog(String title, String message) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message).setCancelable(false).setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss()).create().show();
    }

    @Override
    public void onBackPressed() {
        showExitDialog(this);
    }
}

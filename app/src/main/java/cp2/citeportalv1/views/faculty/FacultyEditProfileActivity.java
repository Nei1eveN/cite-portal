package cp2.citeportalv1.views.faculty;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.presenters.profile.faculty.editprofile.FacultyEditPresenter;
import cp2.citeportalv1.presenters.profile.faculty.editprofile.FacultyEditPresenterImpl;
import cp2.citeportalv1.utils.FileUtil;
import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cp2.citeportalv1.utils.Constants.PICK_IMAGE_REQUEST;
import static cp2.citeportalv1.utils.FileUtil.fullyReadFileToBytes;
import static cp2.citeportalv1.utils.FileUtil.getFileName;
import static cp2.citeportalv1.utils.FileUtil.getReadableFileSize;

public class FacultyEditProfileActivity extends AppCompatActivity implements FacultyEditPresenter.View {

    @BindView(R.id.facEditCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.fabSelectImage) FloatingActionButton selectImage;
    @BindView(R.id.ivEtFacProfileImage) ImageView currentUserProfileImage;
    @BindView(R.id.facTvEmail) TextView currentUserEmail;
    @BindView(R.id.etFirstName) EditText currentUserFirstName;
    @BindView(R.id.etMiddleName) EditText currentUserMiddleName;
    @BindView(R.id.etLastName) EditText currentUserLastName;
    @BindView(R.id.etContactNumber) EditText currentUserContactNumber;
    @BindView(R.id.etFacEmployeeId) EditText currentUserEmployeeId;
    @BindView(R.id.spFacDept) Spinner spinnerDepartment;

    /***
     * CONSTRAINT LAYOUTS
     * **/
    @BindView(R.id.constraintEmail)
    ConstraintLayout constraintEmail;

    FacultyEditPresenter presenter;
    ProgressDialog progressDialog;

    protected Uri mainImageURI = null;
    protected File actualImage;
    protected File compressedImage;

    private String userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_edit_profile);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new FacultyEditPresenterImpl(this, this);
        progressDialog = new ProgressDialog(this);

        currentUserEmployeeId.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data == null) {
                Snackbar.make(coordinatorLayout, "Failed to open picture", Snackbar.LENGTH_SHORT).show();
                return;
            }
            try {
                mainImageURI = data.getData();
                actualImage = FileUtil.from(this, data.getData());
                compressImage(currentUserProfileImage);
            } catch (IOException e) {
                Snackbar.make(coordinatorLayout, "Failed to read picture data", Snackbar.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("CheckResult")
    public void compressImage(View view) {
        if (actualImage == null) {
            Snackbar.make(coordinatorLayout, "Please select an image.", Snackbar.LENGTH_SHORT).show();
        } else {
            new Compressor(this)
                    .compressToFileAsFlowable(actualImage)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(file -> {
                        compressedImage = file;
                        setCompressedImage();
                    }, Throwable::printStackTrace);


        }
    }

    public void setCompressedImage() {
        currentUserProfileImage.setImageBitmap(BitmapFactory.decodeFile(compressedImage.getAbsolutePath()));
        Log.d("URI String", getFileName(this, mainImageURI) + "\n Size: " + getReadableFileSize(compressedImage.length()));
    }

    @OnClick(R.id.fabSelectImage)
    protected void selectImage(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_changes_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.saveChanges:
                try {
                    if (mainImageURI != null) {
                        presenter.submitCredentials(fullyReadFileToBytes(compressedImage), mainImageURI, currentUserEmail.getText().toString(), currentUserFirstName.getText().toString(), currentUserMiddleName.getText().toString(), currentUserLastName.getText().toString(), currentUserContactNumber.getText().toString(), currentUserEmployeeId.getText().toString(), spinnerDepartment.getSelectedItem().toString());
                    } else {
                        presenter.submitCredentialsWithoutFiles(currentUserEmail.getText().toString(), currentUserFirstName.getText().toString(), currentUserMiddleName.getText().toString(), currentUserLastName.getText().toString(), currentUserContactNumber.getText().toString(), currentUserEmployeeId.getText().toString(), spinnerDepartment.getSelectedItem().toString());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showDialog(String title, String caption) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle(title).setMessage(caption).setPositiveButton("OK", (dialog, which) -> {
            startActivity(new Intent(this, FacultyProfileActivity.class));
            finish();
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void showErrorDialog(String errorTitle, String errorCaption) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setIcon(R.drawable.ic_error_red_24dp);
        dialogBuilder.setTitle(errorTitle).setMessage(errorCaption).setPositiveButton("OK", (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void changedEmailDialog(String title, String message) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle(title).setMessage(message).setPositiveButton("OK", (dialog, which) -> presenter.signOutUser());

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void notChangedEmailDialog(String exitTitle, String exitMessage) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle(exitTitle).setMessage(exitMessage).setPositiveButton("OK", (dialog, which) -> {
            startActivity(new Intent(this, FacultyProfileActivity.class));
            finish();
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void showExitDialog(String title, String message) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle(title).setMessage(message).setPositiveButton("GO TO LOGIN PAGE", (dialog, which) -> {
            startActivity(new Intent(this, FacultyLoginActivity.class));
            finish();
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void setUserInfo(Faculty faculty) {
        Glide.with(this)
                .load(faculty.getFacultyImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px))
                .into(currentUserProfileImage);
        currentUserEmail.setText(faculty.getEmail());
        currentUserFirstName.setText(faculty.getfName());
        currentUserMiddleName.setText(faculty.getMidName());
        currentUserLastName.setText(faculty.getLastName());
        currentUserContactNumber.setText(faculty.getContactNumber());
        currentUserEmployeeId.setText(faculty.getEmployeeID());

        userEmail = faculty.getEmail();
    }

    @Override
    public void setSpinnerItems(List<String> spinnerItems) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDepartment.setAdapter(adapter);
        spinnerDepartment.setPrompt("Select Department");
    }

    @Override
    public void setUserReAuthenticate() {
        LayoutInflater loginFormInflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View loginForm = loginFormInflater.inflate(R.layout.login_form, null);

        TextInputEditText email = loginForm.findViewById(R.id.etEmail);
        TextInputEditText password = loginForm.findViewById(R.id.etPass);

        password.requestFocus();

        email.setText(userEmail);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle("Authentication Form");
        dialogBuilder.setIcon(R.drawable.ic_info);
        dialogBuilder.setView(loginForm);

        dialogBuilder.setPositiveButton("SUBMIT", (dialog, which) -> {
            try {
                if (mainImageURI != null) {
                    presenter.requestAuthentication
                            (Objects.requireNonNull(email.getText()).toString(), Objects.requireNonNull(password.getText()).toString(),
                                    fullyReadFileToBytes(compressedImage), mainImageURI, currentUserEmail.getText().toString(),
                                    currentUserFirstName.getText().toString(), currentUserMiddleName.getText().toString(),
                                    currentUserLastName.getText().toString(), currentUserContactNumber.getText().toString(),
                                    currentUserEmployeeId.getText().toString(), spinnerDepartment.getSelectedItem().toString());
                } else {
                    presenter.requestAuthentication
                            (Objects.requireNonNull(email.getText()).toString(), Objects.requireNonNull(password.getText()).toString(),
                                    null, null, currentUserEmail.getText().toString(),
                                    currentUserFirstName.getText().toString(), currentUserMiddleName.getText().toString(),
                                    currentUserLastName.getText().toString(), currentUserContactNumber.getText().toString(),
                                    currentUserEmployeeId.getText().toString(), spinnerDepartment.getSelectedItem().toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).setNegativeButton("CANCEL", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();

    }

    @OnClick(R.id.constraintEmail)
    protected void changeEmail(){
        LayoutInflater changedEmailInflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View changeEmailForm = changedEmailInflater.inflate(R.layout.layout_change_email, null);

        TextInputEditText currentEmail = changeEmailForm.findViewById(R.id.etCurrentEmail);
        TextInputEditText newEmail = changeEmailForm.findViewById(R.id.etNewEmail);

        newEmail.requestFocus();

        AlertDialog.Builder changeEmailDialogBuilder = new AlertDialog.Builder(this);
        changeEmailDialogBuilder.setCancelable(false);
        changeEmailDialogBuilder.setTitle("Change Email");
        changeEmailDialogBuilder.setIcon(R.drawable.ic_edit);
        changeEmailDialogBuilder.setView(changeEmailForm);

        currentEmail.setText(currentUserEmail.getText().toString());

        changeEmailDialogBuilder
                .setPositiveButton("CHANGE", (dialog, which)
                        -> {
                    if (Objects.requireNonNull(newEmail.getText()).toString().isEmpty()){
                        dialog.dismiss();
                    } else {
                        currentUserEmail.setText(newEmail.getText().toString());
                    }
                })
                .setNegativeButton("CANCEL", (dialog, which)
                        -> dialog.dismiss());

        AlertDialog dialog = changeEmailDialogBuilder.create();
        dialog.show();
    }
}

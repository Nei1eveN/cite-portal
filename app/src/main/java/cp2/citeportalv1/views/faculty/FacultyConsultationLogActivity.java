package cp2.citeportalv1.views.faculty;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.base.FacultyBaseActivity;
import cp2.citeportalv1.presenters.consult.faculty.load_pages.FacultyConsultPresenter;
import cp2.citeportalv1.presenters.consult.faculty.load_pages.FacultyConsultPresenterImpl;

public class FacultyConsultationLogActivity extends FacultyBaseActivity implements FacultyConsultPresenter.View {

    ViewPager viewPager;
    TabLayout tabLayout;

    Intent intent;
    Bundle data;

    FacultyConsultPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_consultation_log);

        intent = getIntent();
        data = intent.getExtras();

        toolbar.setTitle("Consultation Log");

        navigationView.setCheckedItem(R.id.consultation_history);

        viewPager = findViewById(R.id.vpConRequest);
        tabLayout = findViewById(R.id.tabConRequest);

        presenter = new FacultyConsultPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void initializeTabs(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(numberOfPages);

        if (data != null){
            int position = data.getInt("tabNumber");
            viewPager.setCurrentItem(position, false);
        }
    }
}

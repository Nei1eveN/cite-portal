package cp2.citeportalv1.views.faculty;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.presenters.consult.faculty.consultationdetails.ConsultationDetailsPresenter;
import cp2.citeportalv1.presenters.consult.faculty.consultationdetails.ConsultationDetailsPresenterImpl;

public class FacultyConsultationDetailsActivity extends AppCompatActivity implements ConsultationDetailsPresenter.View {

    @BindView(R.id.consultCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.ivSenderImage) ImageView senderImage;
    @BindView(R.id.tvSenderName) TextView senderName;
    @BindView(R.id.tvProgramYearLevel) TextView senderProgramYearLevel;
    @BindView(R.id.tvCourseDetail) TextView concernTitle;
    @BindView(R.id.tvConcernDetails) TextView concernDetails;
    @BindView(R.id.tvReqDay) TextView requestedDay;
    @BindView(R.id.tvReqDate) TextView requestedDate;
    @BindView(R.id.tvReqSchedule) TextView requestedSchedule;

    @BindView(R.id.btnApprove) MaterialButton btnApprove;
    @BindView(R.id.btnResched) MaterialButton btnResched;
    @BindView(R.id.btnIgnore) MaterialButton btnIgnore;

    Intent intent;
    AlertDialog alertDialogApprove, alertDialogReject;

    ProgressDialog progressDialog;

    ConsultationDetailsPresenter presenter;

    String notificationId, facultyUid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_consultation_details);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Consultation Details");

        presenter = new ConsultationDetailsPresenterImpl(this, this);
        progressDialog = new ProgressDialog(this);
        intent = getIntent();

        notificationId = intent.getStringExtra("notificationId");
    }

    @OnClick(R.id.btnApprove)
    protected void sendApprovedDetails() {
        alertDialogApprove.setTitle("Send Remarks");
        alertDialogApprove.show();
    }

    @OnClick(R.id.btnResched)
    protected void setReScheduleRequest() {
        Intent rescheduleIntent = new Intent(this, FacultyRescheduleConsultationRequestActivity.class);
        rescheduleIntent.putExtra("notificationId", notificationId);
        rescheduleIntent.putExtra("requestReceiverUid", facultyUid);
        startActivity(rescheduleIntent);
        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        finish();
    }

    @OnClick(R.id.btnIgnore)
    protected void sendRejectConsultation() {
        alertDialogReject.setTitle("Reason for Decline");
        alertDialogReject.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(notificationId);
        Log.d("concernDetails", concernDetails.getText().toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void initViews(ConsultationRequest request) {
        Glide.with(this).load(request.getSenderImageURL()).apply(new RequestOptions().circleCrop()).into(senderImage);
        senderName.setText(String.format("%s, %s %s", request.getSenderLastName().toUpperCase(), request.getSenderFirstName(), request.getSenderMiddleName()));
        senderProgramYearLevel.setText(String.format("%s (%s Year)", request.getSenderProgram(), request.getSenderYearLevel()));
        concernTitle.setText(request.getMessageTitle());
        concernDetails.setText(request.getMessageBody());

        requestedDay.setText(request.getRequestedDay());
        requestedDate.setText(request.getRequestedDate());
        requestedSchedule.setText(String.format("%s - %s @ %s", request.getRequestedTimeStart(), request.getRequestedTimeEnd(), request.getVenue()));

        facultyUid = request.getReceiverId();
    }

    @SuppressLint("InflateParams")
    @Override
    public void initSetAppointmentDialog(ConsultationRequest request) {
        AlertDialog.Builder alertDialogBuilderApprove = new AlertDialog.Builder(this);

        LayoutInflater layoutInflaterApprove = LayoutInflater.from(this);
        View promptViewApprove = layoutInflaterApprove.inflate(R.layout.faculty_set_appointment_remarks, null);
        alertDialogBuilderApprove.setView(promptViewApprove);

        TextView finalStartTime = promptViewApprove.findViewById(R.id.tvFinalStartTime);
        TextView finalEndTime = promptViewApprove.findViewById(R.id.tvFinalEndTime);
        TextView finalVenue = promptViewApprove.findViewById(R.id.tvFinalVenue);
        TextView finalRequestedDay = promptViewApprove.findViewById(R.id.tvFinalRequestedDay);
        TextView finalDate = promptViewApprove.findViewById(R.id.tvFinalDate);

        TextInputEditText remarksApprove = promptViewApprove.findViewById(R.id.etApproveRemarks);

        finalStartTime.setText(request.getRequestedTimeStart());
        finalEndTime.setText(request.getRequestedTimeEnd());
        finalVenue.setText(request.getVenue());
        finalRequestedDay.setText(request.getRequestedDay());
        finalDate.setText(request.getRequestedDate());

        alertDialogBuilderApprove.setCancelable(false)
                .setPositiveButton("APPROVE", (dialog, id) ->
                        presenter.sendApprovedConsultation(
                                request.notificationId,
                                request.getRequestedDay(), request.getRequestedDate(),
                                request.getRequestedTimeStart(), request.getRequestedTimeEnd(), request.getVenue(),
                                request.getMessageTitle(), request.getMessageBody(), Objects.requireNonNull(remarksApprove.getText()).toString()))
                .setNegativeButton("CANCEL", (dialog, id) ->
                        dialog.cancel())
                .setNeutralButton("RESCHEDULE", ((dialog, which) -> {
                    Intent rescheduleIntent = new Intent(this, FacultyRescheduleConsultationRequestActivity.class);
                    rescheduleIntent.putExtra("notificationId", notificationId);
                    rescheduleIntent.putExtra("requestReceiverUid", request.getReceiverId());
                    startActivity(rescheduleIntent);
                    overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                    finish();
        }));

        // create alert dialog
        alertDialogApprove = alertDialogBuilderApprove.create();
    }

    @SuppressLint("InflateParams")
    @Override
    public void initSetRejectRequestDialog(ConsultationRequest request) {
        AlertDialog.Builder alertDialogBuilderReject = new AlertDialog.Builder(this);
        LayoutInflater layoutInflaterReject = LayoutInflater.from(this);
        View promptViewReject = layoutInflaterReject.inflate(R.layout.faculty_set_reject_remarks, null);
        alertDialogBuilderReject.setView(promptViewReject);

        TextView finalStartTime = promptViewReject.findViewById(R.id.tvFinalStartTime);
        TextView finalEndTime = promptViewReject.findViewById(R.id.tvFinalEndTime);
        TextView finalVenue = promptViewReject.findViewById(R.id.tvFinalVenue);
        TextView finalRequestedDay = promptViewReject.findViewById(R.id.tvFinalRequestedDay);
        TextView finalDate = promptViewReject.findViewById(R.id.tvFinalDate);

        finalStartTime.setText(request.getRequestedTimeStart());
        finalEndTime.setText(request.getRequestedTimeEnd());
        finalVenue.setText(request.getVenue());
        finalRequestedDay.setText(request.getRequestedDay());
        finalDate.setText(request.getRequestedDate());

        TextInputEditText remarksReject = promptViewReject.findViewById(R.id.etRejectRemarks);

        alertDialogBuilderReject.setCancelable(false)
                .setPositiveButton("DECLINE", (dialog, which) ->
                presenter.sendRejectedConsultation(
                        request.notificationId,
                        request.getRequestedDay(), request.getRequestedDate(),
                        request.getRequestedTimeStart(), request.getRequestedTimeEnd(), request.getVenue(),
                        request.getMessageTitle(), request.getMessageBody(), Objects.requireNonNull(remarksReject.getText()).toString()
                ))
                .setNegativeButton("CANCEL", (dialog, which) ->
                        dialog.cancel())
                .setNeutralButton("RESCHEDULE", (dialog, which) -> {
                    Intent rescheduleIntent = new Intent(this, FacultyRescheduleConsultationRequestActivity.class);
                    rescheduleIntent.putExtra("notificationId", notificationId);
                    rescheduleIntent.putExtra("requestReceiverUid", request.getReceiverId());
                    startActivity(rescheduleIntent);
                    overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                    finish();
                })
        ;

        alertDialogReject = alertDialogBuilderReject.create();
    }

    @Override
    public void setButtonsDisabled() {
        btnApprove.setActivated(false);
        btnResched.setActivated(false);
        btnIgnore.setActivated(false);
    }

    @Override
    public void showProgress() {
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
        btnApprove.setActivated(true);
        btnResched.setActivated(true);
        btnIgnore.setActivated(true);
    }

    @Override
    public void setToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setExitDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setPositiveButton("EXIT", (dialog, which) -> {
                    dialog.dismiss();
                    startActivity(new Intent(this, FacultyConsultationRequestsLogActivity.class));
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                });
        builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss());
        builder.setCancelable(false);
        builder.create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, FacultyConsultationRequestsLogActivity.class));
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, FacultyConsultationRequestsLogActivity.class));
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}

package cp2.citeportalv1.views.faculty;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.base.FacultyBaseActivity;
import cp2.citeportalv1.presenters.consult.faculty.schedule.consultation_hours.ConsultationHoursPresenter;
import cp2.citeportalv1.presenters.consult.faculty.schedule.consultation_hours.ConsultationHoursPresenterImpl;

public class ConsultationHoursActivity extends FacultyBaseActivity implements ConsultationHoursPresenter.View {
    //implements TabLayout.BaseOnTabSelectedListener

    CoordinatorLayout coordinatorLayout;

    TabLayout tabLayout;
    ViewPager viewPager;

    ConsultationHoursPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation_hours);
        navigationView.setCheckedItem(R.id.consultation_hours);
        toolbar.setTitle("Consultation Schedules");

        tabLayout = findViewById(R.id.tabSched);

        viewPager = findViewById(R.id.vpConsultSched);

        presenter = new ConsultationHoursPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void loadDayPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager, true);
        viewPager.setOffscreenPageLimit(numberOfPages);
        viewPager.setCurrentItem(0, false);
    }
}

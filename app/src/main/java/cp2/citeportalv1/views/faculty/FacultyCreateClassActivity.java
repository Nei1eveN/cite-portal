package cp2.citeportalv1.views.faculty;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import cp2.citeportalv1.R;
import cp2.citeportalv1.presenters.classes.faculty.create.FacultyClassCreatePresenter;
import cp2.citeportalv1.presenters.classes.faculty.create.FacultyClassCreatePresenterImpl;

public class FacultyCreateClassActivity extends AppCompatActivity implements FacultyClassCreatePresenter.View {

    @BindView(R.id.createClassCoor) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.txClassSection) TextInputLayout classSectionWrapper;
    @BindView(R.id.etClassSection) TextInputEditText classSection;
    @BindView(R.id.spinnerCourseCodes) Spinner courseCodeItem;
    @BindView(R.id.etCourseDescription) EditText courseDescriptionField;

    FacultyClassCreatePresenter facultyClassCreatePresenter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_class);
        ButterKnife.bind(this);

        facultyClassCreatePresenter = new FacultyClassCreatePresenterImpl(this, this);
        progressDialog = new ProgressDialog(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        facultyClassCreatePresenter.requestCourses();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        facultyClassCreatePresenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @OnClick(R.id.btnCreateClass)
    protected void submitClassCredentials(){
        String section = classSection.getText().toString();
        String courseCode = courseCodeItem.getSelectedItem().toString();
        String courseDescription = courseDescriptionField.getText().toString();
        facultyClassCreatePresenter.submitClassCredentials(section, courseCode, courseDescription);
    }

    @OnItemSelected(R.id.spinnerCourseCodes)
    public void selectCourseCode(Spinner spinner){
        facultyClassCreatePresenter.requestCourseDescription(spinner.getSelectedItem().toString());
    }

    @Override
    public void showProgress() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void loadCoursesToSpinner(ArrayList<String> courseList) {
        ArrayAdapter<String> courseCodeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, courseList);
        courseCodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        courseCodeItem.setAdapter(courseCodeAdapter);
        facultyClassCreatePresenter.requestCourseDescription(courseCodeItem.getSelectedItem().toString());
    }

    @Override
    public void loadCourseDescriptionToField(String courseDescription) {
        courseDescriptionField.setText(courseDescription);
    }

    @Override
    public void setEmptySectionWrapper(String emptySectionMessage) {
        classSectionWrapper.setError(emptySectionMessage);
    }

    @Override
    public void setEmptyCourseCodeWrapper(String emptyCourseCodeMessage) {
        Snackbar.make(coordinatorLayout, emptyCourseCodeMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setEmptyCourseDescWrapper(String emptyCourseDescMessage) {
        Snackbar.make(coordinatorLayout, emptyCourseDescMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setNullWrapper() {
        classSectionWrapper.setError(null);
    }

    @Override
    public void setSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}

package cp2.citeportalv1.views.faculty;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ClassAdapter;
import cp2.citeportalv1.base.FacultyBaseActivity;
import cp2.citeportalv1.models.Class;
import cp2.citeportalv1.presenters.classes.faculty.display.FacultyClassPresenter;
import cp2.citeportalv1.presenters.classes.faculty.display.FacultyClassPresenterImpl;

import static cp2.citeportalv1.utils.Constants.ACTIVITY_REQUEST_CODE;

public class FacultyClassesActivity extends FacultyBaseActivity implements FacultyClassPresenter.FacultyClassView {

    protected RecyclerView recyclerView;
    protected TextView tvFacultyEmptyState;
    protected ImageView ivFacultyEmptyState;
    protected FloatingActionButton fabCreateClass;

    protected ClassAdapter classAdapter;

    FacultyClassPresenter facultyClassPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classes);
        toolbar.setTitle("Class");
        toolbar.setSubtitle(null);

//        navigationView.setCheckedItem(R.id.classes);

        recyclerView = findViewById(R.id.rvFacultyClasses);
        tvFacultyEmptyState = findViewById(R.id.tvClassEmptyState);
        ivFacultyEmptyState = findViewById(R.id.ivClassEmptyState);
        fabCreateClass = findViewById(R.id.fabAddClass);

        facultyClassPresenter = new FacultyClassPresenterImpl(this, this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        facultyClassPresenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        facultyClassPresenter.onDestroy();
    }

    @Override
    public void setUpFabAddClass() {
        fabCreateClass.setOnClickListener(v -> {
            startActivity(new Intent(this, FacultyCreateClassActivity.class));
            overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    public void setUpEmptyImageAddClass() {
        ivFacultyEmptyState.setOnClickListener(v -> {
            startActivity(new Intent(this, FacultyCreateClassActivity.class));
            overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            facultyClassPresenter.onStart();
        }
    }

    @Override
    public void setErrorState(String errorMessage) {
        fabCreateClass.hide();
        recyclerView.setVisibility(View.GONE);
        tvFacultyEmptyState.setText(errorMessage);
        Log.d("characters in a string", String.valueOf(errorMessage.length()));
    }

    @Override
    public void setClassesCreatedByFaculty(List<Class> createdByFaculty) {
        tvFacultyEmptyState.setVisibility(View.GONE);
        ivFacultyEmptyState.setVisibility(View.GONE);

        classAdapter = new ClassAdapter(createdByFaculty, this);
        classAdapter.notifyDataSetChanged();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(classAdapter);
    }
}

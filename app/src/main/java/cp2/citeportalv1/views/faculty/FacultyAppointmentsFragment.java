package cp2.citeportalv1.views.faculty;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;
import cp2.citeportalv1.presenters.consult.faculty.appointment.all_appointments.FacultyAppointmentsPresenter;
import cp2.citeportalv1.presenters.consult.faculty.appointment.all_appointments.FacultyAppointmentsPresenterImpl;

import static cp2.citeportalv1.utils.Constants.SCHEDULE_DAY;


/**
 * A simple {@link Fragment} subclass.
 */
public class FacultyAppointmentsFragment extends Fragment implements FacultyAppointmentsPresenter.View, SwipeRefreshLayout.OnRefreshListener {

    private Unbinder unbinder;
    @BindView(R.id.apptsCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.apptsSwipe) SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ivEmptyAppt) ImageView emptyAppointmentImage;
    @BindView(R.id.tvEmptyAppt) TextView emptyAppointmentText;
    @BindView(R.id.rvAppts) RecyclerView recyclerView;

    private FacultyAppointmentsPresenter presenter;

    private boolean hasLoadedOnce = false;

    public FacultyAppointmentsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new FacultyAppointmentsPresenterImpl(this, getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_faculty_appointments, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (isVisibleToUser && !hasLoadedOnce) {
                Bundle bundle = this.getArguments();
                String day = Objects.requireNonNull(bundle).getString(SCHEDULE_DAY);
                presenter.findAppointmentsAccordingToDay(day);
                hasLoadedOnce = true;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle bundle = this.getArguments();
        String day = Objects.requireNonNull(bundle).getString(SCHEDULE_DAY);
        presenter.onStart(day);
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle bundle = this.getArguments();
        String day = Objects.requireNonNull(bundle).getString(SCHEDULE_DAY);
        presenter.findAppointmentsAccordingToDay(day);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.ivEmptyAppt)
    void goToConsultationRequests() {
        startActivity(new Intent(getActivity(), FacultyConsultationRequestsLogActivity.class));
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setAppointments(FacultyAppointmentsGroupAdapter adapter) {
        emptyAppointmentImage.setVisibility(View.GONE);
        emptyAppointmentText.setVisibility(View.GONE);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyAppointment(String emptyMessage) {
        recyclerView.setVisibility(View.GONE);
        emptyAppointmentImage.setVisibility(View.VISIBLE);
        emptyAppointmentText.setVisibility(View.VISIBLE);

        emptyAppointmentText.setText(emptyMessage);
    }

    @Override
    public void onRefresh() {
        Bundle bundle = this.getArguments();
        String day = Objects.requireNonNull(bundle).getString(SCHEDULE_DAY);
        presenter.findAppointmentsAccordingToDay(day);
    }
}

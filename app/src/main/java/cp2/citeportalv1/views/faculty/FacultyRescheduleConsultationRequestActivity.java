package cp2.citeportalv1.views.faculty;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.StudentConsultScheduleAdapter;
import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;
import cp2.citeportalv1.adapters.listeners.RecyclerOnClickListener;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.presenters.consult.faculty.consultationdetails.reschedule.RescheduleRequestPresenter;
import cp2.citeportalv1.presenters.consult.faculty.consultationdetails.reschedule.RescheduleRequestPresenterImpl;
import cp2.citeportalv1.utils.EventDecorator;
import cp2.citeportalv1.utils.OneDayDecorator;

import static cp2.citeportalv1.utils.Constants.calendar;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;
import static cp2.citeportalv1.utils.Constants.fixedFormat;
import static cp2.citeportalv1.utils.Constants.generateTimepoints;
import static cp2.citeportalv1.utils.Constants.hourFormat;
import static cp2.citeportalv1.utils.Constants.minuteFormat;
import static cp2.citeportalv1.utils.Constants.secondsFormat;
import static cp2.citeportalv1.utils.Constants.timeFormat12hr;

public class FacultyRescheduleConsultationRequestActivity extends AppCompatActivity
        implements OnDateSelectedListener, RescheduleRequestPresenter.View, RecyclerOnClickListener {

    /**REQUESTED SCHEDULE DETAILS**/
    @BindView(R.id.reSchedCoor) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.tvReqStart) TextView requestedTimeStart;
    @BindView(R.id.tvUpto) TextView requestedTimeUpto;
    @BindView(R.id.tvReqEnd) TextView requestedTimeEnd;
    @BindView(R.id.tvReqVenue) TextView requestedVenue;
    @BindView(R.id.tvRequestedDay) TextView requestedDay;
    @BindView(R.id.tvReqDayDateUpto) TextView requestedDayDateUpto;
    @BindView(R.id.tvRequestedDate) TextView requestedDate;

    /**Material Calendar View**/
    @BindView(R.id.cvReScheduleDate) MaterialCalendarView calendarView;

    /**SCHEDULE DETAILS PER DAY**/
    @BindView(R.id.ivEmptySchedule) ImageView emptySchedImage;
    @BindView(R.id.tvEmptySchedule) TextView emptySchedText;
    @BindView(R.id.rvDaySched) RecyclerView recyclerViewScheds;
    @BindView(R.id.tvSessionHint) TextView sessionHint;

    /**APPOINTMENTS**/
    @BindView(R.id.tvApptHeader) TextView appointmentHeader;
    @BindView(R.id.ivEmptyAppt) ImageView emptyApptImage;
    @BindView(R.id.tvEmptyAppt) TextView emptyApptText;
    @BindView(R.id.rvAppt) RecyclerView recyclerViewAppts;

    /**RESCHEDULE DETAILS MADE BY FACULTY / INSTRUCTOR**/
    @BindView(R.id.tvNewSchedHeader) TextView newSchedHeader;
    @BindView(R.id.tvNewStart) TextView newTimeStart;
    @BindView(R.id.tvNewUpto) TextView newUpto;
    @BindView(R.id.tvNewEnd) TextView newTimeEnd;
    @BindView(R.id.tvNewVenue) TextView newVenue;
    @BindView(R.id.tvNewDay) TextView newDay;
    @BindView(R.id.tvNewDateUpto) TextView newDayDateUpto;
    @BindView(R.id.tvNewDate) TextView newDate;

    /**BUTTON/S**/
    @BindView(R.id.btnReSched) MaterialButton btnReSched;

    Date selectedDate, selectedStartTime;

    Intent intent;

    String notificationId, facultyUid, consultationTitle, consultationMessage;

    ProgressDialog progressDialog;

    RescheduleRequestPresenter presenter;

    TimePickerDialog pickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_reschedule_consultation_request);
        ButterKnife.bind(this);

        intent = getIntent();
        notificationId = intent.getStringExtra("notificationId");
        facultyUid = intent.getStringExtra("requestReceiverUid");

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Reschedule Request");

        progressDialog = new ProgressDialog(this);

        presenter = new RescheduleRequestPresenterImpl(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.info_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, FacultyConsultationDetailsActivity.class);
                intent.putExtra("notificationId", notificationId);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
            case R.id.guide:
                LayoutInflater inflater = LayoutInflater.from(this);
                @SuppressLint("InflateParams") View calendarGuideline = inflater.inflate(R.layout.calendar_guideline, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(this).setView(calendarGuideline).setCancelable(false).setTitle("Calendar Guidelines").setIcon(R.drawable.ic_insert_invitation).setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss());
                builder.create().show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, FacultyConsultationDetailsActivity.class);
        intent.putExtra("notificationId", notificationId);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        calendarView.setDateSelected(date.getDate(), selected);
        selectedDate = calendarView.getSelectedDate().getDate();

        appointmentHeader.setText(String.format("APPOINTMENTS FOR %s", dateFormat.format(date.getDate())));

        presenter.findScheduleFromDate(facultyUid, selectedDate);
        presenter.findAppointmentFromDate(facultyUid, selectedDate);
    }

    @OnClick(R.id.btnReSched)
    protected void reScheduleRequest() {
        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilderApprove = new androidx.appcompat.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View promptView = layoutInflater.inflate(R.layout.faculty_set_appointment_remarks, null);
        alertDialogBuilderApprove.setView(promptView);

        TextView finalScheduleDetail = promptView.findViewById(R.id.tvFinalSchedHeader);
        TextView finalStartTime = promptView.findViewById(R.id.tvFinalStartTime);
        TextView finalEndTime = promptView.findViewById(R.id.tvFinalEndTime);
        TextView finalVenue = promptView.findViewById(R.id.tvFinalVenue);
        TextView finalRequestedDay = promptView.findViewById(R.id.tvFinalRequestedDay);
        TextView finalDate = promptView.findViewById(R.id.tvFinalDate);

        TextInputEditText remarksApprove = promptView.findViewById(R.id.etApproveRemarks);

        TextView sideNote = promptView.findViewById(R.id.tvSideNote);

        finalScheduleDetail.setText(getString(R.string.resched_header_description));
        finalStartTime.setText(newTimeStart.getText().toString());
        finalEndTime.setText(newTimeEnd.getText().toString());
        finalVenue.setText(newVenue.getText().toString());
        finalRequestedDay.setText(newDay.getText().toString());
        finalDate.setText(newDate.getText().toString());

        sideNote.setText(null);

        alertDialogBuilderApprove.setCancelable(false).setPositiveButton("RESCHEDULE", (dialog, id) -> presenter.sendRescheduledRequest(notificationId, newTimeStart.getText().toString(), newTimeEnd.getText().toString(), newVenue.getText().toString(), newDay.getText().toString(), newDate.getText().toString(), consultationTitle, consultationMessage, Objects.requireNonNull(remarksApprove.getText()).toString())).setNegativeButton("CANCEL", (dialog, id) -> dialog.cancel());

        alertDialogBuilderApprove.create().show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(notificationId);
        presenter.findAppointments(facultyUid);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String caption) {
        progressDialog.setCancelable(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(caption);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setUpCalendar() {
        calendarView.setVisibility(View.VISIBLE);
        calendarView.addDecorator(new OneDayDecorator());
        calendarView.setTileSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 44, getResources().getDisplayMetrics()));
        calendarView.setSelectedDate(new Date());
        calendarView.newState().setMinimumDate(Calendar.getInstance()).setCalendarDisplayMode(CalendarMode.MONTHS).commit();
        calendarView.setOnDateChangedListener(this);

        presenter.findScheduleFromDate(facultyUid, calendarView.getSelectedDate().getDate());
        presenter.findAppointmentFromDate(facultyUid, calendarView.getSelectedDate().getDate());

        appointmentHeader.setText(String.format("APPOINTMENTS FOR %s", dateFormat.format(calendarView.getSelectedDate().getDate())));
    }

    @Override
    public void setUpDotsToCalendar(EventDecorator eventDecorator) {
        calendarView.addDecorator(eventDecorator);
    }

    @Override
    public void setUpApprovedTimeSlotsToTimePicker(Timepoint[] existingTimes) {
        pickerDialog.setDisabledTimes(existingTimes);
    }

    @Override
    public void initViews(ConsultationRequest request) {
        requestedTimeStart.setText(request.getRequestedTimeStart());
        requestedTimeEnd.setText(request.getRequestedTimeEnd());
        requestedVenue.setText(request.getVenue());
        requestedDay.setText(request.getRequestedDay());
        requestedDate.setText(request.getRequestedDate());

        consultationTitle = request.getMessageTitle();
        consultationMessage = request.getMessageBody();

        sessionHint.setVisibility(View.GONE);

        newSchedHeader.setVisibility(View.GONE);
        newTimeStart.setVisibility(View.GONE);
        newUpto.setVisibility(View.GONE);
        newTimeEnd.setVisibility(View.GONE);
        newVenue.setVisibility(View.GONE);
        newDay.setVisibility(View.GONE);
        newDayDateUpto.setVisibility(View.GONE);
        newDate.setVisibility(View.GONE);

        btnReSched.setVisibility(View.GONE);
    }

    @Override
    public void setEmptySchedule(String emptyMessage) {
        recyclerViewScheds.setVisibility(View.GONE);
        sessionHint.setVisibility(View.GONE);

        emptySchedImage.setVisibility(View.VISIBLE);
        emptySchedText.setVisibility(View.VISIBLE);

        emptySchedText.setText(emptyMessage);
    }

    @Override
    public void setEmptyAppointment(String emptyMessage) {
        recyclerViewAppts.setVisibility(View.GONE);

        emptyApptImage.setVisibility(View.VISIBLE);
        emptyApptText.setVisibility(View.VISIBLE);

        emptyApptText.setText(emptyMessage);

        newSchedHeader.setVisibility(View.GONE);
        newTimeStart.setVisibility(View.GONE);
        newUpto.setVisibility(View.GONE);
        newTimeEnd.setVisibility(View.GONE);
        newVenue.setVisibility(View.GONE);
        newDay.setVisibility(View.GONE);
        newDayDateUpto.setVisibility(View.GONE);
        newDate.setVisibility(View.GONE);

        btnReSched.setVisibility(View.GONE);
    }

    @Override
    public void setSchedulesFromDate(List<Schedule> schedules) {
        emptySchedImage.setVisibility(View.GONE);
        emptySchedText.setVisibility(View.GONE);

        sessionHint.setVisibility(View.VISIBLE);
        recyclerViewScheds.setVisibility(View.VISIBLE);

        recyclerViewScheds.setHasFixedSize(true);
        recyclerViewScheds.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewScheds.setNestedScrollingEnabled(false);
        recyclerViewScheds.setAdapter(new StudentConsultScheduleAdapter(this, schedules, this));
    }

    @Override
    public void setAppointmentsFromDate(FacultyAppointmentsGroupAdapter adapter) {
        emptyApptImage.setVisibility(View.GONE);
        emptyApptText.setVisibility(View.GONE);

        recyclerViewAppts.setVisibility(View.VISIBLE);
        recyclerViewAppts.setHasFixedSize(true);
        recyclerViewAppts.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAppts.setNestedScrollingEnabled(false);
        recyclerViewAppts.setAdapter(adapter);
    }

    @Override
    public void showSnackMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setExitDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("EXIT", (dialogInterface, i) ->
        {
            dialogInterface.dismiss();
            startActivity(new Intent(this, FacultyConsultationRequestsLogActivity.class));
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        });
        builder.create().show();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("DISMISS", (dialogInterface, i) -> dialogInterface.dismiss());
        builder.create().show();
    }

    @Override
    public void OnDetailClick(String startTime, String endTime, String venue, String day) {
        LayoutInflater schedInflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View schedView = schedInflater.inflate(R.layout.faculty_request_time, null);
        TextView timeFrom = schedView.findViewById(R.id.tvTimeFrom);
        TextView timeTo = schedView.findViewById(R.id.tvTimeTo);
        Button startFrom = schedView.findViewById(R.id.btnStartFrom);
        Button endTo = schedView.findViewById(R.id.btnEndTo);

        try {
            Date startingTime = timeFormat12hr.parse(startTime);
            Date endingTime = timeFormat12hr.parse(endTime);
            Log.d("getStartingTime", timeFormat12hr.format(startingTime));
            Log.d("getStartingTimeInDate", String.valueOf(startingTime));

            startFrom.setOnClickListener(v -> {

                presenter.findExistingTimeSlotAppointmentsFromDate(facultyUid, calendarView.getSelectedDate().getDate());

                TimePickerDialog.OnTimeSetListener setListener = (view, hourOfDay, minute, second) -> {
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    calendar.set(Calendar.MINUTE, minute);

                    timeFrom.setText(timeFormat12hr.format(calendar.getTime()));
                    Log.d("getReqTimeStart", fixedFormat.format(calendar.getTime()));
                    Log.d("getReqTimeStartInDate", String.valueOf(calendar.getTime()));
                    selectedStartTime = calendar.getTime();
                };

                pickerDialog = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(setListener, Integer.valueOf(hourFormat.format(startingTime)), Integer.valueOf(minuteFormat.format(startingTime)), false);
                pickerDialog.setMinTime(Integer.valueOf(hourFormat.format(startingTime)), Integer.valueOf(minuteFormat.format(startingTime)), Integer.valueOf(secondsFormat.format(startingTime)));
                pickerDialog.setMaxTime(Integer.valueOf(hourFormat.format(endingTime)), Integer.valueOf(minuteFormat.format(endingTime)), Integer.valueOf(secondsFormat.format(endingTime)));
                pickerDialog.setSelectableTimes(generateTimepoints(15));

                pickerDialog.show(getSupportFragmentManager(), "TimePickerDialogStartTime");
            });

            endTo.setOnClickListener(v -> {
                TimePickerDialog.OnTimeSetListener setListener = (view, hourOfDay, minute, second) -> {
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    calendar.set(Calendar.MINUTE, minute);

                    timeTo.setText(timeFormat12hr.format(calendar.getTime()));
                    Log.d("getReqTimeEnd", fixedFormat.format(calendar.getTime()));
                    Log.d("getReqTimeEndInDate", String.valueOf(calendar.getTime()));
                };

                if (selectedStartTime == null) {
                    androidx.appcompat.app.AlertDialog.Builder emptyStartTimeBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
                    emptyStartTimeBuilder.setTitle("No Starting Time").setMessage("Please select a starting time.");
                    emptyStartTimeBuilder.setPositiveButton("GOT IT", (dialog, which) -> dialog.dismiss());

                    emptyStartTimeBuilder.create().show();
                } else {
                    TimePickerDialog pickerDialog = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(setListener, Integer.valueOf(hourFormat.format(startingTime)), Integer.valueOf(minuteFormat.format(startingTime)), false);
                    pickerDialog.setMinTime(Integer.valueOf(hourFormat.format(selectedStartTime)), Integer.valueOf(minuteFormat.format(selectedStartTime)), Integer.valueOf(secondsFormat.format(selectedStartTime)));
                    pickerDialog.setMaxTime(Integer.valueOf(hourFormat.format(endingTime)), Integer.valueOf(minuteFormat.format(endingTime)), Integer.valueOf(secondsFormat.format(endingTime)));

                    pickerDialog.show(getSupportFragmentManager(), "TimePickerDialogStartTime");
                }

            });

        } catch (ParseException e) {
            e.printStackTrace();
        }

        timeFrom.setText(startTime);
        timeTo.setText(endTime);

        AlertDialog.Builder schedSetBuilder = new AlertDialog.Builder(this).setView(schedView).setTitle(String.format("%s - %s", startTime, endTime)).setIcon(R.drawable.ic_timer).setPositiveButton("Set", (dialog, which) ->
                {
                    newSchedHeader.setVisibility(View.VISIBLE);
                    newTimeStart.setVisibility(View.VISIBLE);
                    newUpto.setVisibility(View.VISIBLE);
                    newTimeEnd.setVisibility(View.VISIBLE);
                    newVenue.setVisibility(View.VISIBLE);
                    newDayDateUpto.setVisibility(View.VISIBLE);
                    newDay.setVisibility(View.VISIBLE);
                    newDate.setVisibility(View.VISIBLE);
                    btnReSched.setVisibility(View.VISIBLE);

                    if (selectedDate != null) {
                        newTimeStart.setText(timeFrom.getText().toString());
                        newTimeEnd.setText(timeTo.getText().toString());
                        newVenue.setText(venue);
                        newDay.setText(dayFormat.format(calendarView.getSelectedDate().getDate()));
                        newDate.setText(dateFormat.format(calendarView.getSelectedDate().getDate()));
                    } else {
                        newTimeStart.setText(timeFrom.getText().toString());
                        newTimeEnd.setText(timeTo.getText().toString());
                        newVenue.setText(venue);
                        newDay.setText(dayFormat.format(calendarView.getSelectedDate().getDate()));
                        newDate.setText(dateFormat.format(new Date()));
                    }

                    dialog.dismiss();
                }).setNegativeButton("Cancel", null);

        schedSetBuilder.create().show();
    }
}
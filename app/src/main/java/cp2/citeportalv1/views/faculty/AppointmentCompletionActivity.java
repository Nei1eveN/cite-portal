package cp2.citeportalv1.views.faculty;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail.finishing_appointment.AppointmentCompletionPresenter;
import cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail.finishing_appointment.AppointmentCompletionPresenterImpl;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class AppointmentCompletionActivity extends AppCompatActivity implements AppointmentCompletionPresenter.View {

    @BindView(R.id.coorCompleteAppt) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.tvApptDate) TextView apptDate;
    @BindView(R.id.tvApptDay) TextView apptDay;
    @BindView(R.id.tvTimeStart) TextView apptStart;
    @BindView(R.id.tvTimeEnd) TextView apptEnd;
    @BindView(R.id.tvVenue) TextView apptVenue;

    @BindView(R.id.tvMessageTitle) TextView messageTitle;
    @BindView(R.id.tvMessageBody) TextView messageBody;

    @BindView(R.id.tvStatus) TextView apptStatus;

    @BindView(R.id.ivStudentProfileImage) ImageView senderImage;
    @BindView(R.id.tvSenderName) TextView senderName;
    @BindView(R.id.tvStudentYearLevel) TextView senderYrLvlProg;

    @BindView(R.id.txMessage) TextInputLayout apptRemarksHeader;
    @BindView(R.id.etMessage) TextInputEditText apptRemarks;

    @BindView(R.id.btnFinish) MaterialButton btnFinish;
    @BindView(R.id.btnCancelAppt) MaterialButton btnCancelAppt;
    @BindView(R.id.btnReschedAppt) MaterialButton btnResched;

    Intent intent;
    String notificationId, facultyUid;

    String appointmentDate, timeStart;

    ProgressDialog progressDialog;

    AppointmentCompletionPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_completion);
        ButterKnife.bind(this);

        intent = getIntent();
        notificationId = intent.getStringExtra("notificationId");

        progressDialog = new ProgressDialog(this);

        presenter = new AppointmentCompletionPresenterImpl(this, this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Appointment Completion");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                intent = new Intent(this, FacultyAppointmentDetailsActivity.class);
                intent.putExtra("notificationId", notificationId);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        intent = new Intent(this, FacultyAppointmentDetailsActivity.class);
        intent.putExtra("notificationId", notificationId);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @OnClick(R.id.btnFinish)
    void submitFinishingAppointment() {
        presenter.submitFinishingAppointment(notificationId, Objects.requireNonNull(apptRemarks.getText()).toString(), appointmentDate, timeStart, apptStatus.getText().toString());
    }

    @OnClick(R.id.btnReschedAppt)
    void rescheduleAppointment() {
        intent = new Intent(this, AppointmentRescheduleActivity.class);
        intent.putExtra("notificationId", notificationId);
        intent.putExtra("requestReceiverUid", facultyUid);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
    }

    @OnClick(R.id.btnCancelAppt)
    void cancelAppointment() {
        presenter.submitCancelledAppointment(notificationId, Objects.requireNonNull(apptRemarks.getText()).toString(), appointmentDate, timeStart, "CANCELLED");
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(notificationId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setAppointmentDetails(Appointments appointment) {
        apptDate.setText(appointment.getRequestedDate());
        apptDay.setText(String.format("(%s)", appointment.getRequestedDay()));
        apptStart.setText(appointment.getRequestedTimeStart());
        apptEnd.setText(appointment.getRequestedTimeEnd());
        apptVenue.setText(appointment.getVenue());

        messageTitle.setText(appointment.getMessageTitle());
        messageBody.setText(appointment.getMessageBody());

        Glide.with(this).load(appointment.getSenderImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(senderImage);
        senderName.setText(String.format("%s, %s %s", appointment.getSenderLastName(), appointment.getSenderFirstName(), appointment.getSenderMiddleName()));
        senderYrLvlProg.setText(String.format("%s (%s Year)", appointment.getSenderProgram(), appointment.getSenderYearLevel()));

        appointmentDate = appointment.getRequestedDate();
        timeStart = appointment.getRequestedTimeStart();
        facultyUid = appointment.getReceiverId();
    }

    @Override
    public void setNullWrapper(String message) {
        apptRemarksHeader.setError(message);
    }

    @Override
    public void showErrorDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("DISMISS", (dialogInterface, i) -> dialogInterface.dismiss());
        builder.create().show();
    }

    @Override
    public void showExitDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setCancelable(false).setTitle(title).setMessage(message).setPositiveButton("EXIT", (dialogInterface, i) ->
        {
            dialogInterface.dismiss();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        });
        builder.create().show();
    }

    @Override
    public void showSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }
}

package cp2.citeportalv1.views.faculty;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.presenters.profile.faculty.FacultyUserPresenter;
import cp2.citeportalv1.presenters.profile.faculty.FacultyUserPresenterImpl;

public class FacultyProfileActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, FacultyUserPresenter.View {

    @BindView(R.id.facProfCoor) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.facProfAppBar) AppBarLayout appBarLayout;
    @BindView(R.id.facCollapsingToolbarLayout) CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.facProfToolbar) Toolbar toolbar;

    @BindView(R.id.facultyProfileImage) ImageView facultyProfileImage;
    @BindView(R.id.tvFacultyProfileName) TextView facultyProfileName;
    @BindView(R.id.facultyProfileDepartment) TextView facultyProfileDepartment;

    @BindView(R.id.facTabLayout) TabLayout tabLayout;
    @BindView(R.id.vpFacProf) ViewPager viewPager;

    FacultyUserPresenter presenter;

    AlertDialog.Builder exitBuilder;
    AlertDialog exitDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_profile);
        ButterKnife.bind(this);

        presenter = new FacultyUserPresenterImpl(this, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0)
        {
            //  Collapsed
            Objects.requireNonNull(getSupportActionBar()).setTitle("Profile");
        }
        else
        {
            //Expanded
            Objects.requireNonNull(getSupportActionBar()).setTitle(null);
        }
    }

    @Override
    public void setPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(numberOfPages);
        viewPager.setCurrentItem(0, false);

        tabLayout.setupWithViewPager(viewPager);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back);
        upArrow.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    public void setUserInfo(Faculty faculty) {
        Glide.with(this).load(faculty.getFacultyImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px)).into(facultyProfileImage);
        facultyProfileName.setText(String.format("%s, %s %s", faculty.getLastName(), faculty.getfName(), faculty.getMidName()));
        facultyProfileDepartment.setText(String.format("%s Department", faculty.getDepartment()));
    }

    @Override
    public void setExitDialog(String title, String message) {
        exitBuilder = new AlertDialog.Builder(this);
        exitBuilder.setCancelable(false);
        exitBuilder.setTitle(title);
        exitBuilder.setMessage(message);
        exitBuilder.setPositiveButton("EXIT", (dialog, which) -> finish());

        exitDialog = exitBuilder.create();
        exitDialog.show();
    }
}

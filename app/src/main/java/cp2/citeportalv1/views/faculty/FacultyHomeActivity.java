package cp2.citeportalv1.views.faculty;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import java.util.Date;

import androidx.viewpager.widget.ViewPager;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.base.FacultyBaseActivity;
import cp2.citeportalv1.presenters.home.FacultyHomePresenter;
import cp2.citeportalv1.presenters.home.FacultyHomePresenterImpl;

import static cp2.citeportalv1.utils.Constants.dayFormat;

public class FacultyHomeActivity extends FacultyBaseActivity implements FacultyHomePresenter.View {

    TabLayout tabLayout;
    ViewPager viewPager;

    FacultyHomePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_home);
        toolbar.setTitle("Home");
        toolbar.setSubtitle("Today is "+dayFormat.format(new Date()));

        tabLayout = findViewById(R.id.tabSelections);
        viewPager = findViewById(R.id.vpSelections);

        navigationView.setCheckedItem(R.id.facHomeMenu);
        presenter = new FacultyHomePresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void loadPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager, true);
        viewPager.setOffscreenPageLimit(numberOfPages);
        viewPager.setCurrentItem(0, false);
    }
}

package cp2.citeportalv1.views.faculty;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import cp2.citeportalv1.R;
import cp2.citeportalv1.presenters.profile.faculty.signature.FacultyUserSignaturePresenter;
import cp2.citeportalv1.presenters.profile.faculty.signature.FacultyUserSignaturePresenterImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Objects;

public class FacultySignatureActivity extends AppCompatActivity implements FacultyUserSignaturePresenter.View {

    @BindView(R.id.ivUserSignature) ImageView userSignatureImage;
    @BindView(R.id.tvEmptyUserSignature) TextView emptyUserSignatureText;

    ProgressDialog progressDialog;

    FacultyUserSignaturePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_signature);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        presenter = new FacultyUserSignaturePresenterImpl(this, this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("User Signature");

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.signature_view_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
            case R.id.edit_signature:
                startActivity(new Intent(this, FacultyUserEditSignatureActivity.class));
                overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setUserSignature(String userSignature) {
        emptyUserSignatureText.setVisibility(View.GONE);

        userSignatureImage.setVisibility(View.VISIBLE);

        Glide.with(this).load(userSignature).apply(new RequestOptions().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(userSignatureImage);
    }

    @Override
    public void setNoSignature(String emptySignature) {
        userSignatureImage.setVisibility(View.GONE);

        emptyUserSignatureText.setVisibility(View.VISIBLE);

        emptyUserSignatureText.setText(emptySignature);
    }
}

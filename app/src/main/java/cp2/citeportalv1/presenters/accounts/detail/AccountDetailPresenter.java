package cp2.citeportalv1.presenters.accounts.detail;

import cp2.citeportalv1.models.Accounts;

public interface AccountDetailPresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
        void showSnackMessage(String message);
        void setUserDetail(Accounts account);
    }
    void onStart(String userUid);
    void onDestroy();
    void sendEmailVerification(String userUid);
    void sendPasswordReset(String userUid);
    void setUserRemoved(String userUid);
}

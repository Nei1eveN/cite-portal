package cp2.citeportalv1.presenters.accounts.admin;

import cp2.citeportalv1.adapters.ModeratorAdapter;

public interface AdminAccountsPresenter {
    interface View {
        void setProgress();
        void hideProgress();
        void setSnackMessage(String message);
        void setEmptyState(String emptyStateMessage);
        void setList(ModeratorAdapter adapter);
    }
    void onStart();
    void onDestroy();
    void requestList();
}

package cp2.citeportalv1.presenters.accounts.detail.create;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Accounts;

import static cp2.citeportalv1.utils.Constants.ACCOUNTS;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_ACCESS_LEVEL;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_FIRST_NAME;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_ID;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_LAST_NAME;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_MIDDLE_NAME;
import static cp2.citeportalv1.utils.Constants.ADMIN;
import static cp2.citeportalv1.utils.Constants.DEPARTMENT;
import static cp2.citeportalv1.utils.Constants.EMAIL;
import static cp2.citeportalv1.utils.Constants.TIMESTAMP;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

class AccountCreateInteractorImpl implements AccountCreateInteractor {
    private Context context;

    AccountCreateInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void submitCredentials(String accountID,
                                  String firstName, String middleName, String lastName,
                                  String departmentSelected,
                                  String email, String password,
                                  String accessLevel, AccountCreateListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onCreateFailure("Network Error", "Please check your network connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(ACCOUNTS);
            databaseReference.keepSynced(true);

            Map<String, Object> accountMap = new HashMap<>();
            accountMap.put(ACCOUNT_ACCESS_LEVEL, accessLevel);
            accountMap.put(ACCOUNT_ID, accountID);
            accountMap.put(DEPARTMENT, departmentSelected);
            accountMap.put(ACCOUNT_FIRST_NAME, firstName);
            accountMap.put(ACCOUNT_MIDDLE_NAME, middleName);
            accountMap.put(ACCOUNT_LAST_NAME, lastName);
            accountMap.put(EMAIL, email);
            accountMap.put(TIMESTAMP, System.currentTimeMillis() / 1000L);


            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnSuccessListener(((Activity) context), authResult -> databaseReference.child(authResult.getUser().getUid()).updateChildren(accountMap).addOnSuccessListener(((Activity) context), aVoid -> authResult.getUser().sendEmailVerification().addOnSuccessListener(((Activity) context), aVoid1 -> listener.onCreateSuccess("Account Creation Success", "Email Verification Link has been sent to "+authResult.getUser().getEmail())).addOnFailureListener(((Activity) context), e -> listener.onCreateFailure("Verification Sending Failure", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onCreateFailure("Database Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onCreateFailure("Account Creation Error", e.getMessage()));
        }
    }

    @Override
    public void getLoginCredentials(String email, String password, ReAuthenticationListener listener) {
        if (!isNetworkAvailable(context)) {
            FirebaseAuth.getInstance().signOut();
            listener.onAuthenticationFailure("Network Error", "You cannot login without network connection available. You will be redirected to Login Page.");
        } else {
            FirebaseAuth.getInstance().signOut();
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnSuccessListener(((Activity) context), authResult -> {
                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                DatabaseReference accountReference = firebaseDatabase.getReference(ACCOUNTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
                accountReference.keepSynced(true);

                accountReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            Accounts account = Objects.requireNonNull(dataSnapshot.getValue(Accounts.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                            if (account.getAccessLevel().equals(ADMIN)) {
                                listener.onAuthenticationSuccess("Admin Login Successful", "Heading back to Accounts Page");
                            } else {
                                FirebaseAuth.getInstance().signOut();
                                listener.onAuthenticationFailure("Credentials Not Qualified", "You will be redirected to Login Page.");
                            }
                        } else {
                            FirebaseAuth.getInstance().signOut();
                            listener.onAuthenticationFailure("Credentials Not Qualified", "You will be redirected to Login Page.");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        FirebaseAuth.getInstance().signOut();
                        listener.onAuthenticationFailure("Database Error", databaseError.getMessage());
                    }
                });
            }).addOnFailureListener(((Activity) context), e -> listener.onAuthenticationFailure("Wrong Credentials", e.getMessage()+"\n\nHeading back to Login Page."));
        }
    }

    @Override
    public void userLogOut(UserLogOutListener listener) {
        FirebaseAuth.getInstance().signOut();
        listener.onLogOutSuccess("User Logged Out", "Logged Out Successfully");
    }
}

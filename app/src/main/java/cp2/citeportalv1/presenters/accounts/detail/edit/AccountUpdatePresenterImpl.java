package cp2.citeportalv1.presenters.accounts.detail.edit;

import android.content.Context;

import cp2.citeportalv1.models.Accounts;

import static cp2.citeportalv1.utils.Constants.ADMIN;
import static cp2.citeportalv1.utils.Constants.SECRETARY;
import static cp2.citeportalv1.utils.Constants.digitCasePattern;

public class AccountUpdatePresenterImpl implements AccountUpdatePresenter, AccountUpdateInteractor.UserDetailsListener, AccountUpdateInteractor.AccountUpdateListener {
    private AccountUpdatePresenter.View view;
    private AccountUpdateInteractor interactor;

    public AccountUpdatePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new AccountUpdateInteractorImpl(context);
    }

    @Override
    public void onStart(String userUid) {
        if (view != null) {
            view.showProgress("Loading User Detail", "Please wait...");
        }
        interactor.getUserDetails(userUid, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void submitCredentials(String userUid, String accountID, String firstName, String middleName, String lastName, String departmentSelected, String accessLevel) {
        if (accountID.isEmpty()) {
            view.showErrorDialog("Empty Employee ID", "Please fill in the required field.");
        } else if (accountID.length() > 6) {
            view.showErrorDialog("Employee ID Length", "Employee ID length cannot be more than 6 digits.");
        } else if (!accountID.contains("Q-")) {
            view.showErrorDialog("Employee ID Error", "Employee ID must contain Q- before the number");
        } else if (!accountID.startsWith("Q-")) {
            view.showErrorDialog("Employee ID Error", "Employee ID must start with Q- before the number");
        }
        else if (digitCasePattern.matcher(firstName).find()) {
            view.showErrorDialog("Name with Numbers", "Name with Special Characters is not allowed.");
        }  else if (digitCasePattern.matcher(middleName).find()) {
            view.showErrorDialog("Name with Numbers", "Name with Special Characters is not allowed.");
        }  else if (digitCasePattern.matcher(lastName).find()) {
            view.showErrorDialog("Name with Numbers", "Name with Special Characters is not allowed.");
        } else if (firstName.isEmpty()) {
            view.showErrorDialog("Empty Name", "Please fill in the required field.");
        } else if (middleName.isEmpty()) {
            view.showErrorDialog("Empty Name", "Please fill in the required field.");
        } else if (lastName.isEmpty()) {
            view.showErrorDialog("Empty Name", "Please fill in the required field.");
        } else if (accessLevel.equals(ADMIN) && !departmentSelected.isEmpty()) {
            if (view != null) {
                view.showProgress("Submitting Credentials", "Updating "+accountID+"\n\nPlease wait...");
            }
            interactor.getCredentials(userUid, accountID, firstName, middleName, lastName, "", ADMIN, this);
        } else if (accessLevel.equals(SECRETARY) && departmentSelected.isEmpty()) {
            view.showErrorDialog("No Department Selected For Secretary Access", "Please select a department");
        } else {
            if (view != null) {
                view.showProgress("Submitting Credentials", "Updating "+accountID+"\n\nPlease wait...");
            }
            interactor.getCredentials(userUid, accountID, firstName, middleName, lastName, departmentSelected, accessLevel, this);
        }
    }

    @Override
    public void onDetailSuccess(Accounts account) {
        if (view != null) {
            view.hideProgress();
            view.setUserDetail(account);
        }
    }

    @Override
    public void onDetailFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(errorMessage);
            view.showExitDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onUpdateSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(title, message);
        }
    }

    @Override
    public void onUpdateFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.accounts.detail;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Accounts;
import cp2.citeportalv1.models.Result;
import cp2.citeportalv1.services.APIService;
import cp2.citeportalv1.utils.APIClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cp2.citeportalv1.utils.Constants.ACCOUNTS;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

class AccountDetailInteractorImpl implements AccountDetailInteractor {
    private Context context;

    AccountDetailInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserDetails(String userUid, UserDetailsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(ACCOUNTS).child(userUid);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Accounts account = Objects.requireNonNull(dataSnapshot.getValue(Accounts.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    listener.onDetailSuccess(account);
                } else {
                    listener.onDetailFailure("Account Detail Error", "User does not exist.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onDetailFailure("Database Error", databaseError.getMessage());
            }
        });
    }

    @Override
    public void sendEmailVerification(String userUid, EmailVerificationListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onVerificationFailure("Network Error", "Please check your network connection.");
        } else {
            APIService emailVerificationService = APIClient.getClient(context).create(APIService.class);
            Call<Result> resultCall = emailVerificationService.sendVerification(userUid);
            resultCall.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                    Result result = response.body();

                    if (!Objects.requireNonNull(result).getStatus()) {
                        listener.onVerificationFailure("Email Verification Failure", "Cannot send email verification to this user. Please try again later.");
                    } else {
                        listener.onVerificationSuccess("User Validation Success", "Email Verification Link has been sent.");
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                    listener.onVerificationFailure("Validation Error", t.getMessage());
                }
            });
        }
    }

    @Override
    public void sendPasswordReset(String userUid, PasswordResetListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onResetFailure("Network Error", "Please check your network connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(ACCOUNTS).child(userUid);
            databaseReference.keepSynced(true);

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Accounts account = Objects.requireNonNull(dataSnapshot.getValue(Accounts.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                        FirebaseAuth.getInstance().sendPasswordResetEmail(account.getEmail()).addOnSuccessListener(((Activity) context), aVoid -> listener.onResetSuccess("Password Reset", "Password Reset Link has been sent to "+account.getEmail()+".")).addOnFailureListener(((Activity) context), e -> listener.onResetFailure("Password Reset Error", e.getMessage()));
                    } else {
                        listener.onResetSuccess("Account Detail Error", "This user does not exist in the database.");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onResetFailure("Database Error", databaseError.getMessage());
                }
            });


        }
    }

    @Override
    public void getUserRemove(String userUid, UserRemoveListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onRemoveFailure("Network Error", "Please check your network connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(ACCOUNTS).child(userUid);
            databaseReference.keepSynced(true);

            databaseReference.removeValue().addOnSuccessListener(((Activity) context), aVoid -> listener.onRemoveSuccess("User Removed", "User has been removed successfully.")).addOnFailureListener(((Activity) context), e -> listener.onRemoveFailure("User Removing Failure", e.getMessage()));
        }
    }
}

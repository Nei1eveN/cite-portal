package cp2.citeportalv1.presenters.accounts.detail;

import android.content.Context;

import cp2.citeportalv1.models.Accounts;

public class AccountDetailPresenterImpl implements AccountDetailPresenter, AccountDetailInteractor.UserDetailsListener, AccountDetailInteractor.EmailVerificationListener, AccountDetailInteractor.PasswordResetListener, AccountDetailInteractor.UserRemoveListener {
    private AccountDetailPresenter.View view;
    private AccountDetailInteractor interactor;

    public AccountDetailPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new AccountDetailInteractorImpl(context);
    }

    @Override
    public void onStart(String userUid) {
        if (view != null) {
            view.showProgress("Loading User Detail", "Please wait...");
        }
        interactor.getUserDetails(userUid, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void sendEmailVerification(String userUid) {
        if (view != null) {
            view.showProgress("Sending Email Verification Link", "Please wait...");
        }
        interactor.sendEmailVerification(userUid, this);
    }

    @Override
    public void sendPasswordReset(String userUid) {
        if (view != null) {
            view.showProgress("Sending Password Reset Link", "Please wait...");
        }
        interactor.sendPasswordReset(userUid, this);
    }

    @Override
    public void setUserRemoved(String userUid) {
        if (view != null) {
            view.showProgress("Removing User", "Please wait...");
        }
        interactor.getUserRemove(userUid, this);
    }

    @Override
    public void onDetailSuccess(Accounts account) {
        if (view != null) {
            view.hideProgress();
            view.setUserDetail(account);
        }
    }

    @Override
    public void onDetailFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onVerificationSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(title, message);
        }
    }

    @Override
    public void onVerificationFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onResetSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(message);
            view.showExitDialog(title, message);
        }
    }

    @Override
    public void onResetFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onRemoveSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(message);
            view.showExitDialog(title, message);
        }
    }

    @Override
    public void onRemoveFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }
}

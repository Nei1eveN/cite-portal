package cp2.citeportalv1.presenters.accounts.admin;

import cp2.citeportalv1.adapters.ModeratorAdapter;

public interface AdminAccountsInteractor {
    void getList(OnAccountsSyncListener listener);
    interface OnAccountsSyncListener {
        void onSuccess(ModeratorAdapter adapter);
        void onFailure(String errorMessage);
    }
}

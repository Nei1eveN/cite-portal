package cp2.citeportalv1.presenters.accounts.secretary;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.ModeratorAdapter;
import cp2.citeportalv1.models.Accounts;

import static cp2.citeportalv1.utils.Constants.ACCOUNTS;

class SecretaryAccountsInteractorImpl implements SecretaryAccountsInteractor {
    private Context context;

    SecretaryAccountsInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getList(OnAccountsSyncListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(ACCOUNTS);
        databaseReference.orderByChild("accessLevel").equalTo("SECRETARY").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Accounts> accounts = new ArrayList<>();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Accounts account = Objects.requireNonNull(snapshot.getValue(Accounts.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        accounts.add(account);
                    }

                    ModeratorAdapter adapter = new ModeratorAdapter(context, accounts);
                    listener.onSuccess(adapter);

                } else {
                    listener.onFailure("There are no Department Secretaries created. Click the icon to create.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFailure(databaseError.getMessage());
            }
        });
    }
}

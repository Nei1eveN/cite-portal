package cp2.citeportalv1.presenters.accounts.detail.create;

public interface AccountCreatePresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
        void showLoginPageDialog(String title, String message);
        void showSnackMessage(String message);
        void setUpReAuthenticationDialog(String title, String message);
    }
    void onDestroy();
    void submitCredentials(String accountID,
                           String firstName, String middleName, String lastName,
                           String departmentSelected,
                           String email, String password,
                           String accessLevel);
    void submitLoginCredentials(String email, String password);
    void userLogOut();
}

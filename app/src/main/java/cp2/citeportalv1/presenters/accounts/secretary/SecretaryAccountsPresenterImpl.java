package cp2.citeportalv1.presenters.accounts.secretary;

import android.content.Context;

import cp2.citeportalv1.adapters.ModeratorAdapter;

public class SecretaryAccountsPresenterImpl implements SecretaryAccountsPresenter, SecretaryAccountsInteractor.OnAccountsSyncListener {
    private SecretaryAccountsPresenter.View view;
    private SecretaryAccountsInteractor interactor;

    public SecretaryAccountsPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new SecretaryAccountsInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.setProgress();
        }
        interactor.getList(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestList() {
        if (view != null) {
            view.setProgress();
        }
        interactor.getList(this);
    }

    @Override
    public void onSuccess(ModeratorAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setList(adapter);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

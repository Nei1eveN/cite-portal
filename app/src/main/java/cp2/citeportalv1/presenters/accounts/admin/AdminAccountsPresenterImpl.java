package cp2.citeportalv1.presenters.accounts.admin;

import android.content.Context;

import cp2.citeportalv1.adapters.ModeratorAdapter;

public class AdminAccountsPresenterImpl implements AdminAccountsPresenter, AdminAccountsInteractor.OnAccountsSyncListener {
    private AdminAccountsPresenter.View view;
    private AdminAccountsInteractor interactor;

    public AdminAccountsPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new AdminAccountsInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.setProgress();
        }
        interactor.getList(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestList() {
        if (view != null) {
            view.setProgress();
        }
        interactor.getList(this);
    }

    @Override
    public void onSuccess(ModeratorAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setList(adapter);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

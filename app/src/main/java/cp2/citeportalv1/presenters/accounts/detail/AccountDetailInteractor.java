package cp2.citeportalv1.presenters.accounts.detail;

import cp2.citeportalv1.models.Accounts;

public interface AccountDetailInteractor {
    interface UserDetailsListener {
        void onDetailSuccess(Accounts account);

        void onDetailFailure(String errorTitle, String errorMessage);
    }

    void getUserDetails(String userUid, UserDetailsListener listener);

    interface EmailVerificationListener {
        void onVerificationSuccess(String title, String message);

        void onVerificationFailure(String errorTitle, String errorMessage);
    }

    void sendEmailVerification(String userUid, EmailVerificationListener listener);

    interface PasswordResetListener {
        void onResetSuccess(String title, String message);

        void onResetFailure(String errorTitle, String errorMessage);
    }

    void sendPasswordReset(String userUid, PasswordResetListener listener);

    interface UserRemoveListener {
        void onRemoveSuccess(String title, String message);

        void onRemoveFailure(String errorTitle, String errorMessage);
    }

    void getUserRemove(String userUid, UserRemoveListener listener);
}

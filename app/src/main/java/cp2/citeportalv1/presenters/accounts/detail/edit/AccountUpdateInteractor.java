package cp2.citeportalv1.presenters.accounts.detail.edit;

import cp2.citeportalv1.models.Accounts;

public interface AccountUpdateInteractor {
    interface UserDetailsListener {
        void onDetailSuccess(Accounts account);
        void onDetailFailure(String errorTitle, String errorMessage);
    }

    void getUserDetails(String userUid, UserDetailsListener listener);

    interface AccountUpdateListener {
        void onUpdateSuccess(String title, String message);
        void onUpdateFailure(String errorTitle, String errorMessage);
    }

    void getCredentials(String userUid,
                        String accountID,
                        String firstName, String middleName, String lastName,
                        String departmentSelected, String accessLevel, AccountUpdateListener listener);
}

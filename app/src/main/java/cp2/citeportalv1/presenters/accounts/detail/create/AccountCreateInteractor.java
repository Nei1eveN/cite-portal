package cp2.citeportalv1.presenters.accounts.detail.create;

public interface AccountCreateInteractor {
    interface AccountCreateListener {
        void onCreateSuccess(String title, String message);
        void onCreateFailure(String errorTitle, String errorMessage);
    }
    void submitCredentials(String accountID,
                           String firstName, String middleName, String lastName,
                           String departmentSelected,
                           String email, String password,
                           String accessLevel, AccountCreateListener listener);

    interface ReAuthenticationListener {
        void onAuthenticationSuccess(String title, String message);
        void onAuthenticationFailure(String errorTitle, String errorMessage);
    }

    void getLoginCredentials(String email, String password, ReAuthenticationListener listener);

    interface UserLogOutListener {
        void onLogOutSuccess(String title, String message);
//        void onLogOutFailure(String errorTitle, String errorMessage);
    }

    void userLogOut(UserLogOutListener listener);
}

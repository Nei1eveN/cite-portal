package cp2.citeportalv1.presenters.accounts.detail.edit;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Accounts;

import static cp2.citeportalv1.utils.Constants.ACCOUNTS;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_ACCESS_LEVEL;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_FIRST_NAME;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_ID;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_LAST_NAME;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_MIDDLE_NAME;
import static cp2.citeportalv1.utils.Constants.DEPARTMENT;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

class AccountUpdateInteractorImpl implements AccountUpdateInteractor {
    private Context context;

    public AccountUpdateInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserDetails(String userUid, UserDetailsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(ACCOUNTS).child(userUid);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Accounts account = Objects.requireNonNull(dataSnapshot.getValue(Accounts.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    listener.onDetailSuccess(account);
                } else {
                    listener.onDetailFailure("Account Detail Error", "User does not exist.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onDetailFailure("Database Error", databaseError.getMessage());
            }
        });
    }

    @Override
    public void getCredentials(String userUid,
                               String accountID,
                               String firstName, String middleName, String lastName,
                               String departmentSelected, String accessLevel, AccountUpdateListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onUpdateFailure("Network Error", "Please check your network connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(ACCOUNTS).child(userUid);
            databaseReference.keepSynced(true);

            Map<String, Object> accountMap = new HashMap<>();
            accountMap.put(ACCOUNT_ACCESS_LEVEL, accessLevel);
            accountMap.put(ACCOUNT_ID, accountID);
            accountMap.put(DEPARTMENT, departmentSelected);
            accountMap.put(ACCOUNT_FIRST_NAME, firstName);
            accountMap.put(ACCOUNT_MIDDLE_NAME, middleName);
            accountMap.put(ACCOUNT_LAST_NAME, lastName);

            databaseReference.updateChildren(accountMap).addOnSuccessListener(((Activity) context), aVoid -> listener.onUpdateSuccess("Account Updated", "Account Successfully Updated.")).addOnFailureListener(((Activity) context), e -> listener.onUpdateFailure("Update Error", e.getMessage()));
        }
    }
}

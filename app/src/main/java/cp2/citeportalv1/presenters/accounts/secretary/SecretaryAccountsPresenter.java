package cp2.citeportalv1.presenters.accounts.secretary;

import cp2.citeportalv1.adapters.ModeratorAdapter;

public interface SecretaryAccountsPresenter {
    interface View {
        void setProgress();
        void hideProgress();
        void setSnackMessage(String message);
        void setEmptyState(String emptyStateMessage);
        void setList(ModeratorAdapter adapter);
    }
    void onStart();
    void onDestroy();
    void requestList();
}

package cp2.citeportalv1.presenters.accounts.detail.edit;

import cp2.citeportalv1.models.Accounts;

public interface AccountUpdatePresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
        void showSnackMessage(String message);
        void setUserDetail(Accounts account);
    }
    void onStart(String userUid);
    void onDestroy();
    void submitCredentials(String userUid, String accountID, String firstName, String middleName, String lastName, String departmentSelected, String accessLevel);
}

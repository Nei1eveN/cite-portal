package cp2.citeportalv1.presenters.accounts.detail.create;

import android.content.Context;

import static cp2.citeportalv1.utils.Constants.ADMIN;
import static cp2.citeportalv1.utils.Constants.SECRETARY;
import static cp2.citeportalv1.utils.Constants.digitCasePattern;
import static cp2.citeportalv1.utils.Constants.lowerCasePattern;
import static cp2.citeportalv1.utils.Constants.specialCharPatten;
import static cp2.citeportalv1.utils.Constants.upperCasePattern;

public class AccountCreatePresenterImpl implements AccountCreatePresenter,
        AccountCreateInteractor.AccountCreateListener, AccountCreateInteractor.ReAuthenticationListener, AccountCreateInteractor.UserLogOutListener {
    private AccountCreatePresenter.View view;
    private AccountCreateInteractor interactor;

    public AccountCreatePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new AccountCreateInteractorImpl(context);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void submitCredentials(String accountID,
                                  String firstName, String middleName, String lastName,
                                  String departmentSelected,
                                  String email, String password,
                                  String accessLevel) {

        if (accountID.isEmpty()) {
            view.showErrorDialog("Empty Employee ID", "Please fill in the required field.");
        } else if (accountID.length() > 6) {
            view.showErrorDialog("Employee ID Length", "Employee ID length cannot be more than 4 digits.");
        } else if (!accountID.contains("Q-")) {
            view.showErrorDialog("Employee ID Error", "Employee ID must contain Q- before the number");
        } else if (!accountID.startsWith("Q-")) {
            view.showErrorDialog("Employee ID Error", "Employee ID must start with Q- before the number");
        } else if (digitCasePattern.matcher(firstName).find()) {
            view.showErrorDialog("Name with Numbers", "Name with Special Characters is not allowed.");
        } else if (digitCasePattern.matcher(middleName).find()) {
            view.showErrorDialog("Name with Numbers", "Name with Special Characters is not allowed.");
        } else if (digitCasePattern.matcher(lastName).find()) {
            view.showErrorDialog("Name with Numbers", "Name with Special Characters is not allowed.");
        } else if (firstName.isEmpty()) {
            view.showErrorDialog("Empty Name", "Please fill in the required field.");
        } else if (middleName.isEmpty()) {
            view.showErrorDialog("Empty Name", "Please fill in the required field.");
        } else if (lastName.isEmpty()) {
            view.showErrorDialog("Empty Name", "Please fill in the required field.");
        } else if (accessLevel.equals(ADMIN) && !departmentSelected.isEmpty()) {
            if (view != null) {
                view.showProgress("Submitting Credentials", "Creating "+accountID+"\n\nPlease wait...");
            }
            interactor.submitCredentials(accountID, firstName, middleName, lastName, "", email, password, ADMIN, this);
        } else if (accessLevel.equals(SECRETARY) && departmentSelected.isEmpty()) {
            view.showErrorDialog("No Department Selected For Secretary Access", "Please select a department");
        } else if (email.isEmpty()) {
            view.showErrorDialog("Empty Email", "Please fill in the required field.");
        } else if (password.isEmpty()) {
            view.showErrorDialog("Empty Password", "Please fill in the required field.");
        } else if (password.length() < 8) {
            view.showErrorDialog("Insufficient Password Length", "Please create a password with at least 8 characters.");
        } else if (!specialCharPatten.matcher(password).find()) {
            view.showErrorDialog("Password Error", "Password must contain Special characters");
        } else if (!upperCasePattern.matcher(password).find()) {
            view.showErrorDialog("Password Error", "Password must contain Upper Case character");
        } else if (!lowerCasePattern.matcher(password).find()) {
            view.showErrorDialog("Password Error", "Password must contain Lower Case character");
        } else if (!digitCasePattern.matcher(password).find()) {
            view.showErrorDialog("Password Error", "Password must contain a Number or Digit character");
        } else {
            if (view != null) {
                view.showProgress("Submitting Credentials", "Creating "+accountID+"\n\nPlease wait...");
            }
            interactor.submitCredentials(accountID, firstName, middleName, lastName, departmentSelected, email, password, accessLevel, this);
        }
    }

    @Override
    public void submitLoginCredentials(String email, String password) {
        if (email.isEmpty()) {
            view.setUpReAuthenticationDialog("Empty Email", "Please fill in the required field.");
        } else if (password.isEmpty()) {
            view.setUpReAuthenticationDialog("Empty Password", "Please fill in the required field.");
        } else {
            if (view != null) {
                view.showProgress("Verifying Account Login", "Please wait...");
            }
            interactor.getLoginCredentials(email, password, this);
        }
    }

    @Override
    public void userLogOut() {
        if (view != null) {
            view.showProgress("User Log Out", "Please wait...");
        }
        interactor.userLogOut(this);
    }

    @Override
    public void onCreateSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.setUpReAuthenticationDialog(title, message);
        }
    }

    @Override
    public void onCreateFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onAuthenticationSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(title, message);
        }
    }

    @Override
    public void onAuthenticationFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showLoginPageDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onLogOutSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showLoginPageDialog(title, message);
        }
    }
}

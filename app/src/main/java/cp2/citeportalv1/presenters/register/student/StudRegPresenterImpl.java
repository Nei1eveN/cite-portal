package cp2.citeportalv1.presenters.register.student;

import android.content.Context;
import android.net.Uri;

import com.google.android.material.textfield.TextInputLayout;

import static cp2.citeportalv1.utils.Constants.digitCasePattern;
import static cp2.citeportalv1.utils.Constants.lowerCasePattern;
import static cp2.citeportalv1.utils.Constants.specialCharPatten;
import static cp2.citeportalv1.utils.Constants.upperCasePattern;

public class StudRegPresenterImpl implements StudRegPresenter, StudRegInteractor.RegisterListener {

    private StudRegPresenter.View view;
    private StudRegInteractor interactor;

    public StudRegPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudRegInteractorImpl(context);
    }

    @Override
    public void onStart() {
        view.initSpinnerItems();
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void submitCredentials(byte[] thumbData, Uri imageURI,
                                  String studentId,
                                  String firstName, String middleName, String lastName,
                                  String email, String password,
                                  String program, String yearLevel,
                                  TextInputLayout studentIdWrapper, TextInputLayout firstNameWrapper,
                                  TextInputLayout middleNameWrapper, TextInputLayout lastNameWrapper,
                                  TextInputLayout emailWrapper, TextInputLayout passwordWrapper) {
        view.nullWrapper(studentIdWrapper);
        view.nullWrapper(firstNameWrapper);
        view.nullWrapper(middleNameWrapper);
        view.nullWrapper(lastNameWrapper);
        view.nullWrapper(emailWrapper);
        view.nullWrapper(passwordWrapper);
        if (thumbData == null) {
            view.setSnackMessage("Please select an image");
        } else if (imageURI == null) {
            view.setSnackMessage("Please select an image");
        } else if (studentId.isEmpty()) {
            view.errorWrapper(studentIdWrapper,"Required Field");
            view.showAlertDialog("Student ID Field Error", "Field cannot be empty.");
        } else if (studentId.length() > 7) {
            view.showAlertDialog("Student ID Field Error", "Student ID length cannot be more than 7 digits.");
            view.errorWrapper(studentIdWrapper, "Student ID length cannot be more than 7 digits.");
            view.setSnackMessage("Student ID length cannot be more than 7 digits.");
        } else if (firstName.isEmpty()) {
            view.showAlertDialog("First Name Field Error", "Field cannot be empty.");
            view.errorWrapper(firstNameWrapper, "Required Field");
        } else if (middleName.isEmpty()) {
            view.showAlertDialog("Middle Name Field Error", "Field cannot be empty.");
            view.errorWrapper(middleNameWrapper, "Required Field");
        } else if (lastName.isEmpty()) {
            view.showAlertDialog("Last Name Field Error", "Field cannot be empty.");
            view.errorWrapper(lastNameWrapper, "Required Field");
        } else if (digitCasePattern.matcher(firstName).find()) {
            view.showAlertDialog("First Name Field Error", "First Name Field must not have numbers.");
            view.errorWrapper(firstNameWrapper, "First Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(middleName).find()) {
            view.showAlertDialog("Middle Name Field Error", "Middle Name Field must not have numbers.");
            view.errorWrapper(middleNameWrapper, "Middle Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(lastName).find()) {
            view.showAlertDialog("Last Name Field Error", "Last Name Field must not have numbers.");
            view.errorWrapper(lastNameWrapper, "Last Name Field must not have numbers");
        } else if (email.isEmpty()) {
            view.showAlertDialog("Email Field Error", "Field cannot be empty.");
            view.errorWrapper(emailWrapper, "Required Field");
        } else if (password.isEmpty()) {
            view.showAlertDialog("Password Field Error", "Field cannot be empty.");
            view.errorWrapper(passwordWrapper, "Required Field");
        } else if (program.isEmpty()) {
            view.showAlertDialog("Program Error", "Please select your Program.");
            view.setSnackMessage("Please choose your Program");
        } else if (yearLevel.isEmpty()) {
            view.showAlertDialog("Year Level Error", "Please select your Year Level.");
            view.setSnackMessage("Please choose your Year Level");
        } else if (password.length() < 8) {
            view.showAlertDialog("Password Error", "Password must have at least 8 characters.");
            view.errorWrapper(passwordWrapper, "Password must have at least 8 characters.");
            view.setSnackMessage("Password must have at least 8 characters");
        } else if (!specialCharPatten.matcher(password).find()) {
            view.showAlertDialog("Password Error", "Password must contain Special characters.");
            view.errorWrapper(passwordWrapper, "Password must contain Special characters");
            view.setSnackMessage("Password must contain Special characters"); //like ~!@#$%^&*_-+=`|\(){}[]:;"'<>,.?/
        } else if (!upperCasePattern.matcher(password).find()) {
            view.showAlertDialog("Password Error", "Password must contain Upper Case character");
            view.errorWrapper(passwordWrapper, "Password must contain Upper Case character");
            view.setSnackMessage("Password must contain Upper Case character");
        } else if (!lowerCasePattern.matcher(password).find()) {
            view.showAlertDialog("Password Error", "Password must contain Lower Case character");
            view.errorWrapper(passwordWrapper, "Password must contain Lower Case character");
            view.setSnackMessage("Password must contain Lower Case character");
        } else if (!digitCasePattern.matcher(password).find()) {
            view.showAlertDialog("Password Error", "Password must contain a Number or Digit character.");
            view.errorWrapper(passwordWrapper, "Password must contain a Number or Digit character");
            view.setSnackMessage("Password must contain a Number or Digit character");
        } else {
            view.showProgress("Submitting credentials", "Please wait...");
            interactor.registerCredentials(thumbData, imageURI, studentId, firstName, middleName, lastName, email, password, program, yearLevel, this);
        }
    }

    @Override
    public void onSuccess(String successMessage) {
        view.hideProgress();
        view.setToastMessage(successMessage);
    }

    @Override
    public void onFailed(String errorMessage) {
        view.hideProgress();
        view.setSnackMessage(errorMessage);
    }
}

package cp2.citeportalv1.presenters.register.faculty;

import android.net.Uri;

import com.google.android.material.textfield.TextInputLayout;

public interface FacRegPresenter {
    interface FacultyRegisterView {
        void showProgress(String title, String message);
        void hideProgress();
        void setToastMessage(String message);
        void snackMessage(String message);
        void initSpinnerItems();
        void errorWrapper(TextInputLayout textInputLayout, String message);
        void nullWrapper(TextInputLayout textInputLayout);
        void showAlertDialog(String title, String message);
    }
    void onStart();
    void onDestroy();
    void submitCredentials(byte[] thumbData, Uri imageURI, String employeeId, String firstName, String middleName,
                           String lastName, String email, String password, String department,
                           TextInputLayout employeeIdWrapper, TextInputLayout firstNameWrapper,
                           TextInputLayout middleNameWrapper, TextInputLayout lastNameWrapper,
                           TextInputLayout emailWrapper, TextInputLayout passwordWrapper);
}

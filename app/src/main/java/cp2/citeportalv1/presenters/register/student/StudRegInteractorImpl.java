package cp2.citeportalv1.presenters.register.student;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.utils.Constants;
import cp2.citeportalv1.utils.FileUtil;
import cp2.citeportalv1.views.student.login_register.StudentLoginActivity;

import static cp2.citeportalv1.utils.Constants.*;

public class StudRegInteractorImpl implements StudRegInteractor {

    private Context context;
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
    private StorageReference storageReference = firebaseStorage.getReference();

    StudRegInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void registerCredentials(byte[] thumbData, Uri imageURL, String studentId,
                                    String firstName, String middleName, String lastName,
                                    String email, String password,
                                    String program, String yearLevel,
                                    RegisterListener listener) {
        if (Constants.isNetworkAvailable(context)) {
            firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                    assert firebaseUser != null;

                    firebaseUser.sendEmailVerification().addOnCompleteListener(task1 -> {
                                StorageReference rootReference = storageReference.child(STUDENT_USERS).child(PROFILE_IMAGES)
                                        .child(firebaseUser.getUid());
                                rootReference.putBytes(thumbData).addOnCompleteListener(task2 -> {
                                    if (task2.isSuccessful()) {
                                        rootReference.getDownloadUrl().addOnSuccessListener(uri -> {
                                            Log.d("imageLink", uri.toString());
                                            Log.d("imageSize", FileUtil.getReadableFileSize(thumbData.length));
                                            uploadToFirebase(uri, studentId, firstName, middleName, lastName, email, password, program, yearLevel, listener);
                                        });
                                    } else {
                                        listener.onFailed(Objects.requireNonNull(task2.getException()).getMessage());
                                    }
                                });
                            }
                    );
                } else {
                    listener.onFailed(Objects.requireNonNull(task.getException()).getMessage());
                }
            });
        } else {
            listener.onFailed("Please check your internet connection");
        }
    }

    @Override
    public void uploadToFirebase(Uri uri, String studentId, String firstName, String middleName, String lastName, String email, String password, String program, String yearLevel, RegisterListener listener) {
        Log.d("interactor--imageURL", uri.toString());
        Log.d("interactor--studentID", studentId);
        Log.d("interactor--studentName", firstName + " " + middleName + " " + lastName);
        Log.d("interactor--program", program + " - " + yearLevel);

        Student student = new Student();
        student.setStudentImageURL(uri.toString());
        student.setStudentID(Long.valueOf(studentId));
        student.setfName(firstName);
        student.setMidName(middleName);
        student.setLastName(lastName);
        student.setProgram(program);
        student.setYearLvl(yearLevel);
        student.setEmail(email);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(((Activity) context), instanceIdResult -> {
            Map<String, Object> studentMap = new HashMap<>();
            studentMap.put(EMAIL, student.getEmail());
            studentMap.put(FIRST_NAME, student.getfName());
            studentMap.put(LAST_NAME, student.getLastName());
            studentMap.put(MID_NAME, student.getMidName());
            studentMap.put(PROGRAM, student.getProgram());
            studentMap.put(STUDENT_ID, student.getStudentID());
            studentMap.put(STUDENT_IMAGE_URL, student.getStudentImageURL());
//            studentMap.put(TOKEN_ID, instanceIdResult.getToken());
            studentMap.put(YEAR_LVL, student.getYearLvl());
            studentMap.put(VALID, false);

            FirebaseDatabase.getInstance().getReference(STUDENTS)
                    .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                    .setValue(studentMap).addOnCompleteListener(task11 -> {
                if (task11.isSuccessful()) {
                    listener.onSuccess("Account Creation Successful. Please check your email to verify your account.");
                    FirebaseAuth.getInstance().signOut();
                    Intent intent = new Intent(context, StudentLoginActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                } else {
                    listener.onFailed(Objects.requireNonNull(task11.getException()).getMessage());
                    Log.d("regError", task11.getException().getMessage());
                    Log.d("sameID", "possible duplicate ID in database");
                }
            });
        });
    }
}

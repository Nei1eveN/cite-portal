package cp2.citeportalv1.presenters.register.faculty;

import android.net.Uri;

public interface FacRegInteractor {
    void registerFacultyCredentials(byte[] thumbData, Uri imageURI,
                                    String employeeId,
                                    String firstName, String middleName, String lastName,
                                    String department,
                                    String email, String password,
                                    FacultyRegisterListener listener);
    void uploadToFirebase(Uri imageURI, String employeeId, String firstName, String middleName,
                          String lastName, String department, String email, String password, FacultyRegisterListener listener);
    interface FacultyRegisterListener {
        void onSuccess(String successMessage);
        void onFailure(String errorMessage);
    }
}

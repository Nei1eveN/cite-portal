package cp2.citeportalv1.presenters.register.student;

import android.net.Uri;

import com.google.android.material.textfield.TextInputLayout;

public interface StudRegPresenter {
    interface View{
        void showProgress(String title, String caption);
        void hideProgress();
        void initSpinnerItems();
        void errorWrapper(TextInputLayout textInputLayout, String message);
        void nullWrapper(TextInputLayout textInputLayout);
        void setSnackMessage(String message);
        void setToastMessage(String message);
        void showAlertDialog(String title, String message);
    }
    void onStart();
    void onDestroy();
    void submitCredentials(byte[] thumbData, Uri imageURI, String studentId, String firstName, String middleName,
                           String lastName, String email, String password, String program, String yearLevel,
                           TextInputLayout studentIdWrapper, TextInputLayout firstNameWrapper, TextInputLayout middleNameWrapper,
                           TextInputLayout lastNameWrapper, TextInputLayout emailWrapper, TextInputLayout passwordWrapper);
}

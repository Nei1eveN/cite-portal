package cp2.citeportalv1.presenters.register.student;

import android.net.Uri;

import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.UploadTask;

public interface StudRegInteractor {
    void registerCredentials(byte[] thumbData, Uri imageURI, String studentId, String firstName, String middleName,
                             String lastName, String program, String yearLevel, String email, String password, RegisterListener listener);
    void uploadToFirebase(Uri uri, String studentId, String firstName, String middleName,
                          String lastName, String program, String yearLevel, String email, String password, RegisterListener listener);
    interface RegisterListener{
        void onSuccess(String successMessage);
        void onFailed(String errorMessage);
    }
}

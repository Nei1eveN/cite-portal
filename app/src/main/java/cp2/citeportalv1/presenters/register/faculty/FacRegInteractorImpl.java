package cp2.citeportalv1.presenters.register.faculty;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.utils.Constants;
import cp2.citeportalv1.views.faculty.FacultyLoginActivity;

import static cp2.citeportalv1.utils.Constants.*;

public class FacRegInteractorImpl implements FacRegInteractor {
    private Context context;

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
    private StorageReference storageReference = firebaseStorage.getReference();


    FacRegInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void registerFacultyCredentials(byte[] thumbData, Uri imageURI,
                                           String employeeId,
                                           String firstName, String middleName, String lastName,
                                           String department,
                                           String email, String password,
                                           FacultyRegisterListener listener) {
        if (Constants.isNetworkAvailable(context)) {
            Log.d("interactor--cred1", "empId: " + employeeId + " / fName: " + firstName + " / midName: " + middleName + " / lastName: " + lastName);
            Log.d("interactor--cred2", "department: " + department + " / email: " + email + " / password: " + password);
            Log.d("interactor--FileBytes", "" + thumbData.length);
            firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                    assert firebaseUser != null;
                    firebaseUser.sendEmailVerification().addOnCompleteListener(task12 -> {
                        StorageReference rootReference = storageReference.child(FACULTY_USERS).child(PROFILE_IMAGES)
                                .child(firebaseUser.getUid());
                        rootReference.putBytes(thumbData).addOnCompleteListener(task1 -> {
                            if (task1.isSuccessful()) {
                                rootReference.getDownloadUrl().addOnSuccessListener(uri -> uploadToFirebase(uri, employeeId, firstName, middleName, lastName, department, email, password, listener));
                            } else {
                                listener.onFailure(Objects.requireNonNull(task1.getException()).getMessage());
                            }
                        });
                    });
                } else {
                    listener.onFailure(Objects.requireNonNull(task.getException()).getMessage());
                }
            });
        } else {
            listener.onFailure("Please connect to the internet");
        }
    }

    @Override
    public void uploadToFirebase(Uri imageURI, String employeeId, String firstName, String middleName, String lastName, String department, String email, String password, FacultyRegisterListener listener) {
        Log.d("interactor--imageURL", imageURI.toString());
        Log.d("interactor--studentID", employeeId);
        Log.d("interactor--studentName", firstName + " " + middleName + " " + lastName);
        Log.d("interactor--department", department);

        Faculty faculty = new Faculty();
        faculty.setFacultyImageURL(imageURI.toString());
        faculty.setEmployeeID(employeeId);
        faculty.setfName(firstName);
        faculty.setMidName(middleName);
        faculty.setLastName(lastName);
        faculty.setDepartment(department);
        faculty.setEmail(email);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(((android.app.Activity) context), instanceIdResult -> {
            Map<String, Object> facultyMap = new HashMap<>();
            facultyMap.put(DEPARTMENT, department);
            facultyMap.put(EMAIL, email);
            facultyMap.put(EMPLOYEE_ID, employeeId);
            facultyMap.put(FIRST_NAME, firstName);
            facultyMap.put(FACULTY_IMAGE_URL, imageURI.toString());
            facultyMap.put(LAST_NAME, lastName);
            facultyMap.put(MID_NAME, middleName);
//            facultyMap.put(TOKEN_ID, instanceIdResult.getToken());
            facultyMap.put(VALID, false);

            FirebaseDatabase.getInstance().getReference(FACULTY)
                    .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                    .setValue(facultyMap).addOnCompleteListener(task1 -> {
                if (task1.isSuccessful()) {
                    Log.d("adding to Faculty", employeeId + " added");
                    listener.onSuccess("Account Creation Successful. Please check your email to verify your account.");
                    FirebaseAuth.getInstance().signOut();
                    Intent intent = new Intent(context, FacultyLoginActivity.class);
                    context.startActivity(intent);
                    ((android.app.Activity) context).finish();
                } else {
                    listener.onFailure(Objects.requireNonNull(task1.getException()).getMessage());
                }
            });
        });
    }
}

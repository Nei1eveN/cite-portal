package cp2.citeportalv1.presenters.register.faculty;

import android.content.Context;
import android.net.Uri;

import com.google.android.material.textfield.TextInputLayout;

import static cp2.citeportalv1.utils.Constants.digitCasePattern;
import static cp2.citeportalv1.utils.Constants.lowerCasePattern;
import static cp2.citeportalv1.utils.Constants.specialCharPatten;
import static cp2.citeportalv1.utils.Constants.upperCasePattern;

public class FacRegPresenterImpl implements FacRegPresenter, FacRegInteractor.FacultyRegisterListener {
    private FacRegPresenter.FacultyRegisterView view;
    private FacRegInteractor interactor;

    public FacRegPresenterImpl(FacultyRegisterView view, Context context) {
        this.view = view;
        this.interactor = new FacRegInteractorImpl(context);
    }

    @Override
    public void onStart() {
        view.initSpinnerItems();
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void submitCredentials(byte[] thumbData, Uri imageURI, String employeeId, String firstName, String middleName, String lastName, String email, String password, String department, TextInputLayout employeeIdWrapper, TextInputLayout firstNameWrapper, TextInputLayout middleNameWrapper, TextInputLayout lastNameWrapper, TextInputLayout emailWrapper, TextInputLayout passwordWrapper) {
        view.nullWrapper(employeeIdWrapper);
        view.nullWrapper(firstNameWrapper);
        view.nullWrapper(middleNameWrapper);
        view.nullWrapper(lastNameWrapper);
        view.nullWrapper(emailWrapper);
        view.nullWrapper(passwordWrapper);
        if (thumbData == null) {
            view.snackMessage("Please select an image");
        } else if (employeeId.isEmpty()) {
            view.errorWrapper(employeeIdWrapper, "Required Field");
            view.showAlertDialog("Employee ID Field Error", "Field cannot be empty.");
        } else if (employeeId.length() > 6) {
            view.showAlertDialog("Employee ID Field Error", "Employee ID length cannot be more than 4 digits.");
            view.errorWrapper(employeeIdWrapper, "Employee ID length cannot be more than 4 digits.");
            view.snackMessage("Employee ID length cannot be more than 4 digits.");
        } else if (!employeeId.contains("Q-")) {
            view.showAlertDialog("Employee ID Error", "Employee ID must contain Q- before the number");
        } else if (!employeeId.startsWith("Q-")) {
            view.showAlertDialog("Employee ID Error", "Employee ID must start with Q- before the number");
        }
        else if (firstName.isEmpty()) {
            view.errorWrapper(firstNameWrapper, "Required Field");
            view.showAlertDialog("First Name Field Error", "Field cannot be empty.");
        } else if (middleName.isEmpty()) {
            view.errorWrapper(middleNameWrapper, "Required Field");
            view.showAlertDialog("Middle Name Field Error", "Field cannot be empty.");
        } else if (lastName.isEmpty()) {
            view.errorWrapper(lastNameWrapper, "Required Field");
            view.showAlertDialog("Last Name Field Error", "Field cannot be empty.");
        } else if (digitCasePattern.matcher(firstName).find()) {
            view.showAlertDialog("First Name Field Error", "First Name Field must not have numbers.");
            view.errorWrapper(firstNameWrapper, "First Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(middleName).find()) {
            view.showAlertDialog("Middle Name Field Error", "Middle Name Field must not have numbers.");
            view.errorWrapper(middleNameWrapper, "Middle Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(lastName).find()) {
            view.showAlertDialog("Last Name Field Error", "Last Name Field must not have numbers.");
            view.errorWrapper(lastNameWrapper, "Last Name Field must not have numbers");
        } else if (email.isEmpty()) {
            view.errorWrapper(emailWrapper, "Required Field");
            view.showAlertDialog("Email Field Error", "Field cannot be empty.");
        } else if (password.isEmpty()) {
            view.showAlertDialog("Password Error", "Field cannot be empty.");
            view.errorWrapper(passwordWrapper, "Required Field");
        } else if (department.isEmpty()) {
            view.snackMessage("Please choose your Department");
            view.showAlertDialog("Department Error", "Department cannot be empty.");
        } else if (password.length() < 8) {
            view.showAlertDialog("Password Error", "Password must have at least 8 characters.");
            view.errorWrapper(passwordWrapper, "Password must have at least 8 characters.");
            view.snackMessage("Password must have at least 8 characters");
        } else if (!specialCharPatten.matcher(password).find()) {
            view.showAlertDialog("Password Error", "Password must contain Special characters");
            view.errorWrapper(passwordWrapper, "Password must contain Special characters");
            view.snackMessage("Password must contain Special characters"); //like ~!@#$%^&*_-+=`|\(){}[]:;"'<>,.?/
        } else if (!upperCasePattern.matcher(password).find()) {
            view.showAlertDialog("Password Error", "Password must contain Upper Case character");
            view.errorWrapper(passwordWrapper, "Password must contain Upper Case character");
            view.snackMessage("Password must contain Upper Case character");
        } else if (!lowerCasePattern.matcher(password).find()) {
            view.showAlertDialog("Password Error", "Password must contain Lower Case character");
            view.errorWrapper(passwordWrapper, "Password must contain Lower Case character");
            view.snackMessage("Password must contain Lower Case character");
        } else if (!digitCasePattern.matcher(password).find()) {
            view.showAlertDialog("Password Error", "Password must contain a Number or Digit character");
            view.errorWrapper(passwordWrapper,"Password must contain a Number or Digit character");
            view.snackMessage("Password must contain a Number or Digit character");
        } else {
            view.showProgress("Submitting Credentials", "Creating "+employeeId+"\n\nPlease wait...");
            interactor.registerFacultyCredentials(thumbData, imageURI,
                    employeeId,
                    firstName,
                    middleName,
                    lastName,
                    department,
                    email,
                    password, this);
        }
    }

    @Override
    public void onSuccess(String successMessage) {
        view.hideProgress();
        view.setToastMessage(successMessage);
    }

    @Override
    public void onFailure(String errorMessage) {
        view.hideProgress();
        view.snackMessage(errorMessage);
    }
}

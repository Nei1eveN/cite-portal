package cp2.citeportalv1.presenters.home;

import android.content.Context;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public class FacultyHomePresenterImpl implements FacultyHomePresenter, FacultyHomeInteractor.FacultyHomeListener {
    private FacultyHomePresenter.View view;
    private FacultyHomeInteractor interactor;

    public FacultyHomePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyHomeInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getPages(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void onPageSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null) {
            view.loadPages(viewPagerAdapter, numberOfPages);
        }
    }
}

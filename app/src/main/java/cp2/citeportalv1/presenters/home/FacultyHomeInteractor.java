package cp2.citeportalv1.presenters.home;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface FacultyHomeInteractor {
    interface FacultyHomeListener {
        void onPageSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
//        void onPageFailure(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }

    void getPages(FacultyHomeListener daysListener);
}

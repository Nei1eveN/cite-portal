package cp2.citeportalv1.presenters.home.student;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface StudentHomeInteractor {
    interface StudentHomeListener {
        void onPageSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
//        void onPageFailure(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }

    void getPages(StudentHomeListener daysListener);
}

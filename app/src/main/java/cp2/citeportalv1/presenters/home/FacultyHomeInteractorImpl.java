package cp2.citeportalv1.presenters.home;

import android.content.Context;
import android.os.Bundle;

import java.util.Date;

import androidx.fragment.app.FragmentActivity;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.views.faculty.FacultyConsultationRequestLogFragment;
import cp2.citeportalv1.views.faculty.FacultyDayAppointmentFragment;

import static cp2.citeportalv1.utils.Constants.SCHEDULE_DAY;
import static cp2.citeportalv1.utils.Constants.dayFormat;

class FacultyHomeInteractorImpl implements FacultyHomeInteractor {
    private Context context;

    FacultyHomeInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getPages(FacultyHomeListener daysListener) {
        Bundle bundle = new Bundle();
        bundle.putString(SCHEDULE_DAY, dayFormat.format(new Date()));

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(((FragmentActivity) context).getSupportFragmentManager());

        FacultyDayAppointmentFragment appointmentFragment = new FacultyDayAppointmentFragment();
        appointmentFragment.setArguments(bundle);

        viewPagerAdapter.addFragment(appointmentFragment, "Appointments");
        viewPagerAdapter.addFragment(new FacultyConsultationRequestLogFragment(), "Consultation Requests");
        daysListener.onPageSuccess(viewPagerAdapter, 2);
    }
}

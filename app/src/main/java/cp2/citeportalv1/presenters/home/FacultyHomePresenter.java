package cp2.citeportalv1.presenters.home;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface FacultyHomePresenter {
    interface View {
        void loadPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }
    void onStart();
    void onDestroy();
}

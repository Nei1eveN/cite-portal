package cp2.citeportalv1.presenters.home.student;

import android.content.Context;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public class StudentHomePresenterImpl implements StudentHomePresenter, StudentHomeInteractor.StudentHomeListener {
    private StudentHomePresenter.View view;
    private StudentHomeInteractor interactor;

    public StudentHomePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentHomeInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getPages(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void onPageSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null) {
            view.loadPages(viewPagerAdapter, numberOfPages);
        }
    }
}

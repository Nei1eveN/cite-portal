package cp2.citeportalv1.presenters.home.student;

import android.content.Context;
import android.os.Bundle;

import java.util.Date;

import androidx.fragment.app.FragmentActivity;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.views.student.appointment.StudentDayAppointmentFragment;
import cp2.citeportalv1.views.student.classmate.ClassmateRequestFragment;

import static cp2.citeportalv1.utils.Constants.SCHEDULE_DAY;
import static cp2.citeportalv1.utils.Constants.dayFormat;

class StudentHomeInteractorImpl implements StudentHomeInteractor {
    private Context context;

    StudentHomeInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getPages(StudentHomeListener daysListener) {
        Bundle bundle = new Bundle();
        bundle.putString(SCHEDULE_DAY, dayFormat.format(new Date()));

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(((FragmentActivity) context).getSupportFragmentManager());

        StudentDayAppointmentFragment dayAppointmentFragment = new StudentDayAppointmentFragment();
        dayAppointmentFragment.setArguments(bundle);

        viewPagerAdapter.addFragment(dayAppointmentFragment, "Appointments");
        viewPagerAdapter.addFragment(new ClassmateRequestFragment(), "Schoolmate Requests");
        daysListener.onPageSuccess(viewPagerAdapter, 2);
    }
}

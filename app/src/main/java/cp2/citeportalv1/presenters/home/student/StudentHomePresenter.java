package cp2.citeportalv1.presenters.home.student;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface StudentHomePresenter {
    interface View {
        void loadPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }
    void onStart();
    void onDestroy();
}

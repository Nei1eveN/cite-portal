package cp2.citeportalv1.presenters.base.faculty;

import cp2.citeportalv1.models.Faculty;

public interface FacultyBaseInteractor {
    void getData(OnDataListener listener);
    void userLogout(OnUserLogoutListener listener);
    interface OnDataListener{
//        void onSuccessDataChanged(String lastName, String firstName, String midName, String department, String imageURI);
        void onSuccessDataChanged(Faculty faculty);
        void onFailureDataChange(String errorMessage);
    }
    interface OnUserLogoutListener {
        void onSuccessLogout(String successMessage);
        void onFailureLogout(String errorMessage);
    }
}

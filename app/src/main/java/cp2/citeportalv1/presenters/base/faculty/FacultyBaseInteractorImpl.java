package cp2.citeportalv1.presenters.base.faculty;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.utils.Constants;
import cp2.citeportalv1.views.faculty.FacultyLoginActivity;

import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.TOKEN_ID;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

public class FacultyBaseInteractorImpl implements FacultyBaseInteractor {
    private Context context;

    FacultyBaseInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getData(OnDataListener listener) {
        try {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference().child(Constants.FACULTY) //;
                    .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            databaseReference.keepSynced(true);
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Faculty faculty = dataSnapshot.getValue(Faculty.class);

                        Log.d("interactor--changeData", "changing data from console...");
                        listener.onSuccessDataChanged(faculty);
                    } else {
                        listener.onFailureDataChange("Your information does not exist. Please contact Help Support.");
                        FirebaseAuth.getInstance().signOut();
                        context.startActivity(new Intent(context, FacultyLoginActivity.class));
                        ((android.app.Activity) context).finish();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.d("dbError", databaseError.getDetails());
                    listener.onFailureDataChange(databaseError.getMessage());
                    FirebaseAuth.getInstance().signOut();
                    context.startActivity(new Intent(context, FacultyLoginActivity.class));
                    ((android.app.Activity) context).finish();
                }
            });
        }
        catch (NullPointerException e) {
            FirebaseAuth.getInstance().signOut();
            context.startActivity(new Intent(context, FacultyLoginActivity.class));
            ((android.app.Activity) context).finish();
        }

    }

    @Override
    public void userLogout(OnUserLogoutListener listener) {
        if (isNetworkAvailable(context)) {
            FirebaseDatabase
                    .getInstance().getReference(FACULTY)
                    .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                    .child(TOKEN_ID).removeValue();

            listener.onSuccessLogout("User logged out");
            FirebaseAuth.getInstance().signOut();
            context.startActivity(new Intent(context, FacultyLoginActivity.class));
            ((android.app.Activity) context).finish();
        }
        else {
            listener.onFailureLogout("Please check your internet connection before logging out");
        }
    }
}

package cp2.citeportalv1.presenters.base.student;

import android.content.Context;

import cp2.citeportalv1.models.Student;

public class StudentBasePresenterImpl implements StudentBasePresenter, StudentBaseInteractor.OnDataListener, StudentBaseInteractor.OnUserLogoutListener {
    private StudentBasePresenter.View view;
    private StudentBaseInteractor studentBaseInteractor;

    public StudentBasePresenterImpl(View view, Context context) {
        this.view = view;
        this.studentBaseInteractor = new StudentBaseInteractorImpl(context);
    }

    @Override
    public void onStart() {
        studentBaseInteractor.getData(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestLogout() {
        if (view != null) {
            view.showProgress();
        }
        studentBaseInteractor.userLogout(this);
    }

    @Override
    public void onSuccessDataChanged(Student student) {
        if (view != null) {
            view.setUserInfo(student);
        }
    }

    @Override
    public void onFailureDataChange(String errorMessage) {
        if (view != null) {
            view.setToastMessage(errorMessage);
        }
    }

    @Override
    public void onSuccessLogout(String successMessage) {
        if (view != null) {
            view.hideProgress();
            view.setToastMessage(successMessage);
        }
    }

    @Override
    public void onFailureLogout(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setToastMessage(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.base.admin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Accounts;
import cp2.citeportalv1.views.guest.GuestViewActivity;

import static cp2.citeportalv1.utils.Constants.ACCOUNTS;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

class AdminBaseInteractorImpl implements AdminBaseInteractor {
    private Context context;

    AdminBaseInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getData(OnDataListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(ACCOUNTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Accounts account = dataSnapshot.getValue(Accounts.class);

                    Log.d("interactor--changeData", "changing data from console...");
                    listener.onSuccessDataChanged(account);
                } else {
                    listener.onFailureDataChange("Your information does not exist. Please contact Help Support.");
                    FirebaseAuth.getInstance().signOut();
                    context.startActivity(new Intent(context, GuestViewActivity.class));
                    ((Activity) context).finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("dbError", databaseError.getDetails());
                listener.onFailureDataChange(databaseError.getMessage());
                FirebaseAuth.getInstance().signOut();
                context.startActivity(new Intent(context, GuestViewActivity.class));
                ((Activity) context).finish();
            }
        });
    }

    @Override
    public void userLogout(OnUserLogoutListener listener) {
        if (isNetworkAvailable(context)) {
            FirebaseAuth.getInstance().signOut();
            listener.onSuccessLogout("User logged out");
            context.startActivity(new Intent(context, GuestViewActivity.class));
            ((Activity) context).finish();
        }
        else {
            listener.onFailureLogout("Please check your internet connection before logging out");
        }
    }
}

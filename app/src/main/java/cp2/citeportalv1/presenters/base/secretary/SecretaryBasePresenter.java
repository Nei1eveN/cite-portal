package cp2.citeportalv1.presenters.base.secretary;

import cp2.citeportalv1.models.Accounts;

public interface SecretaryBasePresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setUserInfo(Accounts account);
        void setToastMessage(String message);
    }
    void onStart();
    void onDestroy();
    void requestLogout();
}

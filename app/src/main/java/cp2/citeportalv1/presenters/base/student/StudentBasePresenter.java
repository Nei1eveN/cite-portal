package cp2.citeportalv1.presenters.base.student;

import cp2.citeportalv1.models.Student;

public interface StudentBasePresenter {
    interface View{
        void showProgress();
        void hideProgress();
        void setUserInfo(Student student);
        void setToastMessage(String message);
    }
    void onStart();
    void onDestroy();
    void requestLogout();
}

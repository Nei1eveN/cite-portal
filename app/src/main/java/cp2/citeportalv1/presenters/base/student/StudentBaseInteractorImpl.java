package cp2.citeportalv1.presenters.base.student;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.utils.Constants;
import cp2.citeportalv1.views.student.home.StudHomeActivity;
import cp2.citeportalv1.views.student.login_register.StudentLoginActivity;

import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

public class StudentBaseInteractorImpl implements StudentBaseInteractor {

    private Context context;

    StudentBaseInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getData(OnDataListener listener) {
        try {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference().child(Constants.STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            databaseReference.keepSynced(true);
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Student student = dataSnapshot.getValue(Student.class);

                        Log.d("interactor--changeData", "changing data from console...");
                        try {
                            listener.onSuccessDataChanged(student);
                        } catch (NullPointerException e) {
                            Toast.makeText(context, "Some data has been changed. Heading back to home page.", Toast.LENGTH_SHORT).show();
                            context.startActivity(new Intent(context, StudHomeActivity.class));
                            ((Activity) context).finish();
                        }
                    } else {
                        listener.onFailureDataChange("Your information does not exist. Please contact Help Support.");
                        FirebaseAuth.getInstance().signOut();
                        context.startActivity(new Intent(context, StudentLoginActivity.class));
                        ((Activity) context).finish();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.d("dbError", databaseError.getDetails());
                }
            });
        } catch (NullPointerException e) {
            listener.onFailureDataChange("Your information does not exist. Please contact Help Support.");
            FirebaseAuth.getInstance().signOut();
            context.startActivity(new Intent(context, StudentLoginActivity.class));
            ((Activity) context).finish();
        }
    }

    @Override
    public void userLogout(OnUserLogoutListener listener) {
        if (isNetworkAvailable(context)) {
            FirebaseDatabase
                    .getInstance().getReference(STUDENTS)
                    .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                    .child("tokenId").removeValue();

            listener.onSuccessLogout("User logged out");
            FirebaseAuth.getInstance().signOut();
            context.startActivity(new Intent(context, StudentLoginActivity.class));
            ((Activity) context).finish();

        } else {
            listener.onFailureLogout("Please check your internet connection before logging out");
        }
    }
}

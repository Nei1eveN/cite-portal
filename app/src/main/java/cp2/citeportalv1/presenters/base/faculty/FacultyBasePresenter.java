package cp2.citeportalv1.presenters.base.faculty;

import cp2.citeportalv1.models.Faculty;

public interface FacultyBasePresenter {
    interface FacultyBaseView {
        void showProgress();
        void hideProgress();
//        void setUserInfo(String lastName, String firstName, String midName, String department, String facultyImageURI);
        void setUserInfo(Faculty faculty);
        void setToastMessage(String message);
    }
    void onStart();
    void onDestroy();
    void requestLogout();
}

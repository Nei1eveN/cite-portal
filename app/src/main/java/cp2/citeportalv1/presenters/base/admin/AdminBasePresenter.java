package cp2.citeportalv1.presenters.base.admin;

import cp2.citeportalv1.models.Accounts;

public interface AdminBasePresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setUserInfo(Accounts account);
        void setToastMessage(String message);
    }
    void onStart();
    void onDestroy();
    void requestLogout();
}

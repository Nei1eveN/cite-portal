package cp2.citeportalv1.presenters.base.student;

import cp2.citeportalv1.models.Student;

public interface StudentBaseInteractor {
    void getData(OnDataListener listener);
    void userLogout(OnUserLogoutListener listener);
    interface OnDataListener{
        void onSuccessDataChanged(Student student);
        void onFailureDataChange(String errorMessage);
    }

    interface OnUserLogoutListener {
        void onSuccessLogout(String successMessage);
        void onFailureLogout(String errorMessage);
    }
}

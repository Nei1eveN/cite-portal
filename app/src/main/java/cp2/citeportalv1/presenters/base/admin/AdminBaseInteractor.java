package cp2.citeportalv1.presenters.base.admin;

import cp2.citeportalv1.models.Accounts;

public interface AdminBaseInteractor {
    void getData(OnDataListener listener);
    void userLogout(OnUserLogoutListener listener);
    interface OnDataListener{
        void onSuccessDataChanged(Accounts account);
        void onFailureDataChange(String errorMessage);
    }
    interface OnUserLogoutListener {
        void onSuccessLogout(String successMessage);
        void onFailureLogout(String errorMessage);
    }
}

package cp2.citeportalv1.presenters.base.faculty;

import android.content.Context;

import cp2.citeportalv1.models.Faculty;

public class FacultyBasePresenterImpl implements FacultyBasePresenter, FacultyBaseInteractor.OnDataListener, FacultyBaseInteractor.OnUserLogoutListener {
    private FacultyBasePresenter.FacultyBaseView view;
    private FacultyBaseInteractor interactor;

    public FacultyBasePresenterImpl(FacultyBaseView view, Context context) {
        this.view = view;
        this.interactor = new FacultyBaseInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getData(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestLogout() {
        if (view != null) {
            view.showProgress();
        }
        interactor.userLogout(this);
    }

    @Override
    public void onSuccessDataChanged(Faculty faculty) {
        if (view != null){
            view.setUserInfo(faculty);
        }
    }

    @Override
    public void onFailureDataChange(String errorMessage) {
        if (view != null){
            view.setToastMessage(errorMessage);
        }

    }

    @Override
    public void onSuccessLogout(String successMessage) {
        if (view != null){
            view.hideProgress();
            view.setToastMessage(successMessage);
        }
    }

    @Override
    public void onFailureLogout(String errorMessage) {
        if (view != null){
            view.hideProgress();
            view.setToastMessage(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.base.admin;

import android.content.Context;

import cp2.citeportalv1.models.Accounts;

public class AdminBasePresenterImpl implements AdminBasePresenter, AdminBaseInteractor.OnDataListener, AdminBaseInteractor.OnUserLogoutListener {
    private View view;
    private AdminBaseInteractor interactor;

    public AdminBasePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new AdminBaseInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getData(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestLogout() {
        if (view != null) {
            view.showProgress();
        }
        interactor.userLogout(this);
    }

    @Override
    public void onSuccessDataChanged(Accounts account) {
        if (view != null){
            view.setUserInfo(account);
        }
    }

    @Override
    public void onFailureDataChange(String errorMessage) {
        if (view != null){
            view.setToastMessage(errorMessage);
        }
    }

    @Override
    public void onSuccessLogout(String successMessage) {
        if (view != null){
            view.hideProgress();
            view.setToastMessage(successMessage);
        }
    }

    @Override
    public void onFailureLogout(String errorMessage) {
        if (view != null){
            view.hideProgress();
            view.setToastMessage(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.splash;

public interface SplashPresenter {
    interface SplashView {
        void checkUser();
        void setToastMessage(String message);
    }
    void onStart();
    void onDestroy();
    void checkUser();
}

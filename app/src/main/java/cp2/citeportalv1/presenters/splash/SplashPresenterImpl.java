package cp2.citeportalv1.presenters.splash;

import android.content.Context;

public class SplashPresenterImpl implements SplashPresenter, SplashInteractor.SplashOnSyncListener {
    private SplashPresenter.SplashView splashView;
    private SplashInteractor splashInteractor;

    public SplashPresenterImpl(SplashView splashView, Context context) {
        this.splashView = splashView;
        this.splashInteractor = new SplashInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (splashView != null) {
            splashView.checkUser();
        }
    }

    @Override
    public void onDestroy() {
        if (splashView != null) {
            splashView = null;
        }
    }

    @Override
    public void checkUser() {
        splashInteractor.getUser(this);
    }

    @Override
    public void onSuccess(String userEmail) {
        if (splashView != null) {
            splashView.setToastMessage(userEmail);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (splashView != null) {
            splashView.setToastMessage(errorMessage);
        }
    }
}

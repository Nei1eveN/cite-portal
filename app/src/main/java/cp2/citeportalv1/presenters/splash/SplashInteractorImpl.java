package cp2.citeportalv1.presenters.splash;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.views.admin.AccountsActivity;
import cp2.citeportalv1.views.faculty.FacultyHomeActivity;
import cp2.citeportalv1.views.guest.GuestViewActivity;
import cp2.citeportalv1.views.secretary.StudentAccountsActivity;
import cp2.citeportalv1.views.student.home.StudHomeActivity;

import static cp2.citeportalv1.utils.Constants.ACCOUNTS;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_ACCESS_LEVEL;
import static cp2.citeportalv1.utils.Constants.ADMIN;
import static cp2.citeportalv1.utils.Constants.EMAIL;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.STUDENTS;

public class SplashInteractorImpl implements SplashInteractor {
    private Context context;

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

    SplashInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUser(SplashOnSyncListener listener) {
        if (firebaseUser != null){
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference studentReference = firebaseDatabase.getReference(STUDENTS);
            studentReference.keepSynced(true);

            Query studentQuery = studentReference.orderByChild(EMAIL).equalTo(firebaseUser.getEmail());
            studentQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()){
                        Log.d("email", firebaseUser.getEmail());
                        listener.onSuccess("Logged in as: "+firebaseUser.getEmail());
                        context.startActivity(new Intent(context, StudHomeActivity.class));
                        ((Activity) context).finish();
                    }
                    else {
                        DatabaseReference facultyReference = firebaseDatabase.getReference(FACULTY);
                        facultyReference.keepSynced(true);
                        Query facultyQuery = facultyReference.orderByChild(EMAIL).equalTo(firebaseUser.getEmail());
                        facultyQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    Log.d("email", firebaseUser.getEmail());
                                    listener.onSuccess("Logged in as: "+firebaseUser.getEmail());
                                    context.startActivity(new Intent(context, FacultyHomeActivity.class));
                                    ((Activity) context).finish();
                                }
                                else {
                                    DatabaseReference accountReference = firebaseDatabase.getReference(ACCOUNTS);
                                    accountReference.keepSynced(true);
                                    Query accountQuery = accountReference.orderByChild(EMAIL).equalTo(firebaseUser.getEmail());
                                    accountQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                    String accessLevel = snapshot.child(ACCOUNT_ACCESS_LEVEL).getValue(String.class);
                                                    if (Objects.equals(accessLevel, ADMIN)) {
                                                        context.startActivity(new Intent(context, AccountsActivity.class));
                                                        ((Activity) context).finish();
                                                    } else {
                                                        context.startActivity(new Intent(context, StudentAccountsActivity.class));
                                                        ((Activity) context).finish();
                                                    }
                                                }
                                            } else {
                                                listener.onFailure("Something happened");
                                                context.startActivity(new Intent(context, GuestViewActivity.class));
                                                ((Activity) context).finish();
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            listener.onFailure(databaseError.getMessage());
                                        }
                                    });

                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.onFailure(databaseError.getMessage());
                                context.startActivity(new Intent(context, GuestViewActivity.class));
                                ((Activity) context).finish();
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onFailure(databaseError.getMessage());
                    context.startActivity(new Intent(context, GuestViewActivity.class));
                    ((Activity) context).finish();
                }
            });
        }
        else {
            context.startActivity(new Intent(context, GuestViewActivity.class));
            ((Activity) context).finish();
        }
    }
}

package cp2.citeportalv1.presenters.splash;

public interface SplashInteractor {
    void getUser(SplashOnSyncListener listener);
    interface SplashOnSyncListener {
        void onSuccess(String userEmail);
        void onFailure(String errorMessage);
    }
}

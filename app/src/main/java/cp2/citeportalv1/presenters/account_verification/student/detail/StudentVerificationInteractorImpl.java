package cp2.citeportalv1.presenters.account_verification.student.detail;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Result;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.services.APIService;
import cp2.citeportalv1.utils.APIClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cp2.citeportalv1.utils.Constants.PROFILE_IMAGES;
import static cp2.citeportalv1.utils.Constants.SIGNATURES;
import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.STUDENT_USERS;
import static cp2.citeportalv1.utils.Constants.VALID;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

class StudentVerificationInteractorImpl implements StudentVerificationInteractor {
    private Context context;

    StudentVerificationInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserDetails(String studentUid, UserDetailsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(studentUid);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    listener.onDetailSuccess(student);
                } else {
                    listener.onDetailFailure("Account Detail Error", "User does not exist.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onDetailFailure("Database Error", databaseError.getMessage());
            }
        });
    }

    @Override
    public void getUserValid(String studentUid, UserValidateListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onValidateFailure("Network Error", "Please check your network connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(studentUid);
            databaseReference.keepSynced(true);

            Map<String, Object> validateMap = new HashMap<>();
            validateMap.put(VALID, true);

            databaseReference.updateChildren(validateMap)
                    .addOnSuccessListener(((Activity) context), aVoid ->
                    {
                        APIService emailVerificationService = APIClient.getClient(context).create(APIService.class);
                        Call<Result> resultCall = emailVerificationService.sendVerification(studentUid);
                        resultCall.enqueue(new Callback<Result>() {
                            @Override
                            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                                Result result = response.body();

                                if (!Objects.requireNonNull(result).getStatus()) {
                                    listener.onValidateFailure("Email Verification Failure", "Cannot send email verification to this user. Please try again later.");
                                } else {
                                    listener.onValidateSuccess("User Validation Success", "Email Verification Link has been sent.");
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                                listener.onValidateFailure("Validation Error", t.getMessage());
                            }
                        });
                    })
                    .addOnFailureListener(((Activity) context), e ->
                            listener.onValidateFailure("Update Failure", e.getMessage()));
        }
    }

    @Override
    public void getUserRemove(String studentUid, UserRemoveListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onRemoveFailure("Network Error", "Please check your network connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(studentUid);
            databaseReference.keepSynced(true);

            databaseReference.removeValue()
                    .addOnSuccessListener(((Activity) context), aVoid ->
                    {
                        StorageReference imageReference = FirebaseStorage.getInstance().getReference(STUDENT_USERS).child(PROFILE_IMAGES);
                        imageReference.child(studentUid).delete().addOnSuccessListener(((Activity) context), aVoid1 -> {
                            StorageReference signatureReference = FirebaseStorage.getInstance().getReference(STUDENT_USERS).child(SIGNATURES);
                            signatureReference.child(studentUid).delete().addOnSuccessListener(((Activity) context), aVoid11 -> listener.onRemoveSuccess("User Removed", "User has been removed successfully.")).addOnFailureListener(((Activity) context), e -> listener.onRemoveSuccess("User Removed", e.getMessage()));
                        }).addOnFailureListener(((Activity) context), e -> {
                            StorageReference signatureReference = FirebaseStorage.getInstance().getReference(STUDENT_USERS).child(SIGNATURES);
                            signatureReference.child(studentUid).delete().addOnSuccessListener(((Activity) context), aVoid12 -> listener.onRemoveSuccess("User Removed", "User has been removed successfully.")).addOnFailureListener(((Activity) context), e1 -> listener.onRemoveSuccess("User Removed", e1.getMessage()));
                        });
                    })
                    .addOnFailureListener(((Activity) context), e ->
                            listener.onRemoveFailure("User Removing Failure", e.getMessage()));
        }
    }
}

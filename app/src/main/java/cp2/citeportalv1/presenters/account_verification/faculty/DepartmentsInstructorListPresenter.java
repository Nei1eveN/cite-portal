package cp2.citeportalv1.presenters.account_verification.faculty;

import cp2.citeportalv1.adapters.FacultyAccountsAdapter;

public interface DepartmentsInstructorListPresenter {
    interface View {
        void setProgress();
        void hideProgress();
        void setSnackMessage(String message);
        void setEmptyState(String emptyStateMessage);
        void setFacultyEmployees(FacultyAccountsAdapter adapter);
    }
    void onStart();
    void onDestroy();
    void requestFacultyEmployees();
}

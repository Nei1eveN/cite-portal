package cp2.citeportalv1.presenters.account_verification.student.detail;

import cp2.citeportalv1.models.Student;

public interface StudentVerificationInteractor {
    interface UserDetailsListener {
        void onDetailSuccess(Student student);
        void onDetailFailure(String errorTitle, String errorMessage);
    }

    void getUserDetails(String studentUid, UserDetailsListener listener);

    interface UserValidateListener {
        void onValidateSuccess(String title, String message);
        void onValidateFailure(String errorTitle, String errorMessage);
    }

    void getUserValid(String studentUid, UserValidateListener listener);

    interface UserRemoveListener {
        void onRemoveSuccess(String title, String message);
        void onRemoveFailure(String errorTitle, String errorMessage);
    }

    void getUserRemove(String studentUid, UserRemoveListener listener);

}

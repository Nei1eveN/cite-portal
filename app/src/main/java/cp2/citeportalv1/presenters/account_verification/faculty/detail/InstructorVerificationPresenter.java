package cp2.citeportalv1.presenters.account_verification.faculty.detail;

import cp2.citeportalv1.models.Faculty;

public interface InstructorVerificationPresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
        void showSnackMessage(String message);
        void setUserDetail(Faculty faculty);
    }
    void onStart(String facultyUid);
    void onDestroy();
    void setUserValid(String facultyUid);
    void setUserRemoved(String facultyUid);
}

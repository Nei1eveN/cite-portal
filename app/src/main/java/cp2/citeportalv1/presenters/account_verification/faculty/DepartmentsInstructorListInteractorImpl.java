package cp2.citeportalv1.presenters.account_verification.faculty;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.FacultyAccountsAdapter;
import cp2.citeportalv1.models.Accounts;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.views.guest.GuestViewActivity;

import static cp2.citeportalv1.utils.Constants.ACCOUNTS;
import static cp2.citeportalv1.utils.Constants.DEPARTMENT;
import static cp2.citeportalv1.utils.Constants.FACULTY;

public class DepartmentsInstructorListInteractorImpl implements DepartmentsInstructorListInteractor {
    private Context context;

    DepartmentsInstructorListInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getList(OnDepartmentSyncListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        DatabaseReference departmentSecReference = firebaseDatabase.getReference(ACCOUNTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        departmentSecReference.keepSynced(true);

        departmentSecReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Accounts account = Objects.requireNonNull(dataSnapshot.getValue(Accounts.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                    DatabaseReference facultyDepartmentReference = firebaseDatabase.getReference(FACULTY);
                    facultyDepartmentReference.keepSynced(true);
                    facultyDepartmentReference.orderByChild(DEPARTMENT).equalTo(account.getDepartment()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                List<Faculty> facultyList = new ArrayList<>();
                                facultyList.clear();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    String facultyId = snapshot.getKey();
                                    Faculty faculty = Objects.requireNonNull(snapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(facultyId));
                                    facultyList.add(faculty);
                                }

                                FacultyAccountsAdapter adapter = new FacultyAccountsAdapter(context, facultyList);

                                listener.onSuccess(adapter);
                            }
                            else {
                                listener.onFailure("Instructors under "+account.getDepartment()+" are not yet registered.");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            listener.onFailure(databaseError.getMessage());
                        }
                    });
                } else {
                    FirebaseAuth.getInstance().signOut();
                    listener.onFailure("Your account details have been removed from the database. Please contact the System Administrator");
                    context.startActivity(new Intent(context, GuestViewActivity.class));
                    ((Activity) context).finish();
                    ((Activity) context).overridePendingTransition(0,0);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFailure(databaseError.getMessage());
            }
        });
    }
}

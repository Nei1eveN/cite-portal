package cp2.citeportalv1.presenters.account_verification.student;

import android.content.Context;

import cp2.citeportalv1.adapters.StudentAccountsAdapter;

public class StudentListPresenterImpl implements StudentListPresenter, StudentListInteractor.OnDepartmentSyncListener {
    private StudentListPresenter.View view;
    private StudentListInteractor interactor;

    public StudentListPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentListInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.setProgress();
        }
        interactor.getList(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestStudentList() {
        if (view != null){
            view.setProgress();
        }
        interactor.getList(this);
    }

    @Override
    public void onSuccess(StudentAccountsAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setStudentList(adapter);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

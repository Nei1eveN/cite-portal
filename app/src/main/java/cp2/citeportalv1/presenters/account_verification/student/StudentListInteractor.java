package cp2.citeportalv1.presenters.account_verification.student;

import cp2.citeportalv1.adapters.StudentAccountsAdapter;

public interface StudentListInteractor {
    void getList(OnDepartmentSyncListener listener);
    interface OnDepartmentSyncListener {
        void onSuccess(StudentAccountsAdapter adapter);
        void onFailure(String errorMessage);
    }
}

package cp2.citeportalv1.presenters.account_verification.faculty;

import cp2.citeportalv1.adapters.FacultyAccountsAdapter;

public interface DepartmentsInstructorListInteractor {
    void getList(OnDepartmentSyncListener listener);
    interface OnDepartmentSyncListener {
        void onSuccess(FacultyAccountsAdapter adapter);
        void onFailure(String errorMessage);
    }
}

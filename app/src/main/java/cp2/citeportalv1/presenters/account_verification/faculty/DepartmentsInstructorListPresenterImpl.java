package cp2.citeportalv1.presenters.account_verification.faculty;

import android.content.Context;

import cp2.citeportalv1.adapters.FacultyAccountsAdapter;

public class DepartmentsInstructorListPresenterImpl implements
        DepartmentsInstructorListPresenter, DepartmentsInstructorListInteractor.OnDepartmentSyncListener {
    private View view;
    private DepartmentsInstructorListInteractor interactor;

    public DepartmentsInstructorListPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new DepartmentsInstructorListInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.setProgress();
        }
        interactor.getList(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestFacultyEmployees() {
        if (view != null){
            view.setProgress();
        }
        interactor.getList(this);
    }

    @Override
    public void onSuccess(FacultyAccountsAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setFacultyEmployees(adapter);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

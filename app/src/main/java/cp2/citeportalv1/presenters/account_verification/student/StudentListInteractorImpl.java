package cp2.citeportalv1.presenters.account_verification.student;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.StudentAccountsAdapter;
import cp2.citeportalv1.models.Accounts;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.views.guest.GuestViewActivity;

import static cp2.citeportalv1.utils.Constants.*;

class StudentListInteractorImpl implements StudentListInteractor {
    private Context context;

    StudentListInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getList(OnDepartmentSyncListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        DatabaseReference departmentSecReference = firebaseDatabase.getReference(ACCOUNTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        departmentSecReference.keepSynced(true);

        departmentSecReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Accounts account = Objects.requireNonNull(dataSnapshot.getValue(Accounts.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                    DatabaseReference studentDepartmentReference = firebaseDatabase.getReference(STUDENTS);
                    studentDepartmentReference.keepSynced(true);
                    Query studentQuery = studentDepartmentReference.orderByChild(PROGRAM).equalTo(account.getDepartment());
                    studentQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                List<Student> studentList = new ArrayList<>();
                                studentList.clear();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    Student student = Objects.requireNonNull(snapshot.getValue(Student.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                                    studentList.add(student);
                                }

                                StudentAccountsAdapter adapter = new StudentAccountsAdapter(context, studentList);

                                listener.onSuccess(adapter);
                            }
                            else {
                                listener.onFailure("Students under "+account.getDepartment()+" are not yet registered.");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            listener.onFailure(databaseError.getMessage());
                        }
                    });
                } else {
                    FirebaseAuth.getInstance().signOut();
                    listener.onFailure("Your account details have been removed from the database. Please contact the System Administrator");
                    context.startActivity(new Intent(context, GuestViewActivity.class));
                    ((Activity) context).finish();
                    ((Activity) context).overridePendingTransition(0,0);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFailure(databaseError.getMessage());
            }
        });
    }
}

package cp2.citeportalv1.presenters.account_verification.student.detail;

import cp2.citeportalv1.models.Student;

public interface StudentVerificationPresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showExitDialog(String title, String message);
        void showErrorDialog(String title, String message);
        void showSnackMessage(String message);
        void setUserDetail(Student student);
    }
    void onStart(String userUid);
    void onDestroy();
    void setUserValid(String userUid);
    void setUserRemoved(String userUid);
}

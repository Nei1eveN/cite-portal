package cp2.citeportalv1.presenters.account_verification.student;

import cp2.citeportalv1.adapters.StudentAccountsAdapter;

public interface StudentListPresenter {
    interface View {
        void setProgress();
        void hideProgress();
        void setSnackMessage(String message);
        void setEmptyState(String emptyStateMessage);
        void setStudentList(StudentAccountsAdapter adapter);
    }
    void onStart();
    void onDestroy();
    void requestStudentList();
}

package cp2.citeportalv1.presenters.account_verification.student.detail;

import android.content.Context;

import cp2.citeportalv1.models.Student;

public class StudentVerificationPresenterImpl implements StudentVerificationPresenter,
        StudentVerificationInteractor.UserDetailsListener, StudentVerificationInteractor.UserValidateListener, StudentVerificationInteractor.UserRemoveListener {
    private View view;
    private StudentVerificationInteractor interactor;

    public StudentVerificationPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentVerificationInteractorImpl(context);
    }

    @Override
    public void onStart(String userUid) {
        if (view != null) {
            view.showProgress("Retrieving User Details","Please wait...");
        }
        interactor.getUserDetails(userUid, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void setUserValid(String userUid) {
        if (view != null) {
            view.showProgress("Validating User", "Please wait...");
        }
        interactor.getUserValid(userUid, this);
    }

    @Override
    public void setUserRemoved(String userUid) {
        if (view != null) {
            view.showProgress("Removing User", "Please wait...");
        }
        interactor.getUserRemove(userUid, this);
    }

    @Override
    public void onDetailSuccess(Student student) {
        if (view != null) {
            view.hideProgress();
            view.setUserDetail(student);
        }
    }

    @Override
    public void onDetailFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onValidateSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(message);
            view.showExitDialog(title, message);
        }
    }

    @Override
    public void onValidateFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(errorMessage);
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onRemoveSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(message);
            view.showExitDialog(title, message);
        }
    }

    @Override
    public void onRemoveFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(errorMessage);
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }
}

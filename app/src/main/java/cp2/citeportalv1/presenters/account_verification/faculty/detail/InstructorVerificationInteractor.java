package cp2.citeportalv1.presenters.account_verification.faculty.detail;

import cp2.citeportalv1.models.Faculty;

public interface InstructorVerificationInteractor {
    interface UserDetailsListener {
        void onDetailSuccess(Faculty faculty);
        void onDetailFailure(String errorTitle, String errorMessage);
    }

    void getUserDetails(String facultyUid, UserDetailsListener listener);

    interface UserValidateListener {
        void onValidateSuccess(String title, String message);
        void onValidateFailure(String errorTitle, String errorMessage);
    }

    void getUserValid(String facultyUid, UserValidateListener listener);

    interface UserRemoveListener {
        void onRemoveSuccess(String title, String message);
        void onRemoveFailure(String errorTitle, String errorMessage);
    }

    void getUserRemove(String facultyUid, UserRemoveListener listener);

}

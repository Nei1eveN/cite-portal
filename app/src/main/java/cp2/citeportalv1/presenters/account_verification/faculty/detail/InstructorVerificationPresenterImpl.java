package cp2.citeportalv1.presenters.account_verification.faculty.detail;

import android.content.Context;

import cp2.citeportalv1.models.Faculty;

public class InstructorVerificationPresenterImpl implements InstructorVerificationPresenter,
        InstructorVerificationInteractor.UserDetailsListener, InstructorVerificationInteractor.UserValidateListener, InstructorVerificationInteractor.UserRemoveListener {
    private InstructorVerificationPresenter.View view;
    private InstructorVerificationInteractor interactor;

    public InstructorVerificationPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new InstructorVerificationInteractorImpl(context);
    }

    @Override
    public void onStart(String facultyUid) {
        if (view != null) {
            view.showProgress("Retrieving User Details","Please wait...");
        }
        interactor.getUserDetails(facultyUid, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void setUserValid(String facultyUid) {
        if (view != null) {
            view.showProgress("Validating User", "Please wait...");
        }
        interactor.getUserValid(facultyUid, this);
    }

    @Override
    public void setUserRemoved(String facultyUid) {
        if (view != null) {
            view.showProgress("Removing User", "Please wait...");
        }
        interactor.getUserRemove(facultyUid, this);
    }

    @Override
    public void onDetailSuccess(Faculty faculty) {
        if (view != null) {
            view.hideProgress();
            view.setUserDetail(faculty);
        }
    }

    @Override
    public void onDetailFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onValidateSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(message);
            view.showExitDialog(title, message);
        }
    }

    @Override
    public void onValidateFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(errorMessage);
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onRemoveSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(message);
            view.showExitDialog(title, message);
        }
    }

    @Override
    public void onRemoveFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(errorMessage);
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }
}

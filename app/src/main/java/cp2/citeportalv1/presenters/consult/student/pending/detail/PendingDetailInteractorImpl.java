package cp2.citeportalv1.presenters.consult.student.pending.detail;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.models.Faculty;

import static cp2.citeportalv1.utils.Constants.CONSULTATION_REQUESTS;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.STUDENTS;

class PendingDetailInteractorImpl implements PendingDetailInteractor {
    private Context context;

    PendingDetailInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getNotificationDetail(String notificationId, DetailListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference notificationReference = firebaseDatabase.getReference(STUDENTS)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                .child(CONSULTATION_REQUESTS);
        notificationReference.keepSynced(true);

        notificationReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    ConsultationRequest response = Objects.requireNonNull(dataSnapshot.getValue(ConsultationRequest.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    DatabaseReference senderReference = firebaseDatabase.getReference(FACULTY).child(response.getReceiverId());
                    senderReference.keepSynced(true);

                    senderReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                            if (!faculty.getfName().equals(response.getSenderFirstName())) {
                                response.setSenderFirstName(faculty.getfName());
                                response.setSenderMiddleName(faculty.getMidName());
                                response.setSenderLastName(faculty.getLastName());
                                response.setSenderImageURL(faculty.getFacultyImageURL());
                                response.setSenderProgram(faculty.getDepartment());
                                try {
                                    listener.getNotificationDetail(response);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            else {
                                try {
                                    listener.getNotificationDetail(response);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                }
                else {
                    listener.getDetailError("Request does not exist");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.getDetailError(databaseError.getMessage());
            }
        });
    }
}

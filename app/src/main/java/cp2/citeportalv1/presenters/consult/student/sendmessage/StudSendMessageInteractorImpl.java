package cp2.citeportalv1.presenters.consult.student.sendmessage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;
import org.threeten.bp.Month;
import org.threeten.bp.Year;
import org.threeten.bp.YearMonth;
import org.threeten.bp.temporal.TemporalAdjusters;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.Classmate;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.models.Interval;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.utils.Constants;
import cp2.citeportalv1.utils.EventDecorator;
import cp2.citeportalv1.views.student.consultation.SendConcernActivity;

import static cp2.citeportalv1.utils.Constants.*;

public class StudSendMessageInteractorImpl implements StudSendMessageInteractor {
    private Context context;
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    StudSendMessageInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getSenderDetails(SenderDetailListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        /**STUDENT SIGNATURE REFERENCE**/
        DatabaseReference studentSignatureReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        studentSignatureReference.keepSynced(true);

        studentSignatureReference.child("userSignature").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                Log.d("snapshot key", dataSnapshot.getKey());
                                Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                                listener.onSenderDetailSuccess(student);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            listener.onSenderDetailFailure("Database Error", databaseError.getMessage());
                        }
                    });
                } else {
                    listener.onSenderDetailFailure("Student Signature Not Found", "It seems you haven't created a signature. Please create by going to your Profile.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onSenderDetailFailure("Database Error", databaseError.getMessage());
            }
        });
    }

    @Override
    public void loadCourses(LoadCoursesListener loadCoursesListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(Constants.COURSES);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<String> stringArrayList = new ArrayList<>();
                stringArrayList.clear();
                Iterable<DataSnapshot> snapshots = dataSnapshot.getChildren();
                for (DataSnapshot data : snapshots) {
                    String courses = data.child("courseDesc").getValue(String.class) + " (" + data.child("courseCode").getValue(String.class) + ") ";
                    Log.d("courses", courses);
                    stringArrayList.add(courses);
                }
                try {
                    loadCoursesListener.onLoadCourseSuccess(stringArrayList);
                } catch (NullPointerException e) {
                    Toast.makeText(context, "Changes applied. Please wait a moment.", Toast.LENGTH_SHORT).show();
                    context.startActivity(new Intent(context, SendConcernActivity.class));
                    ((Activity) context).finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                loadCoursesListener.onLoadCourseFailure(databaseError.getMessage());
            }
        });

    }

    @Override
    public void getConsultationDetails(String concernTitle, String concernType, String concernMessage,
                                       String requestedDay, String requestedDate,
                                       String requestedTimeStart, String requestedTimeEnd, String venue,
                                       String toConcernedFacultyUserId, SendingMessageListener listener) {

        try {
            if (!isNetworkAvailable(context)) {
                listener.onSendingFailure("Network Error", "Please check your internet connection");
            } else if (yearDateFormat.format(new Date()).equals(requestedDate) && timeFormat12hr.parse(timeFormat12hr.format(new Date())).after(timeFormat12hr.parse(requestedTimeEnd))) {
                /**REQUEST FAILED: CURRENT TIME IS ALREADY AHEAD OF CONSULTATION SCHEDULE**/
                listener.onSendingFailure("Consultation Request Failed", "Requesting beyond the scheduled time of consultation is no longer allowed for this day.");
            } else {
                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

                /**STUDENT DETAILS REFERENCE**/
                DatabaseReference studentDetailsReference = firebaseDatabase.getReference().child(STUDENTS).child(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid());
                studentDetailsReference.keepSynced(true);
                studentDetailsReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Student student = dataSnapshot.getValue(Student.class);

                        /**FACULTY & STUDENT CONSULTATION REQUEST DETAILS**/
                        Map<String, Object> requestMap = new HashMap<>();
                        requestMap.put(SENDER_ID, firebaseAuth.getCurrentUser().getUid());
                        requestMap.put(RECEIVER_ID, toConcernedFacultyUserId);

                        requestMap.put(SENDER_FIRST_NAME, Objects.requireNonNull(student).getfName());
                        requestMap.put(SENDER_MIDDLE_NAME, student.getMidName());
                        requestMap.put(SENDER_LAST_NAME, student.getLastName());
                        requestMap.put(SENDER_IMAGE_URL, student.getStudentImageURL());
                        requestMap.put(SENDER_PROGRAM, student.getProgram());
                        requestMap.put(SENDER_YEAR_LEVEL, student.getYearLvl());

                        requestMap.put(MESSAGE_TITLE, concernTitle);
                        requestMap.put(MESSAGE_BODY, "Concern Type: "+concernType+"\n\n"+concernMessage);
                        requestMap.put(MESSAGE_STATUS, "PENDING");

                        requestMap.put(REQUESTED_DAY, requestedDay);
                        requestMap.put(REQUESTED_DATE, requestedDate);
                        requestMap.put(REQUESTED_SCHEDULE, requestedDate + " - " + requestedDay + " - " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue);

                        requestMap.put(REQUESTED_TIME_START, requestedTimeStart);
                        requestMap.put(REQUESTED_TIME_END, requestedTimeEnd);
                        requestMap.put(VENUE, venue);

                        requestMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

                        requestMap.put(REQUEST_CODE, Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid() + " / " + requestedDay + " / " + requestedDate + " / " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue);

                        requestMap.put(SENDER_SIGNATURE, student.getUserSignature());

                        /**FACULTY CONSULTATION REQUEST REFERENCE**/
                        DatabaseReference consultationRequestReference = FirebaseDatabase.getInstance().getReference(FACULTY).child(toConcernedFacultyUserId).child(CONSULTATION_REQUESTS);
                        consultationRequestReference.keepSynced(true);

                        /**CONSULTATION REQUEST KEY**/
                        String postKey = consultationRequestReference.push().getKey();

                        /**FACULTY CONSULTATION REQUEST SCHEDULE REFERENCE**/
                        consultationRequestReference.orderByChild("requestedSchedule").equalTo(requestedDate + " - " + requestedDay + " - " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    listener.onSendingFailure("Consultation Request Failed", "There is already an existing request with the same schedule.");
                                } else {
                                    DatabaseReference checkStudentConRequestReference = firebaseDatabase.getReference(STUDENTS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(CONSULTATION_REQUESTS);
                                    checkStudentConRequestReference.keepSynced(true);

                                    /**STUDENT CONSULTATION REQUEST REFERENCE**/
                                    checkStudentConRequestReference.orderByChild("requestedSchedule").equalTo(requestedDate + " - " + requestedDay + " - " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                listener.onSendingFailure("Consultation Request Failed", "You already have an existing request with the same schedule.");
                                            } else {
                                                /**CHECK REQUESTED DATE FOR CHECKING TIME CONFLICTS ON FACULTY CONSULTATION REQUEST**/
                                                consultationRequestReference.orderByChild("requestedDate").equalTo(requestedDate).addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        if (dataSnapshot.exists()) {
                                                            /**EXISTING REQUESTED DATE: CHECK TIME CONFLICT**/
                                                            List<ConsultationRequest> consultationRequests = new ArrayList<>();
                                                            consultationRequests.clear();

                                                            List<Date> timeStarts = new ArrayList<>();
                                                            List<Date> timeEnds = new ArrayList<>();

                                                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                                ConsultationRequest consultationRequest = Objects.requireNonNull(snapshot.getValue(ConsultationRequest.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                                                                consultationRequests.add(consultationRequest);
                                                            }

                                                            List<cp2.citeportalv1.models.Interval> intervals = new ArrayList<>(consultationRequests.size());

                                                            try {
                                                                for (int i = 0, consultationRequestsSize = consultationRequests.size(); i < consultationRequestsSize; i++) {
                                                                    ConsultationRequest consultationRequest = consultationRequests.get(i);

                                                                    timeStarts.add(timeFormat12hr.parse(consultationRequest.getRequestedTimeStart()));
                                                                    timeEnds.add(timeFormat12hr.parse(consultationRequest.getRequestedTimeEnd()));

                                                                    intervals.add(new cp2.citeportalv1.models.Interval(Integer.valueOf(String.valueOf(timeStarts.get(i).getTime())), Integer.valueOf(String.valueOf(timeEnds.get(i).getTime()))));

                                                                    if (i == (consultationRequestsSize - 1)) {

                                                                        Interval inputInterval = new Interval(Integer.valueOf(String.valueOf(timeFormat12hr.parse(requestedTimeStart).getTime())), Integer.valueOf(String.valueOf(timeFormat12hr.parse(requestedTimeEnd).getTime())));
                                                                        intervals.add(inputInterval);

                                                                        List<Interval> result = findIntervalsThatOverlap(intervals);

                                                                        Log.d("interactor--intvls", String.valueOf(intervals));

                                                                        if (!result.isEmpty()) {

                                                                            Log.d("interactor--result", String.valueOf(result));

                                                                            for (int j = 0, resultSize = result.size(); j < resultSize; j++) {
                                                                                Interval interval = result.get(j);
                                                                                if (interval.start < interval.end) {
                                                                                    /**REQUEST FAILED: TIME CONFLICT**/
                                                                                    listener.onSendingFailure("Consultation Request Failed", "There is an existing request that conflicts with your requested time.\n\nDetails:\n" + consultationRequest.getMessageTitle() + "\n" + consultationRequest.getRequestedTimeStart() + " - " + consultationRequest.getRequestedTimeEnd() + "\n" + consultationRequest.getVenue());
                                                                                    break;
                                                                                }
                                                                            }
                                                                        } else {
                                                                            /**REQUEST SUCCESS: SENDER DATA TO SENDER * RECEIVER**/
                                                                            consultationRequestReference.child(Objects.requireNonNull(postKey)).updateChildren(requestMap).addOnSuccessListener(((Activity) context), aVoid -> {
                                                                                DatabaseReference studentConRequestReference = FirebaseDatabase.getInstance().getReference(STUDENTS).child(firebaseAuth.getCurrentUser().getUid()).child(CONSULTATION_REQUESTS);
                                                                                studentConRequestReference.child(postKey).updateChildren(requestMap).addOnSuccessListener(((Activity) context), aVoid1 -> listener.onSendingSuccess("Request Sent", "Your message has been sent successfully.")).addOnFailureListener(((Activity) context), e -> listener.onSendingFailure("Database Error", e.getMessage()));
                                                                            }).addOnFailureListener(((Activity) context), e -> listener.onSendingFailure("Database Error", e.getMessage()));
                                                                        }
                                                                    }
                                                                }
                                                            } catch (ParseException e) {
                                                                listener.onSendingFailure("Parsing Error", e.getMessage());
                                                            }
                                                        } else {
                                                            /**NOT EXISTING REQUESTED DATE: SEND DATA TO SENDER AND RECEIVER**/
                                                            consultationRequestReference.child(Objects.requireNonNull(postKey)).updateChildren(requestMap).addOnSuccessListener(((Activity) context), aVoid -> {
                                                                DatabaseReference studentConRequestReference = FirebaseDatabase.getInstance().getReference(STUDENTS).child(firebaseAuth.getCurrentUser().getUid()).child(CONSULTATION_REQUESTS);
                                                                studentConRequestReference.child(postKey).updateChildren(requestMap).addOnSuccessListener(((Activity) context), aVoid1 -> listener.onSendingSuccess("Request Sent", "Your message has been sent successfully.")).addOnFailureListener(((Activity) context), e -> listener.onSendingFailure("Database Error", e.getMessage()));
                                                            }).addOnFailureListener(((Activity) context), e -> listener.onSendingFailure("Database Error", e.getMessage()));
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        listener.onSendingFailure("Database Error", databaseError.getMessage());
                                                    }
                                                });
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            listener.onSendingFailure("Database Error", databaseError.getMessage());
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.onSendingFailure("Database Error", databaseError.getMessage());
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        listener.onSendingFailure("Database Error", databaseError.getMessage());
                    }
                });
            }
        } catch (ParseException e) {
            listener.onSendingFailure("Parsing Error", e.getMessage());
        }
    }

    @Override
    public void getClassmates(OnLoadClassmatesListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CLASSMATES);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Classmate> classmates = new ArrayList<>();
                    classmates.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Classmate classmate = Objects.requireNonNull(snapshot.getValue(Classmate.class)).withId(snapshot.getKey());
                        classmates.add(classmate);
                    }
                    listener.onLoadClassmateSuccess(classmates);
                } else {
                    listener.onLoadClassmateFailure("You do not have classmates to tag as recipients.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onLoadClassmateFailure(
                        "Code:\n" + String.valueOf(databaseError.getCode()) + "\n\n" +
                                "Message:\n" + databaseError.getMessage() + "\n\n" +
                                "Details:\n" + databaseError.getDetails());
            }
        });
    }

    @Override
    public void getScheduleFromDate(String facultyId, Date dateSelected, FindScheduleListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY)
                .child(facultyId).child(CONSULTATION_SCHEDULES);
        databaseReference.keepSynced(true);

        databaseReference.child(dayFormat.format(dateSelected)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Schedule> schedules = new ArrayList<>();
                    schedules.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Schedule schedule = Objects.requireNonNull(snapshot.getValue(Schedule.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        schedules.add(schedule);
                    }
                    listener.onFindScheduleDetailsSuccess(schedules);
                } else {
                    listener.onFindScheduleDetailFailure("There are no Schedules Available for " + dayFormat.format(dateSelected));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFindScheduleDetailFailure(databaseError.getMessage());
            }
        });
    }

    @Override
    public void getAppointments(String facultyId, OnAppointmentsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(facultyId).child(CONSULTATION_SCHEDULES);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<String> days = new ArrayList<>();
                    List<CalendarDay> calendarDays = new ArrayList<>();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        days.add(snapshot.getKey());
                    }

                    for (int i = 0, daysSize = days.size(); i < daysSize; i++) {
                        String day = days.get(i);
                        addDecoratorAccordingToWeekDay(day, calendarDays, listener);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void findExistingTimeSlotAppointmentsFromDate(String facultyId, Date dateClicked, OnAppointmentTimeSlotsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(facultyId).child(APPOINTMENTS);
        databaseReference.keepSynced(true);

        databaseReference.orderByChild("requestedDate").equalTo(yearDateFormat.format(dateClicked)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Appointments> appointments = new ArrayList<>();
                    appointments.clear();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        appointments.add(appointment);
                    }

                    for (Appointments appointment : appointments) {
                        try {
                            Date date = timeFormat12hr.parse(appointment.getRequestedTimeStart());
                            Timepoint[] existingTimes = new Timepoint[]{new Timepoint(Integer.valueOf(hourFormat.format(date)), Integer.valueOf(minuteFormat.format(date)))};
                            listener.onFoundExistingAppointments(existingTimes);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void getSchedules(String facultyId, FindSchedulesListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(facultyId).child(CONSULTATION_SCHEDULES);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot scheduleDataSnapshot) {
                if (scheduleDataSnapshot.exists()) {
                    List<Schedule> schedules = new ArrayList<>();
                    for (DataSnapshot snapshot : scheduleDataSnapshot.getChildren()) {
                        Schedule schedule = Objects.requireNonNull(snapshot.getValue(Schedule.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        schedules.add(schedule);
                    }
                    listener.onFindScheduleDetailsSuccess(schedules);
                } else {
                    listener.onFindScheduleDetailFailure("No Schedules Available", "This Instructor does not have Consultation Schedules.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFindScheduleDetailFailure("Database Error", databaseError.getMessage());
            }
        });
    }

    private void addDecoratorAccordingToWeekDay(String day, List<CalendarDay> calendarDays, OnAppointmentsListener listener) {

        List<LocalDate> localDates = new ArrayList<>();

        if (day.equals("Monday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = Year.now().atMonth(month).atDay(1)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));

            Month monday = date.getMonth();
            while (monday == month) {
                Log.d("dates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
                monday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Tuesday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = Year.now().atMonth(month).atDay(2)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.TUESDAY));

            Month tuesday = date.getMonth();
            while (tuesday == month) {
                Log.d("dates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.TUESDAY));
                tuesday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Wednesday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = Year.now().atMonth(month).atDay(3)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.WEDNESDAY));

            Month wednesday = date.getMonth();
            while (wednesday == month) {
                Log.d("dates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.WEDNESDAY));
                wednesday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Thursday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = Year.now().atMonth(month).atDay(4)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.THURSDAY));

            Month thursday = date.getMonth();
            while (thursday == month) {
                Log.d("dates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.THURSDAY));
                thursday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Friday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = YearMonth.now().atDay(5)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.FRIDAY));

            Month friday = date.getMonth();
            while (friday == month) {
                Log.d("fridayDates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
                friday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Saturday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = YearMonth.now().atDay(6)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));

            Month saturday = date.getMonth();
            while (saturday == month) {
                Log.d("fridayDates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
                saturday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Sunday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = YearMonth.now().atDay(7)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.SUNDAY));

            Month sunday = date.getMonth();
            while (sunday == month) {
                Log.d("fridayDates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.SUNDAY));
                sunday = date.getMonth();

                localDates.add(date);
            }
        }

        for (LocalDate date : localDates) {
            try {
                Date gatheredDates = yearDateFormat.parse(String.valueOf(date));
                CalendarDay calendarDay = CalendarDay.from(gatheredDates);
                calendarDays.add(calendarDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        EventDecorator eventDecorator = new EventDecorator(context.getResources().getColor(R.color.fullBluePrimary), calendarDays);
        listener.onFoundAppointments(eventDecorator);
    }

    @Override
    public void getConsultationDetailsWithCc(List<String> emails,
                                             String concernTitle, String concernType,
                                             String concernMessage,
                                             String requestedDay, String requestedDate,
                                             String requestedTimeStart, String requestedTimeEnd, String venue,
                                             String toConcernedFacultyUserId,
                                             SendingMessageListener listener) {

        try {
            if (!isNetworkAvailable(context)) {
                listener.onSendingFailure("Network Error", "Please check your internet connection");
            } else if (yearDateFormat.format(new Date()).equals(requestedDate) && timeFormat12hr.parse(timeFormat12hr.format(new Date())).after(timeFormat12hr.parse(requestedTimeEnd))) {
                /**REQUEST FAILED: CURRENT TIME IS ALREADY AHEAD OF CONSULTATION SCHEDULE**/
                listener.onSendingFailure("Consultation Request Failed", "Requesting beyond the scheduled time of consultation is no longer allowed for this day. Please try again tomorrow.");
            }
            else {
                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

                /**STUDENT DETAILS REFERENCE**/
                DatabaseReference studentDetailReference = firebaseDatabase.getReference().child(STUDENTS).child(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid());
                studentDetailReference.keepSynced(true);

                studentDetailReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                        /**FACULTY & STUDENT CONSULTATION REQUEST DETAILS**/
                        Map<String, Object> requestMap = new HashMap<>();
                        requestMap.put(SENDER_ID, firebaseAuth.getCurrentUser().getUid());
                        requestMap.put(RECEIVER_ID, toConcernedFacultyUserId);

                        requestMap.put(SENDER_FIRST_NAME, Objects.requireNonNull(student).getfName());
                        requestMap.put(SENDER_MIDDLE_NAME, student.getMidName());
                        requestMap.put(SENDER_LAST_NAME, student.getLastName());
                        requestMap.put(SENDER_IMAGE_URL, student.getStudentImageURL());
                        requestMap.put(SENDER_PROGRAM, student.getProgram());
                        requestMap.put(SENDER_YEAR_LEVEL, student.getYearLvl());

                        requestMap.put(MESSAGE_TITLE, concernTitle);
                        requestMap.put(MESSAGE_BODY, "Concern Type: "+concernType+"\n\n"+concernMessage);
                        requestMap.put(MESSAGE_STATUS, "PENDING");

                        requestMap.put(REQUESTED_DAY, requestedDay);
                        requestMap.put(REQUESTED_DATE, requestedDate);
                        requestMap.put(REQUESTED_SCHEDULE, requestedDate + " - " + requestedDay + " - " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue);

                        requestMap.put(REQUESTED_TIME_START, requestedTimeStart);
                        requestMap.put(REQUESTED_TIME_END, requestedTimeEnd);
                        requestMap.put(VENUE, venue);

                        requestMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

                        requestMap.put(REQUEST_CODE, Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid() + " / " + requestedDay + " / " + requestedDate + " / " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue);

                        requestMap.put(SENDER_SIGNATURE, student.getUserSignature());

                        DatabaseReference emailReference = FirebaseDatabase.getInstance().getReference(STUDENTS);
                        DatabaseReference carbonCopyReference = FirebaseDatabase.getInstance().getReference(STUDENTS);
                        emailReference.keepSynced(true);
                        carbonCopyReference.keepSynced(true);

                        List<String> carbonCopies = new ArrayList<>();
                        for (String email : emails) {
                            emailReference.orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                        carbonCopies.add(snapshot.getKey());
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    listener.onSendingFailure("Fetching Error", "Code:\n" + databaseError.getCode() + "\n\nMessage:\n" + databaseError.getMessage() + "\n\n:Details:\n" + databaseError.getDetails());
                                }
                            });
                        }
                        requestMap.put(SENDER_CC, carbonCopies);

                        /**FACULTY CONSULTATION REQUEST REFERENCE**/
                        DatabaseReference consultationRequestReference = FirebaseDatabase.getInstance().getReference(FACULTY).child(toConcernedFacultyUserId).child(CONSULTATION_REQUESTS);
                        consultationRequestReference.keepSynced(true);

                        /**CONSULTATION REQUEST KEY**/
                        String postKey = consultationRequestReference.push().getKey();

                        /**FACULTY CONSULTATION REQUEST SCHEDULE REFERENCE**/
                        consultationRequestReference.orderByChild("requestedSchedule").equalTo(requestedDate + " - " + requestedDay + " - " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    listener.onSendingFailure("Consultation Request Failed", "There is already an existing request with the same schedule.");
                                } else {
                                    DatabaseReference studentConRequestReference = FirebaseDatabase.getInstance().getReference(STUDENTS).child(firebaseAuth.getCurrentUser().getUid()).child(CONSULTATION_REQUESTS);
                                    studentConRequestReference.keepSynced(true);

                                    /**STUDENT CONSULTATION REQUEST REFERENCE**/
                                    DatabaseReference checkStudentConRequestReference = firebaseDatabase.getReference(STUDENTS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(CONSULTATION_REQUESTS);
                                    checkStudentConRequestReference.keepSynced(true);

                                    /**STUDENT CONSULTATION REQUEST SCHEDULE REFERENCE**/
                                    checkStudentConRequestReference.orderByChild("requestedSchedule").equalTo(requestedDate + " - " + requestedDay + " - " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                listener.onSendingFailure("Consultation Request Failed", "You already have an existing request with the same schedule.");
                                            } else {
                                                /**CHECK REQUESTED DATE FOR CHECKING TIME CONFLICTS ON FACULTY CONSULTATION REQUEST**/
                                                consultationRequestReference.orderByChild("requestedDate").equalTo(requestedDate).addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        if (dataSnapshot.exists()) {
                                                            /**EXISTING REQUESTED DATE: CHECK TIME CONFLICT**/
                                                            List<ConsultationRequest> consultationRequests = new ArrayList<>();
                                                            consultationRequests.clear();

                                                            List<Date> timeStarts = new ArrayList<>();
                                                            List<Date> timeEnds = new ArrayList<>();

                                                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                                ConsultationRequest consultationRequest = Objects.requireNonNull(snapshot.getValue(ConsultationRequest.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                                                                consultationRequests.add(consultationRequest);
                                                            }

                                                            List<cp2.citeportalv1.models.Interval> intervals = new ArrayList<>(consultationRequests.size());

                                                            try {
                                                                for (int i = 0, consultationRequestsSize = consultationRequests.size(); i < consultationRequestsSize; i++) {
                                                                    ConsultationRequest consultationRequest = consultationRequests.get(i);

                                                                    timeStarts.add(timeFormat12hr.parse(consultationRequest.getRequestedTimeStart()));
                                                                    timeEnds.add(timeFormat12hr.parse(consultationRequest.getRequestedTimeEnd()));

                                                                    intervals.add(new cp2.citeportalv1.models.Interval(Integer.valueOf(String.valueOf(timeStarts.get(i).getTime())), Integer.valueOf(String.valueOf(timeEnds.get(i).getTime()))));

                                                                    if (i == (consultationRequestsSize - 1)) {
                                                                        Interval inputInterval = new Interval(Integer.valueOf(String.valueOf(timeFormat12hr.parse(requestedTimeStart).getTime())), Integer.valueOf(String.valueOf(timeFormat12hr.parse(requestedTimeEnd).getTime())));
                                                                        intervals.add(inputInterval);

                                                                        List<Interval> result = findIntervalsThatOverlap(intervals);

                                                                        Log.d("interactor--intvls", String.valueOf(intervals));

                                                                        if (!result.isEmpty()) {
                                                                            for (int j = 0, resultSize = result.size(); j < resultSize; j++) {
                                                                                Interval interval = result.get(j);
                                                                                if (interval.start < interval.end) {
                                                                                    listener.onSendingFailure("Consultation Request Failed", "There is an existing request that conflicts with your requested time.\n\nDetails:\n" + consultationRequest.getMessageTitle() + "\n" + consultationRequest.getRequestedTimeStart() + " - " + consultationRequest.getRequestedTimeEnd() + "\n" + consultationRequest.getVenue());
                                                                                    break;
                                                                                }
                                                                            }
                                                                        } else {
                                                                            /**REQUEST SUCCESSFUL: SEND DATA TO SENDER, RECEIVER, & CC's**/
                                                                            for (int j = 0; j < emails.size(); j++) {
                                                                                String email = emails.get(j);
                                                                                Log.d("interactor--email", email);
                                                                                Log.d("interactor--last-emails", String.valueOf(emails));

                                                                                queryReference(postKey, emailReference, carbonCopyReference, email.trim(), requestMap, consultationRequestReference, studentConRequestReference, listener);

                                                                                if (j == (emails.size() - 1)) { //j == (emails.size() - 1)
                                                                                    Log.d("interactor--finishLoop", "Loop finished.");
                                                                                    if (email.isEmpty()) {
                                                                                        listener.onSendingSuccess("Request Sent", "Your message has been sent successfully.\n\nNote:\nThis message has been sent without recipients.");
                                                                                    } else {
                                                                                        listener.onSendingSuccess("Request Sent", "Your message has been sent successfully.");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                }
                                                            }catch (ParseException e) {
                                                                listener.onSendingFailure("Parsing Error", e.getMessage());
                                                            }
                                                        } else {
                                                            /**NOT EXISTING REQUESTED DATE: SEND DATA TO SENDER, RECEIVER & CC's**/
                                                            for (int i = 0; i < emails.size(); i++) {
                                                                String email = emails.get(i);
                                                                Log.d("interactor--email", email);
                                                                Log.d("interactor--last-emails", String.valueOf(emails));

                                                                queryReference(postKey, emailReference, carbonCopyReference, email.trim(), requestMap, consultationRequestReference, studentConRequestReference, listener);

                                                                if (i == (emails.size() - 1)) { //i == (emails.size() - 1)
                                                                    Log.d("interactor--finishLoop", "Loop finished.");
                                                                    if (email.isEmpty()) {
                                                                        listener.onSendingSuccess("Request Sent", "Your message has been sent successfully.\n\nNote:\nThis message has been sent without recipients.");
                                                                    } else {
                                                                        listener.onSendingSuccess("Request Sent", "Your message has been sent successfully.");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        listener.onSendingFailure("Database Error", databaseError.getMessage());
                                                    }
                                                });
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            listener.onSendingFailure("Database Error", databaseError.getMessage());
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.onSendingFailure("Database Error", databaseError.getMessage());
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        listener.onSendingFailure("Database Error", databaseError.getMessage());
                    }
                });
            }
        } catch (ParseException e) {
            listener.onSendingFailure("Parsing Error", e.getMessage());
        }
    }

    private void queryReference(String postKey, DatabaseReference emailReference, DatabaseReference carbonCopyReference, String email, Map<String, Object> requestMap, DatabaseReference consultationRequestReference, DatabaseReference studentConRequestReference, SendingMessageListener listener) {
        if (email.isEmpty()) {
            studentConRequestReference.child(Objects.requireNonNull(postKey)).updateChildren(requestMap);
            consultationRequestReference.child(Objects.requireNonNull(postKey)).updateChildren(requestMap);
            listener.onSendingSuccess("Request Sent", "Your message has been sent successfully.\n\nNote:\nThis message has been sent without recipients.");
        } else {
            emailReference.orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Student student = Objects.requireNonNull(snapshot.getValue(Student.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                            carbonCopyReference.child(student.studentUId).child(CONSULTATION_REQUESTS).child(postKey).updateChildren(requestMap);
                        }
                        studentConRequestReference.child(Objects.requireNonNull(postKey)).updateChildren(requestMap);
                        consultationRequestReference.child(Objects.requireNonNull(postKey)).updateChildren(requestMap);
                    } else {
                        studentConRequestReference.child(Objects.requireNonNull(postKey)).updateChildren(requestMap);
                        consultationRequestReference.child(Objects.requireNonNull(postKey)).updateChildren(requestMap);
//                        listener.onSendingSuccess("Request Sent", "Your message has been sent successfully.\n\nNote:\nThis message has been sent without recipients.");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onSendingFailure("Database Error", databaseError.getMessage());
                }
            });
        }
    }
}
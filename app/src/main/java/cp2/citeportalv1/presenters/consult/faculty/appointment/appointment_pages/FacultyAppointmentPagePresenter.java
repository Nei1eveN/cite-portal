package cp2.citeportalv1.presenters.consult.faculty.appointment.appointment_pages;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface FacultyAppointmentPagePresenter {
    interface View {
        void loadDayPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }
    void onStart();
    void onDestroy();
}

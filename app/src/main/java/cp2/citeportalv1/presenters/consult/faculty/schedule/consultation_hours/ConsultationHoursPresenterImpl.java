package cp2.citeportalv1.presenters.consult.faculty.schedule.consultation_hours;

import android.content.Context;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public class ConsultationHoursPresenterImpl implements ConsultationHoursPresenter, ConsultationHoursInteractor.ConsultationDaysListener{
    private ConsultationHoursPresenter.View view;
    private ConsultationHoursInteractor interactor;

    public ConsultationHoursPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new ConsultationHoursInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getPages(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void onPageSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null) {
            view.loadDayPages(viewPagerAdapter, numberOfPages);
        }

    }

    @Override
    public void onPageFailure(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null) {
            view.loadDayPages(viewPagerAdapter, numberOfPages);
        }
    }
}

package cp2.citeportalv1.presenters.consult.faculty.appointment.all_appointments;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;

public interface FacultyAppointmentsPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void showSnackMessage(String message);
        void setAppointments(FacultyAppointmentsGroupAdapter adapter);
        void setEmptyAppointment(String emptyMessage);
    }
    void onStart(String day);
    void onDestroy();
    void findAppointmentsAccordingToDay(String day);
}

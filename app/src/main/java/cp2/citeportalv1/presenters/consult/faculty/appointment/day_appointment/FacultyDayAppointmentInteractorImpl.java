package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentDayGroupAdapter;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.utils.Constants;

import static cp2.citeportalv1.utils.Constants.APPOINTMENTS;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

public class FacultyDayAppointmentInteractorImpl implements FacultyDayAppointmentInteractor {
    private Context context;

    FacultyDayAppointmentInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getAppointments(LoadAppointmentsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        Query query = databaseReference.child(APPOINTMENTS)
                .orderByChild("requestedDate")
                .equalTo(yearDateFormat.format(new Date()))
                .limitToFirst(10);

//        FirebaseRecyclerOptions<Appointments> options = new FirebaseRecyclerOptions.Builder<Appointments>().setQuery(query, Appointments.class).build();
//
//        FirebaseAppointmentAdapter adapter = new FirebaseAppointmentAdapter(options, context);
//        listener.onSuccessLoadingAppointment("Appointments Loaded", adapter);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<Appointments> appointments = new ArrayList<>();
                    appointments.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        appointments.add(appointment);
                    }
                    LinkedHashMap<String, Set<Appointments>> groupedHashMap = Constants.groupAppointmentDataIntoHashMap(appointments);
                    List<ListItem> consolidatedList = new ArrayList<>();
                    consolidatedList.clear();

                    for (String date : groupedHashMap.keySet()) {
                        Log.d("dates", date);
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(dateFormat.format(new Date(Long.parseLong(date))));
                        dateItem.setDay(dayFormat.format(new Date(Long.parseLong(date))));
                        consolidatedList.add(dateItem);
                        for (Appointments schedule : Objects.requireNonNull(groupedHashMap.get(date))) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setAppointments(schedule);
                            consolidatedList.add(generalItem);
                        }
                    }

                    FacultyAppointmentDayGroupAdapter adapter = new FacultyAppointmentDayGroupAdapter(context, consolidatedList);


                    listener.onSuccessLoadingAppointment("Appointments Loaded", adapter);
                }
                else {
                    listener.onFailureLoadingAppointment("There are no Approved Scheduled Appointments for "+dateFormat.format(new Date())+".\n\nClick the icon to see your Appointments.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFailureLoadingAppointment(databaseError.getMessage());
            }
        });
    }

    @Override
    public void findAppointmentToDatabase(Date dateClicked, FindAppointmentListener appointmentListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.child(APPOINTMENTS).orderByChild("requestedDate").equalTo(yearDateFormat.format(dateClicked)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<Appointments> appointments = new ArrayList<>();
                    appointments.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Appointments appointment = snapshot.getValue(Appointments.class);
                        appointments.add(appointment);
                    }
                    LinkedHashMap<String, Set<Appointments>> groupedHashMap = Constants.groupAppointmentDataIntoHashMap(appointments);
                    List<ListItem> consolidatedList = new ArrayList<>();

                    for (String date : groupedHashMap.keySet()) {
                        Log.d("dates", date);
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(dateFormat.format(new Date(Long.parseLong(date))));
                        dateItem.setDay(dayFormat.format(new Date(Long.parseLong(date))));
                        consolidatedList.add(dateItem);
                        for (Appointments schedule : Objects.requireNonNull(groupedHashMap.get(date))) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setAppointments(schedule);
                            consolidatedList.add(generalItem);
                        }
                    }

                    FacultyAppointmentDayGroupAdapter adapter = new FacultyAppointmentDayGroupAdapter(context, consolidatedList);

                    appointmentListener.onFindingSuccess(adapter);
                }
                else {
                    appointmentListener.onFindingFailure("Appointments for "+dateFormat.format(dateClicked)+" not found.\n\nThere are no Scheduled Appointments for this date.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                appointmentListener.onFindingFailure(databaseError.getMessage());
            }
        });
    }


}

package cp2.citeportalv1.presenters.consult.student.consultation_log.detail;

import cp2.citeportalv1.models.ConsultationLog;

public interface StudentConsultationLogDetailInteractor {
    void getNotificationDetail(String notificationId, ConsultationLogDetailListener listener);
    interface ConsultationLogDetailListener {
        void getNotificationDetail(ConsultationLog consultationLog);
        void getDetailError(String errorTitle, String errorMessage);
    }
}

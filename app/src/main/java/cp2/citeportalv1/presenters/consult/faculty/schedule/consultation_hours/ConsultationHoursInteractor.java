package cp2.citeportalv1.presenters.consult.faculty.schedule.consultation_hours;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface ConsultationHoursInteractor {
    interface ConsultationDaysListener {
        void onPageSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
        void onPageFailure(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }

    void getPages(ConsultationDaysListener daysListener);
}

package cp2.citeportalv1.presenters.consult.faculty.schedule;

import cp2.citeportalv1.adapters.grouplist.ScheduleGroupAdapter;

public interface SchedulePresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void showToastMessage(String message);
        void showSnackMessage(String message);
        void setSchedules(ScheduleGroupAdapter adapter);
        void setEmptySchedule(String emptyMessage);
        void setUpCreateScheduleDialog();
        void setUpConflictScheduleDialog(String conflictMessage);
    }
    void onStart();
    void onDestroy();
    void requestSchedules();
    void requestScheduleAccordingToDay(String day);
    void submitCreatingSchedule(String day, String timeStart, String timeEnd, String venue);
}

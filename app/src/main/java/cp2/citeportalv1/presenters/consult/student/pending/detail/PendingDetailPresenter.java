package cp2.citeportalv1.presenters.consult.student.pending.detail;

import cp2.citeportalv1.models.ConsultationRequest;

public interface PendingDetailPresenter {
    interface View {
        void setPendingDetail(ConsultationRequest request);
        void setPendingNotExist(String notExistMessage);
    }
    void onStart(String notificationId);
    void onDestroy();
}

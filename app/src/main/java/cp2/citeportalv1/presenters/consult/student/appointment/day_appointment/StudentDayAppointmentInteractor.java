package cp2.citeportalv1.presenters.consult.student.appointment.day_appointment;

import java.util.Date;

import cp2.citeportalv1.adapters.grouplist.StudentAppointmentDayGroupAdapter;

public interface StudentDayAppointmentInteractor {
    void getAppointments(LoadAppointmentsListener listener);

    void findAppointmentToDatabase(Date dateClicked, FindAppointmentListener appointmentListener);

    interface LoadAppointmentsListener {
        void onSuccessLoadingAppointment(String successMessage, StudentAppointmentDayGroupAdapter adapter);

        void onFailureLoadingAppointment(String errorMessage);
    }

    interface FindAppointmentListener {
        void onFindingSuccess(StudentAppointmentDayGroupAdapter appointments);

        void onFindingFailure(String errorMessage);
    }
}

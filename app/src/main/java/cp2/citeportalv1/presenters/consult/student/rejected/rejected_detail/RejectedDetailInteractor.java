package cp2.citeportalv1.presenters.consult.student.rejected.rejected_detail;

import java.text.ParseException;

import cp2.citeportalv1.models.ConsultationResponse;

public interface RejectedDetailInteractor {
    void getNotificationDetail(String notificationId, DetailListener listener);
    interface DetailListener {
        void getNotificationDetail(ConsultationResponse response) throws ParseException;
        void getDetailError(String errorMessage);
    }
}

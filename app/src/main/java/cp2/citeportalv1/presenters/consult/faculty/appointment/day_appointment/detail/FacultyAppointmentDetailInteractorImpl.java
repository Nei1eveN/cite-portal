package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Appointments;

import static cp2.citeportalv1.utils.Constants.APPOINTMENTS;
import static cp2.citeportalv1.utils.Constants.FACULTY;

class FacultyAppointmentDetailInteractorImpl implements FacultyAppointmentDetailInteractor {
    private Context context;

    FacultyAppointmentDetailInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getNotificationDetail(String notificationId, AppointmentDetailListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference notificationReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(APPOINTMENTS);
        notificationReference.keepSynced(true);

        notificationReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Appointments appointment = Objects.requireNonNull(dataSnapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    listener.getNotificationDetail(appointment);
                }
                else {
                    listener.getDetailError("Detail Error","Appointment Details does not exist");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.getDetailError("Database Error", databaseError.getMessage());
            }
        });
    }
}

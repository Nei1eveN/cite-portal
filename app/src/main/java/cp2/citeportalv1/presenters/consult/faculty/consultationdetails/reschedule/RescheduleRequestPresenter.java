package cp2.citeportalv1.presenters.consult.faculty.consultationdetails.reschedule;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.Date;
import java.util.List;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.utils.EventDecorator;


public interface RescheduleRequestPresenter {
    interface View {
        void showProgress(String title, String caption);

        void hideProgress();

        void setUpCalendar();

        void setUpDotsToCalendar(EventDecorator eventDecorator);

        void setUpApprovedTimeSlotsToTimePicker(Timepoint[] existingTimes);

        void initViews(ConsultationRequest request);

        void setEmptySchedule(String emptyMessage);

        void setEmptyAppointment(String emptyMessage);

        void setSchedulesFromDate(List<Schedule> schedules);

        void setAppointmentsFromDate(FacultyAppointmentsGroupAdapter adapter);

        void showSnackMessage(String message);

        void setExitDialog(String title, String message);

        void showErrorDialog(String title, String message);
    }

    void onStart(String notificationId);

    void onDestroy();

    void findAppointments(String facultyId);

    void findScheduleFromDate(String facultyId, Date dateSelected);

    void findAppointmentFromDate(String facultyId, Date dateSelected);

    void findExistingTimeSlotAppointmentsFromDate(String facultyId, Date dateSelected);

    void sendRescheduledRequest(String notificationId,
                                String rescheduledTimeStart, String rescheduledTimeEnd,
                                String rescheduledVenue, String rescheduledDay, String rescheduledDate,
                                String consultationTitle, String consultationBody, String rescheduledRemarks);
}

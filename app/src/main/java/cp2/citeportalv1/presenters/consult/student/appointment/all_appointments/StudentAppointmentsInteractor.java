package cp2.citeportalv1.presenters.consult.student.appointment.all_appointments;

import cp2.citeportalv1.adapters.grouplist.StudentAppointmentsGroupAdapter;

public interface StudentAppointmentsInteractor {
    void getAppointmentsAccordingToDay(String day, LoadAppointmentsListener listener);

    interface LoadAppointmentsListener {
        void onSuccessLoadingAppointment(String successMessage, StudentAppointmentsGroupAdapter adapter);

        void onFailureLoadingAppointment(String errorMessage);
    }
}

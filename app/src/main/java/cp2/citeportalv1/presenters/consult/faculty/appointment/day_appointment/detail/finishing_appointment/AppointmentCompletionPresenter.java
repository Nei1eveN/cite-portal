package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail.finishing_appointment;

import cp2.citeportalv1.models.Appointments;

public interface AppointmentCompletionPresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void setAppointmentDetails(Appointments appointment);
        void setNullWrapper(String message);
        void showErrorDialog(String title, String message);
        void showExitDialog(String title, String message);
        void showSnackMessage(String snackMessage);
    }
    void onStart(String notificationId);
    void onDestroy();

    void submitFinishingAppointment(String notificationId, String appointmentRemarks, String appointmentDate, String timeStart, String status);
    void submitCancelledAppointment(String notificationId, String appointmentRemarks, String appointmentDate, String timeStart, String cancelledStatus);
}

package cp2.citeportalv1.presenters.consult.student.rejected;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public interface RejectedResponseInteractor {
    void getDetails(OnRejectedResponseListener listener);

    interface OnRejectedResponseListener {
        void OnResponseSuccess(List<ListItem> list);
        void OnResponseFailure(String errorMessage);
    }
}

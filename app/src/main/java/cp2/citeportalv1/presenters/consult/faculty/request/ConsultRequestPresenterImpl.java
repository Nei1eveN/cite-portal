package cp2.citeportalv1.presenters.consult.faculty.request;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public class ConsultRequestPresenterImpl implements ConsultRequestPresenter, ConsultRequestInteractor.ResponseOnSyncListener {
    private ConsultRequestPresenter.View view;
    private ConsultRequestInteractor consultRequestInteractor;

    public ConsultRequestPresenterImpl(View view, Context context) {
        this.view = view;
        this.consultRequestInteractor = new ConsultRequestInteractorImpl(context);
    }

    @Override
    public void onStart() {
        view.initViews();
        if (view != null) {
            view.showProgress();
//            view.hideViewsState();
        }
        consultRequestInteractor.getNotifications(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestNotificationList() {
        if (view != null) {
            view.showProgress();
//            view.hideViewsState();
        }
        consultRequestInteractor.getNotifications(this);
    }

    @Override
    public void onSuccess(List<ListItem> notifications) {
        if (view != null) {
            view.hideProgress();
            view.setNotifications(notifications);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail.finishing_appointment;

import android.content.Context;

import java.text.ParseException;
import java.util.Date;

import cp2.citeportalv1.models.Appointments;

import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.timeFormat12hr;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

public class AppointmentCompletionPresenterImpl implements AppointmentCompletionPresenter,
        AppointmentCompletionInteractor.AppointmentDetailListener,
        AppointmentCompletionInteractor.FinishAppointmentListener, AppointmentCompletionInteractor.CancelAppointmentListener {
    private AppointmentCompletionPresenter.View view;
    private AppointmentCompletionInteractor interactor;

    public AppointmentCompletionPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new AppointmentCompletionInteractorImpl(context);
    }

    @Override
    public void onStart(String notificationId) {
        if (view != null) {
            view.showProgress("Loading Appointment", "Please wait...");
        }
        interactor.getAppointmentDetails(notificationId, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void submitFinishingAppointment(String notificationId, String appointmentRemarks, String appointmentDate, String timeStart, String status) {
        try {
            Date apptDate = dateFormat.parse(appointmentDate);
            if (new Date().before(apptDate)) {
                view.showErrorDialog("Finishing Error", "You cannot finish an Upcoming Appointment earlier than the Appointment Date.");
            } else if (new Date().before(apptDate) && !timeFormat12hr.format(new Date()).equals(timeStart)) {
                view.showErrorDialog("Finishing Error", "You cannot finish an Upcoming Appointment.");
            } else if (new Date().equals(apptDate) && !timeFormat12hr.format(new Date()).equals(timeStart)) {
                view.showErrorDialog("Finishing Error", "You cannot finish an on-going Appointment.");
            }
            else if (appointmentRemarks.isEmpty()) {
                view.showErrorDialog("Empty Remarks", "Remarks cannot be empty for finishing an appointment");
                view.setNullWrapper("Required field");
            } else {
                if (view != null) {
                    view.showProgress("Saving Details", "Finishing Appointment. Please wait...");
                    interactor.finishAppointment(notificationId, appointmentRemarks, status, this);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void submitCancelledAppointment(String notificationId, String appointmentRemarks, String appointmentDate, String timeStart, String cancelledStatus) {
        if (appointmentRemarks.isEmpty()) {
            view.showErrorDialog("Empty Remarks", "Remarks cannot be empty for finishing an appointment");
            view.setNullWrapper("Required field");
        } else {
            if (view != null) {
                view.showProgress("Saving Details", "Finishing Appointment. Please wait...");
                interactor.cancelAppointment(notificationId, appointmentRemarks, cancelledStatus, this);
            }
        }
    }

    @Override
    public void onAppointmentDetailSuccess(Appointments appointment) throws ParseException {
        if (view != null) {
            Date date = yearDateFormat.parse(appointment.getRequestedDate());
            view.hideProgress();
            appointment.setRequestedDate(dateFormat.format(date));
            view.setAppointmentDetails(appointment);
        }
    }

    @Override
    public void onAppointmentError(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onFinishingSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(title, message);
        }
    }

    @Override
    public void onFinishingError(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onCancelSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(title, message);
        }
    }

    @Override
    public void onCancelError(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.consult.faculty.request;

import android.content.Context;
import android.text.format.DateUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;

import static cp2.citeportalv1.utils.Constants.CONSULTATION_REQUESTS;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;
import static cp2.citeportalv1.utils.Constants.groupRequestDataIntoHashMap;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

public class ConsultRequestInteractorImpl implements ConsultRequestInteractor {
    private Context context;
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    ConsultRequestInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getNotifications(ResponseOnSyncListener onSyncListener) {
        DatabaseReference consultationRequestReference = FirebaseDatabase.getInstance().getReference(FACULTY)
                .child(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid()).child(CONSULTATION_REQUESTS);
        consultationRequestReference.keepSynced(true);

        Query consultationRequests = consultationRequestReference.orderByChild("timeStamp"); //.equalTo(firebaseAuth.getCurrentUser().getUid())
        consultationRequests.keepSynced(true);
        consultationRequests.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.exists()){
                            onSyncListener.onFailure("There are no Consultation Requests.");
                        }
                        else {
                            List<ConsultationRequest> notificationsList = new ArrayList<>();
                            notificationsList.clear();
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                                String notificationId = snapshot.getKey();
                                assert notificationId != null;
                                ConsultationRequest request = Objects.requireNonNull(snapshot.getValue(ConsultationRequest.class)).withId(notificationId);
                                Date timeStamp = new Date(Long.parseLong(request.getTimeStamp())*1000);
                                request.setTimeStamp(yearDateFormat.format(timeStamp));
                                notificationsList.add(request);
                            }

                            LinkedHashMap<String, List<ConsultationRequest>> groupedHashMap = groupRequestDataIntoHashMap(notificationsList);
                            List<ListItem> consolidatedList = new ArrayList<>();
                            for (String date : groupedHashMap.keySet()){

                                DateItem dateItem = new DateItem();
                                try {
                                    Date formatStamp = yearDateFormat.parse(date);
                                    if (DateUtils.isToday(formatStamp.getTime())){
                                        dateItem.setDay("Today");
                                        dateItem.setDate(dateFormat.format(formatStamp));
                                    }
                                    else {
                                        dateItem.setDay(dayFormat.format(formatStamp));
                                        dateItem.setDate(dateFormat.format(formatStamp));
                                    }
                                    consolidatedList.add(dateItem);

                                    for (ConsultationRequest request : groupedHashMap.get(date)){
                                        GeneralItem generalItem = new GeneralItem();
                                        generalItem.setRequest(request);
                                        consolidatedList.add(generalItem);
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            onSyncListener.onSuccess(consolidatedList);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        onSyncListener.onFailure(databaseError.getMessage());
                    }
                });
    }
}

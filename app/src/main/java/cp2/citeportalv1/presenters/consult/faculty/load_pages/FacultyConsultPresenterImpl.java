package cp2.citeportalv1.presenters.consult.faculty.load_pages;

import android.content.Context;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public class FacultyConsultPresenterImpl implements FacultyConsultPresenter, FacultyConsultInteractor.LoadPagesListener {

    private FacultyConsultPresenter.View view;
    private FacultyConsultInteractor interactor;

    public FacultyConsultPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyConsultInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getFragmentPages(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void onSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null) {
            view.initializeTabs(viewPagerAdapter, numberOfPages);
        }
    }
}

package cp2.citeportalv1.presenters.consult.faculty.schedule;

import android.content.Context;
import android.util.Log;

import java.text.ParseException;
import java.util.Date;

import cp2.citeportalv1.adapters.grouplist.ScheduleGroupAdapter;
import cp2.citeportalv1.utils.Constants;

public class SchedulePresenterImpl implements
        SchedulePresenter, ScheduleInteractor.OnFindingScheduleListener,
        ScheduleInteractor.OnSavingScheduleListener, ScheduleInteractor.OnFindingDayScheduleListener {

    private SchedulePresenter.View view;
    private ScheduleInteractor interactor;

    public SchedulePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new ScheduleInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.setUpCreateScheduleDialog();
        }
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestSchedules() {
        if (view != null){
            view.showProgress();
        }
        interactor.getSchedules(this);
    }

    @Override
    public void requestScheduleAccordingToDay(String day) {
        if (view != null){
            view.showProgress();
        }
        interactor.getScheduleAccordingToDay(day, this);
    }

    @Override
    public void submitCreatingSchedule(String day, String timeStart, String timeEnd, String venue) {

        try {
            Date startTime = Constants.timeFormat12hr.parse(timeStart); //Constants.fixedFormat.parse(timeStart)
            Date endTime = Constants.timeFormat12hr.parse(timeEnd); //Constants.fixedFormat.parse(timeEnd)

            Log.d("schedTimePresenter1", String.valueOf(startTime.getTime()));
            Log.d("schedTimePresenter2", String.valueOf(endTime.getTime()));

            if (day.isEmpty()) {
                view.showSnackMessage("Please set a day for creating a consultation");
            } else if (timeStart.isEmpty()) {
                view.showSnackMessage("Please set a starting time for creating a consultation");
            } else if (timeEnd.isEmpty()) {
                view.showSnackMessage("Please set a time limit for creating a consultation");
            } else if (venue.isEmpty()) {
                view.showSnackMessage("Please set a venue for creating a consultation");
            } else if (startTime.after(endTime)) {
                view.showSnackMessage("Starting time must not be greater than Ending time");
            } else if (endTime.before(startTime)) {
                view.showSnackMessage("Ending time must not be lesser than Starting time");
            } else if (startTime.compareTo(endTime) == 0) {
                view.showSnackMessage("You cannot set starting time same as ending time");
            } else {
                if (view != null) {
                    view.showProgress();
                }
                interactor.saveSchedule(day, timeStart, timeEnd, venue, this);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFindingScheduleSuccess(ScheduleGroupAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setSchedules(adapter);
        }
    }

    @Override
    public void onFindingScheduleFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptySchedule(errorMessage);
        }
    }

    @Override
    public void onSavingSuccess(String successMessage, String day) {
        if (view != null) {
            view.hideProgress();
            view.showToastMessage(successMessage);
            view.showProgress();
        }
//        interactor.getSchedules(this);
        interactor.getScheduleAccordingToDay(day, this);
    }

    @Override
    public void onSavingFailure(String errorMessage, String day) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(errorMessage);
            view.setUpConflictScheduleDialog(errorMessage);
            view.showProgress();
        }
        interactor.getScheduleAccordingToDay(day, this);
    }

    @Override
    public void onFindingDaySuccess(ScheduleGroupAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setSchedules(adapter);
        }
    }

    @Override
    public void onFindingDayFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptySchedule(errorMessage);
        }
    }
}

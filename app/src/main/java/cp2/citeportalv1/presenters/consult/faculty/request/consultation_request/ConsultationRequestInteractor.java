package cp2.citeportalv1.presenters.consult.faculty.request.consultation_request;

import cp2.citeportalv1.adapters.grouplist.ConsultationRequestsGroupAdapter;

public interface ConsultationRequestInteractor {
    void getRequests(ResponseOnSyncListener onSyncListener);

    interface ResponseOnSyncListener {
        void onSuccess(ConsultationRequestsGroupAdapter adapter);
        void onFailure(String errorMessage);
    }
}

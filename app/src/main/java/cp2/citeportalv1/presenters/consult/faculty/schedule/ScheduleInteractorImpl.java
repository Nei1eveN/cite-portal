package cp2.citeportalv1.presenters.consult.faculty.schedule;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import cp2.citeportalv1.adapters.grouplist.ScheduleGroupAdapter;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.Interval;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.views.faculty.ConsultationHoursActivity;

import static cp2.citeportalv1.utils.Constants.*;

public class ScheduleInteractorImpl implements ScheduleInteractor {
    private Context context;

    ScheduleInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getSchedules(OnFindingScheduleListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);
        DatabaseReference query = databaseReference.child(CONSULTATION_SCHEDULES);
        query.keepSynced(true);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Schedule> schedules = new ArrayList<>();

                    List<String> days = new ArrayList<>();
                    Iterable<DataSnapshot> snapshots = dataSnapshot.getChildren();
                    for (DataSnapshot snapshot : snapshots) {
                        Log.d("key1", snapshot.getKey());

                        days.add(snapshot.getKey());
                    }

                    for (String day : days) {
                        Query scheduleSortReference = query.child(day).orderByChild("timeStart"); //.orderByChild("day").equalTo(snapshot.getKey())
                        scheduleSortReference.keepSynced(true);
                        scheduleSortReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot snapshot1 : dataSnapshot.getChildren()) {
                                    Schedule schedule = Objects.requireNonNull(snapshot1.getValue(Schedule.class)).withId(Objects.requireNonNull(snapshot1.getKey()));
                                    schedules.add(schedule);
                                }

                                LinkedHashMap<String, Set<Schedule>> groupedHashMap = groupScheduleDataIntoHashMap(schedules);
                                List<ListItem> consolidatedList = new ArrayList<>();

                                for (String date : groupedHashMap.keySet()) {
                                    Log.d("dates", date);
                                    DateItem dateItem = new DateItem();
                                    dateItem.setDate(date);
                                    dateItem.setDay(dayFormat.format(new Date(Long.parseLong(date))));
                                    consolidatedList.add(dateItem);
                                    for (Schedule schedule : Objects.requireNonNull(groupedHashMap.get(date))) {
                                        GeneralItem generalItem = new GeneralItem();
                                        generalItem.setSchedule(schedule);
                                        consolidatedList.add(generalItem);
                                    }
                                }

                                ScheduleGroupAdapter adapter = new ScheduleGroupAdapter(context, consolidatedList);
                                listener.onFindingScheduleSuccess(adapter);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.onFindingScheduleFailure(databaseError.getMessage());
                            }
                        });
                    }

                } else {
                    listener.onFindingScheduleFailure("You haven't created a schedule yet. Click the image above to start.");
                    //You may also ask your Department Secretary to arrange your Consultation Schedules.
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFindingScheduleFailure(databaseError.getMessage());
            }
        });
    }

    @Override
    public void saveSchedule(String day, String timeStart, String timeEnd, String venue, OnSavingScheduleListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onSavingFailure("Please check your internet connection", day);
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference facultyDetailReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            facultyDetailReference.keepSynced(true);
            facultyDetailReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String department = dataSnapshot.child("department").getValue(String.class);
                        String firstName = dataSnapshot.child("fName").getValue(String.class);
                        String midName = dataSnapshot.child("midName").getValue(String.class);
                        String lastName = dataSnapshot.child("lastName").getValue(String.class);

                        DatabaseReference scheduleReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CONSULTATION_SCHEDULES);
                        scheduleReference.keepSynced(true);

                        scheduleReference.child(day).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Log.d("initialDataCount", String.valueOf(dataSnapshot.getChildrenCount()));
                                if (dataSnapshot.exists()) {

                                    List<Schedule> schedules = new ArrayList<>();

                                    List<Date> timeStarts = new ArrayList<>();
                                    List<Date> timeEnds = new ArrayList<>();

                                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                        Schedule schedule = Objects.requireNonNull(snapshot.getValue(Schedule.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                                        schedules.add(schedule);
                                    }

                                    List<cp2.citeportalv1.models.Interval> intervals = new ArrayList<>(schedules.size());

                                    try {
                                        for (int i = 0, schedulesSize = schedules.size(); i < schedulesSize; i++) {
                                            Schedule schedule = schedules.get(i);

                                            timeStarts.add(timeFormat12hr.parse(schedule.getTimeStart()));
                                            timeEnds.add(timeFormat12hr.parse(schedule.getTimeEnd()));

                                            intervals.add(new cp2.citeportalv1.models.Interval(Integer.valueOf(String.valueOf(timeStarts.get(i).getTime())), Integer.valueOf(String.valueOf(timeEnds.get(i).getTime()))));

                                            if (i == (schedulesSize - 1)) {
                                                Interval inputInterval = new Interval(Integer.valueOf(String.valueOf(timeFormat12hr.parse(timeStart).getTime())), Integer.valueOf(String.valueOf(timeFormat12hr.parse(timeEnd).getTime())));
                                                intervals.add(inputInterval);

                                                List<Interval> result = findIntervalsThatOverlap(intervals);

                                                Log.d("interactor--intvls", String.valueOf(intervals));

                                                if (!result.isEmpty()) {
                                                    for (int j = 0, resultSize = result.size(); j < resultSize; j++) {
                                                        Interval interval = result.get(j);
                                                        if (j == (resultSize - 1)) {
                                                            if (interval.start < interval.end) {
                                                                listener.onSavingFailure(
                                                                        "Schedule Conflict. Please check your schedule carefully.", day);
                                                                Log.d("interactor--schedCode", "onDataChange: this logic fires");
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    saveFinalSchedule(day,
                                                            timeStart,
                                                            timeEnd,
                                                            venue, department, lastName, firstName, midName, listener);
                                                }
                                            }
                                        }
                                    } catch (ParseException e) {
                                       listener.onSavingFailure(e.getMessage(), day);
                                    }

                                } else {
                                    saveFinalSchedule(day,
                                            timeStart,
                                            timeEnd,
                                            venue, department, lastName, firstName, midName, listener);
                                    Log.d("else snapshot", "Snapshot does not exists");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.onSavingFailure(databaseError.getCode()
                                        + ": " + databaseError.getMessage() + "\n Details: " + databaseError.getDetails(), day);
                            }
                        });

                    }
                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onSavingFailure(databaseError.getMessage(), day);
                }
            });
        }
    }

    @Override
    public void saveFinalSchedule(String day, String timeStart, String timeEnd,
                                  String venue, String department,
                                  String lastName, String firstName, String midName,
                                  OnSavingScheduleListener listener) {

        Schedule schedule = new Schedule();
        schedule.setDay(day);
        schedule.setTimeStart(timeStart);
        schedule.setTimeEnd(timeEnd);
        schedule.setVenue(venue);
        schedule.setDepartment(department);
        schedule.setFacultyName(lastName + ", " + firstName + " " + midName);

        /**DETERMINING THE SEMESTER / TERM**/
        int i = calendar.get(Calendar.MONTH);
        if (i == Calendar.NOVEMBER || i == Calendar.DECEMBER || i == Calendar.JANUARY || i == Calendar.FEBRUARY || i == Calendar.MARCH) {
            schedule.setTerm("2nd Semester");

        } else if (i == Calendar.APRIL || i == Calendar.MAY) {
            schedule.setTerm("Summer");

        } else {
            schedule.setTerm("1st Semester");
        }

        /**DETERMINING THE ACADEMIC YEAR**/
        int i1 = calendar.get(Calendar.MONTH);
        if (i1 == Calendar.JUNE || i1 == Calendar.JULY ||
                i1 == Calendar.AUGUST || i1 == Calendar.SEPTEMBER || i1 == Calendar.OCTOBER ||
                i1 == Calendar.NOVEMBER || i1 == Calendar.DECEMBER) {
            Calendar calendar = Calendar.getInstance();
            int currentYear = calendar.get(Calendar.YEAR);
            calendar.add(Calendar.YEAR, 1);
            int nextYear = calendar.get(Calendar.YEAR);
            schedule.setAcademicYear(currentYear + " - " + nextYear);

        } else if (i1 == Calendar.JANUARY || i1 == Calendar.FEBRUARY || i1 == Calendar.MARCH) {
            Calendar calendar = Calendar.getInstance();
            int currentYear = calendar.get(Calendar.YEAR);
            calendar.add(Calendar.YEAR, -1);
            int previousYear = calendar.get(Calendar.YEAR);
            schedule.setAcademicYear(previousYear + " - " + currentYear);
        } else if (i1 == Calendar.APRIL || i1 == Calendar.MAY) {
            Calendar calendar = Calendar.getInstance();
            int currentYear = calendar.get(Calendar.YEAR);
            schedule.setAcademicYear(String.valueOf(currentYear));
        }

        schedule.setScheduleCode(timeStart + " - " + timeEnd + " @ " + venue);


        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CONSULTATION_SCHEDULES);

        databaseReference.child(day).push().setValue(schedule)
                .addOnCompleteListener(((FragmentActivity) context), task -> {
                    if (task.isSuccessful()) {
                        listener.onSavingSuccess(day + " Schedule created successfully.", day);
                    } else {
                        listener.onSavingFailure(Objects.requireNonNull(task.getException()).getMessage(), day);
                    }
                    context.startActivity(new Intent(context, ConsultationHoursActivity.class));
                    ((FragmentActivity) context).overridePendingTransition(0,0);
                });
    }

    @Override
    public void getScheduleAccordingToDay(String day, OnFindingDayScheduleListener listener) {

        if (day == null) {
            listener.onFindingDayFailure("You haven't created a schedule yet.\nClick the image above to start.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference =
                    firebaseDatabase.getReference(FACULTY)
                            .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser())
                                    .getUid()).child(CONSULTATION_SCHEDULES).child(day);
            databaseReference.keepSynced(true);

            databaseReference.orderByChild("timeStart").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        List<Schedule> schedules = new ArrayList<>();
                        schedules.clear();
                        Iterable<DataSnapshot> dataSnapshots = dataSnapshot.getChildren();
                        for (DataSnapshot snapshot1 : dataSnapshots) {
                            Schedule schedule = Objects.requireNonNull(snapshot1.getValue(Schedule.class)).withId(Objects.requireNonNull(snapshot1.getKey()));
                            schedules.add(schedule);
                        }

                        LinkedHashMap<String, Set<Schedule>> groupedHashMap = groupScheduleDataIntoHashMap(schedules);
                        List<ListItem> consolidatedList = new ArrayList<>();

                        for (String date : groupedHashMap.keySet()) {
                            Log.d("dates", date);
                            DateItem dateItem = new DateItem();
                            dateItem.setDate(date);
                            dateItem.setDay(dayFormat.format(new Date(Long.parseLong(date))));
                            consolidatedList.add(dateItem);
                            for (Schedule schedule : Objects.requireNonNull(groupedHashMap.get(date))) {
                                GeneralItem generalItem = new GeneralItem();
                                generalItem.setSchedule(schedule);
                                consolidatedList.add(generalItem);
                            }
                        }

                        ScheduleGroupAdapter adapter = new ScheduleGroupAdapter(context, consolidatedList);

                        listener.onFindingDaySuccess(adapter);
                    } else {
                        listener.onFindingDayFailure("You haven't created a schedule yet. Click the image above to start.");
                        //You may also ask your Department Secretary to arrange your Consultation Schedules.
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onFindingDayFailure(databaseError.getMessage());
                }
            });
        }
    }
}
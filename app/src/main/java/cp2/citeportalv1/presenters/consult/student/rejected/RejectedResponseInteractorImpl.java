package cp2.citeportalv1.presenters.consult.student.rejected;

import android.content.Context;
import android.text.format.DateUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.ConsultationResponse;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;

import static cp2.citeportalv1.utils.Constants.CONSULTATION_RESPONSES;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

class RejectedResponseInteractorImpl implements RejectedResponseInteractor {
    private Context context;

    RejectedResponseInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getDetails(OnRejectedResponseListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CONSULTATION_RESPONSES);
        databaseReference.keepSynced(true);

        Query query = databaseReference.orderByChild("messageStatus").equalTo("REJECTED");
        query.keepSynced(true);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<ConsultationResponse> responseList = new ArrayList<>();
                    responseList.clear();
                    Iterable<DataSnapshot> snapshots = dataSnapshot.getChildren();
                    for (DataSnapshot snapshot : snapshots){
                        ConsultationResponse consultationResponse = Objects.requireNonNull(snapshot.getValue(ConsultationResponse.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        Date timeStamp = new Date(Long.parseLong(consultationResponse.getTimeStamp())*1000);
                        consultationResponse.setTimeStamp(yearDateFormat.format(timeStamp));

                        DatabaseReference senderReference = firebaseDatabase.getReference(FACULTY).child(consultationResponse.getSenderId());
                        senderReference.keepSynced(true);

                        senderReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                                if (!faculty.getfName().equals(consultationResponse.getSenderFirstName())) {
                                    consultationResponse.setSenderFirstName(faculty.getfName());
                                    consultationResponse.setSenderMiddleName(faculty.getMidName());
                                    consultationResponse.setSenderLastName(faculty.getLastName());
                                    consultationResponse.setSenderDepartment(faculty.getDepartment());
                                    consultationResponse.setSenderImageURL(faculty.getFacultyImageURL());

                                    responseList.add(consultationResponse);
                                }
                                else {
                                    responseList.add(consultationResponse);
                                }
                                LinkedHashMap<String, List<ConsultationResponse>> groupedHashMap = groupDataIntoHashMap(responseList, listener);

                                List<ListItem> consolidatedList = new ArrayList<>();

                                for (String date : groupedHashMap.keySet()){

                                    DateItem dateItem = new DateItem();
                                    try {
                                        Date formatStamp = yearDateFormat.parse(date);
                                        if (DateUtils.isToday(formatStamp.getTime())){
                                            dateItem.setDay("Today");
                                            dateItem.setDate(dateFormat.format(formatStamp));
                                        }
                                        else {
                                            dateItem.setDay(dayFormat.format(formatStamp));
                                            dateItem.setDate(dateFormat.format(formatStamp));
                                        }
                                        consolidatedList.add(dateItem);

                                        for (ConsultationResponse response : groupedHashMap.get(date)){
                                            GeneralItem generalItem = new GeneralItem();
                                            generalItem.setResponse(response);
                                            consolidatedList.add(generalItem);
                                        }

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }

                                listener.OnResponseSuccess(consolidatedList);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.OnResponseFailure(databaseError.getMessage());
                            }
                        });
                    }

                }
                else {
                    listener.OnResponseFailure("There are no rejected requests.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.OnResponseFailure(databaseError.getMessage());
            }
        });
    }

    private LinkedHashMap<String, List<ConsultationResponse>> groupDataIntoHashMap(List<ConsultationResponse> responses, OnRejectedResponseListener listener){
        LinkedHashMap<String, List<ConsultationResponse>> groupedHashMap = new LinkedHashMap<>();

        for (ConsultationResponse response : responses){
            String hashMapKey = response.getTimeStamp(); //String.valueOf(new Date(Long.parseLong(response.getTimeStamp())*1000))
            if (groupedHashMap.containsKey(hashMapKey)){
                groupedHashMap.get(hashMapKey).add(response);
            }
            else {
                List<ConsultationResponse> responseList = new ArrayList<>();
                responseList.add(response);
                groupedHashMap.put(hashMapKey, responseList);
            }
        }

//        List<ListItem> consolidatedList = new ArrayList<>();
//
//        for (String date : groupedHashMap.keySet()){
//            DateItem dateItem = new DateItem();
//            dateItem.setDate(date);
//            consolidatedList.add(dateItem);
//
//            for (ConsultationResponse response : groupedHashMap.get(date)){
//                GeneralItem generalItem = new GeneralItem();
//                generalItem.setResponse(response);
//                consolidatedList.add(generalItem);
//            }
//        }

//        listener.OnResponseSuccess(consolidatedList);

        return groupedHashMap;
    }
}

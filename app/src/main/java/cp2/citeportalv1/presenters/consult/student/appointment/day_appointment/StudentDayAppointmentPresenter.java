package cp2.citeportalv1.presenters.consult.student.appointment.day_appointment;

import java.util.Date;

import cp2.citeportalv1.adapters.grouplist.StudentAppointmentDayGroupAdapter;

public interface StudentDayAppointmentPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void showSnackMessage(String message);
        void setAppointments(StudentAppointmentDayGroupAdapter adapter);
        void setEmptyAppointment(String emptyMessage);
    }
    void onStart();
    void onDestroy();
    void findAppointments();
    void findAppointment(Date dateClicked);
}

package cp2.citeportalv1.presenters.consult.student.appointment.day_appointment;

import android.content.Context;

import java.util.Date;

import cp2.citeportalv1.adapters.grouplist.StudentAppointmentDayGroupAdapter;

public class StudentDayAppointmentPresenterImpl implements StudentDayAppointmentPresenter,
        StudentDayAppointmentInteractor.FindAppointmentListener, StudentDayAppointmentInteractor.LoadAppointmentsListener {

    private StudentDayAppointmentPresenter.View view;
    private StudentDayAppointmentInteractor interactor;

    public StudentDayAppointmentPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentDayAppointmentInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.showProgress();
        }
        interactor.getAppointments(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void findAppointments() {
        if (view != null) {
            view.showProgress();
        }
        interactor.getAppointments(this);
    }

    @Override
    public void findAppointment(Date dateClicked) {
        interactor.findAppointmentToDatabase(dateClicked, this);
    }

    @Override
    public void onFindingSuccess(StudentAppointmentDayGroupAdapter appointments) {
        if (view != null) {
            view.setAppointments(appointments);
        }
    }

    @Override
    public void onFindingFailure(String errorMessage) {
        if (view != null) {
            view.showSnackMessage(errorMessage);
            view.setEmptyAppointment(errorMessage);
        }
    }

    @Override
    public void onSuccessLoadingAppointment(String successMessage, StudentAppointmentDayGroupAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setAppointments(adapter);
        }
    }

    @Override
    public void onFailureLoadingAppointment(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyAppointment(errorMessage);
            view.showSnackMessage(errorMessage);
        }
    }
}

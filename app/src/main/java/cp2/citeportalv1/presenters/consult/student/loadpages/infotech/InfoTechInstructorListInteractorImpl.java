package cp2.citeportalv1.presenters.consult.student.loadpages.infotech;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;

import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.groupFacultyDataIntoHashMap;

public class InfoTechInstructorListInteractorImpl implements InfoTechInstructorListInteractor {
    private Context context;

    InfoTechInstructorListInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getList(OnDepartmentSyncListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY);
        databaseReference.keepSynced(true);
        databaseReference.orderByChild("department").equalTo("Information Technology").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Faculty> facultyList = new ArrayList<>();
                    facultyList.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String facultyId = snapshot.getKey();
                        Faculty faculty = Objects.requireNonNull(snapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(facultyId));
                        facultyList.add(faculty);
                    }
                    LinkedHashMap<String, List<Faculty>> groupedHashMap = groupFacultyDataIntoHashMap(facultyList);
                    List<ListItem> consolidatedList = new ArrayList<>();
                    for (String department : groupedHashMap.keySet()){
                        DateItem dateItem = new DateItem();
                        dateItem.setDepartment(department);
                        consolidatedList.add(dateItem);

                        for (Faculty faculty : Objects.requireNonNull(groupedHashMap.get(department))){
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setFaculty(faculty);
                            consolidatedList.add(generalItem);
                        }
                    }

                    listener.onSuccess(consolidatedList);
                }
                else {
                    listener.onFailure("Instructors under Information Technology Department are not yet registered.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFailure(databaseError.getMessage());
            }
        });
    }
}

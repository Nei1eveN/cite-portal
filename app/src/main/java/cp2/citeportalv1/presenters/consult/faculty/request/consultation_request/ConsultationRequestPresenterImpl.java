package cp2.citeportalv1.presenters.consult.faculty.request.consultation_request;

import android.content.Context;

import cp2.citeportalv1.adapters.grouplist.ConsultationRequestsGroupAdapter;

public class ConsultationRequestPresenterImpl implements ConsultationRequestPresenter, ConsultationRequestInteractor.ResponseOnSyncListener {

    private ConsultationRequestPresenter.View view;
    private ConsultationRequestInteractor interactor;

    public ConsultationRequestPresenterImpl(ConsultationRequestPresenter.View view, Context context) {
        this.view = view;
        this.interactor = new ConsultationRequestInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        interactor.getRequests(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void requestPendingList() {
        if (view != null){
            view.showProgress();
        }
        interactor.getRequests(this);
    }

    @Override
    public void onSuccess(ConsultationRequestsGroupAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.loadPendingResponses(adapter);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setSnackMessage(errorMessage);
            view.setEmptyState(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.consult.student.rejected;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public class RejectedResponsePresenterImpl implements RejectedResponsePresenter, RejectedResponseInteractor.OnRejectedResponseListener {
    private RejectedResponsePresenter.View view;
    private RejectedResponseInteractor interactor;

    public RejectedResponsePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new RejectedResponseInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        interactor.getDetails(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void requestRejectRequests() {
        if (view != null){
            view.showProgress();
        }
        interactor.getDetails(this);
    }

    @Override
    public void OnResponseSuccess(List<ListItem> list) {
        if (view != null){
            view.hideProgress();
            view.loadRejectedResponses(list);
        }
    }

    @Override
    public void OnResponseFailure(String errorMessage) {
        if (view != null){
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

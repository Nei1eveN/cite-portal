package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail.rescheduling_appointment;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.Date;
import java.util.List;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.utils.EventDecorator;

public interface AppointmentRescheduleInteractor {
    interface OnRequestDetailsListener {
        void onRequestDetailSuccess(Appointments appointment);
        void onRequestDetailFailure(String errorTitle, String errorMessage);
    }

    void getConsultationRequestDetails(String notificationId, OnRequestDetailsListener listener);

    interface OnScheduleFromDateListener {
        void onScheduleFound(List<Schedule> schedules);
        void onScheduleNotFound(String errorMessage);
    }

    void getScheduleFromDate(String facultyId, Date dateSelected, OnScheduleFromDateListener listener);

    interface OnAppointmentFromDateListener {
        void onAppointmentFound(FacultyAppointmentsGroupAdapter adapter);
        void onAppointmentNotFound(String errorMessage);
    }

    void getAppointmentFromDate(String facultyId, Date dateSelected, OnAppointmentFromDateListener listener);

    interface OnAppointmentsListener {
        void onFoundAppointments(EventDecorator eventDecorator);
    }

    void getAppointments(String facultyId, OnAppointmentsListener listener);

    interface OnAppointmentTimeSlotsListener {
        void onFoundExistingAppointments(Timepoint[] existingTimes);
    }

    void findExistingTimeSlotAppointmentsFromDate(String facultyId, Date dateSelected, OnAppointmentTimeSlotsListener listener);

    interface OnAppointmentTimeSlotsEndingListener {
        void onFoundExistingTimeEndings(Timepoint[] existingTimeEndings);
    }

    void findExistingTimeSlotEndingsFromDate(String facultyId, Date dateSelected, OnAppointmentTimeSlotsEndingListener listener);

    interface RescheduleRequestListener {
        void onRescheduleSuccess(String title, String message);
        void onRescheduleFailure(String errorTitle, String errorMessage);
    }

    void saveApprovedRequestToDatabase(String notificationId,
                                       String rescheduledTimeStart, String rescheduledTimeEnd,
                                       String rescheduledVenue, String rescheduledDay, String rescheduledDate,
                                       String consultationTitle, String consultationBody, String rescheduledRemarks,
                                       RescheduleRequestListener listener);
}

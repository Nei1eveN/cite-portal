package cp2.citeportalv1.presenters.consult.student.pending;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public interface PendingRequestPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setSnackMessage(String snackMessage);
        void loadPendingResponses(List<ListItem> list);
        void setEmptyState(String emptyMessage);
    }
    void onStart();
    void onDestroy();
    void requestPendingList();
}

package cp2.citeportalv1.presenters.consult.student.consultation_log;

import android.content.Context;

import cp2.citeportalv1.adapters.grouplist.StudentConsultationLogGroupAdapter;

public class StudentConsultationLogPresenterImpl implements StudentConsultationLogPresenter, StudentConsultationLogInteractor.OnApprovedResponseListener {
    private View view;
    private StudentConsultationLogInteractor responseInteractor;

    public StudentConsultationLogPresenterImpl(View view, Context context) {
        this.view = view;
        this.responseInteractor = new StudentConsultationLogInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        responseInteractor.getDetails(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void requestConsultationLogs() {
        if (view != null){
            view.showProgress();
        }
        responseInteractor.getDetails(this);
    }

    @Override
    public void OnResponseSuccess(StudentConsultationLogGroupAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.loadConsultationLogs(adapter);
        }
    }

    @Override
    public void OnResponseFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.consult.student.rejected;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public interface RejectedResponsePresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setSnackMessage(String snackMessage);
        void loadRejectedResponses(List<ListItem> list);
        void setEmptyState(String emptyMessage);
    }
    void onStart();
    void onDestroy();
    void requestRejectRequests();
}

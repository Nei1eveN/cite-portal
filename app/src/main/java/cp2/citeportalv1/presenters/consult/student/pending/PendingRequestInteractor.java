package cp2.citeportalv1.presenters.consult.student.pending;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public interface PendingRequestInteractor {
    void getDetails(OnPendingRequestListener detailsListener);

    interface OnPendingRequestListener {
        void OnRequestSuccess(List<ListItem> list);
        void OnRequestFailure(String errorMessage);
    }
}

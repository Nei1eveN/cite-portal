package cp2.citeportalv1.presenters.consult.faculty.appointment.all_appointments;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;

public interface FacultyAppointmentsInteractor {
    void getAppointmentsAccordingToDay(String day, LoadAppointmentsListener listener);

    interface LoadAppointmentsListener {
        void onSuccessLoadingAppointment(String successMessage, FacultyAppointmentsGroupAdapter adapter);

        void onFailureLoadingAppointment(String errorMessage);
    }
}

package cp2.citeportalv1.presenters.consult.student.pending;

import android.content.Context;
import android.text.format.DateUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;

import static cp2.citeportalv1.utils.Constants.CONSULTATION_REQUESTS;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

class PendingRequestInteractorImpl implements PendingRequestInteractor {
    private Context context;

    PendingRequestInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getDetails(OnPendingRequestListener detailsListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CONSULTATION_REQUESTS);
        databaseReference.keepSynced(true);

        Query query = databaseReference.orderByChild("messageStatus").equalTo("PENDING");
        query.keepSynced(true);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<ConsultationRequest> requests = new ArrayList<>();
                    requests.clear();
                    Iterable<DataSnapshot> snapshots = dataSnapshot.getChildren();
                    for (DataSnapshot snapshot : snapshots){
                        ConsultationRequest request = Objects.requireNonNull(snapshot.getValue(ConsultationRequest.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        Date timeStamp = new Date(Long.parseLong(request.getTimeStamp())*1000);
                        request.setTimeStamp(yearDateFormat.format(timeStamp));

                        DatabaseReference senderReference = firebaseDatabase.getReference(FACULTY).child(request.getReceiverId());
                        senderReference.keepSynced(true);

                        senderReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                                if (!faculty.getfName().equals(request.getSenderFirstName())) {
                                    request.setSenderFirstName(faculty.getfName());
                                    request.setSenderMiddleName(faculty.getMidName());
                                    request.setSenderLastName(faculty.getLastName());
                                    request.setSenderImageURL(faculty.getFacultyImageURL());
                                    requests.add(request);
                                } else {
                                    requests.add(request);
                                }
                                LinkedHashMap<String, List<ConsultationRequest>> groupedHashMap = groupDataIntoHashMap(requests);
                                List<ListItem> consolidatedList = new ArrayList<>();
                                for (String date : groupedHashMap.keySet()){

                                    DateItem dateItem = new DateItem();
                                    try {
                                        Date formatStamp = yearDateFormat.parse(date);
                                        if (DateUtils.isToday(formatStamp.getTime())){
                                            dateItem.setDay("Today");
                                            dateItem.setDate(dateFormat.format(formatStamp));
                                        }
                                        else {
                                            dateItem.setDay(dayFormat.format(formatStamp));
                                            dateItem.setDate(dateFormat.format(formatStamp));
                                        }
                                        consolidatedList.add(dateItem);

                                        for (ConsultationRequest request : groupedHashMap.get(date)){
                                            GeneralItem generalItem = new GeneralItem();
                                            generalItem.setRequest(request);
                                            consolidatedList.add(generalItem);
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                                detailsListener.OnRequestSuccess(consolidatedList);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
                else {
                    detailsListener.OnRequestFailure("There are no Pending Request.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                detailsListener.OnRequestFailure(databaseError.getMessage());
            }
        });
    }

    private LinkedHashMap<String, List<ConsultationRequest>> groupDataIntoHashMap(List<ConsultationRequest> requests) {
        LinkedHashMap<String, List<ConsultationRequest>> groupedHashMap = new LinkedHashMap<>();

        for (ConsultationRequest request : requests) {
            String hashMapKey = request.getTimeStamp(); //String.valueOf(new Date(Long.parseLong(response.getTimeStamp())*1000))
            if (groupedHashMap.containsKey(hashMapKey)) {
                groupedHashMap.get(hashMapKey).add(request);
            } else {
                List<ConsultationRequest> requestList = new ArrayList<>();
                requestList.add(request);
                groupedHashMap.put(hashMapKey, requestList);
            }
        }
        return groupedHashMap;
    }
}

package cp2.citeportalv1.presenters.consult.faculty.request;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public interface ConsultRequestInteractor {
    void getNotifications(ResponseOnSyncListener onSyncListener);

    interface ResponseOnSyncListener {
        void onSuccess(List<ListItem> listItems);
        void onFailure(String errorMessage);
    }
}

package cp2.citeportalv1.presenters.consult.student.approved;

import cp2.citeportalv1.adapters.grouplist.StudentAppointmentsGroupAdapter;

public interface ApprovedResponseInteractor {
    void getDetails(OnApprovedResponseListener detailsListener);

    interface OnApprovedResponseListener {
        void OnResponseSuccess(StudentAppointmentsGroupAdapter adapter);
        void OnResponseFailure(String errorMessage);
    }
}

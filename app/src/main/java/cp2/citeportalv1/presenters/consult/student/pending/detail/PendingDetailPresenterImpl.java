package cp2.citeportalv1.presenters.consult.student.pending.detail;

import android.content.Context;

import java.text.ParseException;
import java.util.Date;

import cp2.citeportalv1.models.ConsultationRequest;

import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

public class PendingDetailPresenterImpl implements PendingDetailPresenter, PendingDetailInteractor.DetailListener {
    private PendingDetailPresenter.View view;
    private PendingDetailInteractor detailInteractor;

    public PendingDetailPresenterImpl(View view, Context context) {
        this.view = view;
        this.detailInteractor = new PendingDetailInteractorImpl(context);
    }

    @Override
    public void onStart(String notificationId) {
        detailInteractor.getNotificationDetail(notificationId, this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void getNotificationDetail(ConsultationRequest response) throws ParseException {
        Date date = yearDateFormat.parse(response.getRequestedDate());
        response.setRequestedDate(dateFormat.format(date));
        view.setPendingDetail(response);
    }

    @Override
    public void getDetailError(String errorMessage) {
        view.setPendingNotExist(errorMessage);
    }
}

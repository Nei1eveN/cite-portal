package cp2.citeportalv1.presenters.consult.student.approved;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.grouplist.StudentAppointmentsGroupAdapter;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.utils.Constants;

import static cp2.citeportalv1.utils.Constants.APPOINTMENTS;
import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;

class ApprovedResponseInteractorImpl implements ApprovedResponseInteractor {
    private Context context;

    ApprovedResponseInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getDetails(OnApprovedResponseListener responseListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(APPOINTMENTS);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<Appointments> approvedResponses = new ArrayList<>();
                    approvedResponses.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        String appointmentId = snapshot.getKey();
                        assert appointmentId != null;
                        Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(appointmentId);
                        approvedResponses.add(appointment);
                    }
                    LinkedHashMap<String, Set<Appointments>> groupedHashMap = Constants.groupAppointmentDataIntoHashMap(approvedResponses);
                    List<ListItem> consolidatedList = new ArrayList<>();

                    for (String date : groupedHashMap.keySet()) {
                        Log.d("dates", date);
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(dateFormat.format(new Date(Long.parseLong(date))));
                        dateItem.setDay(dayFormat.format(new Date(Long.parseLong(date))));
                        consolidatedList.add(dateItem);
                        for (Appointments appointment : Objects.requireNonNull(groupedHashMap.get(date))) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setAppointments(appointment);
                            consolidatedList.add(generalItem);
                        }
                    }

                    StudentAppointmentsGroupAdapter adapter = new StudentAppointmentsGroupAdapter(context, consolidatedList);

                    responseListener.OnResponseSuccess(adapter);
                }
                else {
                    responseListener.OnResponseFailure("Your Requests may be Pending for a while");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                responseListener.OnResponseFailure(databaseError.getMessage());
            }
        });
    }
}

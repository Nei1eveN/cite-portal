package cp2.citeportalv1.presenters.consult.faculty.consultationdetails;

import cp2.citeportalv1.models.ConsultationRequest;

public interface ConsultationDetailsInteractor {

    void getConsultationDetails(String notificationId, OnDetailsListener detailsListener);

    void saveApprovedRequestToDatabase(String notificationId,
                                       String requestedDay, String requestedDate,
                                       String requestedTimeStart, String requestedTimeEnd, String venue,
                                       String consultationTitle, String consultationBody,
                                       String consultationRemarks, OnResponseListener responseListener);
    void saveIgnoredRequestToDatabase(String notificationId,
                                      String requestedDay, String requestedDate,
                                      String requestedTimeStart, String requestedTimeEnd, String venue,
                                      String consultationTitle, String consultationBody, String reasonToRejectConsultationRequest,
                                      OnResponseListener responseListener);
    interface OnResponseListener {
        void onResponseSuccess(String successMessage);
        void onResponseFailure(String errorTitle, String errorMessage);
    }

    interface OnDetailsListener {
        void onDetailSuccess(ConsultationRequest request);
        void onDetailFailure(String errorMessage);
    }
}

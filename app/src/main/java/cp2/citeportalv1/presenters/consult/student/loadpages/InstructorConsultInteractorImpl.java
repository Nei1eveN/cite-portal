package cp2.citeportalv1.presenters.consult.student.loadpages;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.grouplist.InstructorGroupAdapter;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;

import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.groupFacultyDataIntoHashMap;

public class InstructorConsultInteractorImpl implements InstructorConsultInteractor {
    private Context context;

    InstructorConsultInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getDataFromQuery(String stringQuery, InstructorSearchListener searchListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY);
        databaseReference.keepSynced(true);
        Query query = databaseReference.orderByChild("lastName").startAt(stringQuery).endAt(stringQuery + "\uf8ff"); //.equalTo(stringQuery)
        query.keepSynced(true);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.d("key", dataSnapshot.getKey());
                    List<Faculty> facultyList = new ArrayList<>();
                    facultyList.clear();
                    Iterable<DataSnapshot> snapshots = dataSnapshot.getChildren();
                    for (DataSnapshot snapshot : snapshots) {
                        Log.d("studentUid", snapshot.getKey());
                        String studentUid = snapshot.getKey();
                        assert studentUid != null;
                        Faculty student = Objects.requireNonNull(snapshot.getValue(Faculty.class)).withId(studentUid);
                        facultyList.add(student);
                    }
                    LinkedHashMap<String, List<Faculty>> groupedHashMap = groupFacultyDataIntoHashMap(facultyList);
                    List<ListItem> consolidatedList = new ArrayList<>();
                    for (String department : groupedHashMap.keySet()){
                        DateItem dateItem = new DateItem();
                        dateItem.setDepartment(department);
                        consolidatedList.add(dateItem);

                        for (Faculty faculty : Objects.requireNonNull(groupedHashMap.get(department))){
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setFaculty(faculty);
                            consolidatedList.add(generalItem);
                        }
                    }

                    InstructorGroupAdapter adapter = new InstructorGroupAdapter(context, consolidatedList);
                    adapter.notifyDataSetChanged();

                    searchListener.onSuccess(adapter);
                } else {
                    searchListener.onFailure("Instructor " + stringQuery + " is not found. Maybe, your Instructor is not yet registered.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                searchListener.onFailure(databaseError.getMessage());
            }
        });
    }

}

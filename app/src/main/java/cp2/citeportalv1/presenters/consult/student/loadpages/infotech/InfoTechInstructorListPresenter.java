package cp2.citeportalv1.presenters.consult.student.loadpages.infotech;

import java.util.List;

import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.ListItem;

public interface InfoTechInstructorListPresenter {
    interface InfoTechView {
        void setProgress();
        void hideProgress();
        void setSnackMessage(String message);
        void setEmptyState(String emptyStateMessage);
        void setFacultyEmployees(List<ListItem> facultyEmployees);
    }
    void onStart();
    void onDestroy();
    void requestFacultyEmployees();
}

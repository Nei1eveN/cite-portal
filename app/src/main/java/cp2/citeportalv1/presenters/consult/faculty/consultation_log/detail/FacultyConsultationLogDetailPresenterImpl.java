package cp2.citeportalv1.presenters.consult.faculty.consultation_log.detail;

import android.content.Context;

import java.text.ParseException;
import java.util.Date;

import cp2.citeportalv1.models.ConsultationLog;
import cp2.citeportalv1.utils.Constants;

public class FacultyConsultationLogDetailPresenterImpl implements FacultyConsultationLogDetailPresenter, FacultyConsultationLogDetailInteractor.ConsultationLogDetailListener {
    private View view;
    private FacultyConsultationLogDetailInteractor interactor;

    public FacultyConsultationLogDetailPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyConsultationLogDetailInteractorImpl(context);
    }

    @Override
    public void onStart(String notificationId) {
        if (view != null) {
            view.showProgress("Loading Details", "Loading Appointment Details. Please wait...");
        }
        interactor.getNotificationDetail(notificationId, this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void getNotificationDetail(ConsultationLog consultationLog) {
        if (view != null) {
            view.hideProgress();
            try {
                Date date = Constants.yearDateFormat.parse(consultationLog.getRequestedDate());
                consultationLog.setRequestedDate(Constants.dateFormat.format(date));
                view.setApprovedDetail(consultationLog);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void getDetailError(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setDetailNotExist(errorTitle, errorMessage);
        }
    }
}

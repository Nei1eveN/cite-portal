package cp2.citeportalv1.presenters.consult.student.rejected.rejected_detail;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.ConsultationResponse;

import static cp2.citeportalv1.utils.Constants.CONSULTATION_RESPONSES;
import static cp2.citeportalv1.utils.Constants.STUDENTS;

class RejectedDetailInteractorImpl implements RejectedDetailInteractor {
    private Context context;

    RejectedDetailInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getNotificationDetail(String notificationId, DetailListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference notificationReference = firebaseDatabase.getReference(STUDENTS)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                .child(CONSULTATION_RESPONSES);
        notificationReference.keepSynced(true);

        notificationReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    ConsultationResponse response = Objects.requireNonNull(dataSnapshot.getValue(ConsultationResponse.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    try {
                        listener.getNotificationDetail(response);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    listener.getDetailError("Detail does not exist");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.getDetailError(databaseError.getMessage());
            }
        });
    }
}

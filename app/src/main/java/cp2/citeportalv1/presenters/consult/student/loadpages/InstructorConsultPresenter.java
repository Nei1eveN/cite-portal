package cp2.citeportalv1.presenters.consult.student.loadpages;

import cp2.citeportalv1.adapters.grouplist.InstructorGroupAdapter;

public interface InstructorConsultPresenter {
    interface View {
        void initViews();
        void setEmptyState(String emptyStateMessage);
        void setInstructorFound(InstructorGroupAdapter adapter);
        void setNoInstructorFound(String noClassmateFoundMessage);
    }
    void onStart();
    void onDestroy();
    void submitQuery(String query);
}

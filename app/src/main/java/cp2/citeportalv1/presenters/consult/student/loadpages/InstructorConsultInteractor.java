package cp2.citeportalv1.presenters.consult.student.loadpages;

import cp2.citeportalv1.adapters.grouplist.InstructorGroupAdapter;

public interface InstructorConsultInteractor {
    void getDataFromQuery(String stringQuery, InstructorSearchListener searchListener);

    interface InstructorSearchListener {
        void onSuccess(InstructorGroupAdapter adapter);

        void onFailure(String errorMessage);
    }
}

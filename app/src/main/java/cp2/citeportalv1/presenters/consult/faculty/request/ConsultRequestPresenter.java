package cp2.citeportalv1.presenters.consult.faculty.request;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public interface ConsultRequestPresenter {
    interface View {
        void initViews();
        void showProgress();
        void hideProgress();
        void setSnackMessage(String snackMessage);
        void hideViewsState();
        void setEmptyState(String message);
        void setNotifications(List<ListItem> listItems);

    }
    void onStart();
    void onDestroy();
    void requestNotificationList();
}

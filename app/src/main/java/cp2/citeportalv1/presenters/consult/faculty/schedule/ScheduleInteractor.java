package cp2.citeportalv1.presenters.consult.faculty.schedule;

import cp2.citeportalv1.adapters.grouplist.ScheduleGroupAdapter;

public interface ScheduleInteractor {
    void getSchedules(OnFindingScheduleListener listener);

    void saveSchedule(String day, String timeStart, String timeEnd, String venue, OnSavingScheduleListener listener);

    void saveFinalSchedule(String day, String timeStart, String timeEnd, String venue, String department, String lastName, String firstName, String midName, OnSavingScheduleListener listener);

    void getScheduleAccordingToDay(String day, OnFindingDayScheduleListener listener);

    interface OnFindingScheduleListener {
        void onFindingScheduleSuccess(ScheduleGroupAdapter adapter);
        void onFindingScheduleFailure(String errorMessage);
    }

    interface OnSavingScheduleListener {
        void onSavingSuccess(String successMessage, String day);
        void onSavingFailure(String errorMessage, String day);
    }

    interface OnFindingDayScheduleListener {
        void onFindingDaySuccess(ScheduleGroupAdapter adapter);
        void onFindingDayFailure(String errorMessage);
    }
}

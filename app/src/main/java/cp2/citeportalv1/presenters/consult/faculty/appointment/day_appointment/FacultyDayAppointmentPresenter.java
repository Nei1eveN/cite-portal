package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment;

import java.util.Date;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentDayGroupAdapter;

public interface FacultyDayAppointmentPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void showSnackMessage(String message);
        void setAppointments(FacultyAppointmentDayGroupAdapter adapter);
        void setEmptyAppointment(String emptyMessage);
    }
    void onStart();
    void onDestroy();
    void findAppointments();
    void findAppointment(Date dateClicked);
}

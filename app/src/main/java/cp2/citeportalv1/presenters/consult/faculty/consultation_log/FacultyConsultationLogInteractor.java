package cp2.citeportalv1.presenters.consult.faculty.consultation_log;

import cp2.citeportalv1.adapters.grouplist.FacultyConsultationLogGroupAdapter;

public interface FacultyConsultationLogInteractor {
    void getDetails(FacultyConsultationLogListener detailsListener);

    interface FacultyConsultationLogListener {
        void OnResponseSuccess(FacultyConsultationLogGroupAdapter adapter);
        void OnResponseFailure(String errorMessage);
    }
}

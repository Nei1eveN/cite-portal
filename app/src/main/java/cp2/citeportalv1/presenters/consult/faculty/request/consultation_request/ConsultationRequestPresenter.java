package cp2.citeportalv1.presenters.consult.faculty.request.consultation_request;

import cp2.citeportalv1.adapters.grouplist.ConsultationRequestsGroupAdapter;

public interface ConsultationRequestPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setSnackMessage(String snackMessage);
        void loadPendingResponses(ConsultationRequestsGroupAdapter adapter);
        void setEmptyState(String emptyMessage);
    }
    void onStart();
    void onDestroy();
    void requestPendingList();
}

package cp2.citeportalv1.presenters.consult.faculty.consultationdetails.reschedule;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.Date;
import java.util.List;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.utils.EventDecorator;

public interface RescheduleRequestInteractor {

    interface OnRequestDetailsListener {
        void onRequestDetailSuccess(ConsultationRequest request);
        void onRequestDetailFailure(String errorTitle, String errorMessage);
    }

    void getConsultationRequestDetails(String notificationId, OnRequestDetailsListener listener);

    interface OnScheduleFromDateListener {
        void onScheduleFound(List<Schedule> schedules);
        void onScheduleNotFound(String errorMessage);
    }

    void getScheduleFromDate(String facultyId, Date dateSelected, OnScheduleFromDateListener listener);

    interface OnAppointmentFromDateListener {
        void onAppointmentFound(FacultyAppointmentsGroupAdapter adapter);
        void onAppointmentNotFound(String errorMessage);
    }

    void getAppointmentFromDate(String facultyId, Date dateSelected, OnAppointmentFromDateListener listener);

    interface OnAppointmentsListener {
        void onFoundAppointments(EventDecorator eventDecorator);
    }

    void getAppointments(String facultyId, OnAppointmentsListener listener);

    interface OnAppointmentTimeSlotsListener {
        void onFoundExistingAppointments(Timepoint[] existingTimes);
    }

    void findExistingTimeSlotAppointmentsFromDate(String facultyId, Date dateSelected, OnAppointmentTimeSlotsListener listener);

    interface RescheduleRequestListener {
        void onRescheduleSuccess(String title, String message);
        void onRescheduleFailure(String errorTitle, String errorMessage);
    }

    void saveApprovedRequestToDatabase(String notificationId,
                                       String rescheduledTimeStart, String rescheduledTimeEnd,
                                       String rescheduledVenue, String rescheduledDay, String rescheduledDate,
                                       String consultationTitle, String consultationBody, String rescheduledRemarks,
                                       RescheduleRequestListener listener);
}

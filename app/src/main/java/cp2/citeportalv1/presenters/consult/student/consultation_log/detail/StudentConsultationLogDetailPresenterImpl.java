package cp2.citeportalv1.presenters.consult.student.consultation_log.detail;

import android.content.Context;

import java.text.ParseException;
import java.util.Date;

import cp2.citeportalv1.models.ConsultationLog;
import cp2.citeportalv1.utils.Constants;

public class StudentConsultationLogDetailPresenterImpl implements StudentConsultationLogDetailPresenter, StudentConsultationLogDetailInteractor.ConsultationLogDetailListener {
    private View view;
    private StudentConsultationLogDetailInteractor interactor;

    public StudentConsultationLogDetailPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentConsultationLogDetailInteractorImpl(context);
    }

    @Override
    public void onStart(String notificationId) {
        if (view != null) {
            view.showProgress("Loading Details", "Loading Appointment Details. Please wait...");
        }
        interactor.getNotificationDetail(notificationId, this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void getNotificationDetail(ConsultationLog consultationLog) {
        if (view != null) {
            view.hideProgress();
            try {
                Date date = Constants.yearDateFormat.parse(consultationLog.getRequestedDate());
                consultationLog.setRequestedDate(Constants.dateFormat.format(date));
                view.setApprovedDetail(consultationLog);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void getDetailError(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setDetailNotExist(errorTitle, errorMessage);
        }
    }
}

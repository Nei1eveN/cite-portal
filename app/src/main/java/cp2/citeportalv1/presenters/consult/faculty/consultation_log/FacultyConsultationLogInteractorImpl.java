package cp2.citeportalv1.presenters.consult.faculty.consultation_log;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.grouplist.FacultyConsultationLogGroupAdapter;
import cp2.citeportalv1.models.ConsultationLog;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.utils.Constants;

import static cp2.citeportalv1.utils.Constants.CONSULTATION_LOG;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;

class FacultyConsultationLogInteractorImpl implements FacultyConsultationLogInteractor {
    private Context context;

    FacultyConsultationLogInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getDetails(FacultyConsultationLogListener detailsListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CONSULTATION_LOG);
        databaseReference.keepSynced(true);

        databaseReference.orderByChild("requestedTimeStart").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<ConsultationLog> appointments = new ArrayList<>();
                    appointments.clear();
                    List<ListItem> consolidatedList = new ArrayList<>();
                    consolidatedList.clear();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        ConsultationLog appointment = Objects.requireNonNull(snapshot.getValue(ConsultationLog.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        appointments.add(appointment);
                    }

                    LinkedHashMap<String, Set<ConsultationLog>> groupedHashMap = Constants.groupConsultationLogDataIntoHashMap(appointments);

                    List<Date> dates = new ArrayList<>();
                    for (String date : groupedHashMap.keySet()) {
                        dates.add(new Date(Long.parseLong(date)));
                    }
                    Collections.sort(dates);

                    Log.d("datesCollection", String.valueOf(dates));

                    for (Date date : dates) {
                        Log.d("date", String.valueOf(date));
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(dateFormat.format(date));
                        dateItem.setDay(dayFormat.format(date));
                        consolidatedList.add(dateItem);
                        for (ConsultationLog schedule : Objects.requireNonNull(groupedHashMap.get(String.valueOf(date.getTime())))) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setConsultationLog(schedule);
                            consolidatedList.add(generalItem);
                        }
                    }

                    FacultyConsultationLogGroupAdapter adapter = new FacultyConsultationLogGroupAdapter(context, consolidatedList);

                    detailsListener.OnResponseSuccess(adapter);

                } else {
                    detailsListener.OnResponseFailure("Your Requests may be Pending for a while");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                detailsListener.OnResponseFailure(
                        "Reading Error\n\n" +
                                "Code:\n" + databaseError.getCode() + "\n\n" +
                                "Message:\n" + databaseError.getMessage() + "\n\n" +
                                "Details:\n" + databaseError.getDetails());
            }
        });
    }
}

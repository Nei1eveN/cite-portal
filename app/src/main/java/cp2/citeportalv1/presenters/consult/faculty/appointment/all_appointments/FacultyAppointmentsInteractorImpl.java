package cp2.citeportalv1.presenters.consult.faculty.appointment.all_appointments;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.utils.Constants;

import static cp2.citeportalv1.utils.Constants.APPOINTMENTS;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;

class FacultyAppointmentsInteractorImpl implements FacultyAppointmentsInteractor {
    private Context context;

    FacultyAppointmentsInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getAppointmentsAccordingToDay(String day, LoadAppointmentsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.child(APPOINTMENTS).orderByChild("requestedDay").equalTo(day).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<Appointments> appointments = new ArrayList<>();
                    appointments.clear();
                    List<ListItem> consolidatedList = new ArrayList<>();
                    consolidatedList.clear();
                    List<Date> dates = new ArrayList<>();
                    dates.clear();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        String appointmentId = snapshot.getKey();
                        assert appointmentId != null;
                        Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(appointmentId);
                        appointments.add(appointment);
                    }
                    LinkedHashMap<String, Set<Appointments>> groupedHashMap = Constants.groupAppointmentDataIntoHashMap(appointments);

                    for (String date : groupedHashMap.keySet()) {
                        dates.add(new Date(Long.parseLong(date)));
                    }
                    Collections.sort(dates);

                    Log.d("datesCollection", String.valueOf(dates));

                    for (Date date : dates) {
                        Log.d("date", String.valueOf(date));
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(dateFormat.format(date));
                        dateItem.setDay(dayFormat.format(date));
                        consolidatedList.add(dateItem);
                        for (Appointments schedule : Objects.requireNonNull(groupedHashMap.get(String.valueOf(date.getTime())))) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setAppointments(schedule);
                            consolidatedList.add(generalItem);
                        }
                    }

                    FacultyAppointmentsGroupAdapter adapter = new FacultyAppointmentsGroupAdapter(context, consolidatedList);

                    listener.onSuccessLoadingAppointment("Appointments Loaded", adapter);
                }
                else {
                    listener.onFailureLoadingAppointment("There are no Scheduled Appointments for "+day+".\n\nClick the icon to check pending requests.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFailureLoadingAppointment(databaseError.getMessage());
            }
        });
    }
}

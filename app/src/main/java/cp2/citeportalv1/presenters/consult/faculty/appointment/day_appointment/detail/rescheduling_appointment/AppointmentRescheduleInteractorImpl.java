package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail.rescheduling_appointment;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;
import org.threeten.bp.Month;
import org.threeten.bp.Year;
import org.threeten.bp.YearMonth;
import org.threeten.bp.temporal.TemporalAdjusters;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import cp2.citeportalv1.R;
import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.Interval;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.utils.Constants;
import cp2.citeportalv1.utils.EventDecorator;

import static cp2.citeportalv1.utils.Constants.*;

class AppointmentRescheduleInteractorImpl implements AppointmentRescheduleInteractor {
    private Context context;

    AppointmentRescheduleInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getConsultationRequestDetails(String notificationId, OnRequestDetailsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference notificationReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(APPOINTMENTS);
        notificationReference.keepSynced(true);
        notificationReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Appointments appointments = Objects.requireNonNull(dataSnapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    listener.onRequestDetailSuccess(appointments);
                } else {
                    listener.onRequestDetailFailure("Appointment Detail Error", "Appointment Details does not exist");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onRequestDetailFailure("Database Error", databaseError.getMessage());
            }
        });
    }

    @Override
    public void getScheduleFromDate(String facultyId, Date dateSelected, OnScheduleFromDateListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference scheduleReference = firebaseDatabase.getReference(FACULTY).child(facultyId).child(CONSULTATION_SCHEDULES);
        scheduleReference.keepSynced(true);

        scheduleReference.child(dayFormat.format(dateSelected)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Schedule> schedules = new ArrayList<>();
                    schedules.clear();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Schedule schedule = Objects.requireNonNull(snapshot.getValue(Schedule.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        schedules.add(schedule);
                    }

                    listener.onScheduleFound(schedules);
                } else {
                    listener.onScheduleNotFound("You don't have a Consultation Schedule for " + dayFormat.format(dateSelected));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onScheduleNotFound(databaseError.getMessage());
            }
        });
    }

    @Override
    public void getAppointmentFromDate(String facultyId, Date dateSelected, OnAppointmentFromDateListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(facultyId).child(APPOINTMENTS);
        databaseReference.keepSynced(true);

        databaseReference.orderByChild("requestedDate").equalTo(yearDateFormat.format(dateSelected)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Appointments> appointmentsList = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        appointmentsList.add(appointment);
                    }

                    LinkedHashMap<String, Set<Appointments>> groupedHashMap = Constants.groupAppointmentDataIntoHashMap(appointmentsList);
                    List<ListItem> consolidatedList = new ArrayList<>();

                    for (String date : groupedHashMap.keySet()) {
                        Log.d("dates", date);
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(dateFormat.format(new Date(Long.parseLong(date))));
                        dateItem.setDay(dayFormat.format(new Date(Long.parseLong(date))));
                        consolidatedList.add(dateItem);
                        for (Appointments appointment : Objects.requireNonNull(groupedHashMap.get(date))) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setAppointments(appointment);
                            consolidatedList.add(generalItem);
                        }
                    }

                    FacultyAppointmentsGroupAdapter adapter = new FacultyAppointmentsGroupAdapter(context, consolidatedList);
                    listener.onAppointmentFound(adapter);

                } else {
                    listener.onAppointmentNotFound("You don't have a Consultation Appointment for " + dateFormat.format(dateSelected));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onAppointmentNotFound(databaseError.getMessage());
            }
        });
    }

    @Override
    public void getAppointments(String facultyId, OnAppointmentsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(facultyId).child(CONSULTATION_SCHEDULES);
//        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(facultyId).child(APPOINTMENTS);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<String> days = new ArrayList<>();
                    List<CalendarDay> calendarDays = new ArrayList<>();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        days.add(snapshot.getKey());
                    }

                    for (int i = 0, daysSize = days.size(); i < daysSize; i++) {
                        String day = days.get(i);
                        addDecoratorAccordingToWeekDay(day, calendarDays, listener);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addDecoratorAccordingToWeekDay(String day, List<CalendarDay> calendarDays, OnAppointmentsListener listener) {
        List<LocalDate> localDates = new ArrayList<>();

        if (day.equals("Monday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = Year.now().atMonth(month).atDay(1)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));

            Month monday = date.getMonth();
            while (monday == month) {
                Log.d("dates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
                monday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Tuesday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = Year.now().atMonth(month).atDay(2)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.TUESDAY));

            Month tuesday = date.getMonth();
            while (tuesday == month) {
                Log.d("dates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.TUESDAY));
                tuesday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Wednesday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = Year.now().atMonth(month).atDay(3)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.WEDNESDAY));

            Month wednesday = date.getMonth();
            while (wednesday == month) {
                Log.d("dates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.WEDNESDAY));
                wednesday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Thursday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = Year.now().atMonth(month).atDay(4)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.THURSDAY));

            Month thursday = date.getMonth();
            while (thursday == month) {
                Log.d("dates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.THURSDAY));
                thursday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Friday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = YearMonth.now().atDay(5)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.FRIDAY));

            Month friday = date.getMonth();
            while (friday == month) {
                Log.d("fridayDates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
                friday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Saturday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = YearMonth.now().atDay(6)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));

            Month saturday = date.getMonth();
            while (saturday == month) {
                Log.d("fridayDates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
                saturday = date.getMonth();

                localDates.add(date);
            }
        }
        if (day.equals("Sunday")) {
            Month month = Month.valueOf(monthFormat.format(new Date()).toUpperCase());
            LocalDate date = YearMonth.now().atDay(7)
                    .with(TemporalAdjusters.firstInMonth(DayOfWeek.SUNDAY));

            Month sunday = date.getMonth();
            while (sunday == month) {
                Log.d("fridayDates", String.valueOf(date));
                date = date.with(TemporalAdjusters.next(DayOfWeek.SUNDAY));
                sunday = date.getMonth();

                localDates.add(date);
            }
        }

        for (LocalDate date : localDates) {
            try {
                Date gatheredDates = yearDateFormat.parse(String.valueOf(date));
                CalendarDay calendarDay = CalendarDay.from(gatheredDates);
                calendarDays.add(calendarDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        EventDecorator eventDecorator = new EventDecorator(context.getResources().getColor(R.color.fullBluePrimary), calendarDays);
        listener.onFoundAppointments(eventDecorator);
    }

    @Override
    public void findExistingTimeSlotAppointmentsFromDate(String facultyId, Date dateSelected, OnAppointmentTimeSlotsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(facultyId).child(APPOINTMENTS);
        databaseReference.keepSynced(true);

        databaseReference.orderByChild("requestedDate").equalTo(yearDateFormat.format(dateSelected)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Appointments> appointments = new ArrayList<>();
                    appointments.clear();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        appointments.add(appointment);
                    }

                    for (Appointments appointment : appointments) {
                        try {
                            Date date = timeFormat12hr.parse(appointment.getRequestedTimeStart());
                            Timepoint[] existingTimes = new Timepoint[]{new Timepoint(Integer.valueOf(hourFormat.format(date)), Integer.valueOf(minuteFormat.format(date)))};
                            listener.onFoundExistingAppointments(existingTimes);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void findExistingTimeSlotEndingsFromDate(String facultyId, Date dateSelected, OnAppointmentTimeSlotsEndingListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(facultyId).child(APPOINTMENTS);
        databaseReference.keepSynced(true);

        databaseReference.orderByChild("requestedDate").equalTo(yearDateFormat.format(dateSelected)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Appointments> appointments = new ArrayList<>();
                    appointments.clear();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        appointments.add(appointment);
                    }

                    for (Appointments appointment : appointments) {
                        try {
                            Date date = timeFormat12hr.parse(appointment.getRequestedTimeEnd());
                            Timepoint[] existingTimes = new Timepoint[]{new Timepoint(Integer.valueOf(hourFormat.format(date)), Integer.valueOf(minuteFormat.format(date)))};
                            listener.onFoundExistingTimeEndings(existingTimes);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void saveApprovedRequestToDatabase(String notificationId, String rescheduledTimeStart, String rescheduledTimeEnd, String rescheduledVenue, String rescheduledDay, String rescheduledDate, String consultationTitle, String consultationBody, String rescheduledRemarks, RescheduleRequestListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onRescheduleFailure("Network Error", "Please check your internet connection.");
        } else {
            try {
                if (yearDateFormat.format(new Date()).equals(rescheduledDate) && timeFormat12hr.parse(timeFormat12hr.format(new Date())).after(timeFormat12hr.parse(rescheduledTimeEnd))) {
                    /**SAVING ERROR: CURRENT TIME GREATER THAN CONSULTATION HOURS**/
                    listener.onRescheduleFailure("Appointment Saving Failed", "Requesting beyond the scheduled time of consultation is no longer allowed for this day. Please reschedule from tomorrow onwards.");
                } else {
                    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

                    /**FACULTY CONSULTATION REQUEST DETAILS**/
                    DatabaseReference facultyConsultationRequestReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(APPOINTMENTS);
                    facultyConsultationRequestReference.keepSynced(true);

                    facultyConsultationRequestReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                Appointments appointment = Objects.requireNonNull(dataSnapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                                /**FACULTY DETAILS**/
                                DatabaseReference facultyDetailReference = firebaseDatabase.getReference().child(FACULTY).child(Objects.requireNonNull(appointment).getReceiverId());
                                facultyDetailReference.keepSynced(true);

                                facultyDetailReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                                            /**APPOINTMENT SCHEDULE DETAIL CHECKER**/
                                            DatabaseReference checkAppointmentReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(faculty).facultyId).child(APPOINTMENTS);
                                            checkAppointmentReference.keepSynced(true);

                                            Query query = checkAppointmentReference.orderByChild("requestedSchedule").equalTo(rescheduledDate + " - " + rescheduledDay + " - " + rescheduledTimeStart + " - " + rescheduledTimeEnd + " @ " + rescheduledVenue);
                                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    if (dataSnapshot.exists()) {
                                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                            Appointments appointments = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                                                            listener.onRescheduleFailure("Existing Appointment", "You already have an approved appointment for this time schedule with the following details:\n\nStudent Name:\n" + appointments.getSenderLastName() + ", " + appointments.getSenderFirstName() + " " + appointments.getSenderMiddleName() + "\n\nDetails:\n" + appointments.getMessageTitle() + "\n" + appointments.getRequestedDay() + "\n" + appointments.getRequestedDate() + "\n" + appointments.getRequestedTimeStart() + " - " + appointments.getRequestedTimeEnd() + " @ " + appointments.getVenue());
                                                        }
                                                    } else {
                                                        /**STUDENT APPOINTMENT DETAILS**/
                                                        Map<String, Object> studentAppointmentMap = new HashMap<>();

                                                        studentAppointmentMap.put(SENDER_ID, appointment.getReceiverId());
                                                        studentAppointmentMap.put(RECEIVER_ID, appointment.getSenderId());

                                                        studentAppointmentMap.put(SENDER_FIRST_NAME, faculty.getfName());
                                                        studentAppointmentMap.put(SENDER_MIDDLE_NAME, faculty.getMidName());
                                                        studentAppointmentMap.put(SENDER_LAST_NAME, faculty.getLastName());
                                                        studentAppointmentMap.put(SENDER_IMAGE_URL, faculty.getFacultyImageURL());
                                                        studentAppointmentMap.put(SENDER_DEPARTMENT, faculty.getDepartment());

                                                        studentAppointmentMap.put(MESSAGE_TITLE, appointment.getMessageTitle());
                                                        studentAppointmentMap.put(MESSAGE_BODY, appointment.getMessageBody());
                                                        studentAppointmentMap.put(MESSAGE_REMARKS, appointment.getMessageRemarks());
                                                        studentAppointmentMap.put(MESSAGE_SIDE_NOTE, "This is a rescheduled appointment.\n\n" + "Previous Appointment Details:\n\n" + "" + appointment.getRequestedDay() + "\n" + appointment.getRequestedDate() + "\n" + appointment.getRequestedTimeStart() + " - " + appointment.getRequestedTimeEnd() + " @ " + appointment.getVenue() + "\n\nPlease be informed that this appointment will be held from " + "" + rescheduledTimeStart + " - " + rescheduledTimeEnd + " @ " + rescheduledVenue + " on " + rescheduledDay + ", " + rescheduledDate);
                                                        studentAppointmentMap.put(MESSAGE_STATUS, "APPROVED (RESCHEDULED)");

                                                        studentAppointmentMap.put(REQUESTED_DAY, rescheduledDay);
                                                        studentAppointmentMap.put(REQUESTED_DATE, rescheduledDate);
                                                        studentAppointmentMap.put(REQUESTED_SCHEDULE, rescheduledDate + " - " + rescheduledDay + " - " + rescheduledTimeStart + " - " + rescheduledTimeEnd + " @ " + rescheduledVenue);
                                                        studentAppointmentMap.put(REQUESTED_TIME_START, rescheduledTimeStart);
                                                        studentAppointmentMap.put(REQUESTED_TIME_END, rescheduledTimeEnd);
                                                        studentAppointmentMap.put(VENUE, rescheduledVenue);

                                                        studentAppointmentMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

                                                        studentAppointmentMap.put(APPOINTMENT_CODE, appointment.getSenderId() + " / " + rescheduledDay + " / " + rescheduledDate + " / " + rescheduledTimeStart + " - " + rescheduledTimeEnd + " @ " + rescheduledVenue);
                                                        studentAppointmentMap.put(APPOINTMENT_CC, appointment.getSenderCc());

                                                        studentAppointmentMap.put(APPOINTMENT_FEEDBACK, rescheduledRemarks);

                                                        /**STUDENT APPOINTMENT REMOVE**/
                                                        DatabaseReference studentAppointmentReference = firebaseDatabase.getReference(STUDENTS).child(appointment.getSenderId()).child(APPOINTMENTS);
                                                        studentAppointmentReference.keepSynced(true);

                                                        DatabaseReference facultyAppointmentsFromDateReference = FirebaseDatabase.getInstance().getReference(FACULTY).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(APPOINTMENTS);
                                                        facultyAppointmentsFromDateReference.keepSynced(true);

                                                        facultyAppointmentsFromDateReference.orderByChild("requestedDate").equalTo(rescheduledDate).addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                if (dataSnapshot.exists()) {
                                                                    List<Appointments> appointments = new ArrayList<>();

                                                                    List<Date> timeStarts = new ArrayList<>();
                                                                    List<Date> timeEnds = new ArrayList<>();

                                                                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                                        Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                                                                        appointments.add(appointment);
                                                                    }

                                                                    List<cp2.citeportalv1.models.Interval> intervals = new ArrayList<>(appointments.size());

                                                                    try {
                                                                        for (int i = 0, appointmentsSize = appointments.size(); i < appointmentsSize; i++) {
                                                                            Appointments appointment = appointments.get(i);
                                                                            timeStarts.add(timeFormat12hr.parse(appointment.getRequestedTimeStart()));
                                                                            timeEnds.add(timeFormat12hr.parse(appointment.getRequestedTimeEnd()));

                                                                            intervals.add(new cp2.citeportalv1.models.Interval(Integer.valueOf(String.valueOf(timeStarts.get(i).getTime())), Integer.valueOf(String.valueOf(timeEnds.get(i).getTime()))));

                                                                            if (i == (appointmentsSize - 1)) {
                                                                                Interval inputInterval = new Interval(Integer.valueOf(String.valueOf(timeFormat12hr.parse(rescheduledTimeStart).getTime())), Integer.valueOf(String.valueOf(timeFormat12hr.parse(rescheduledTimeEnd).getTime())));
                                                                                intervals.add(inputInterval);

                                                                                Log.d("interactor--intvls1", String.valueOf(intervals));

                                                                                List<Interval> result = findIntervalsThatOverlap(intervals);
                                                                                if (!result.isEmpty()) {
                                                                                    Log.d("interactor--intvls2", String.valueOf(result));

                                                                                    for (int j = 0, resultSize = result.size(); j < resultSize; j++) {
                                                                                        Interval interval = result.get(j);

                                                                                        if (j == (resultSize - 1)) {
                                                                                            if (interval.start < interval.end) {
                                                                                                /**SAVING ERROR: TIME CONFLICT**/
                                                                                                //timeFormat12hr.format(interval.start) timeFormat12hr.format(interval.end)
                                                                                                listener.onRescheduleFailure("Appointment Time Conflict", "There is a conflicting time of appointment.\n\nDetails:\n" + appointment.getMessageTitle() + "\n" + appointment.getRequestedTimeStart() + " - " + appointment.getRequestedTimeEnd() + " @ " + appointment.getVenue());
                                                                                            } else {
                                                                                                /**SAVING SUCCESS: SAVE FINAL APPOINTMENT DETAILS**/
                                                                                                saveFinalAppointmentDetails(notificationId, appointment, studentAppointmentMap, firebaseDatabase, studentAppointmentReference, listener, rescheduledRemarks, rescheduledTimeStart, rescheduledTimeEnd, rescheduledVenue, rescheduledDay, rescheduledDate);
                                                                                            }
                                                                                        }

                                                                                    }
                                                                                } else {
                                                                                    /**SAVING SUCCESS: SAVE FINAL APPOINTMENT DETAILS
                                                                                     *
                                                                                     * RESULT INTERVALS IS EMPTY: NO CONFLICT**/
                                                                                    saveFinalAppointmentDetails(notificationId, appointment, studentAppointmentMap, firebaseDatabase, studentAppointmentReference, listener, rescheduledRemarks, rescheduledTimeStart, rescheduledTimeEnd, rescheduledVenue, rescheduledDay, rescheduledDate);
                                                                                }
                                                                            }
                                                                        }
                                                                    } catch (ParseException e) {
                                                                        listener.onRescheduleFailure("Parsing Error", e.getMessage());
                                                                    }


                                                                } else {
                                                                    saveFinalAppointmentDetails(notificationId, appointment, studentAppointmentMap, firebaseDatabase, studentAppointmentReference, listener, rescheduledRemarks, rescheduledTimeStart, rescheduledTimeEnd, rescheduledVenue, rescheduledDay, rescheduledDate);
                                                                }
                                                            }

                                                            @Override
                                                            public void onCancelled
                                                                    (@NonNull DatabaseError databaseError) {
                                                                listener.onRescheduleFailure("Database Error", databaseError.getMessage());
                                                            }
                                                        });
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError
                                                                                databaseError) {
                                                    listener.onRescheduleFailure("Database Error", databaseError.getMessage());
                                                }
                                            });
                                        } else {
                                            listener.onRescheduleFailure("Not Existing Detail", "Cannot read notification details. Please check if it is still on your list of requests.");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        listener.onRescheduleFailure("Database Error", databaseError.getMessage());
                                    }
                                });
                            } else {
                                listener.onRescheduleFailure("Not Existing Detail", "Cannot read notification details. Please check if it is still on your list of requests.");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            listener.onRescheduleFailure("Database Error", databaseError.getMessage());
                        }
                    });
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

    }

    private void saveFinalAppointmentDetails(String notificationId,
                                             Appointments appointment, Map<String, Object> studentAppointmentMap,
                                             FirebaseDatabase firebaseDatabase, DatabaseReference studentConsultationRequestReference,
                                             RescheduleRequestListener listener, String rescheduledRemarks,
                                             String rescheduledTimeStart, String rescheduledTimeEnd, String rescheduledVenue,
                                             String rescheduledDay, String rescheduledDate) {

        /**FACULTY APPOINTMENT DETAILS**/
        Map<String, Object> facultyAppointmentMap = new HashMap<>();

        facultyAppointmentMap.put(SENDER_ID, appointment.getSenderId());
        facultyAppointmentMap.put(RECEIVER_ID, appointment.getReceiverId());

        facultyAppointmentMap.put(SENDER_FIRST_NAME, appointment.getSenderFirstName());
        facultyAppointmentMap.put(SENDER_MIDDLE_NAME, appointment.getSenderMiddleName());
        facultyAppointmentMap.put(SENDER_LAST_NAME, appointment.getSenderLastName());
        facultyAppointmentMap.put(SENDER_IMAGE_URL, appointment.getSenderImageURL());
        facultyAppointmentMap.put(SENDER_PROGRAM, appointment.getSenderProgram());
        facultyAppointmentMap.put(SENDER_YEAR_LEVEL, appointment.getSenderYearLevel());

        facultyAppointmentMap.put(MESSAGE_TITLE, appointment.getMessageTitle());
        facultyAppointmentMap.put(MESSAGE_BODY, appointment.getMessageBody());
        facultyAppointmentMap.put(MESSAGE_REMARKS, appointment.getMessageRemarks());
        facultyAppointmentMap.put(MESSAGE_SIDE_NOTE, "This is a rescheduled appointment.\n\n" + "Previous Appointment Details:\n\n" + "" + appointment.getRequestedDay() + "\n" + appointment.getRequestedDate() + "\n" + appointment.getRequestedTimeStart() + " - " + appointment.getRequestedTimeEnd() + " @ " + appointment.getVenue() + "\n\nPlease be informed that this appointment will be held from " + "" + rescheduledTimeStart + " - " + rescheduledTimeEnd + " @ " + rescheduledVenue + " on " + rescheduledDay + ", " + rescheduledDate);
        facultyAppointmentMap.put(MESSAGE_STATUS, "APPROVED (RESCHEDULED)");

        facultyAppointmentMap.put(REQUESTED_DAY, rescheduledDay);
        facultyAppointmentMap.put(REQUESTED_DATE, rescheduledDate);
        facultyAppointmentMap.put(REQUESTED_SCHEDULE, rescheduledDate + " - " + rescheduledDay + " - " + rescheduledTimeStart + " - " + rescheduledTimeEnd + " @ " + rescheduledVenue);
        facultyAppointmentMap.put(REQUESTED_TIME_START, rescheduledTimeStart);
        facultyAppointmentMap.put(REQUESTED_TIME_END, rescheduledTimeEnd);
        facultyAppointmentMap.put(VENUE, rescheduledVenue);

        facultyAppointmentMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

        facultyAppointmentMap.put(APPOINTMENT_CODE, appointment.getSenderId() + " / " + rescheduledDay + " / " + rescheduledDate + " / " + rescheduledTimeStart + " - " + rescheduledTimeEnd + " @ " + rescheduledVenue);
        facultyAppointmentMap.put(APPOINTMENT_CC, appointment.getSenderCc());

        facultyAppointmentMap.put(APPOINTMENT_FEEDBACK, rescheduledRemarks);


        /**STUDENT APPOINTMENT SAVING**/
        DatabaseReference studentAppointmentSavingReference = FirebaseDatabase.getInstance().getReference(STUDENTS).child(appointment.getSenderId()).child(APPOINTMENTS);
        studentAppointmentSavingReference.keepSynced(true);

        /**APPOINTMENT KEY**/
        String appointmentKey = studentAppointmentSavingReference.push().getKey();

        /**FACULTY APPOINTMENT SAVING**/
        DatabaseReference facultyAppointmentSavingReference = FirebaseDatabase.getInstance().getReference(FACULTY).child(appointment.getReceiverId()).child(APPOINTMENTS);
        facultyAppointmentSavingReference.keepSynced(true);

        if (!appointment.getSenderCc().isEmpty()) {
            Log.d("interactor--ReschedCcs", String.valueOf(appointment.getSenderCc()));

            studentAppointmentSavingReference.child(Objects.requireNonNull(notificationId)).updateChildren(studentAppointmentMap)
                    .addOnSuccessListener(((Activity) context), aVoid ->
                            facultyAppointmentSavingReference.child(Objects.requireNonNull(notificationId))
                                    .updateChildren(facultyAppointmentMap).addOnSuccessListener(((Activity) context), aVoid14 -> {

                                List<String> senderCc = appointment.getSenderCc();
                                for (int i = 0, senderCcSize = senderCc.size(); i < senderCcSize; i++) {
                                    String ccUid = senderCc.get(i);
                                    DatabaseReference carbonAppointmentReference = firebaseDatabase.getReference(STUDENTS).child(ccUid).child(APPOINTMENTS);
                                    carbonAppointmentReference.keepSynced(true);

                                    /**CARBON APPOINTMENT SAVING**/
                                    carbonAppointmentReference.child(Objects.requireNonNull(notificationId)).updateChildren(studentAppointmentMap);

//                                    /**CARBON CONSULTATION REQUEST REMOVE**/
//                                    DatabaseReference carbonReference = firebaseDatabase.getReference(STUDENTS).child(ccUid).child(APPOINTMENTS);
//                                    carbonReference.keepSynced(true);
//
//                                    carbonReference.child(notificationId).removeValue();

                                    if (i == (appointment.getSenderCc().size() - 1)) {
                                        listener.onRescheduleSuccess("Rescheduling Success", "This is a rescheduled request.\n\n" + "Previous Appointment Schedule Details:\n\n" + "" + appointment.getRequestedDay() + "\n" + appointment.getRequestedDate() + "\n" + appointment.getRequestedTimeStart() + " - " + appointment.getRequestedTimeEnd() + " @ " + appointment.getVenue() + "\n\nPlease be informed that this appointment will be held from " + "" + rescheduledTimeStart + " - " + rescheduledTimeEnd + " @ " + rescheduledVenue + " on " + rescheduledDay + ", " + rescheduledDate);
                                    }
                                }
//                /**STUDENT CONSULTATION REQUEST REMOVE**/
//                studentConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((FacultyLoginActivity) context), aVoid13 -> {
//
//                    DatabaseReference facultyRemoveConsultationRequestReference = firebaseDatabase.getReference(FACULTY).child(appointment.getReceiverId()).child(APPOINTMENTS);
//                    facultyRemoveConsultationRequestReference.keepSynced(true);
//
//                    /**FACULTY CONSULTATION REQUEST REMOVE**/
//                    facultyRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((FacultyLoginActivity) context), aVoid12 -> {
//
//
//
//                    }).addOnFailureListener(((FacultyLoginActivity) context), e -> listener.onRescheduleFailure("Database Error", e.getMessage()));
//
//
//                }).addOnFailureListener(((FacultyLoginActivity) context), e -> listener.onRescheduleFailure("Database Error", e.getMessage()));

                            }).addOnFailureListener(((Activity) context), e -> listener.onRescheduleFailure("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onRescheduleFailure("Saving Error", e.getMessage()));
        } else {
            Log.d("interactor--noSenderCcs", "No Resched CCs");

            studentAppointmentSavingReference.child(Objects.requireNonNull(notificationId))
                    .updateChildren(studentAppointmentMap)
                    .addOnSuccessListener(((Activity) context), aVoid ->
                            facultyAppointmentSavingReference.child(Objects.requireNonNull(notificationId))
                                    .updateChildren(facultyAppointmentMap).addOnSuccessListener(((Activity) context), aVoid1 -> {

                                listener.onRescheduleSuccess("Rescheduling Success",
                                        "This is a rescheduled appointment.\n\n" +
                                                "Previous Appointment Schedule Details:\n\n" + "" +
                                                appointment.getRequestedDay() + "\n" + appointment.getRequestedDate() + "\n" +
                                                appointment.getRequestedTimeStart() + " - " + appointment.getRequestedTimeEnd() +
                                                " @ " + appointment.getVenue() +
                                                "\n\nPlease be informed that this appointment will be held from " + "" +
                                                rescheduledTimeStart + " - " + rescheduledTimeEnd
                                                + " @ " + rescheduledVenue + " on " + rescheduledDay + ", " + rescheduledDate);
//                /**STUDENT CONSULTATION REQUEST REMOVE**/
//                studentConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((FacultyLoginActivity) context), aVoid112 -> {
//
//                    DatabaseReference facultyRemoveConsultationRequestReference = firebaseDatabase.getReference(FACULTY).child(appointment.getReceiverId()).child(APPOINTMENTS);
//                    facultyRemoveConsultationRequestReference.keepSynced(true);
//
//
//
////                    /**FACULTY CONSULTATION REQUEST REMOVE**/
////                    facultyRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((FacultyLoginActivity) context), aVoid11 ->
////
////
////                    ).addOnFailureListener(((FacultyLoginActivity) context), e -> listener.onRescheduleFailure("Database Error", e.getMessage()));
//
//
//                }).addOnFailureListener(((FacultyLoginActivity) context), e
//                        -> listener.onRescheduleFailure("Database Error", e.getMessage()));


                            }).addOnFailureListener(((Activity) context), e -> listener.onRescheduleFailure("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onRescheduleFailure("Saving Error", e.getMessage()));
        }
    }
}

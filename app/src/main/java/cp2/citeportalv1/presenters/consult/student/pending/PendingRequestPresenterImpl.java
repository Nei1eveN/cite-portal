package cp2.citeportalv1.presenters.consult.student.pending;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public class PendingRequestPresenterImpl implements PendingRequestPresenter, PendingRequestInteractor.OnPendingRequestListener {
    private PendingRequestPresenter.View view;
    private PendingRequestInteractor responseInteractor;

    public PendingRequestPresenterImpl(View view, Context context) {
        this.view = view;
        this.responseInteractor = new PendingRequestInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        responseInteractor.getDetails(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void requestPendingList() {
        if (view != null) {
            view.showProgress();
        }
        responseInteractor.getDetails(this);
    }

    @Override
    public void OnRequestSuccess(List<ListItem> list) {
        if (view != null) {
            view.hideProgress();
            view.loadPendingResponses(list);
        }
    }

    @Override
    public void OnRequestFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

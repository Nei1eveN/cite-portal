package cp2.citeportalv1.presenters.consult.student.appointment.appointment_pages;

import android.content.Context;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public class StudentAppointmentPagePresenterImpl implements StudentAppointmentPagePresenter, StudentAppointmentPageInteractor.AppointmentPageListener {

    private StudentAppointmentPagePresenter.View view;
    private StudentAppointmentPageInteractor interactor;

    public StudentAppointmentPagePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentAppointmentPageInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getPages(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void onPageSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null) {
            view.loadDayPages(viewPagerAdapter, numberOfPages);
        }
    }

    @Override
    public void onPageFailure(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null) {
            view.loadDayPages(viewPagerAdapter, numberOfPages);
        }
    }
}

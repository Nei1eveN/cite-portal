package cp2.citeportalv1.presenters.consult.student.loadpages.comsci;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public class ComSciInstructorListPresenterImpl implements ComSciInstructorListPresenter, CompSciInstructorListInteractor.OnDepartmentSyncListener {
    private ComSciInstructorListPresenter.ComSciView view;
    private CompSciInstructorListInteractor listInteractor;

    public ComSciInstructorListPresenterImpl(ComSciView view, Context context) {
        this.view = view;
        this.listInteractor = new CompSciInstructorListInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.setProgress();
        }
        listInteractor.getList(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestFacultyEmployees() {
        if (view != null){
            view.setProgress();
        }
        listInteractor.getList(this);
    }

    @Override
    public void onSuccess(List<ListItem> facultyList) {
        if (view != null) {
            view.hideProgress();
            view.setFacultyEmployees(facultyList);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.consult.faculty.consultationdetails;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.Interval;

import static cp2.citeportalv1.utils.Constants.*;

public class ConsultationDetailsInteractorImpl implements ConsultationDetailsInteractor {
    private Context context;

    /**
     * TO ADD FACULTY DIGITAL SIGNATURE LATER
     **/

    ConsultationDetailsInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getConsultationDetails(String notificationId, OnDetailsListener detailsListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        DatabaseReference notificationReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CONSULTATION_REQUESTS);
        notificationReference.keepSynced(true);
        notificationReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    ConsultationRequest request = Objects.requireNonNull(dataSnapshot.getValue(ConsultationRequest.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    detailsListener.onDetailSuccess(request);
                } else {
                    detailsListener.onDetailFailure("Consultation Details does not exist");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                detailsListener.onDetailFailure(databaseError.getMessage());
            }
        });
    }

    @Override
    public void saveApprovedRequestToDatabase(String notificationId,
                                              String requestedDay, String requestedDate,
                                              String requestedTimeStart, String requestedTimeEnd, String venue,
                                              String consultationTitle, String consultationBody,
                                              String consultationRemarks, OnResponseListener responseListener) {

        if (!isNetworkAvailable(context)) {
            responseListener.onResponseFailure("Network Error", "Please check your internet connection.");
        } else {
            try {
                if (yearDateFormat.format(new Date()).equals(requestedDate) && timeFormat12hr.parse(timeFormat12hr.format(new Date())).after(timeFormat12hr.parse(requestedTimeEnd))) {
                    /**SAVING ERROR: CURRENT TIME GREATER THAN CONSULTATION HOURS**/
                    responseListener.onResponseFailure("Appointment Saving Failed", "Requesting beyond the scheduled time of consultation is no longer allowed for this day. Please try again tomorrow.");
                } else {
                    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

                    /**FACULTY CONSULTATION REQUEST DETAILS**/
                    DatabaseReference facultyConsultationRequestReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CONSULTATION_REQUESTS);
                    facultyConsultationRequestReference.keepSynced(true);
                    facultyConsultationRequestReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                ConsultationRequest request = Objects.requireNonNull(dataSnapshot.getValue(ConsultationRequest.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                                /**FACULTY DETAILS**/
                                DatabaseReference facultyDetailReference = firebaseDatabase.getReference().child(FACULTY).child(Objects.requireNonNull(request).getReceiverId());
                                facultyDetailReference.keepSynced(true);

                                facultyDetailReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                                            /**APPOINTMENT SCHEDULE DETAIL CHECKER**/
                                            DatabaseReference checkAppointmentReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(faculty).facultyId).child(APPOINTMENTS);
                                            checkAppointmentReference.keepSynced(true);

                                            checkAppointmentReference.orderByChild("requestedSchedule").equalTo(requestedDate + " - " + requestedDay + " - " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue)
                                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                            if (dataSnapshot.exists()) {
                                                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                                    Appointments appointments = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                                                                    responseListener.onResponseFailure("Existing Appointment", "You already have an approved appointment for this time schedule with the following details:\n\nStudent Name:\n" + appointments.getSenderLastName() + ", " + appointments.getSenderFirstName() + " " + appointments.getSenderMiddleName() + "\n\nDetails:\n" + appointments.getMessageTitle() + "\n" + appointments.getRequestedDay() + "\n" + appointments.getRequestedDate() + "\n" + appointments.getRequestedTimeStart() + " - " + appointments.getRequestedTimeEnd() + " @ " + appointments.getVenue());
                                                                }
                                                            } else {
                                                                /**STUDENT APPOINTMENT DETAILS**/
                                                                Map<String, Object> studentAppointmentMap = new HashMap<>();

                                                                studentAppointmentMap.put(SENDER_ID, request.getReceiverId());
                                                                studentAppointmentMap.put(RECEIVER_ID, request.getSenderId());

                                                                studentAppointmentMap.put(SENDER_FIRST_NAME, faculty.getfName());
                                                                studentAppointmentMap.put(SENDER_MIDDLE_NAME, faculty.getMidName());
                                                                studentAppointmentMap.put(SENDER_LAST_NAME, faculty.getLastName());
                                                                studentAppointmentMap.put(SENDER_IMAGE_URL, faculty.getFacultyImageURL());
                                                                studentAppointmentMap.put(SENDER_DEPARTMENT, faculty.getDepartment());

                                                                studentAppointmentMap.put(MESSAGE_TITLE, request.getMessageTitle());
                                                                studentAppointmentMap.put(MESSAGE_BODY, request.getMessageBody());
                                                                studentAppointmentMap.put(MESSAGE_REMARKS, consultationRemarks);
                                                                studentAppointmentMap.put(MESSAGE_SIDE_NOTE, "This is an approved request.\n\nPlease be informed that this appointment will be held from " + request.getRequestedTimeStart() + " - " + request.getRequestedTimeEnd() + " @ " + request.getVenue() + " on " + request.getRequestedDay() + ", " + request.getRequestedDate() + "\n\nThis is a system-generated message.");
                                                                studentAppointmentMap.put(MESSAGE_STATUS, "APPROVED");

                                                                studentAppointmentMap.put(REQUESTED_DAY, request.getRequestedDay());
                                                                studentAppointmentMap.put(REQUESTED_DATE, request.getRequestedDate());
                                                                studentAppointmentMap.put(REQUESTED_SCHEDULE, request.getRequestedSchedule());
                                                                studentAppointmentMap.put(REQUESTED_TIME_START, request.getRequestedTimeStart());
                                                                studentAppointmentMap.put(REQUESTED_TIME_END, request.getRequestedTimeEnd());
                                                                studentAppointmentMap.put(VENUE, request.getVenue());

                                                                studentAppointmentMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

                                                                studentAppointmentMap.put(APPOINTMENT_CODE, request.getRequestCode());
                                                                studentAppointmentMap.put(SENDER_CC, request.getSenderCc());

                                                                studentAppointmentMap.put(SENDER_SIGNATURE, request.getSenderSignature());

                                                                /**STUDENT CONSULTATION REQUEST REMOVE**/
                                                                DatabaseReference studentRequestReference = firebaseDatabase.getReference(STUDENTS).child(request.getSenderId()).child(CONSULTATION_REQUESTS);
                                                                studentRequestReference.keepSynced(true);

                                                                DatabaseReference facultyAppointmentsFromDateReference = firebaseDatabase.getReference(FACULTY).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(APPOINTMENTS);
                                                                facultyAppointmentsFromDateReference.keepSynced(true);

                                                                /**CHECK FACULTY APPOINTMENT IF REQUESTED DATE EXISTS**/
                                                                Query dateQuery = facultyAppointmentsFromDateReference.orderByChild("requestedDate").equalTo(requestedDate);
                                                                dateQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        if (dataSnapshot.exists()) {
                                                                            /**REQUESTED DATE EXISTS: CHECK FOR TIME CONFLICT**/
                                                                            List<Appointments> appointments = new ArrayList<>();

                                                                            List<Date> timeStarts = new ArrayList<>();
                                                                            List<Date> timeEnds = new ArrayList<>();

                                                                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                                                Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                                                                                appointments.add(appointment);
                                                                            }

                                                                            List<cp2.citeportalv1.models.Interval> intervals = new ArrayList<>(appointments.size());

                                                                            try {
                                                                                for (int i = 0, appointmentsSize = appointments.size(); i < appointmentsSize; i++) {
                                                                                    Appointments appointment = appointments.get(i);

                                                                                    timeStarts.add(timeFormat12hr.parse(appointment.getRequestedTimeStart()));
                                                                                    timeEnds.add(timeFormat12hr.parse(appointment.getRequestedTimeEnd()));

                                                                                    intervals.add(new cp2.citeportalv1.models.Interval(Integer.valueOf(String.valueOf(timeStarts.get(i).getTime())), Integer.valueOf(String.valueOf(timeEnds.get(i).getTime()))));

                                                                                    if (i == (appointmentsSize - 1)) {
                                                                                        Interval inputInterval = new Interval(Integer.valueOf(String.valueOf(timeFormat12hr.parse(requestedTimeStart).getTime())), Integer.valueOf(String.valueOf(timeFormat12hr.parse(requestedTimeEnd).getTime())));
                                                                                        intervals.add(inputInterval);

                                                                                        List<Interval> result = findIntervalsThatOverlap(intervals);

                                                                                        Log.d("interactor--intvls", String.valueOf(intervals));

                                                                                        if (!result.isEmpty()) {
                                                                                            for (int j = 0, resultSize = result.size(); j < resultSize; j++) {
                                                                                                Interval interval = result.get(j);
                                                                                                if (j == (resultSize - 1)) {
                                                                                                    if (interval.start < interval.end) {
                                                                                                        /**SAVING ERROR: TIME CONFLICT**/
                                                                                                        responseListener.onResponseFailure("Appointment Time Conflict", "There is a conflicting time of appointment.\n\nDetails:\n" + appointment.getMessageTitle() + "\n" + appointment.getRequestedTimeStart() + " - " + appointment.getRequestedTimeEnd() + " @ " + appointment.getVenue());
                                                                                                    } else {
                                                                                                        /**SAVING SUCCESS: SAVE FINAL APPOINTMENT DETAILS**/
                                                                                                        saveFinalAppointmentDetails(notificationId, request, faculty, studentAppointmentMap, firebaseDatabase, studentRequestReference, responseListener, consultationTitle, consultationBody, consultationRemarks, requestedTimeStart, requestedTimeEnd, venue, requestedDay, requestedDate);
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        } else {
                                                                                            /**SAVING SUCCESS: SAVE FINAL APPOINTMENT DETAILS
                                                                                             *
                                                                                             * RESULT INTERVALS IS EMPTY: NO CONFLICT**/
                                                                                            saveFinalAppointmentDetails(notificationId, request, faculty, studentAppointmentMap, firebaseDatabase, studentRequestReference, responseListener, consultationTitle, consultationBody, consultationRemarks, requestedTimeStart, requestedTimeEnd, venue, requestedDay, requestedDate);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            } catch (ParseException e) {
                                                                               responseListener.onResponseFailure("Parsing Error", e.getMessage());
                                                                            }

                                                                        } else {
                                                                            /**REQUESTED DATE NOT EXISTING: SAVE FINAL APPOINTMENT DETAILS**/
                                                                            saveFinalAppointmentDetails(notificationId, request, faculty, studentAppointmentMap, firebaseDatabase, studentRequestReference, responseListener, consultationTitle, consultationBody, consultationRemarks, requestedTimeStart, requestedTimeEnd, venue, requestedDay, requestedDate);
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                        responseListener.onResponseFailure("Database Error", databaseError.getMessage());
                                                                    }
                                                                });
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            responseListener.onResponseFailure("Database Error", databaseError.getMessage());
                                                        }
                                                    });
                                        } else {
                                            responseListener.onResponseFailure("Not Existing Detail", "Cannot read notification details. Please check if it is still on your list of requests.");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        responseListener.onResponseFailure("Database Error", databaseError.getMessage());
                                    }
                                });
                            } else {
                                responseListener.onResponseFailure("Not Existing Detail", "Cannot read notification details. Please check if it is still on your list of requests.");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            responseListener.onResponseFailure("Database Error", databaseError.getMessage());
                        }
                    });
                }
            } catch (ParseException e) {
                responseListener.onResponseFailure("Parsing Error", e.getMessage());
            }
        }
    }

    private void saveFinalAppointmentDetails(String notificationId,
                                             ConsultationRequest request, Faculty faculty, Map<String, Object> studentAppointmentMap,
                                             FirebaseDatabase firebaseDatabase, DatabaseReference studentRequestReference,
                                             OnResponseListener responseListener,
                                             String consultationTitle, String consultationBody, String consultationRemarks,
                                             String requestedTimeStart, String requestedTimeEnd, String venue,
                                             String requestedDay, String requestedDate) {

        /**FACULTY APPOINTMENT DETAILS**/
        Map<String, Object> facultyAppointmentMap = new HashMap<>();
        facultyAppointmentMap.put(SENDER_ID, request.getSenderId());
        facultyAppointmentMap.put(RECEIVER_ID, request.getReceiverId());

        facultyAppointmentMap.put(SENDER_FIRST_NAME, request.getSenderFirstName());
        facultyAppointmentMap.put(SENDER_MIDDLE_NAME, request.getSenderMiddleName());
        facultyAppointmentMap.put(SENDER_LAST_NAME, request.getSenderLastName());
        facultyAppointmentMap.put(SENDER_IMAGE_URL, request.getSenderImageURL());
        facultyAppointmentMap.put(SENDER_PROGRAM, request.getSenderProgram());
        facultyAppointmentMap.put(SENDER_YEAR_LEVEL, request.getSenderYearLevel());

        facultyAppointmentMap.put(MESSAGE_TITLE, request.getMessageTitle());
        facultyAppointmentMap.put(MESSAGE_BODY, request.getMessageBody());
        facultyAppointmentMap.put(MESSAGE_REMARKS, consultationRemarks);
        facultyAppointmentMap.put(MESSAGE_SIDE_NOTE, "This is an approved request.\n\nPlease be informed that this appointment will be held from " + request.getRequestedTimeStart() + " - " + request.getRequestedTimeEnd() + " @ " + request.getVenue() + " on " + request.getRequestedDay() + ", " + request.getRequestedDate() + "\n\nThis is a system-generated message.");
        facultyAppointmentMap.put(MESSAGE_STATUS, "APPROVED");

        facultyAppointmentMap.put(REQUESTED_DAY, request.getRequestedDay());
        facultyAppointmentMap.put(REQUESTED_DATE, request.getRequestedDate());
        facultyAppointmentMap.put(REQUESTED_SCHEDULE, request.getRequestedSchedule());
        facultyAppointmentMap.put(REQUESTED_TIME_START, request.getRequestedTimeStart());
        facultyAppointmentMap.put(REQUESTED_TIME_END, request.getRequestedTimeEnd());
        facultyAppointmentMap.put(VENUE, request.getVenue());

        facultyAppointmentMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

        facultyAppointmentMap.put(APPOINTMENT_CODE, request.getRequestCode());
        facultyAppointmentMap.put(SENDER_CC, request.getSenderCc());

        facultyAppointmentMap.put(SENDER_SIGNATURE, request.getSenderSignature());
        facultyAppointmentMap.put(RECEIVER_SIGNATURE, faculty.getUserSignature());

        /**STUDENT APPOINTMENT SAVING**/
        DatabaseReference studentAppointmentSavingReference = FirebaseDatabase.getInstance().getReference(STUDENTS).child(request.getSenderId()).child(APPOINTMENTS);
        studentAppointmentSavingReference.keepSynced(true);

        /**APPOINTMENT KEY**/
        String appointmentKey = studentAppointmentSavingReference.push().getKey();

        /**FACULTY APPOINTMENT SAVING**/
        DatabaseReference facultyAppointmentSavingReference = FirebaseDatabase.getInstance().getReference(FACULTY).child(request.getReceiverId()).child(APPOINTMENTS);
        facultyAppointmentSavingReference.keepSynced(true);

        if (!request.getSenderCc().isEmpty()) {
            /**CC NOT EMPTY**/
            Log.d("interactor--senderCcs", String.valueOf(request.getSenderCc()));

            studentAppointmentSavingReference.child(Objects.requireNonNull(appointmentKey)).updateChildren(studentAppointmentMap).addOnSuccessListener(((Activity) context), aVoid -> facultyAppointmentSavingReference.child(Objects.requireNonNull(appointmentKey)).updateChildren(facultyAppointmentMap).addOnSuccessListener(((Activity) context), aVoid14 -> {

                /**STUDENT CONSULTATION REQUEST REMOVE**/
                studentRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid13 -> {

                    DatabaseReference facultyRemoveConsultationRequestReference = firebaseDatabase.getReference(FACULTY).child(request.getReceiverId()).child(CONSULTATION_REQUESTS);
                    facultyRemoveConsultationRequestReference.keepSynced(true);

                    /**FACULTY CONSULTATION REQUEST REMOVE**/
                    facultyRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid12 -> {

                        List<String> senderCc = request.getSenderCc();
                        for (int i = 0, senderCcSize = senderCc.size(); i < senderCcSize; i++) {
                            String ccUid = senderCc.get(i);
                            DatabaseReference carbonAppointmentReference = firebaseDatabase.getReference(STUDENTS).child(ccUid).child(APPOINTMENTS);
                            carbonAppointmentReference.keepSynced(true);

                            /**CARBON APPOINTMENT SAVING**/
                            carbonAppointmentReference.child(Objects.requireNonNull(appointmentKey)).updateChildren(studentAppointmentMap);

                            /**CARBON CONSULTATION REQUEST REMOVE**/
                            DatabaseReference carbonReference = firebaseDatabase.getReference(STUDENTS).child(ccUid).child(CONSULTATION_REQUESTS);
                            carbonReference.keepSynced(true);

                            carbonReference.child(notificationId).removeValue();

                            if (i == (request.getSenderCc().size() - 1)) {
                                responseListener.onResponseSuccess("Course" + "\n" + consultationTitle + "" + "\n\nDetails:\n" + "" + consultationBody + "\n\n" + "Remarks:\n" + "" + consultationRemarks + "\n\n" + "You have approved this Consultation Request.\n\n" + "Please be informed that this appointment will be held from " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue + " on " + requestedDay + ", " + requestedDate);
                            }
                        }

                    }).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Database Error", e.getMessage()));


                }).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Database Error", e.getMessage()));


            }).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()));
        } else {
            /**CC EMPTY**/
            Log.d("interactor--noSenderCcs", "No CCs");

            studentAppointmentSavingReference.child(Objects.requireNonNull(appointmentKey)).updateChildren(studentAppointmentMap).addOnSuccessListener(((Activity) context), aVoid -> facultyAppointmentSavingReference.child(Objects.requireNonNull(appointmentKey)).updateChildren(facultyAppointmentMap).addOnSuccessListener(((Activity) context), aVoid1 -> {

                /**STUDENT CONSULTATION REQUEST REMOVE**/
                studentRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid112 -> {

                    DatabaseReference facultyRemoveConsultationRequestReference = firebaseDatabase.getReference(FACULTY).child(request.getReceiverId()).child(CONSULTATION_REQUESTS);
                    facultyRemoveConsultationRequestReference.keepSynced(true);

                    /**FACULTY CONSULTATION REQUEST REMOVE**/
                    facultyRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid11 -> responseListener.onResponseSuccess("Course" + "\n" + consultationTitle + "" + "\n\nDetails:\n" + "" + consultationBody + "\n\n" + "Remarks:\n" + "" + consultationRemarks + "\n\n" + "You have approved this Consultation Request.\n\n" + "Please be informed that this appointment will be held from " + requestedTimeStart + " - " + requestedTimeEnd + " @ " + venue + " on " + requestedDay + ", " + requestedDate)).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Database Error", e.getMessage()));


                }).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Database Error", e.getMessage()));

            }).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()));
        }
    }

    @Override
    public void saveIgnoredRequestToDatabase(String notificationId,
                                             String requestedDay, String requestedDate,
                                             String requestedTimeStart, String requestedTimeEnd, String venue,
                                             String consultationTitle, String consultationBody,
                                             String reasonToRejectConsultationRequest,
                                             OnResponseListener responseListener) {

        if (!isNetworkAvailable(context)) {
            responseListener.onResponseFailure("Network Error", "Please check your internet connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

            /**FACULTY CONSULTATION REQUEST DETAILS**/
            DatabaseReference facultyConsultationRequestReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CONSULTATION_REQUESTS);
            facultyConsultationRequestReference.keepSynced(true);
            facultyConsultationRequestReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        ConsultationRequest request = Objects.requireNonNull(dataSnapshot.getValue(ConsultationRequest.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                        /**FACULTY DETAILS**/
                        DatabaseReference facultyDetailReference = firebaseDatabase.getReference().child(FACULTY).child(request.getReceiverId());
                        facultyDetailReference.keepSynced(true);

                        facultyDetailReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                                    /**STUDENT REJECTED REQUEST DETAILS**/
                                    Map<String, Object> studentRejectedRequestMap = new HashMap<>();

                                    studentRejectedRequestMap.put(SENDER_ID, request.getReceiverId());
                                    studentRejectedRequestMap.put(RECEIVER_ID, request.getSenderId());

                                    studentRejectedRequestMap.put(SENDER_FIRST_NAME, faculty.getfName());
                                    studentRejectedRequestMap.put(SENDER_MIDDLE_NAME, faculty.getMidName());
                                    studentRejectedRequestMap.put(SENDER_LAST_NAME, faculty.getLastName());
                                    studentRejectedRequestMap.put(SENDER_IMAGE_URL, faculty.getFacultyImageURL());
                                    studentRejectedRequestMap.put(SENDER_DEPARTMENT, faculty.getDepartment());

                                    studentRejectedRequestMap.put(MESSAGE_TITLE, request.getMessageTitle());
                                    studentRejectedRequestMap.put(MESSAGE_BODY, request.getMessageBody());
                                    studentRejectedRequestMap.put(MESSAGE_REMARKS, reasonToRejectConsultationRequest);
                                    studentRejectedRequestMap.put(MESSAGE_SIDE_NOTE, "Course" + "\n" + request.getMessageTitle() + "" + "\n\nDetails:\n" + "" + request.getMessageBody() + "\n\n" + "Remarks:\n" + "REJECTED" + "\n\n" + "Reason for Rejecting Request:\n" + reasonToRejectConsultationRequest + "\n\nYou have rejected this Consultation Request." + "");
                                    studentRejectedRequestMap.put(MESSAGE_STATUS, "REJECTED");

                                    studentRejectedRequestMap.put(REQUESTED_DAY, request.getRequestedDay());
                                    studentRejectedRequestMap.put(REQUESTED_DATE, request.getRequestedDate());
                                    studentRejectedRequestMap.put(REQUESTED_SCHEDULE, request.getRequestedSchedule());
                                    studentRejectedRequestMap.put(REQUESTED_TIME_START, request.getRequestedTimeStart());
                                    studentRejectedRequestMap.put(REQUESTED_TIME_END, request.getRequestedTimeEnd());
                                    studentRejectedRequestMap.put(VENUE, request.getVenue());

                                    studentRejectedRequestMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

                                    studentRejectedRequestMap.put(RESPONSE_CODE, request.getRequestCode());
                                    studentRejectedRequestMap.put(SENDER_CC, request.getSenderCc());
                                    /**END OF STUDENT REJECTED REQUEST DETAILS**/

                                    /**FACULTY REJECT REQUEST DETAILS**/
                                    Map<String, Object> facultyRejectedRequestMap = new HashMap<>();
                                    facultyRejectedRequestMap.put(SENDER_ID, request.getSenderId());
                                    facultyRejectedRequestMap.put(RECEIVER_ID, request.getReceiverId());

                                    facultyRejectedRequestMap.put(SENDER_FIRST_NAME, request.getSenderFirstName());
                                    facultyRejectedRequestMap.put(SENDER_MIDDLE_NAME, request.getSenderMiddleName());
                                    facultyRejectedRequestMap.put(SENDER_LAST_NAME, request.getSenderLastName());
                                    facultyRejectedRequestMap.put(SENDER_IMAGE_URL, request.getSenderImageURL());
                                    facultyRejectedRequestMap.put(SENDER_PROGRAM, request.getSenderProgram());
                                    facultyRejectedRequestMap.put(SENDER_YEAR_LEVEL, request.getSenderYearLevel());

                                    facultyRejectedRequestMap.put(MESSAGE_TITLE, request.getMessageTitle());
                                    facultyRejectedRequestMap.put(MESSAGE_BODY, request.getMessageBody());
                                    facultyRejectedRequestMap.put(MESSAGE_REMARKS, reasonToRejectConsultationRequest);
                                    facultyRejectedRequestMap.put(MESSAGE_SIDE_NOTE, "Course" + "\n" + request.getMessageTitle() + "" + "\n\nDetails:\n" + "" + request.getMessageBody() + "\n\n" + "Remarks:\n" + "REJECTED" + "\n\n" + "Reason for Rejecting Request:\n" + reasonToRejectConsultationRequest + "\n\nThis is a Declined Consultation Request." + "");
                                    facultyRejectedRequestMap.put(MESSAGE_STATUS, "REJECTED");

                                    facultyRejectedRequestMap.put(REQUESTED_DAY, request.getRequestedDay());
                                    facultyRejectedRequestMap.put(REQUESTED_DATE, request.getRequestedDate());
                                    facultyRejectedRequestMap.put(REQUESTED_SCHEDULE, request.getRequestedSchedule());
                                    facultyRejectedRequestMap.put(REQUESTED_TIME_START, request.getRequestedTimeStart());
                                    facultyRejectedRequestMap.put(REQUESTED_TIME_END, request.getRequestedTimeEnd());
                                    facultyRejectedRequestMap.put(VENUE, request.getVenue());

                                    facultyRejectedRequestMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

                                    facultyRejectedRequestMap.put(RESPONSE_CODE, request.getRequestCode());
                                    facultyRejectedRequestMap.put(SENDER_CC, request.getSenderCc());
                                    /**END OF FACULTY REJECTED REQUEST DETAILS**/

                                    /**STUDENT CONSULTATION RESPONSE SAVING**/
                                    DatabaseReference studentConsultationResponseReference = firebaseDatabase.getReference(STUDENTS).child(request.getSenderId()).child(CONSULTATION_RESPONSES);
                                    studentConsultationResponseReference.keepSynced(true);

                                    /**FACULTY CONSULTATION RESPONSE SAVING**/
                                    DatabaseReference facultyConsultationResponseReference = firebaseDatabase.getReference(FACULTY).child(request.getReceiverId()).child(CONSULTATION_RESPONSES);
                                    facultyConsultationResponseReference.keepSynced(true);

                                    /**STUDENT CONSULTATION REQUEST REMOVE**/
                                    DatabaseReference studentRemoveConsultationRequestReference = firebaseDatabase.getReference(STUDENTS).child(request.getSenderId()).child(CONSULTATION_REQUESTS);
                                    studentRemoveConsultationRequestReference.keepSynced(true);

                                    /**FACULTY CONSULTATION REQUEST REMOVE**/
                                    DatabaseReference facultyRemoveConsultationRequestReference = FirebaseDatabase.getInstance().getReference(FACULTY).child(request.getReceiverId()).child(CONSULTATION_REQUESTS);
                                    facultyRemoveConsultationRequestReference.keepSynced(true);

                                    /**RESPONSE KEY**/
                                    String responseKey = studentRemoveConsultationRequestReference.push().getKey();

                                    if (!request.getSenderCc().isEmpty()) {
                                        /**CC NOT EMPTY**/
                                        Log.d("interactor--rejectedCCs", String.valueOf(request.getSenderCc()));
                                        studentConsultationResponseReference.child(Objects.requireNonNull(responseKey)).updateChildren(studentRejectedRequestMap).addOnSuccessListener(((Activity) context), aVoid -> facultyConsultationResponseReference.child(responseKey).updateChildren(facultyRejectedRequestMap).addOnSuccessListener(((Activity) context), aVoid16 -> studentRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid15 -> facultyRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid14 -> {

                                            List<String> senderCc = request.getSenderCc();
                                            for (int i = 0, senderCcSize = senderCc.size(); i < senderCcSize; i++) {
                                                String ccUid = senderCc.get(i);
                                                DatabaseReference carbonAppointmentReference = firebaseDatabase.getReference(STUDENTS).child(ccUid).child(CONSULTATION_RESPONSES);
                                                carbonAppointmentReference.keepSynced(true);

                                                /**CARBON REJECT REQUEST SAVING**/
                                                carbonAppointmentReference.child(Objects.requireNonNull(responseKey)).updateChildren(studentRejectedRequestMap);

                                                /**CARBON CONSULTATION REQUEST REMOVE**/
                                                DatabaseReference carbonReference = firebaseDatabase.getReference(STUDENTS).child(ccUid).child(CONSULTATION_REQUESTS);
                                                carbonReference.keepSynced(true);

                                                carbonReference.child(notificationId).removeValue();

                                                if (i == (request.getSenderCc().size() - 1)) {

                                                    responseListener.onResponseSuccess("Course" + "\n" + request.getMessageTitle() + "" + "\n\nDetails:\n" + "" + request.getMessageBody() + "\n\n" + "Remarks:\n" + "REJECTED" + "\n\n" + "Reason for Rejecting Request:\n" + reasonToRejectConsultationRequest + "\n\nYou have rejected this Consultation Request." + "");

                                                }
                                            }
                                        }).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()));
                                    } else {
                                        /**CC EMPTY**/
                                        Log.d("interactor--noCC", "No Rejected CCs");
                                        studentConsultationResponseReference.child(Objects.requireNonNull(responseKey)).updateChildren(studentRejectedRequestMap).addOnSuccessListener(((Activity) context), aVoid -> facultyConsultationResponseReference.child(responseKey).updateChildren(facultyRejectedRequestMap).addOnSuccessListener(((Activity) context), aVoid13 -> studentRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid12 -> facultyRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid1 -> responseListener.onResponseSuccess("Course" + "\n" + consultationTitle + "" + "\n\nDetails:\n" + "" + consultationBody + "\n\n" + "Remarks:\n" + "REJECTED" + "\n\n" + "Reason for Rejecting Request:\n" + reasonToRejectConsultationRequest + "\n\nYou have rejected this Consultation Request." + "")).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> responseListener.onResponseFailure("Saving Error", e.getMessage()));
                                    }
                                } else {
                                    responseListener.onResponseFailure("Not Existing Detail", "Cannot read notification details. Please check if it is still on your list of requests.");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                responseListener.onResponseFailure("Database Error", databaseError.getMessage());
                            }
                        });
                    } else {
                        responseListener.onResponseFailure("Not Existing Detail", "Cannot read notification details. Please check if it is still on your list of requests.");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    responseListener.onResponseFailure("Database Error", databaseError.getMessage());
                }
            });
        }
    }
}
package cp2.citeportalv1.presenters.consult.student.appointment.appointment_pages;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface StudentAppointmentPagePresenter {
    interface View {
        void loadDayPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }
    void onStart();
    void onDestroy();
}

package cp2.citeportalv1.presenters.consult.student.consultation_log;

import cp2.citeportalv1.adapters.grouplist.StudentConsultationLogGroupAdapter;

public interface StudentConsultationLogPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setSnackMessage(String snackMessage);
        void loadConsultationLogs(StudentConsultationLogGroupAdapter adapter);
        void setEmptyState(String emptyMessage);
    }
    void onStart();
    void onDestroy();
    void requestConsultationLogs();
}

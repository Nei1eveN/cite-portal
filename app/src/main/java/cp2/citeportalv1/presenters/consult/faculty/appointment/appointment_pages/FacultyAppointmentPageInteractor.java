package cp2.citeportalv1.presenters.consult.faculty.appointment.appointment_pages;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface FacultyAppointmentPageInteractor {
    interface AppointmentPageListener {
        void onPageSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
        void onPageFailure(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }

    void getPages(AppointmentPageListener pageListener);
}

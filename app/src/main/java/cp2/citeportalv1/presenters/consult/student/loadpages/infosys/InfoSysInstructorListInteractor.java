package cp2.citeportalv1.presenters.consult.student.loadpages.infosys;

import java.util.List;

import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.ListItem;

public interface InfoSysInstructorListInteractor {
    void getList(OnDepartmentSyncListener listener);
    interface OnDepartmentSyncListener {
        void onSuccess(List<ListItem> facultyList);
        void onFailure(String errorMessage);
    }
}

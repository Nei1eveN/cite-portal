package cp2.citeportalv1.presenters.consult.student.appointment.day_appointment;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.grouplist.StudentAppointmentDayGroupAdapter;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.utils.Constants;

import static cp2.citeportalv1.utils.Constants.APPOINTMENTS;
import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

class StudentDayAppointmentInteractorImpl implements StudentDayAppointmentInteractor {
    private Context context;

    StudentDayAppointmentInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getAppointments(LoadAppointmentsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(APPOINTMENTS);
        databaseReference.keepSynced(true);

        databaseReference.orderByChild("requestedDate").equalTo(yearDateFormat.format(new Date())).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<Appointments> appointments = new ArrayList<>();
                    appointments.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        String appointmentId = snapshot.getKey();
                        assert appointmentId != null;
                        Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(appointmentId);
                        appointments.add(appointment);
                    }
                    LinkedHashMap<String, Set<Appointments>> groupedHashMap = Constants.groupAppointmentDataIntoHashMap(appointments);
                    List<ListItem> consolidatedList = new ArrayList<>();

                    for (String date : groupedHashMap.keySet()) {
                        Log.d("dates", date);
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(dateFormat.format(new Date(Long.parseLong(date))));
                        dateItem.setDay(dayFormat.format(new Date(Long.parseLong(date))));
                        consolidatedList.add(dateItem);
                        for (Appointments appointment : Objects.requireNonNull(groupedHashMap.get(date))) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setAppointments(appointment);
                            consolidatedList.add(generalItem);
                        }
                    }

                    StudentAppointmentDayGroupAdapter adapter = new StudentAppointmentDayGroupAdapter(context, consolidatedList);

                    listener.onSuccessLoadingAppointment("Appointments Loaded", adapter);
                } else {
                    listener.onFailureLoadingAppointment("There are no Scheduled Appointments for "+dateFormat.format(new Date())+".\n\nClick the calendar image to see your Appointments.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFailureLoadingAppointment("Database Error:\n\nCode:\n"+databaseError.getCode()+"\n\nMessage:\n"+databaseError.getMessage()+"\n\nDetails:\n"+databaseError.getDetails());
            }
        });


    }

    @Override
    public void findAppointmentToDatabase(Date dateClicked, FindAppointmentListener appointmentListener) {

    }
}

package cp2.citeportalv1.presenters.consult.faculty.schedule.consultation_hours;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.views.faculty.ConsultationScheduleFragment;

import static cp2.citeportalv1.utils.Constants.CONSULTATION_SCHEDULES;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.SCHEDULE_DAY;
import static cp2.citeportalv1.utils.Constants.dateComparator;

class ConsultationHoursInteractorImpl implements ConsultationHoursInteractor {
    private Context context;

    private ConsultationScheduleFragment monday = new ConsultationScheduleFragment();
    private ConsultationScheduleFragment tuesday = new ConsultationScheduleFragment();
    private ConsultationScheduleFragment wednesday = new ConsultationScheduleFragment();
    private ConsultationScheduleFragment thursday = new ConsultationScheduleFragment();
    private ConsultationScheduleFragment friday = new ConsultationScheduleFragment();
    private ConsultationScheduleFragment saturday = new ConsultationScheduleFragment();
    private ConsultationScheduleFragment sunday = new ConsultationScheduleFragment();

    private ConsultationScheduleFragment scheduleFragment = new ConsultationScheduleFragment();

    ConsultationHoursInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getPages(ConsultationDaysListener daysListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference dayReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CONSULTATION_SCHEDULES);
        dayReference.keepSynced(true);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(((FragmentActivity) context).getSupportFragmentManager());
        List<String> dayKeys = new ArrayList<>();

        dayReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Bundle mondayBundle = new Bundle();
                    Bundle tuesdayBundle = new Bundle();
                    Bundle wednesdayBundle = new Bundle();
                    Bundle thursdayBundle = new Bundle();
                    Bundle fridayBundle = new Bundle();
                    Bundle saturdayBundle = new Bundle();
                    Bundle sundayBundle = new Bundle();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        dayKeys.add(snapshot.getKey());
                    }
                    List<String> days = new ArrayList<>();
                    for (int i = 0, dayKeysSize = dayKeys.size(); i < dayKeysSize; i++) {
                        String day = dayKeys.get(i);
                        days.add(day);
                        Collections.sort(days, dateComparator);
                    }
                    for (String sortedDay : days){
                        if ("Monday".equals(sortedDay)) {
                            mondayBundle.putString(SCHEDULE_DAY, "Monday");
                            monday.setArguments(mondayBundle);
                            viewPagerAdapter.addFragment(monday, "Monday");
                            viewPagerAdapter.notifyDataSetChanged();
                        }
                        if ("Tuesday".equals(sortedDay)) {
                            tuesdayBundle.putString(SCHEDULE_DAY, "Tuesday");
                            tuesday.setArguments(tuesdayBundle);
                            viewPagerAdapter.addFragment(tuesday, "Tuesday");
                            viewPagerAdapter.notifyDataSetChanged();
                        }
                        if ("Wednesday".equals(sortedDay)) {
                            wednesdayBundle.putString(SCHEDULE_DAY, "Wednesday");
                            wednesday.setArguments(wednesdayBundle);
                            viewPagerAdapter.addFragment(wednesday, "Wednesday");
                            viewPagerAdapter.notifyDataSetChanged();
                        }
                        if ("Thursday".equals(sortedDay)) {
                            thursdayBundle.putString(SCHEDULE_DAY, "Thursday");
                            thursday.setArguments(thursdayBundle);
                            viewPagerAdapter.addFragment(thursday, "Thursday");
                            viewPagerAdapter.notifyDataSetChanged();

                        }
                        if ("Friday".equals(sortedDay)) {
                            fridayBundle.putString(SCHEDULE_DAY, "Friday");
                            friday.setArguments(fridayBundle);
                            viewPagerAdapter.addFragment(friday, "Friday");
                            viewPagerAdapter.notifyDataSetChanged();
                        }
                        if ("Saturday".equals(sortedDay)) {
                            saturdayBundle.putString(SCHEDULE_DAY, "Saturday");
                            saturday.setArguments(saturdayBundle);
                            viewPagerAdapter.addFragment(saturday, "Saturday");
                            viewPagerAdapter.notifyDataSetChanged();
                        }
                        if ("Sunday".equals(sortedDay)) {
                            sundayBundle.putString(SCHEDULE_DAY, "Sunday");
                            sunday.setArguments(sundayBundle);
                            viewPagerAdapter.addFragment(sunday, "Sunday");
                            viewPagerAdapter.notifyDataSetChanged();
                        }
                    }
                    daysListener.onPageSuccess(viewPagerAdapter, days.size());


                } else {
                    Bundle emptyBundle = new Bundle();
                    emptyBundle.putString(SCHEDULE_DAY, "Monday");
                    scheduleFragment.setArguments(emptyBundle);
                    viewPagerAdapter.addFragment(scheduleFragment, "Schedules");
                    viewPagerAdapter.notifyDataSetChanged();
                    daysListener.onPageFailure(viewPagerAdapter, 1);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                viewPagerAdapter.addFragment(new ConsultationScheduleFragment(), "Schedules");
                viewPagerAdapter.notifyDataSetChanged();
                daysListener.onPageFailure(viewPagerAdapter, 1);
            }
        });
    }
}

package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail.rescheduling_appointment;

import android.content.Context;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.utils.Constants;
import cp2.citeportalv1.utils.EventDecorator;

import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

public class AppointmentReschedulePresenterImpl implements AppointmentReschedulePresenter,
        AppointmentRescheduleInteractor.OnRequestDetailsListener,
        AppointmentRescheduleInteractor.OnAppointmentFromDateListener,
        AppointmentRescheduleInteractor.OnAppointmentsListener,
        AppointmentRescheduleInteractor.OnAppointmentTimeSlotsListener,
        AppointmentRescheduleInteractor.OnScheduleFromDateListener,
        AppointmentRescheduleInteractor.RescheduleRequestListener, AppointmentRescheduleInteractor.OnAppointmentTimeSlotsEndingListener {

    private AppointmentReschedulePresenter.View view;
    private AppointmentRescheduleInteractor interactor;

    public AppointmentReschedulePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new AppointmentRescheduleInteractorImpl(context);
    }

    @Override
    public void onStart(String notificationId) {
        if (view != null) {
            view.showProgress("Loading Details", "Loading Request Details. Please wait...");
            view.setUpCalendar();
        }
        interactor.getConsultationRequestDetails(notificationId, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void findAppointments(String facultyId) {
        interactor.getAppointments(facultyId, this);
    }

    @Override
    public void findScheduleFromDate(String facultyId, Date dateSelected) {
        interactor.getScheduleFromDate(facultyId, dateSelected, this);
    }

    @Override
    public void findAppointmentFromDate(String facultyId, Date dateSelected) {
        interactor.getAppointmentFromDate(facultyId, dateSelected, this);
    }

    @Override
    public void findExistingTimeSlotAppointmentsFromDate(String facultyId, Date dateSelected) {
        interactor.findExistingTimeSlotAppointmentsFromDate(facultyId, dateSelected, this);
    }

    @Override
    public void findExistingTimeSlotEndingsFromDate(String facultyId, Date dateSelected) {
        interactor.findExistingTimeSlotEndingsFromDate(facultyId, dateSelected, this);
    }

    @Override
    public void sendRescheduledRequest(String notificationId, String rescheduledTimeStart, String rescheduledTimeEnd, String rescheduledVenue, String rescheduledDay, String rescheduledDate, String consultationTitle, String consultationBody, String rescheduledRemarks) {
        if (rescheduledRemarks.isEmpty()){
            view.showErrorDialog("Empty Remarks","There must be a reason for rescheduling the request. Please tell the reason why.");
        }
        else {
            if (view != null) {
                view.showProgress("Rescheduling Request", "Rescheduling Consultation Request. Please wait...");
            }
            try {
                Date date = Constants.dateFormat.parse(rescheduledDate);
                interactor.saveApprovedRequestToDatabase(
                        notificationId,
                        rescheduledTimeStart, rescheduledTimeEnd, rescheduledVenue, rescheduledDay, Constants.yearDateFormat.format(date),
                        consultationTitle, consultationBody, rescheduledRemarks, this);
            } catch (ParseException e) {
                view.showErrorDialog("Parsing Error", e.getMessage());
            }

        }
    }

    @Override
    public void onRequestDetailSuccess(Appointments appointment) {
        if (view != null) {
            try {
                view.hideProgress();
                Date date = yearDateFormat.parse(appointment.getRequestedDate());
                appointment.setRequestedDate(dateFormat.format(date));
                view.initViews(appointment);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onRequestDetailFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setExitDialog(errorTitle, errorMessage);
            view.showSnackMessage(errorMessage);
        }
    }

    @Override
    public void onScheduleFound(List<Schedule> schedules) {
        if (view != null) {
            view.setSchedulesFromDate(schedules);
        }
    }

    @Override
    public void onScheduleNotFound(String errorMessage) {
        if (view != null) {
            view.setEmptySchedule(errorMessage);
        }
    }

    @Override
    public void onAppointmentFound(FacultyAppointmentsGroupAdapter adapter) {
        if (view != null) {
            view.setAppointmentsFromDate(adapter);
        }
    }

    @Override
    public void onAppointmentNotFound(String errorMessage) {
        if (view != null) {
            view.setEmptyAppointment(errorMessage);
        }
    }

    @Override
    public void onFoundAppointments(EventDecorator eventDecorator) {
        if (view != null) {
            view.setUpDotsToCalendar(eventDecorator);
        }
    }

    @Override
    public void onFoundExistingAppointments(Timepoint[] existingTimes) {
        if (view != null) {
            view.setUpApprovedTimeSlotsToTimePicker(existingTimes);
        }
    }

    @Override
    public void onRescheduleSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.setExitDialog(title, message);
        }
    }

    @Override
    public void onRescheduleFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onFoundExistingTimeEndings(Timepoint[] existingTimeEndings) {
        if (view != null) {
            view.setUpApprovedTimeSlotsEndingToTimePicker(existingTimeEndings);
        }
    }
}

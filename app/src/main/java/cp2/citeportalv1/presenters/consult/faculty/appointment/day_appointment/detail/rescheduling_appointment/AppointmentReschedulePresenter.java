package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail.rescheduling_appointment;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.Date;
import java.util.List;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.utils.EventDecorator;

public interface AppointmentReschedulePresenter {
    interface View {
        void showProgress(String title, String caption);

        void hideProgress();

        void setUpCalendar();

        void setUpDotsToCalendar(EventDecorator eventDecorator);

        void setUpApprovedTimeSlotsToTimePicker(Timepoint[] existingTimes);

        void setUpApprovedTimeSlotsEndingToTimePicker(Timepoint[] existingTimeEndings);

        void initViews(Appointments appointment);

        void setEmptySchedule(String emptyMessage);

        void setEmptyAppointment(String emptyMessage);

        void setSchedulesFromDate(List<Schedule> schedules);

        void setAppointmentsFromDate(FacultyAppointmentsGroupAdapter adapter);

        void showSnackMessage(String message);

        void setExitDialog(String title, String message);

        void showErrorDialog(String title, String message);
    }

    void onStart(String notificationId);

    void onDestroy();

    void findAppointments(String facultyId);

    void findScheduleFromDate(String facultyId, Date dateSelected);

    void findAppointmentFromDate(String facultyId, Date dateSelected);

    void findExistingTimeSlotAppointmentsFromDate(String facultyId, Date dateSelected);

    void findExistingTimeSlotEndingsFromDate(String facultyId, Date dateSelected);

    void sendRescheduledRequest(String notificationId,
                                String rescheduledTimeStart, String rescheduledTimeEnd,
                                String rescheduledVenue, String rescheduledDay, String rescheduledDate,
                                String consultationTitle, String consultationBody, String rescheduledRemarks);
}

package cp2.citeportalv1.presenters.consult.faculty.schedule.consultation_hours;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface ConsultationHoursPresenter {
    interface View {
        void loadDayPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }
    void onStart();
    void onDestroy();
}

package cp2.citeportalv1.presenters.consult.student.rejected.rejected_detail;

import android.content.Context;

import java.text.ParseException;
import java.util.Date;

import cp2.citeportalv1.models.ConsultationResponse;

import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

public class RejectedDetailPresenterImpl implements RejectedDetailPresenter, RejectedDetailInteractor.DetailListener {
    private View view;
    private RejectedDetailInteractor detailInteractor;

    public RejectedDetailPresenterImpl(View view, Context context) {
        this.view = view;
        this.detailInteractor = new RejectedDetailInteractorImpl(context);
    }

    @Override
    public void onStart(String notificationId) {
        detailInteractor.getNotificationDetail(notificationId, this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void getNotificationDetail(ConsultationResponse response) throws ParseException {
        Date date = yearDateFormat.parse(response.getRequestedDate());
        response.setRequestedDate(dateFormat.format(date));
        view.setRejectedDetail(response);
    }

    @Override
    public void getDetailError(String errorMessage) {
        view.setApprovedNotExist(errorMessage);
    }
}

package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment;

import android.content.Context;

import java.util.Date;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentDayGroupAdapter;

public class FacultyDayAppointmentPresenterImpl implements FacultyDayAppointmentPresenter,
        FacultyDayAppointmentInteractor.FindAppointmentListener,
        FacultyDayAppointmentInteractor.LoadAppointmentsListener {

    private FacultyDayAppointmentPresenter.View view;
    private FacultyDayAppointmentInteractor interactor;

    public FacultyDayAppointmentPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyDayAppointmentInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.showProgress();
        }
        interactor.getAppointments(this);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void findAppointments() {
        if (view != null) {
            view.showProgress();
        }
        interactor.getAppointments(this);
    }

    @Override
    public void findAppointment(Date dateClicked) {
        interactor.findAppointmentToDatabase(dateClicked, this);
    }

    @Override
    public void onFindingSuccess(FacultyAppointmentDayGroupAdapter appointments) {
        if (view != null) {
            view.setAppointments(appointments);
        }
    }

    @Override
    public void onFindingFailure(String errorMessage) {
        if (view != null) {
            view.showSnackMessage(errorMessage);
            view.setEmptyAppointment(errorMessage);
        }
    }

    @Override
    public void onSuccessLoadingAppointment(String successMessage, FacultyAppointmentDayGroupAdapter appointmentList) {
        if (view != null) {
            view.hideProgress();
            view.setAppointments(appointmentList);
        }
    }

    @Override
    public void onFailureLoadingAppointment(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyAppointment(errorMessage);
            view.showSnackMessage(errorMessage);
        }
    }
}

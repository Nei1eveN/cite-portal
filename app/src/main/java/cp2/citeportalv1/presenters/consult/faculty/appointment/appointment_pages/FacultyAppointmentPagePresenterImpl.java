package cp2.citeportalv1.presenters.consult.faculty.appointment.appointment_pages;

import android.content.Context;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public class FacultyAppointmentPagePresenterImpl implements FacultyAppointmentPagePresenter, FacultyAppointmentPageInteractor.AppointmentPageListener {
    private FacultyAppointmentPagePresenter.View view;
    private FacultyAppointmentPageInteractor interactor;

    public FacultyAppointmentPagePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyAppointmentPageInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getPages(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void onPageSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null) {
            view.loadDayPages(viewPagerAdapter, numberOfPages);
        }
    }

    @Override
    public void onPageFailure(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null) {
            view.loadDayPages(viewPagerAdapter, numberOfPages);
        }
    }
}

package cp2.citeportalv1.presenters.consult.student.appointment.day_appointment.detail;

import cp2.citeportalv1.models.Appointments;

public interface StudentAppointmentDetailPresenter {
    interface View {
        void showProgress(String title, String caption);
        void hideProgress();
        void setApprovedDetail(Appointments appointment);
        void setApprovedNotExist(String notExistTitle, String notExistMessage);
        void setSnackMessage(String snackMessage);
    }
    void onStart(String notificationId);
    void onDestroy();
}

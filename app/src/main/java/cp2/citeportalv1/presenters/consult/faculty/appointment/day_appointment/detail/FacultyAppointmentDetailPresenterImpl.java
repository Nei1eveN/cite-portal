package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail;

import android.content.Context;

import java.text.ParseException;
import java.util.Date;

import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.utils.Constants;

public class FacultyAppointmentDetailPresenterImpl implements FacultyAppointmentDetailPresenter, FacultyAppointmentDetailInteractor.AppointmentDetailListener {

    private View view;
    private FacultyAppointmentDetailInteractor interactor;

    public FacultyAppointmentDetailPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyAppointmentDetailInteractorImpl(context);
    }

    @Override
    public void onStart(String notificationId) {
        if (view != null) {
            view.showProgress("Loading Details", "Loading Appointment Details. Please wait...");
        }
        interactor.getNotificationDetail(notificationId, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void getNotificationDetail(Appointments appointment) {
        if (view != null) {
            view.hideProgress();
            try {
                Date date = Constants.yearDateFormat.parse(appointment.getRequestedDate());
                appointment.setRequestedDate("(" + Constants.dateFormat.format(date) + ")");
                view.setApprovedDetail(appointment);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void getDetailError(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setApprovedNotExist(errorTitle, errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.consult.student.appointment.all_appointments;

import cp2.citeportalv1.adapters.grouplist.StudentAppointmentsGroupAdapter;

public interface StudentAppointmentsPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void showSnackMessage(String message);
        void setAppointments(StudentAppointmentsGroupAdapter adapter);
        void setEmptyAppointment(String emptyMessage);
    }
    void onStart(String day);
    void onDestroy();
    void findAppointmentsAccordingToDay(String day);
}

package cp2.citeportalv1.presenters.consult.faculty.consultation_log.detail;

import cp2.citeportalv1.models.ConsultationLog;

public interface FacultyConsultationLogDetailPresenter {
    interface View {
        void showProgress(String title, String caption);
        void hideProgress();
        void setApprovedDetail(ConsultationLog consultationLog);
        void setDetailNotExist(String notExistTitle, String notExistMessage);
        void setSnackMessage(String snackMessage);
    }
    void onStart(String notificationId);
    void onDestroy();
}

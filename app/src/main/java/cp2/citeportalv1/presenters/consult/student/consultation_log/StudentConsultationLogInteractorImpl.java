package cp2.citeportalv1.presenters.consult.student.consultation_log;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.grouplist.StudentConsultationLogGroupAdapter;
import cp2.citeportalv1.models.ConsultationLog;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.utils.Constants;

import static cp2.citeportalv1.utils.Constants.CONSULTATION_LOG;
import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;

class StudentConsultationLogInteractorImpl implements StudentConsultationLogInteractor {
    private Context context;

    StudentConsultationLogInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getDetails(OnApprovedResponseListener responseListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CONSULTATION_LOG);
        databaseReference.keepSynced(true);

        databaseReference.orderByChild("requestedTimeStart").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<ConsultationLog> consultationLogs = new ArrayList<>();
                    consultationLogs.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        String appointmentId = snapshot.getKey();
                        assert appointmentId != null;
                        ConsultationLog consultationLog = Objects.requireNonNull(snapshot.getValue(ConsultationLog.class)).withId(appointmentId);
                        consultationLogs.add(consultationLog);
                    }
                    LinkedHashMap<String, Set<ConsultationLog>> groupedHashMap = Constants.groupConsultationLogDataIntoHashMap(consultationLogs);
                    List<ListItem> consolidatedList = new ArrayList<>();

                    for (String date : groupedHashMap.keySet()) {
                        Log.d("dates", date);
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(dateFormat.format(new Date(Long.parseLong(date))));
                        dateItem.setDay(dayFormat.format(new Date(Long.parseLong(date))));
                        consolidatedList.add(dateItem);
                        for (ConsultationLog consultationLog : Objects.requireNonNull(groupedHashMap.get(date))) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setConsultationLog(consultationLog);
                            consolidatedList.add(generalItem);
                        }
                    }

                    StudentConsultationLogGroupAdapter adapter = new StudentConsultationLogGroupAdapter(context, consolidatedList);

                    responseListener.OnResponseSuccess(adapter);
                }
                else {
                    responseListener.OnResponseFailure("Maybe, your Appointments are still on going or just about to happen.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                responseListener.OnResponseFailure(databaseError.getMessage());
            }
        });
    }
}

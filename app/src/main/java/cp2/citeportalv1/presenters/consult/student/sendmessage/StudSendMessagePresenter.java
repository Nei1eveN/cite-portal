package cp2.citeportalv1.presenters.consult.student.sendmessage;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cp2.citeportalv1.models.Classmate;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.utils.EventDecorator;

public interface StudSendMessagePresenter {
    interface View {
        void setSenderDetails(Student student);
        void setNoAvailableSchedules(String emptyMessage);
        void setScheduleDetails(List<Schedule> schedules);
        void initViews(ArrayList<String> stringArrayList);
        void showProgress(String caption);
        void hideProgress();
        void setEmptyWrapper();
        void setNullWrapper();
        void setSnackMessage(String message);
        void showErrorMessage(String title, String errorMessage);
        void setExitMessage(String title, String exitMessage);
        void loadClassmates(List<Classmate> classmates);
        void setEmptyClassmates(String emptyMessage);
        void setUpCalendar();
        void setUpDotsToCalendar(EventDecorator eventDecorator);
        void setUpApprovedTimeSlotsToTimePicker(Timepoint[] existingTimes);
    }
    void onStart();
    void onDestroy();
    void initiateCalendar();
    void findSchedules(String facultyId);
    void findAppointments(String facultyId);
    void submitConsultationDetailsWithCc(String emails, String concernTitle, String concernType, String concernMessage,
                                         String requestedDay, String requestedDate,
                                         String requestedTimeStart, String requestedTimeEnd,
                                         String venue, String toConcernedFacultyUserId);

    void findScheduleFromDate(String facultyId, Date dateClicked);

    void findExistingTimeSlotAppointmentsFromDate(String facultyId, Date dateClicked);
}

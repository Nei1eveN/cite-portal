package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail;

import cp2.citeportalv1.models.Appointments;

public interface FacultyAppointmentDetailInteractor {
    interface AppointmentDetailListener {
        void getNotificationDetail(Appointments appointment);
        void getDetailError(String errorTitle, String errorMessage);
    }
    void getNotificationDetail(String notificationId, AppointmentDetailListener listener);
}

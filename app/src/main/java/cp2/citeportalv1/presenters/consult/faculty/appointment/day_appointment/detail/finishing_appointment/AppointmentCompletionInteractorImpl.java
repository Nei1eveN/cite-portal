package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail.finishing_appointment;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.Faculty;

import static cp2.citeportalv1.utils.Constants.APPOINTMENTS;
import static cp2.citeportalv1.utils.Constants.APPOINTMENT_CODE;
import static cp2.citeportalv1.utils.Constants.APPOINTMENT_FEEDBACK;
import static cp2.citeportalv1.utils.Constants.CONSULTATION_LOG;
import static cp2.citeportalv1.utils.Constants.CONSULTATION_RESPONSES;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.MESSAGE_BODY;
import static cp2.citeportalv1.utils.Constants.MESSAGE_REMARKS;
import static cp2.citeportalv1.utils.Constants.MESSAGE_SIDE_NOTE;
import static cp2.citeportalv1.utils.Constants.MESSAGE_STATUS;
import static cp2.citeportalv1.utils.Constants.MESSAGE_TITLE;
import static cp2.citeportalv1.utils.Constants.RECEIVER_ID;
import static cp2.citeportalv1.utils.Constants.RECEIVER_SIGNATURE;
import static cp2.citeportalv1.utils.Constants.REQUESTED_DATE;
import static cp2.citeportalv1.utils.Constants.REQUESTED_DAY;
import static cp2.citeportalv1.utils.Constants.REQUESTED_SCHEDULE;
import static cp2.citeportalv1.utils.Constants.REQUESTED_TIME_END;
import static cp2.citeportalv1.utils.Constants.REQUESTED_TIME_START;
import static cp2.citeportalv1.utils.Constants.RESPONSE_CODE;
import static cp2.citeportalv1.utils.Constants.SENDER_CC;
import static cp2.citeportalv1.utils.Constants.SENDER_DEPARTMENT;
import static cp2.citeportalv1.utils.Constants.SENDER_FIRST_NAME;
import static cp2.citeportalv1.utils.Constants.SENDER_ID;
import static cp2.citeportalv1.utils.Constants.SENDER_IMAGE_URL;
import static cp2.citeportalv1.utils.Constants.SENDER_LAST_NAME;
import static cp2.citeportalv1.utils.Constants.SENDER_MIDDLE_NAME;
import static cp2.citeportalv1.utils.Constants.SENDER_PROGRAM;
import static cp2.citeportalv1.utils.Constants.SENDER_SIGNATURE;
import static cp2.citeportalv1.utils.Constants.SENDER_YEAR_LEVEL;
import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.TIMESTAMP;
import static cp2.citeportalv1.utils.Constants.VENUE;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;
import static cp2.citeportalv1.utils.Constants.timeFormat12hr;

class AppointmentCompletionInteractorImpl implements AppointmentCompletionInteractor {
    private Context context;

    AppointmentCompletionInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getAppointmentDetails(String notificationId, AppointmentDetailListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(APPOINTMENTS);
        databaseReference.keepSynced(true);

        databaseReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Appointments appointment = Objects.requireNonNull(dataSnapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    try {
                        listener.onAppointmentDetailSuccess(appointment);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    listener.onAppointmentError("Non-Existing Appointment Details", "Appointment Details are not existing.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onAppointmentError("Database Error", databaseError.getMessage());
            }
        });
    }

    @Override
    public void finishAppointment(String notificationId, String appointmentRemarks, String status, FinishAppointmentListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onFinishingError("Network Error", "Please check your network connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

            /**FACULTY SIGNATURE REFERENCE**/
            DatabaseReference facultySignatureReference = firebaseDatabase.getReference(FACULTY).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            facultySignatureReference.keepSynced(true);

            facultySignatureReference.child("userSignature").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(APPOINTMENTS);
                        databaseReference.keepSynced(true);

                        /**FACULTY APPOINTMENT DETAILS**/
                        databaseReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    Appointments appointment = Objects.requireNonNull(dataSnapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                                    DatabaseReference facultyDetailsReference = firebaseDatabase.getReference(FACULTY).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                    facultyDetailsReference.keepSynced(true);

                                    facultyDetailsReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                                            try {
                                                if (timeFormat12hr.parse(timeFormat12hr.format(new Date())).before(timeFormat12hr.parse(appointment.getRequestedTimeStart()))) {
                                                    listener.onFinishingError("Finishing Ahead of Time", "You cannot finish an upcoming appointment.");
                                                } else {
                                                    /**STUDENT CONSULTATION LOG DETAILS**/
                                                    Map<String, Object> studentAppointmentMap = new HashMap<>();

                                                    studentAppointmentMap.put(SENDER_ID, appointment.getReceiverId());
                                                    studentAppointmentMap.put(RECEIVER_ID, appointment.getSenderId());

                                                    studentAppointmentMap.put(SENDER_FIRST_NAME, faculty.getfName());
                                                    studentAppointmentMap.put(SENDER_MIDDLE_NAME, faculty.getMidName());
                                                    studentAppointmentMap.put(SENDER_LAST_NAME, faculty.getLastName());
                                                    studentAppointmentMap.put(SENDER_IMAGE_URL, faculty.getFacultyImageURL());
                                                    studentAppointmentMap.put(SENDER_DEPARTMENT, faculty.getDepartment());

                                                    studentAppointmentMap.put(MESSAGE_TITLE, appointment.getMessageTitle());
                                                    studentAppointmentMap.put(MESSAGE_BODY, appointment.getMessageBody());
                                                    studentAppointmentMap.put(MESSAGE_REMARKS, appointment.getMessageRemarks());

                                                    studentAppointmentMap.put(MESSAGE_SIDE_NOTE, appointment.getMessageSideNote());
                                                    studentAppointmentMap.put(MESSAGE_STATUS, status);

                                                    studentAppointmentMap.put(REQUESTED_DAY, appointment.getRequestedDay());
                                                    studentAppointmentMap.put(REQUESTED_DATE, appointment.getRequestedDate());
                                                    studentAppointmentMap.put(REQUESTED_SCHEDULE, appointment.getRequestedSchedule());
                                                    studentAppointmentMap.put(REQUESTED_TIME_START, appointment.getRequestedTimeStart());
                                                    studentAppointmentMap.put(REQUESTED_TIME_END, appointment.getRequestedTimeEnd());
                                                    studentAppointmentMap.put(VENUE, appointment.getVenue());

                                                    studentAppointmentMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

                                                    studentAppointmentMap.put(APPOINTMENT_CODE, appointment.getAppointmentCode());
                                                    studentAppointmentMap.put(SENDER_CC, appointment.getSenderCc());

                                                    studentAppointmentMap.put(APPOINTMENT_FEEDBACK, appointmentRemarks);
                                                    studentAppointmentMap.put(SENDER_SIGNATURE, appointment.getSenderSignature());
                                                    studentAppointmentMap.put(RECEIVER_SIGNATURE, appointment.getReceiverSignature());

                                                    /**STUDENT APPOINTMENT DETAILS REMOVE**/
                                                    DatabaseReference studentAppointmentReference = firebaseDatabase.getReference(STUDENTS).child(appointment.getSenderId()).child(APPOINTMENTS);
                                                    studentAppointmentReference.keepSynced(true);

                                                    saveLogFinalDetails(notificationId, appointment, studentAppointmentMap, firebaseDatabase, studentAppointmentReference, listener, status, appointmentRemarks);
                                                }
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }


                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                } else {
                                    listener.onFinishingError("Non-Existing Appointment Details", "Appointment Details are not existing.");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.onFinishingError("Database Error", databaseError.getMessage());
                            }
                        });
                    } else {
                        listener.onFinishingError("Faculty Signature Not Found", "You cannot finish this appointment, unless you create a signature. Please go visit your Profile and create a signature.");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onFinishingError("Database Error", databaseError.getMessage());
                }
            });


        }

    }

    @Override
    public void cancelAppointment(String notificationId, String appointmentRemarks, String status, CancelAppointmentListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onCancelError("Network Error", "Please check your network connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(APPOINTMENTS);
            databaseReference.keepSynced(true);

            /**FACULTY APPOINTMENT DETAILS**/
            databaseReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Appointments appointment = Objects.requireNonNull(dataSnapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                        DatabaseReference facultyDetailsReference = firebaseDatabase.getReference(FACULTY).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        facultyDetailsReference.keepSynced(true);

                        facultyDetailsReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                                /**STUDENT REJECTED REQUEST DETAILS**/
                                Map<String, Object> studentRejectedRequestMap = new HashMap<>();

                                studentRejectedRequestMap.put(SENDER_ID, appointment.getSenderId());
                                studentRejectedRequestMap.put(RECEIVER_ID, appointment.getReceiverId());

                                studentRejectedRequestMap.put(SENDER_FIRST_NAME, faculty.getfName());
                                studentRejectedRequestMap.put(SENDER_MIDDLE_NAME, faculty.getMidName());
                                studentRejectedRequestMap.put(SENDER_LAST_NAME, faculty.getLastName());
                                studentRejectedRequestMap.put(SENDER_IMAGE_URL, faculty.getFacultyImageURL());
                                studentRejectedRequestMap.put(SENDER_DEPARTMENT, faculty.getDepartment());

                                studentRejectedRequestMap.put(MESSAGE_TITLE, appointment.getMessageTitle());
                                studentRejectedRequestMap.put(MESSAGE_BODY, appointment.getMessageBody());
                                studentRejectedRequestMap.put(MESSAGE_REMARKS, appointmentRemarks);
                                studentRejectedRequestMap.put(MESSAGE_SIDE_NOTE, "Course" + "\n" + appointment.getMessageTitle() + "" + "\n\nDetails:\n" + "" + appointment.getMessageBody() + "\n\n" + "Remarks:\n" + status + "\n\n" + "Reason for Cancelling Appointment:\n" + appointmentRemarks + "\n\nThis appointment is cancelled." + "");
                                studentRejectedRequestMap.put(MESSAGE_STATUS, status);

                                studentRejectedRequestMap.put(REQUESTED_DAY, appointment.getRequestedDay());
                                studentRejectedRequestMap.put(REQUESTED_DATE, appointment.getRequestedDate());
                                studentRejectedRequestMap.put(REQUESTED_SCHEDULE, appointment.getRequestedSchedule());
                                studentRejectedRequestMap.put(REQUESTED_TIME_START, appointment.getRequestedTimeStart());
                                studentRejectedRequestMap.put(REQUESTED_TIME_END, appointment.getRequestedTimeEnd());
                                studentRejectedRequestMap.put(VENUE, appointment.getVenue());

                                studentRejectedRequestMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

                                studentRejectedRequestMap.put(RESPONSE_CODE, appointment.getAppointmentCode());
                                studentRejectedRequestMap.put(SENDER_CC, appointment.getSenderCc());
                                /**END OF STUDENT REJECTED REQUEST DETAILS**/

                                /**FACULTY REJECT REQUEST DETAILS**/
                                Map<String, Object> facultyRejectedRequestMap = new HashMap<>();
                                facultyRejectedRequestMap.put(SENDER_ID, appointment.getReceiverId());
                                facultyRejectedRequestMap.put(RECEIVER_ID, appointment.getSenderId());

                                facultyRejectedRequestMap.put(SENDER_FIRST_NAME, appointment.getSenderFirstName());
                                facultyRejectedRequestMap.put(SENDER_MIDDLE_NAME, appointment.getSenderMiddleName());
                                facultyRejectedRequestMap.put(SENDER_LAST_NAME, appointment.getSenderLastName());
                                facultyRejectedRequestMap.put(SENDER_IMAGE_URL, appointment.getSenderImageURL());
                                facultyRejectedRequestMap.put(SENDER_PROGRAM, appointment.getSenderProgram());
                                facultyRejectedRequestMap.put(SENDER_YEAR_LEVEL, appointment.getSenderYearLevel());

                                facultyRejectedRequestMap.put(MESSAGE_TITLE, appointment.getMessageTitle());
                                facultyRejectedRequestMap.put(MESSAGE_BODY, appointment.getMessageBody());
                                facultyRejectedRequestMap.put(MESSAGE_REMARKS, appointment);
                                facultyRejectedRequestMap.put(MESSAGE_SIDE_NOTE, "Course" + "\n" + appointment.getMessageTitle() + "" + "\n\nDetails:\n" + "" + appointment.getMessageBody() + "\n\n" + "Remarks:\n" + status + "\n\n" + "Reason for Cancelling Apointment:\n" + appointmentRemarks + "\n\nThis appointment is cancelled." + "");
                                facultyRejectedRequestMap.put(MESSAGE_STATUS, status);

                                facultyRejectedRequestMap.put(REQUESTED_DAY, appointment.getRequestedDay());
                                facultyRejectedRequestMap.put(REQUESTED_DATE, appointment.getRequestedDate());
                                facultyRejectedRequestMap.put(REQUESTED_SCHEDULE, appointment.getRequestedSchedule());
                                facultyRejectedRequestMap.put(REQUESTED_TIME_START, appointment.getRequestedTimeStart());
                                facultyRejectedRequestMap.put(REQUESTED_TIME_END, appointment.getRequestedTimeEnd());
                                facultyRejectedRequestMap.put(VENUE, appointment.getVenue());

                                facultyRejectedRequestMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

                                facultyRejectedRequestMap.put(RESPONSE_CODE, appointment.getAppointmentCode());
                                facultyRejectedRequestMap.put(SENDER_CC, appointment.getSenderCc());
                                /**END OF FACULTY REJECTED REQUEST DETAILS**/

                                /**STUDENT CONSULTATION RESPONSE SAVING**/
                                DatabaseReference studentConsultationResponseReference = firebaseDatabase.getReference(STUDENTS).child(appointment.getSenderId()).child(CONSULTATION_RESPONSES);
                                studentConsultationResponseReference.keepSynced(true);

                                /**FACULTY CONSULTATION RESPONSE SAVING**/
                                DatabaseReference facultyConsultationResponseReference = firebaseDatabase.getReference(FACULTY).child(appointment.getReceiverId()).child(CONSULTATION_RESPONSES);
                                facultyConsultationResponseReference.keepSynced(true);

                                /**STUDENT CONSULTATION REQUEST REMOVE**/
                                DatabaseReference studentRemoveConsultationRequestReference = firebaseDatabase.getReference(STUDENTS).child(appointment.getSenderId()).child(APPOINTMENTS);
                                studentRemoveConsultationRequestReference.keepSynced(true);

                                /**FACULTY CONSULTATION REQUEST REMOVE**/
                                DatabaseReference facultyRemoveConsultationRequestReference = FirebaseDatabase.getInstance().getReference(FACULTY).child(appointment.getReceiverId()).child(APPOINTMENTS);
                                facultyRemoveConsultationRequestReference.keepSynced(true);

                                /**RESPONSE KEY**/
                                String responseKey = studentRemoveConsultationRequestReference.push().getKey();

                                if (!appointment.getSenderCc().isEmpty()) {
                                    /**CC NOT EMPTY**/
                                    Log.d("interactor--rejectedCCs", String.valueOf(appointment.getSenderCc()));
                                    studentConsultationResponseReference.child(Objects.requireNonNull(responseKey)).updateChildren(studentRejectedRequestMap).addOnSuccessListener(((Activity) context), aVoid -> facultyConsultationResponseReference.child(responseKey).updateChildren(facultyRejectedRequestMap).addOnSuccessListener(((Activity) context), aVoid16 -> studentRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid15 -> facultyRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid14 -> {

                                        List<String> senderCc = appointment.getSenderCc();
                                        for (int i = 0, senderCcSize = senderCc.size(); i < senderCcSize; i++) {
                                            String ccUid = senderCc.get(i);
                                            DatabaseReference carbonAppointmentReference = firebaseDatabase.getReference(STUDENTS).child(ccUid).child(CONSULTATION_RESPONSES);
                                            carbonAppointmentReference.keepSynced(true);

                                            /**CARBON REJECT REQUEST SAVING**/
                                            carbonAppointmentReference.child(Objects.requireNonNull(responseKey)).updateChildren(studentRejectedRequestMap);

                                            /**CARBON CONSULTATION REQUEST REMOVE**/
                                            DatabaseReference carbonReference = firebaseDatabase.getReference(STUDENTS).child(ccUid).child(APPOINTMENTS);
                                            carbonReference.keepSynced(true);

                                            carbonReference.child(notificationId).removeValue();

                                            if (i == (appointment.getSenderCc().size() - 1)) {

                                                listener.onCancelSuccess("Appointment Cancellation Success","Course" + "\n" + appointment.getMessageTitle() + "" + "\n\nDetails:\n" + "" + appointment.getMessageBody() + "\n\n" + "Remarks:\n" + status + "\n\n" + "Reason for Cancelling Appointment:\n" + appointmentRemarks + "\n\nYou have rejected this Consultation Request." + "");

                                            }
                                        }
                                    }).addOnFailureListener(((Activity) context), e -> listener.onCancelError("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onCancelError("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onCancelSuccess("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onCancelSuccess("Saving Error", e.getMessage()));
                                } else {
                                    /**CC EMPTY**/
                                    Log.d("interactor--noCC", "No Rejected CCs");
                                    studentConsultationResponseReference.child(Objects.requireNonNull(responseKey)).updateChildren(studentRejectedRequestMap).addOnSuccessListener(((Activity) context), aVoid -> facultyConsultationResponseReference.child(responseKey).updateChildren(facultyRejectedRequestMap).addOnSuccessListener(((Activity) context), aVoid13 -> studentRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid12 -> facultyRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid1 -> listener.onCancelSuccess("Appointment Cancellation Success","Course" + "\n" + appointment.getMessageTitle() + "" + "\n\nDetails:\n" + "" + appointment.getMessageBody() + "\n\n" + "Remarks:\n" + "REJECTED" + "\n\n" + "Reason for Cancelling Appointment:\n" + status + "\n\nYou have rejected this Consultation Request." + "")).addOnFailureListener(((Activity) context), e -> listener.onCancelError("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onCancelError("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onCancelError("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onCancelError("Saving Error", e.getMessage()));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.onCancelError("Database Error", databaseError.getMessage());
                            }
                        });
                    } else {
                        listener.onCancelError("Non-Existing Appointment Details", "Appointment Details are not existing.");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    private void saveLogFinalDetails(String notificationId, Appointments appointment,
                                     Map<String, Object> studentAppointmentMap,
                                     FirebaseDatabase firebaseDatabase, DatabaseReference studentAppointmentReference,
                                     FinishAppointmentListener listener,
                                     String status, String appointmentRemarks) {
        /**FACULTY CONSULTATION LOG DETAILS**/
        Map<String, Object> facultyAppointmentMap = new HashMap<>();
        facultyAppointmentMap.put(SENDER_ID, appointment.getSenderId());
        facultyAppointmentMap.put(RECEIVER_ID, appointment.getReceiverId());

        facultyAppointmentMap.put(SENDER_FIRST_NAME, appointment.getSenderFirstName());
        facultyAppointmentMap.put(SENDER_MIDDLE_NAME, appointment.getSenderMiddleName());
        facultyAppointmentMap.put(SENDER_LAST_NAME, appointment.getSenderLastName());
        facultyAppointmentMap.put(SENDER_IMAGE_URL, appointment.getSenderImageURL());
        facultyAppointmentMap.put(SENDER_PROGRAM, appointment.getSenderProgram());
        facultyAppointmentMap.put(SENDER_YEAR_LEVEL, appointment.getSenderYearLevel());

        facultyAppointmentMap.put(MESSAGE_TITLE, appointment.getMessageTitle());
        facultyAppointmentMap.put(MESSAGE_BODY, appointment.getMessageBody());
        facultyAppointmentMap.put(MESSAGE_REMARKS, appointment.getMessageRemarks());
        facultyAppointmentMap.put(MESSAGE_SIDE_NOTE, "This is an approved request.\n\nPlease be informed that this appointment will be held from " + appointment.getRequestedTimeStart() + " - " + appointment.getRequestedTimeEnd() + " @ " + appointment.getVenue() + " on " + appointment.getRequestedDay() + ", " + appointment.getRequestedDate() + "\n\nThis is a system-generated message.");
        facultyAppointmentMap.put(MESSAGE_STATUS, status);

        facultyAppointmentMap.put(REQUESTED_DAY, appointment.getRequestedDay());
        facultyAppointmentMap.put(REQUESTED_DATE, appointment.getRequestedDate());
        facultyAppointmentMap.put(REQUESTED_SCHEDULE, appointment.getRequestedSchedule());
        facultyAppointmentMap.put(REQUESTED_TIME_START, appointment.getRequestedTimeStart());
        facultyAppointmentMap.put(REQUESTED_TIME_END, appointment.getRequestedTimeEnd());
        facultyAppointmentMap.put(VENUE, appointment.getVenue());

        facultyAppointmentMap.put(TIMESTAMP, String.valueOf(System.currentTimeMillis() / 1000L));

        facultyAppointmentMap.put(APPOINTMENT_CODE, appointment.getAppointmentCode());
        facultyAppointmentMap.put(SENDER_CC, appointment.getSenderCc());

        facultyAppointmentMap.put(APPOINTMENT_FEEDBACK, appointmentRemarks);
        facultyAppointmentMap.put(SENDER_SIGNATURE, appointment.getSenderSignature());
        facultyAppointmentMap.put(RECEIVER_SIGNATURE, appointment.getReceiverSignature());

        /**STUDENT CONSULTATION LOG SAVING**/
        DatabaseReference studentAppointmentSavingReference = FirebaseDatabase.getInstance().getReference(STUDENTS).child(appointment.getSenderId()).child(CONSULTATION_LOG);
        studentAppointmentSavingReference.keepSynced(true);

        /**CONSULTATION LOG KEY**/
        String consultationLogKey = studentAppointmentSavingReference.push().getKey();

        /**FACULTY APPOINTMENT SAVING**/
        DatabaseReference facultyAppointmentSavingReference = FirebaseDatabase.getInstance().getReference(FACULTY).child(appointment.getReceiverId()).child(CONSULTATION_LOG);
        facultyAppointmentSavingReference.keepSynced(true);

        if (!appointment.getSenderCc().isEmpty()) {
            Log.d("interactor--senderCcs", String.valueOf(appointment.getSenderCc()));

            studentAppointmentSavingReference.child(Objects.requireNonNull(consultationLogKey)).updateChildren(studentAppointmentMap).addOnSuccessListener(((Activity) context), aVoid -> facultyAppointmentSavingReference.child(Objects.requireNonNull(consultationLogKey)).updateChildren(facultyAppointmentMap).addOnSuccessListener(((Activity) context), aVoid14 -> {

                /**STUDENT APPOINTMENT DETAILS REMOVE**/
                studentAppointmentReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid13 -> {

                    DatabaseReference facultyRemoveConsultationRequestReference = firebaseDatabase.getReference(FACULTY).child(appointment.getReceiverId()).child(APPOINTMENTS);
                    facultyRemoveConsultationRequestReference.keepSynced(true);

                    /**FACULTY APPOINTMENT DETAIL REMOVE**/
                    facultyRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid12 -> {

                        List<String> senderCc = appointment.getSenderCc();
                        for (int i = 0, senderCcSize = senderCc.size(); i < senderCcSize; i++) {
                            String ccUid = senderCc.get(i);
                            DatabaseReference carbonAppointmentReference = firebaseDatabase.getReference(STUDENTS).child(ccUid).child(APPOINTMENTS);
                            carbonAppointmentReference.keepSynced(true);

                            /**CARBON CONSULTATION LOG SAVING**/
                            carbonAppointmentReference.child(Objects.requireNonNull(consultationLogKey)).updateChildren(studentAppointmentMap);

                            /**CARBON APPOINTMENT REMOVE**/
                            DatabaseReference carbonReference = firebaseDatabase.getReference(STUDENTS).child(ccUid).child(APPOINTMENTS);
                            carbonReference.keepSynced(true);

                            carbonReference.child(notificationId).removeValue();

                            if (i == (appointment.getSenderCc().size() - 1)) {
                                listener.onFinishingSuccess("Saving Success", "Details are now in your Consultation Log.\n\nTo check the details of this appointment, please take a look on Finished Requests.");
                            }
                        }

                    }).addOnFailureListener(((Activity) context), e -> listener.onFinishingError("Database Error", e.getMessage()));


                }).addOnFailureListener(((Activity) context), e -> listener.onFinishingError("Database Error", e.getMessage()));

            }).addOnFailureListener(((Activity) context), e -> listener.onFinishingError("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onFinishingError("Saving Error", e.getMessage()));
        } else {
            Log.d("interactor--noSenderCcs", "No CCs");

            studentAppointmentSavingReference.child(Objects.requireNonNull(consultationLogKey)).updateChildren(studentAppointmentMap).addOnSuccessListener(((Activity) context), aVoid -> facultyAppointmentSavingReference.child(Objects.requireNonNull(consultationLogKey)).updateChildren(facultyAppointmentMap).addOnSuccessListener(((Activity) context), aVoid1 -> {

                /**STUDENT APPOINTMENT DETAILS REMOVE**/
                studentAppointmentReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid112 -> {

                    DatabaseReference facultyRemoveConsultationRequestReference = firebaseDatabase.getReference(FACULTY).child(appointment.getReceiverId()).child(APPOINTMENTS);
                    facultyRemoveConsultationRequestReference.keepSynced(true);

                    /**FACULTY APPOINTMENT DETAILS REMOVE**/

                    facultyRemoveConsultationRequestReference.child(notificationId).removeValue().addOnSuccessListener(((Activity) context), aVoid11 -> listener.onFinishingSuccess("Saving Success", "Details are now in your Consultation Log. To check the details of this appointment, please take a look on Finished Requests.")).addOnFailureListener(((Activity) context), e -> listener.onFinishingError("Database Error", e.getMessage()));

                }).addOnFailureListener(((Activity) context), e -> listener.onFinishingError("Database Error", e.getMessage()));


            }).addOnFailureListener(((Activity) context), e -> listener.onFinishingError("Saving Error", e.getMessage()))).addOnFailureListener(((Activity) context), e -> listener.onFinishingError("Saving Error", e.getMessage()));
        }
    }
}

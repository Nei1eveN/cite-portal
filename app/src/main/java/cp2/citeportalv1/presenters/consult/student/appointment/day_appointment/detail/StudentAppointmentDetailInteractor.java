package cp2.citeportalv1.presenters.consult.student.appointment.day_appointment.detail;

import cp2.citeportalv1.models.Appointments;

public interface StudentAppointmentDetailInteractor {
    void getNotificationDetail(String notificationId, AppointmentDetailListener listener);
    interface AppointmentDetailListener {
        void getNotificationDetail(Appointments appointment);
        void getDetailError(String errorTitle, String errorMessage);
    }
}

package cp2.citeportalv1.presenters.consult.student.approved;

import android.content.Context;

import cp2.citeportalv1.adapters.grouplist.StudentAppointmentsGroupAdapter;

public class ApprovedResponsePresenterImpl implements ApprovedResponsePresenter, ApprovedResponseInteractor.OnApprovedResponseListener {
    private ApprovedResponsePresenter.View view;
    private ApprovedResponseInteractor responseInteractor;

    public ApprovedResponsePresenterImpl(View view, Context context) {
        this.view = view;
        this.responseInteractor = new ApprovedResponseInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        responseInteractor.getDetails(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void requestApprovedList() {
        if (view != null){
            view.showProgress();
        }
        responseInteractor.getDetails(this);
    }

    @Override
    public void OnResponseSuccess(StudentAppointmentsGroupAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.loadApprovedResponses(adapter);
        }
    }

    @Override
    public void OnResponseFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.consult.student.loadpages.infosys;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public interface InfoSysInstructorListPresenter {
    interface InfoTechView {
        void setProgress();
        void hideProgress();
        void setSnackMessage(String message);
        void setEmptyState(String emptyStateMessage);
        void setFacultyEmployees(List<ListItem> facultyEmployees);
    }
    void onStart();
    void onDestroy();
    void requestFacultyEmployees();
}

package cp2.citeportalv1.presenters.consult.student.sendmessage;

import android.content.Context;
import android.util.Log;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cp2.citeportalv1.models.Classmate;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.utils.EventDecorator;

import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

public class StudSendMessagePresenterImpl implements StudSendMessagePresenter,
        StudSendMessageInteractor.SendingMessageListener, StudSendMessageInteractor.LoadCoursesListener,
        StudSendMessageInteractor.SenderDetailListener, StudSendMessageInteractor.OnLoadClassmatesListener,
        StudSendMessageInteractor.FindScheduleListener, StudSendMessageInteractor.OnAppointmentsListener,
        StudSendMessageInteractor.OnAppointmentTimeSlotsListener, StudSendMessageInteractor.FindSchedulesListener {
    private StudSendMessagePresenter.View view;
    private StudSendMessageInteractor messageInteractor;

    public StudSendMessagePresenterImpl(View view, Context context) {
        this.view = view;
        this.messageInteractor = new StudSendMessageInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.setUpCalendar();
        }
        messageInteractor.loadCourses(this);
        messageInteractor.getSenderDetails(this);
        messageInteractor.getClassmates(this);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void initiateCalendar() {
        view.setUpCalendar();
    }

    @Override
    public void findSchedules(String facultyId) {
        messageInteractor.getSchedules(facultyId, this);
    }

    @Override
    public void findAppointments(String facultyId) {
        messageInteractor.getAppointments(facultyId, this);
    }

    @Override
    public void submitConsultationDetailsWithCc(String emails,
                                                String concernTitle, String concernType, String concernMessage,
                                                String requestedDay, String requestedDate,
                                                String requestedTimeStart, String requestedTimeEnd, String venue, String toConcernedFacultyUserId) {
        if (concernTitle.isEmpty()) {
            view.setEmptyWrapper();
        } else if (concernType.equals("Others") && concernMessage.isEmpty()) {
            view.setEmptyWrapper();
        } else if (concernType.equals("Others") && emails.isEmpty()) {
            if (view != null) {
                view.showProgress("Sending Consultation Details. Please wait...");
                view.setNullWrapper();
            }
            try {
                Date comparingDate = dateFormat.parse(requestedDate);
                messageInteractor.getConsultationDetails(concernTitle, concernType, concernMessage,
                        requestedDay, yearDateFormat.format(comparingDate),
                        requestedTimeStart, requestedTimeEnd, venue,
                        toConcernedFacultyUserId, this);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
//        else if (concernMessage.isEmpty()) {
//            view.setEmptyWrapper();
//        }
        else if (requestedDay == null) {
            view.setSnackMessage("Please select preferred Day for your request.");
        } else if(requestedDate == null || requestedDate.isEmpty()) {
            view.setSnackMessage("Please select preferred Starting and Ending Time for your request.");
        }
        else if (requestedTimeStart == null) {
            view.setSnackMessage("Please select preferred Starting Time for your request.");
        } else if (requestedTimeEnd == null) {
            view.setSnackMessage("Please select preferred Ending Time for your request.");
        } else if (venue == null || venue.isEmpty()) {
            view.setSnackMessage("Please select preferred Starting and Ending Time for your request.");
        } else if (emails.isEmpty()) {
            Log.d("presenter--emptyEmail", "empty email trigger");
            if (view != null) {
                view.showProgress("Sending Consultation Details. Please wait...");
                view.setNullWrapper();
            }
            try {
                Date comparingDate = dateFormat.parse(requestedDate);
                messageInteractor.getConsultationDetails(concernTitle, concernType, concernMessage,
                        requestedDay, yearDateFormat.format(comparingDate),
                        requestedTimeStart, requestedTimeEnd, venue,
                        toConcernedFacultyUserId, this);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        else {
            Log.d("presenter--emails", emails);

            String text = emails;
            text = text.substring(0, text.length() - 1);
            String classmateEmails[] = text.split(",");

            List<String> emailList = new ArrayList<>();
            for (String email : classmateEmails){
                Log.d("presenter--email", email);
                emailList.add(email.trim());
                Log.d("presenter--last-emails", String.valueOf(emailList));
            }
            if (view != null) {
                view.showProgress("Sending Consultation Details. Please wait...");
                view.setNullWrapper();
            }
            try {
                Date comparingDate = dateFormat.parse(requestedDate);
                messageInteractor.getConsultationDetailsWithCc(
                        emailList,
                        concernTitle, concernType, concernMessage,
                        requestedDay, yearDateFormat.format(comparingDate),
                        requestedTimeStart, requestedTimeEnd, venue,
                        toConcernedFacultyUserId, this);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void findScheduleFromDate(String facultyId, Date dateClicked) {
        messageInteractor.getScheduleFromDate(facultyId, dateClicked, this);
    }

    @Override
    public void findExistingTimeSlotAppointmentsFromDate(String facultyId, Date dateClicked) {
        messageInteractor.findExistingTimeSlotAppointmentsFromDate(facultyId, dateClicked, this);
    }


    /**
     * SENDING CONSULTATION DETAILS
     * **/
    @Override
    public void onSendingSuccess(String title, String successMessage) {
        if (view != null) {
            view.hideProgress();
            view.setSnackMessage(successMessage);
            view.setExitMessage(title, successMessage);
        }
    }

    @Override
    public void onSendingFailure(String title, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setSnackMessage(errorMessage);
            view.showErrorMessage(title, errorMessage);
//            view.setExitMessage(title, errorMessage);
        }
    }

    /**
     * COURSES
     * **/
    @Override
    public void onLoadCourseSuccess(ArrayList<String> courses) {
        if (view != null) {
            view.initViews(courses);
        }
    }

    @Override
    public void onLoadCourseFailure(String errorMessage) {
        if (view != null) {
            view.setSnackMessage(errorMessage);
        }

    }

    /**
     * SENDER DETAILS
     * **/
    @Override
    public void onSenderDetailSuccess(Student student) {
        if (view != null) {
            view.setSenderDetails(student);
        }
    }

    @Override
    public void onSenderDetailFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.setSnackMessage(errorMessage);
            view.setExitMessage(errorTitle, errorMessage);
        }
    }

    /**
     * LOAD CLASSMATES
     * **/
    @Override
    public void onLoadClassmateSuccess(List<Classmate> classmates) {
        if (view != null) {
            view.loadClassmates(classmates);
        }
    }

    @Override
    public void onLoadClassmateFailure(String errorMessage) {
        if (view != null) {
            view.setEmptyClassmates(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }

    /**
     * LOAD SCHEDULES
     * **/
    @Override
    public void onFindScheduleDetailsSuccess(List<Schedule> schedules) {
        if (view != null) {
            view.setScheduleDetails(schedules);
        }
    }

    @Override
    public void onFindScheduleDetailFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.setExitMessage(errorTitle, errorMessage);
        }
    }

    @Override
    public void onFindScheduleDetailFailure(String errorMessage) {
        if (view != null) {
            view.setNoAvailableSchedules(errorMessage);
        }
    }

    /**
     * LOAD SCHEDULE DATES ACCORDING TO DAY
     * **/
    @Override
    public void onFoundAppointments(EventDecorator eventDecorator) {
        if (view != null) {
            view.setUpDotsToCalendar(eventDecorator);
        }
    }

    /**
     * LOAD EXISTING TIME SLOTS
     * **/
    @Override
    public void onFoundExistingAppointments(Timepoint[] existingTimes) {
        if (view != null) {
            view.setUpApprovedTimeSlotsToTimePicker(existingTimes);
        }
    }
}

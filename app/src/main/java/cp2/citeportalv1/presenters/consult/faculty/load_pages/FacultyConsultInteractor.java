package cp2.citeportalv1.presenters.consult.faculty.load_pages;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface FacultyConsultInteractor {
    void getFragmentPages(LoadPagesListener listener);
    interface LoadPagesListener {
        void onSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }
}

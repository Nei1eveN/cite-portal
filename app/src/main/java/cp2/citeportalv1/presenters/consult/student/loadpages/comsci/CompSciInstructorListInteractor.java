package cp2.citeportalv1.presenters.consult.student.loadpages.comsci;

import java.util.List;

import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.ListItem;

public interface CompSciInstructorListInteractor {
    void getList(OnDepartmentSyncListener listener);
    interface OnDepartmentSyncListener {
        void onSuccess(List<ListItem> list);
        void onFailure(String errorMessage);
    }
}

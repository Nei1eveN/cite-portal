package cp2.citeportalv1.presenters.consult.faculty.consultation_log;

import android.content.Context;

import cp2.citeportalv1.adapters.grouplist.FacultyConsultationLogGroupAdapter;

public class FacultyConsultationLogPresenterImpl implements FacultyConsultationLogPresenter, FacultyConsultationLogInteractor.FacultyConsultationLogListener {
    private FacultyConsultationLogPresenter.View view;
    private FacultyConsultationLogInteractor interactor;

    public FacultyConsultationLogPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyConsultationLogInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        interactor.getDetails(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void requestApprovedList() {
        if (view != null){
            view.showProgress();
        }
        interactor.getDetails(this);
    }

    @Override
    public void OnResponseSuccess(FacultyConsultationLogGroupAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.loadApprovedResponses(adapter);
        }
    }

    @Override
    public void OnResponseFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

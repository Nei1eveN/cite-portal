package cp2.citeportalv1.presenters.consult.student.approved;

import cp2.citeportalv1.adapters.grouplist.StudentAppointmentsGroupAdapter;

public interface ApprovedResponsePresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setSnackMessage(String snackMessage);
        void loadApprovedResponses(StudentAppointmentsGroupAdapter adapter);
        void setEmptyState(String emptyMessage);
    }
    void onStart();
    void onDestroy();
    void requestApprovedList();
}

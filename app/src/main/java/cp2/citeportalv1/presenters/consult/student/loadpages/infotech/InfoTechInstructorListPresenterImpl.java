package cp2.citeportalv1.presenters.consult.student.loadpages.infotech;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.ListItem;

public class InfoTechInstructorListPresenterImpl implements InfoTechInstructorListPresenter, InfoTechInstructorListInteractor.OnDepartmentSyncListener {
    private InfoTechView view;
    private InfoTechInstructorListInteractor listInteractor;

    public InfoTechInstructorListPresenterImpl(InfoTechView view, Context context) {
        this.view = view;
        this.listInteractor = new InfoTechInstructorListInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.setProgress();
        }
        listInteractor.getList(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestFacultyEmployees() {
        if (view != null) {
            view.setProgress();
        }
        listInteractor.getList(this);
    }

    @Override
    public void onSuccess(List<ListItem> facultyList) {
        if (view != null) {
            view.hideProgress();
            view.setFacultyEmployees(facultyList);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.consult.faculty.consultationdetails;

import cp2.citeportalv1.models.ConsultationRequest;

public interface ConsultationDetailsPresenter {
    interface View {
        void initViews(ConsultationRequest request);
        void initSetAppointmentDialog(ConsultationRequest request);
        void initSetRejectRequestDialog(ConsultationRequest request);
        void setButtonsDisabled();
        void showProgress();
        void hideProgress();
        void setToastMessage(String message);
        void setSnackMessage(String message);
        void setExitDialog(String message);
        void showErrorDialog(String title, String message);
    }
    void onStart(String notificationId);
    void onDestroy();
    void sendApprovedConsultation(String notificationId,
                                  String requestedDay, String requestedDate,
                                  String requestedTimeStart, String requestedTimeEnd, String venue,
                                  String consultationTitle, String consultationBody, String consultationRemarks);
    void sendRejectedConsultation(String notificationId,
                                  String requestedDay, String requestedDate,
                                  String requestedTimeStart, String requestedTimeEnd, String venue,
                                  String consultationTitle, String consultationBody, String consultationRemarks);
}

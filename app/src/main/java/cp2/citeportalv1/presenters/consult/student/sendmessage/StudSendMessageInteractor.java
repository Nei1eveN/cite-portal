package cp2.citeportalv1.presenters.consult.student.sendmessage;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cp2.citeportalv1.models.Classmate;
import cp2.citeportalv1.models.Schedule;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.utils.EventDecorator;

public interface StudSendMessageInteractor {
    interface SenderDetailListener {
        void onSenderDetailSuccess(Student student);

        void onSenderDetailFailure(String errorTitle, String errorMessage);
    }
    void getSenderDetails(SenderDetailListener senderDetails);

    interface SendingMessageListener {
        void onSendingSuccess(String title, String successMessage);

        void onSendingFailure(String title, String errorMessage);
    }
    void getConsultationDetails(String concernTitle, String concernType,
                                String concernMessage,
                                String requestedDay, String requestedDate,
                                String requestedTimeStart, String requestedTimeEnd, String venue,
                                String toConcernedFacultyUserId, SendingMessageListener listener);

    void getConsultationDetailsWithCc(List<String> emails, String concernTitle, String concernType,
                                      String concernMessage,
                                      String requestedDay, String requestedDate,
                                      String requestedTimeStart, String requestedTimeEnd, String venue,
                                      String toConcernedFacultyUserId, SendingMessageListener listener);


    interface LoadCoursesListener {
        void onLoadCourseSuccess(ArrayList<String> courses);

        void onLoadCourseFailure(String errorMessage);
    }
    void loadCourses(LoadCoursesListener loadCoursesListener);

    interface OnLoadClassmatesListener {
        void onLoadClassmateSuccess(List<Classmate> classmates);
        void onLoadClassmateFailure(String errorMessage);
    }
    void getClassmates(OnLoadClassmatesListener listener);

    interface FindScheduleListener {
        void onFindScheduleDetailsSuccess(List<Schedule> schedules);

        void onFindScheduleDetailFailure(String errorMessage);
    }
    void getScheduleFromDate(String facultyId, Date dateSelected, FindScheduleListener listener);

    interface OnAppointmentsListener {
        void onFoundAppointments(EventDecorator eventDecorator);
    }

    void getAppointments(String facultyId, OnAppointmentsListener listener);

    interface OnAppointmentTimeSlotsListener {
        void onFoundExistingAppointments(Timepoint[] existingTimes);
    }

    void findExistingTimeSlotAppointmentsFromDate(String facultyId, Date dateClicked, OnAppointmentTimeSlotsListener listener);

    interface FindSchedulesListener {
        void onFindScheduleDetailsSuccess(List<Schedule> schedules);

        void onFindScheduleDetailFailure(String errorTitle, String errorMessage);
    }

    void getSchedules(String facultyId, FindSchedulesListener listener);
}

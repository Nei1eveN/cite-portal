package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment;

import java.util.Date;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentDayGroupAdapter;

public interface FacultyDayAppointmentInteractor {
    void getAppointments(LoadAppointmentsListener listener);

    void findAppointmentToDatabase(Date dateClicked, FindAppointmentListener appointmentListener);

    interface LoadAppointmentsListener {
        void onSuccessLoadingAppointment(String successMessage, FacultyAppointmentDayGroupAdapter adapter);

        void onFailureLoadingAppointment(String errorMessage);
    }

    interface FindAppointmentListener {
        void onFindingSuccess(FacultyAppointmentDayGroupAdapter appointments);

        void onFindingFailure(String errorMessage);
    }
}

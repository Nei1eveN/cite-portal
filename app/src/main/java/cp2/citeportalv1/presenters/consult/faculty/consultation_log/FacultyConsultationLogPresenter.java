package cp2.citeportalv1.presenters.consult.faculty.consultation_log;

import cp2.citeportalv1.adapters.grouplist.FacultyConsultationLogGroupAdapter;

public interface FacultyConsultationLogPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setSnackMessage(String snackMessage);
        void loadApprovedResponses(FacultyConsultationLogGroupAdapter adapter);
        void setEmptyState(String emptyMessage);
    }
    void onStart();
    void onDestroy();
    void requestApprovedList();
}

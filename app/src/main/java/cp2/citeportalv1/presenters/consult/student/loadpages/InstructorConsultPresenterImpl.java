package cp2.citeportalv1.presenters.consult.student.loadpages;

import android.content.Context;

import cp2.citeportalv1.adapters.grouplist.InstructorGroupAdapter;

public class InstructorConsultPresenterImpl implements InstructorConsultPresenter, InstructorConsultInteractor.InstructorSearchListener {
    private InstructorConsultPresenter.View view;
    private InstructorConsultInteractor interactor;

    public InstructorConsultPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new InstructorConsultInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.initViews();
            view.setEmptyState("Search your Instructor by their Last Name / Surname");
        }
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void submitQuery(String query) {
        if (query.isEmpty()){
            view.setEmptyState("Search your Instructor by their Last Name / Surname");
        }
        else {
            interactor.getDataFromQuery(query, this);
        }
    }

    @Override
    public void onSuccess(InstructorGroupAdapter adapter) {
        if (view != null) {
            view.setInstructorFound(adapter);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.setNoInstructorFound(errorMessage);
        }
    }


}

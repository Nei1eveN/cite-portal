package cp2.citeportalv1.presenters.consult.faculty.load_pages;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface FacultyConsultPresenter {
    interface View {
        void initializeTabs(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }
    void onStart();
    void onDestroy();
}

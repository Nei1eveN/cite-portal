package cp2.citeportalv1.presenters.consult.student.appointment.appointment_pages;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface StudentAppointmentPageInteractor {
    interface AppointmentPageListener {
        void onPageSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
        void onPageFailure(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }

    void getPages(AppointmentPageListener pageListener);
}

package cp2.citeportalv1.presenters.consult.faculty.appointment.appointment_pages;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.views.faculty.FacultyAppointmentsActivity;
import cp2.citeportalv1.views.faculty.FacultyAppointmentsFragment;

import static cp2.citeportalv1.utils.Constants.APPOINTMENTS;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.SCHEDULE_DAY;
import static cp2.citeportalv1.utils.Constants.dateComparator;
import static cp2.citeportalv1.utils.Constants.dayFormat;
import static cp2.citeportalv1.utils.Constants.groupDayStringIntoHashMap;

class FacultyAppointmentPageInteractorImpl implements FacultyAppointmentPageInteractor {
    private Context context;

    private FacultyAppointmentsFragment monday = new FacultyAppointmentsFragment();
    private FacultyAppointmentsFragment tuesday = new FacultyAppointmentsFragment();
    private FacultyAppointmentsFragment wednesday = new FacultyAppointmentsFragment();
    private FacultyAppointmentsFragment thursday = new FacultyAppointmentsFragment();
    private FacultyAppointmentsFragment friday = new FacultyAppointmentsFragment();
    private FacultyAppointmentsFragment saturday = new FacultyAppointmentsFragment();
    private FacultyAppointmentsFragment sunday = new FacultyAppointmentsFragment();

    FacultyAppointmentPageInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getPages(AppointmentPageListener pageListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                .child(APPOINTMENTS);
        databaseReference.keepSynced(true);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(((FragmentActivity) context).getSupportFragmentManager());
        Bundle bundle = new Bundle();
        List<String> dayStrings = new ArrayList<>();

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    try {
                        Bundle mondayBundle = new Bundle();
                        Bundle tuesdayBundle = new Bundle();
                        Bundle wednesdayBundle = new Bundle();
                        Bundle thursdayBundle = new Bundle();
                        Bundle fridayBundle = new Bundle();
                        Bundle saturdayBundle = new Bundle();
                        Bundle sundayBundle = new Bundle();

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));

                            dayStrings.add(appointment.getRequestedDay());
                        }

                        LinkedHashMap<String, Set<String>> groupedHashMap = groupDayStringIntoHashMap(dayStrings);

                        List<String> consolidatedString = new ArrayList<>();

                        for (String day : groupedHashMap.keySet()) {
                            consolidatedString.addAll(Objects.requireNonNull(groupedHashMap.get(day)));
                        }

                        Collections.sort(consolidatedString, dateComparator);

                        for (String sortedDay : consolidatedString) {
                            if (sortedDay.equals("Monday")) {
                                mondayBundle.putString(SCHEDULE_DAY, "Monday");
                                monday.setArguments(mondayBundle);
                                viewPagerAdapter.addFragment(monday, "Monday");
                                viewPagerAdapter.notifyDataSetChanged();
                            }
                            if (sortedDay.equals("Tuesday")) {
                                tuesdayBundle.putString(SCHEDULE_DAY, "Tuesday");
                                tuesday.setArguments(tuesdayBundle);
                                viewPagerAdapter.addFragment(tuesday, "Tuesday");
                                viewPagerAdapter.notifyDataSetChanged();
                            }
                            if ("Wednesday".equals(sortedDay)) {
                                wednesdayBundle.putString(SCHEDULE_DAY, "Wednesday");
                                wednesday.setArguments(wednesdayBundle);
                                viewPagerAdapter.addFragment(wednesday, "Wednesday");
                                viewPagerAdapter.notifyDataSetChanged();
                            }
                            if ("Thursday".equals(sortedDay)) {
                                thursdayBundle.putString(SCHEDULE_DAY, "Thursday");
                                thursday.setArguments(thursdayBundle);
                                viewPagerAdapter.addFragment(thursday, "Thursday");
                                viewPagerAdapter.notifyDataSetChanged();

                            }
                            if ("Friday".equals(sortedDay)) {
                                fridayBundle.putString(SCHEDULE_DAY, "Friday");
                                friday.setArguments(fridayBundle);
                                viewPagerAdapter.addFragment(friday, "Friday");
                                viewPagerAdapter.notifyDataSetChanged();
                            }
                            if ("Saturday".equals(sortedDay)) {
                                saturdayBundle.putString(SCHEDULE_DAY, "Saturday");
                                saturday.setArguments(saturdayBundle);
                                viewPagerAdapter.addFragment(saturday, "Saturday");
                                viewPagerAdapter.notifyDataSetChanged();
                            }
                            if ("Sunday".equals(sortedDay)) {
                                sundayBundle.putString(SCHEDULE_DAY, "Sunday");
                                sunday.setArguments(sundayBundle);
                                viewPagerAdapter.addFragment(sunday, "Sunday");
                                viewPagerAdapter.notifyDataSetChanged();
                            }
                        }

                        pageListener.onPageSuccess(viewPagerAdapter, dayStrings.size());

                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                        Intent intent = new Intent(context, FacultyAppointmentsActivity.class);
                        context.startActivity(intent);
                        ((FragmentActivity) context).overridePendingTransition(0,0);
                    }
                    catch (NullPointerException e) {
                        Intent intent = new Intent(context, FacultyAppointmentsActivity.class);
                        context.startActivity(intent);
                        ((FragmentActivity) context).overridePendingTransition(0,0);
                    }

                } else {
                    bundle.putString(SCHEDULE_DAY, dayFormat.format(new Date()));
                    FacultyAppointmentsFragment fragment = new FacultyAppointmentsFragment();
                    fragment.setArguments(bundle);

                    viewPagerAdapter.addFragment(fragment, "Appointments");
                    pageListener.onPageFailure(viewPagerAdapter, 1);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                bundle.putString(SCHEDULE_DAY, dayFormat.format(new Date()));
                FacultyAppointmentsFragment fragment = new FacultyAppointmentsFragment();
                fragment.setArguments(bundle);

                viewPagerAdapter.addFragment(fragment, "Appointments");
                pageListener.onPageFailure(viewPagerAdapter, 1);
            }
        });
    }
}

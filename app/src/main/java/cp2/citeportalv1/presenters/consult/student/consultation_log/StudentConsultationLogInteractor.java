package cp2.citeportalv1.presenters.consult.student.consultation_log;

import cp2.citeportalv1.adapters.grouplist.StudentConsultationLogGroupAdapter;

public interface StudentConsultationLogInteractor {
    void getDetails(OnApprovedResponseListener detailsListener);

    interface OnApprovedResponseListener {
        void OnResponseSuccess(StudentConsultationLogGroupAdapter adapter);
        void OnResponseFailure(String errorMessage);
    }
}

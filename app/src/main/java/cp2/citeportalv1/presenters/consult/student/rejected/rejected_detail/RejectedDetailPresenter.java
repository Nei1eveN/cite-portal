package cp2.citeportalv1.presenters.consult.student.rejected.rejected_detail;

import cp2.citeportalv1.models.ConsultationResponse;

public interface RejectedDetailPresenter {
    interface View {
        void setRejectedDetail(ConsultationResponse response);
        void setApprovedNotExist(String notExistMessage);
    }
    void onStart(String notificationId);
    void onDestroy();
}

package cp2.citeportalv1.presenters.consult.faculty.appointment.all_appointments;

import android.content.Context;

import cp2.citeportalv1.adapters.grouplist.FacultyAppointmentsGroupAdapter;

public class FacultyAppointmentsPresenterImpl implements FacultyAppointmentsPresenter, FacultyAppointmentsInteractor.LoadAppointmentsListener {
    private FacultyAppointmentsPresenterImpl.View view;
    private FacultyAppointmentsInteractor interactor;

    public FacultyAppointmentsPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyAppointmentsInteractorImpl(context);
    }

    @Override
    public void onStart(String day) {
        if (view != null) {
            view.showProgress();
        }
        interactor.getAppointmentsAccordingToDay(day, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void findAppointmentsAccordingToDay(String day) {
        if (view != null) {
            view.showProgress();
        }
        interactor.getAppointmentsAccordingToDay(day, this);
    }

    @Override
    public void onSuccessLoadingAppointment(String successMessage, FacultyAppointmentsGroupAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setAppointments(adapter);
//            view.showSnackMessage(successMessage);
        }
    }

    @Override
    public void onFailureLoadingAppointment(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyAppointment(errorMessage);
            view.showSnackMessage(errorMessage);
        }
    }
}

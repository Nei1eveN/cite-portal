package cp2.citeportalv1.presenters.consult.student.appointment.all_appointments;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.grouplist.StudentAppointmentsGroupAdapter;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.DateItem;
import cp2.citeportalv1.models.GeneralItem;
import cp2.citeportalv1.models.ListItem;
import cp2.citeportalv1.utils.Constants;

import static cp2.citeportalv1.utils.Constants.APPOINTMENTS;
import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.dayFormat;

class StudentAppointmentsInteractorImpl implements StudentAppointmentsInteractor {
    private Context context;

    StudentAppointmentsInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getAppointmentsAccordingToDay(String day, LoadAppointmentsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.child(APPOINTMENTS).orderByChild("requestedDay").equalTo(day).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<Appointments> approvedResponses = new ArrayList<>();
                    approvedResponses.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Appointments appointment = Objects.requireNonNull(snapshot.getValue(Appointments.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        approvedResponses.add(appointment);
                    }
                    LinkedHashMap<String, Set<Appointments>> groupedHashMap = Constants.groupAppointmentDataIntoHashMap(approvedResponses);
                    List<ListItem> consolidatedList = new ArrayList<>();

                    List<Date> dates = new ArrayList<>();
                    for (String date : groupedHashMap.keySet()) {
                        dates.add(new Date(Long.parseLong(date)));
                    }
                    Collections.sort(dates);

                    Log.d("datesCollection", String.valueOf(dates));

                    for (Date date : dates) {
                        Log.d("date", String.valueOf(date));
                        DateItem dateItem = new DateItem();
                        dateItem.setDate(dateFormat.format(date));
                        dateItem.setDay(dayFormat.format(date));
                        consolidatedList.add(dateItem);
                        for (Appointments schedule : Objects.requireNonNull(groupedHashMap.get(String.valueOf(date.getTime())))) {
                            GeneralItem generalItem = new GeneralItem();
                            generalItem.setAppointments(schedule);
                            consolidatedList.add(generalItem);
                        }
                    }

                    StudentAppointmentsGroupAdapter adapter = new StudentAppointmentsGroupAdapter(context, consolidatedList);
                    adapter.notifyDataSetChanged();

                    listener.onSuccessLoadingAppointment("Appointments Loaded", adapter);
                }
                else {
                    listener.onFailureLoadingAppointment("There are no Scheduled Appointments for "+day+".");
                    //\n\nClick the calendar icon above to check if there are pending requests for you."
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onFailureLoadingAppointment(databaseError.getMessage());
            }
        });
    }
}

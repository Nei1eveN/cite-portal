package cp2.citeportalv1.presenters.consult.student.loadpages.comsci;

import java.util.List;

import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.ListItem;

public interface ComSciInstructorListPresenter {
    interface ComSciView {
        void setProgress();
        void hideProgress();
        void setSnackMessage(String message);
        void setEmptyState(String emptyStateMessage);
        void setFacultyEmployees(List<ListItem> list);
    }
    void onStart();
    void onDestroy();
    void requestFacultyEmployees();
}

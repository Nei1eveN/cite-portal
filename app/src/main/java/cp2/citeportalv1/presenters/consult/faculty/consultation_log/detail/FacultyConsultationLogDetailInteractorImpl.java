package cp2.citeportalv1.presenters.consult.faculty.consultation_log.detail;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.ConsultationLog;

import static cp2.citeportalv1.utils.Constants.CONSULTATION_LOG;
import static cp2.citeportalv1.utils.Constants.FACULTY;

class FacultyConsultationLogDetailInteractorImpl implements FacultyConsultationLogDetailInteractor {
    private Context context;

    FacultyConsultationLogDetailInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getNotificationDetail(String notificationId, ConsultationLogDetailListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference notificationReference = firebaseDatabase.getReference(FACULTY)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                .child(CONSULTATION_LOG);
        notificationReference.keepSynced(true);

        notificationReference.child(notificationId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    ConsultationLog consultationLog = Objects.requireNonNull(dataSnapshot.getValue(ConsultationLog.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    if (consultationLog.getAppointmentFeedback() == null) {
                        consultationLog.setAppointmentFeedback("Note:\nNo Feedback from Faculty. Please make sure to have a feedback from the Instructor upon next Consultation.");
                    }
                    listener.getNotificationDetail(consultationLog);
                }
                else {
                    listener.getDetailError("Detail Error","Appointment Details does not exist");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.getDetailError("Database Error", databaseError.getMessage());
            }
        });
    }
}

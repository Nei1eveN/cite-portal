package cp2.citeportalv1.presenters.consult.faculty.load_pages;

import android.content.Context;

import androidx.fragment.app.FragmentActivity;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.views.faculty.FacultyConsultationLogFragment;

class FacultyConsultInteractorImpl implements FacultyConsultInteractor {
    private Context context;

    FacultyConsultInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getFragmentPages(LoadPagesListener listener) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(((FragmentActivity) context).getSupportFragmentManager());

//        viewPagerAdapter.addFragment(new FacultyConsultationRequestLogFragment(), "Consultation Requests");
        viewPagerAdapter.addFragment(new FacultyConsultationLogFragment(), "FINISHED REQUESTS");
        listener.onSuccess(viewPagerAdapter, 1);
    }
}

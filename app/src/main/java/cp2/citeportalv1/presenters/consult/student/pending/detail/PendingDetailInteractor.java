package cp2.citeportalv1.presenters.consult.student.pending.detail;

import java.text.ParseException;

import cp2.citeportalv1.models.ConsultationRequest;

public interface PendingDetailInteractor {
    void getNotificationDetail(String notificationId, DetailListener listener);
    interface DetailListener {
        void getNotificationDetail(ConsultationRequest request) throws ParseException;
        void getDetailError(String errorMessage);
    }
}

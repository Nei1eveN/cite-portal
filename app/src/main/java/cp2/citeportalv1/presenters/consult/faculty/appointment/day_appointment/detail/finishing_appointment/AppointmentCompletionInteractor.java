package cp2.citeportalv1.presenters.consult.faculty.appointment.day_appointment.detail.finishing_appointment;

import java.text.ParseException;

import cp2.citeportalv1.models.Appointments;

public interface AppointmentCompletionInteractor {
    interface AppointmentDetailListener {
        void onAppointmentDetailSuccess(Appointments appointment) throws ParseException;
        void onAppointmentError(String errorTitle, String errorMessage);
    }

    void getAppointmentDetails(String notificationId, AppointmentDetailListener listener);

    interface FinishAppointmentListener {
        void onFinishingSuccess(String title, String message);
        void onFinishingError(String errorTitle, String errorMessage);
    }

    void finishAppointment(String notificationId, String appointmentRemarks, String status, FinishAppointmentListener listener);

    interface CancelAppointmentListener {
        void onCancelSuccess(String title, String message);
        void onCancelError(String errorTitle, String errorMessage);
    }

    void cancelAppointment(String notificationId, String appointmentRemarks, String status, CancelAppointmentListener listener);
}

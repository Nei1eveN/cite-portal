package cp2.citeportalv1.presenters.consult.faculty.consultationdetails;

import android.content.Context;

import java.text.ParseException;
import java.util.Date;

import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.utils.Constants;

import static cp2.citeportalv1.utils.Constants.dateFormat;
import static cp2.citeportalv1.utils.Constants.yearDateFormat;

public class ConsultationDetailsPresenterImpl implements ConsultationDetailsPresenter,
        ConsultationDetailsInteractor.OnResponseListener, ConsultationDetailsInteractor.OnDetailsListener {
    private ConsultationDetailsPresenter.View view;
    private ConsultationDetailsInteractor interactor;

    public ConsultationDetailsPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new ConsultationDetailsInteractorImpl(context);
    }

    @Override
    public void onStart(String notificationId) {
        interactor.getConsultationDetails(notificationId, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void sendApprovedConsultation(String notificationId,
                                         String requestedDay, String requestedDate,
                                         String requestedTimeStart, String requestedTimeEnd, String venue,
                                         String consultationTitle, String consultationBody, String consultationRemarks) {
        if (consultationRemarks.isEmpty()){
            if (view != null) {
                view.showProgress();
                view.setButtonsDisabled();
            }
            try {
                Date date = Constants.dateFormat.parse(requestedDate);
                interactor.saveApprovedRequestToDatabase(
                        notificationId,
                        requestedDay, Constants.yearDateFormat.format(date), requestedTimeStart, requestedTimeEnd, venue,
                        consultationTitle, consultationBody, "none", this
                );
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        else {
            if (view != null) {
                view.showProgress();
                view.setButtonsDisabled();
            }
            try {
                Date date = Constants.dateFormat.parse(requestedDate);
                interactor.saveApprovedRequestToDatabase(
                        notificationId,
                        requestedDay, Constants.yearDateFormat.format(date), requestedTimeStart, requestedTimeEnd, venue,
                        consultationTitle, consultationBody, consultationRemarks, this
                );
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void sendRejectedConsultation(String notificationId,
                                         String requestedDay, String requestedDate,
                                         String requestedTimeStart, String requestedTimeEnd, String venue,
                                         String consultationTitle, String consultationBody, String consultationRemarks) {
        if (consultationRemarks.isEmpty()){
            view.setSnackMessage("Please tell the reason for rejecting this request");
        }
        else {
            if (view != null) {
                view.showProgress();
                view.setButtonsDisabled();
            }
            try {
                Date date = Constants.dateFormat.parse(requestedDate);
                interactor.saveIgnoredRequestToDatabase(
                        notificationId,
                        requestedDay, Constants.yearDateFormat.format(date), requestedTimeStart, requestedTimeEnd, venue,
                        consultationTitle, consultationBody, consultationRemarks, this
                );
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onResponseSuccess(String successMessage) {
        if (view != null) {
            view.hideProgress();
            view.setExitDialog(successMessage);
        }
    }

    @Override
    public void onResponseFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setSnackMessage(errorMessage);
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onDetailSuccess(ConsultationRequest request) {
        if (view != null) {
            try {
                Date date = yearDateFormat.parse(request.getRequestedDate());
                request.setRequestedDate(dateFormat.format(date));
                view.initViews(request);
                view.initSetAppointmentDialog(request);
                view.initSetRejectRequestDialog(request);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDetailFailure(String errorMessage) {
        if (view != null) {
            view.setExitDialog(errorMessage);
        }
    }
}

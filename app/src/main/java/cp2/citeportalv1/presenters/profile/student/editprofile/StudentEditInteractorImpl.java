package cp2.citeportalv1.presenters.profile.student.editprofile;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Student;

import static cp2.citeportalv1.utils.Constants.PNG;
import static cp2.citeportalv1.utils.Constants.PROFILE_IMAGES;
import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.STUDENT_USERS;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

class StudentEditInteractorImpl implements StudentEditInteractor {
    private Context context;

    StudentEditInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserDetails(UserDetailsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                    if (student.getContactNumber() == null) {
                        student.setContactNumber("(Not yet settled.)");
                    }

                    listener.onDetailsSuccess(student);
                } else {
                    listener.onDetailsFailure("Detail Failure", "Your details might not be existing. Please raise this concern by sending details and screenshot of errors to neilpotot@gmail.com");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onDetailsFailure(
                        "Database Error",
                        "Code:\n" + databaseError.getCode() +
                                "\n\nMessage:\n" + databaseError.getMessage() +
                                "\n\nDetails:\n" + databaseError.getDetails());
            }
        });
    }

    @Override
    public void getUserProgram(UserProgramListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<String> programStrings = new ArrayList<>();
                    programStrings.clear();
                    Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                    switch (student.getProgram()) {
                        case "Associate in Computer Technology":
                            programStrings.add("Associate in Computer Technology");
                            programStrings.add("Computer Science");
                            programStrings.add("Information Systems");
                            programStrings.add("Information Technology");
                            break;
                        case "Computer Science":
                            programStrings.add("Computer Science");
                            programStrings.add("Information Systems");
                            programStrings.add("Information Technology");
                            programStrings.add("Associate in Computer Technology");
                            break;
                        case "Information Systems":
                            programStrings.add("Information Systems");
                            programStrings.add("Information Technology");
                            programStrings.add("Associate in Computer Technology");
                            programStrings.add("Computer Science");
                            break;
                        default:
                            programStrings.add("Information Technology");
                            programStrings.add("Associate in Computer Technology");
                            programStrings.add("Computer Science");
                            programStrings.add("Information Systems");
                            break;
                    }
                    listener.onProgramsDetailSuccess(programStrings);

                } else {
                    listener.onProgramsDetailFailure("Department Not Existing", "Have you forgotten where your department is? Please raise this concern by sending details and screenshot of errors to neilpotot@gmail.com");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onProgramsDetailFailure(
                        "Database Error",
                        "Code:\n" + databaseError.getCode() +
                                "\n\nMessage:\n" + databaseError.getMessage() +
                                "\n\nDetails:\n" + databaseError.getDetails());
            }
        });
    }

    @Override
    public void getUserYearLevel(UserYearLevelListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<String> yearLevels = new ArrayList<>();
                    yearLevels.clear();
                    Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                    switch (student.getProgram()) {
                        case "Associate in Computer Technology":
                            switch (student.getYearLvl()) {
                                case "2nd":
                                    yearLevels.add("2nd");
                                    yearLevels.add("1st");
                                    break;
                                case "3rd":
                                    yearLevels.add("1st");
                                    yearLevels.add("2nd");
                                    break;
                                case "4th":
                                    yearLevels.add("1st");
                                    yearLevels.add("2nd");
                                    break;
                                default:
                                    yearLevels.add("1st");
                                    yearLevels.add("2nd");
                                    break;
                            }
                            break;
                        default:
                            switch (student.getYearLvl()) {
                                case "1st":
                                    yearLevels.add("1st");
                                    yearLevels.add("2nd");
                                    yearLevels.add("3rd");
                                    yearLevels.add("4th");
                                    break;
                                case "2nd":
                                    yearLevels.add("2nd");
                                    yearLevels.add("3rd");
                                    yearLevels.add("4th");
                                    yearLevels.add("1st");
                                    break;
                                case "3rd":
                                    yearLevels.add("3rd");
                                    yearLevels.add("4th");
                                    yearLevels.add("1st");
                                    yearLevels.add("2nd");
                                    break;
                                case "4th":
                                    yearLevels.add("4th");
                                    yearLevels.add("1st");
                                    yearLevels.add("2nd");
                                    yearLevels.add("3rd");
                                    break;
                            }
                            break;
                    }
                    listener.onYearLevelDetailSuccess(yearLevels);

                } else {
                    listener.onYearLevelDetailFailure(
                            "Department Not Existing",
                            "Have you forgotten where your department is? Please raise this concern by sending details and screenshot of errors to neilpotot@gmail.com");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onYearLevelDetailFailure(
                        "Database Error",
                        "Code:\n" + databaseError.getCode() +
                                "\n\nMessage:\n" + databaseError.getMessage() +
                                "\n\nDetails:\n" + databaseError.getDetails());
            }
        });
    }

    @Override
    public void getCredentials(byte[] thumbData, Uri imageURI, String email, String firstName, String middleName, String lastName, String contactNumber, String studentId, String selectedProgram, String selectedYearLevel, SaveCredentialsListener listener) {
        if (isNetworkAvailable(context)) {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            databaseReference.keepSynced(true);

            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
            StorageReference storageReference = firebaseStorage.getReference();

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    StorageReference rootReference = storageReference.child(STUDENT_USERS).child(PROFILE_IMAGES)
                            .child(student.studentUId);

                    if (student.getEmail().equals(email)) {
                        rootReference.putBytes(thumbData).addOnCompleteListener(((Activity) context), task -> {
                            if (task.isSuccessful()) {
                                rootReference.getDownloadUrl().addOnSuccessListener(((Activity) context), uri -> {
                                    Map<String, Object> childUpdates = new HashMap<>();
                                    childUpdates.put("studentImageURL", uri.toString());
                                    childUpdates.put("studentID", Long.parseLong(studentId));
                                    childUpdates.put("fName", firstName);
                                    childUpdates.put("midName", middleName);
                                    childUpdates.put("lastName", lastName);
                                    childUpdates.put("program", selectedProgram);
                                    childUpdates.put("yearLvl", selectedYearLevel);
                                    childUpdates.put("email", email);
                                    childUpdates.put("contactNumber", contactNumber);

                                    databaseReference.updateChildren(childUpdates).addOnSuccessListener(((Activity) context), aVoid
                                            -> listener.onSavingSuccess(
                                                    "Preference Saved", "Your changes has been saved. Heading back to your Profile page."))
                                            .addOnFailureListener(((Activity) context), e
                                                    -> listener.onSavingFailure(
                                                            "Saving Error", e.getMessage()));
                                });
                            } else {
                                listener.onSavingFailure("Saving Error", Objects.requireNonNull(task.getException()).getMessage());
                            }
                        });

                    } else {
                        rootReference.putBytes(thumbData).addOnCompleteListener(((Activity) context), task -> {
                            if (task.isSuccessful()) {
                                rootReference.getDownloadUrl().addOnSuccessListener(((Activity) context), uri -> {
                                    Map<String, Object> childUpdates = new HashMap<>();
                                    childUpdates.put("studentImageURL", uri.toString());
                                    childUpdates.put("studentID", Long.parseLong(studentId));
                                    childUpdates.put("fName", firstName);
                                    childUpdates.put("midName", middleName);
                                    childUpdates.put("lastName", lastName);
                                    childUpdates.put("program", selectedProgram);
                                    childUpdates.put("yearLvl", selectedYearLevel);
                                    childUpdates.put("email", email);
                                    childUpdates.put("contactNumber", contactNumber);

                                    databaseReference.updateChildren(childUpdates).addOnSuccessListener(((Activity) context), aVoid -> listener.onChangesSuccess("Preference Changed", "Your Preferences has been changed. You will be logged out for taking effect of changes immediately.")).addOnFailureListener(((Activity) context), e
                                            -> listener.onChangesFailure("Saving Error", e.getMessage()));
                                });
                            } else {
                                listener.onSavingFailure("Saving Error", Objects.requireNonNull(task.getException()).getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onSavingFailure("Database Error", databaseError.getMessage());
                }
            });


        } else {
            listener.onSavingFailure("Network Error", "Please check your internet Connection.");
        }
    }

    @Override
    public void getAuthenticationCredentials(String email, String password, byte[] thumbData, Uri imageURI, String changingEmail, String firstName, String middleName, String lastName, String contactNumber, String studentId, String selectedProgram, String selectedYearLevel, ReAuthenticationListener listener) {
        if (isNetworkAvailable(context)) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            AuthCredential credential = EmailAuthProvider.getCredential(email, password);

            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            databaseReference.keepSynced(true);

            Query emailQuery = databaseReference.orderByChild("email").equalTo(changingEmail);
            emailQuery.keepSynced(true);

            emailQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        listener.onAuthenticationFailure("Existing Email", "Your proposed email already exists.");
                    } else {
                        Objects.requireNonNull(user).reauthenticate(credential).addOnSuccessListener(((Activity) context), (Void aVoid) -> {
                            if (email.equals(changingEmail)) {
                                listener.onAuthenticationSuccess(thumbData, imageURI,
                                        email, firstName, middleName, lastName, contactNumber,
                                        studentId, selectedProgram, selectedYearLevel);
                            } else {
                                user.updateEmail(changingEmail).addOnCompleteListener(((Activity) context), task -> {
                                    if (task.isSuccessful()) {
                                        user.sendEmailVerification().addOnSuccessListener(((Activity) context), aVoid1
                                                -> listener.onAuthenticationSuccess(thumbData, imageURI,
                                                changingEmail, firstName, middleName, lastName, contactNumber,
                                                studentId, selectedProgram, selectedYearLevel)).addOnFailureListener(((Activity) context), e
                                                -> listener.onAuthenticationFailure("Email Verification Error", e.getMessage()));
                                    } else {
                                        listener.onAuthenticationFailure("Saving Error", Objects.requireNonNull(task.getException()).getMessage());
                                    }
                                });
                            }
                        }).addOnFailureListener(((Activity) context),
                                e -> listener.onAuthenticationFailure("Error", e.getMessage()));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onAuthenticationFailure("Database Error", "Code:\n"+databaseError.getCode()+"\n\nMessage:\n"+databaseError.getMessage()+"\n\nDetails:\n"+databaseError.getDetails());
                }
            });

        } else {
            listener.onAuthenticationFailure("Network Error", "Please check your internet connection");
        }
    }

    @Override
    public void getTextCredentials(String email,
                                   String firstName, String middleName, String lastName, String contactNumber,
                                   String studentId, String selectedProgram, String selectedYearLevel,
                                   SaveTextCredentialsListener listener) {
        if (isNetworkAvailable(context)) {

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("studentID", Long.parseLong(studentId));
            childUpdates.put("fName", firstName);
            childUpdates.put("midName", middleName);
            childUpdates.put("lastName", lastName);
            childUpdates.put("program", selectedProgram);
            childUpdates.put("yearLvl", selectedYearLevel);
            childUpdates.put("email", email);
            childUpdates.put("contactNumber", contactNumber);

            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            databaseReference.keepSynced(true);

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Student faculty = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    if (faculty.getEmail().equals(email)) {
                        databaseReference.updateChildren(childUpdates).addOnSuccessListener(((Activity) context), aVoid ->
                                listener.onSavingTextSuccess(
                                        "User Preferences Updated",
                                        "Your Preferences has been updated"))
                                .addOnFailureListener(((Activity) context), e
                                        -> listener.onSavingTextFailure(
                                        "Saving Error", e.getMessage()));
                    } else {
                        databaseReference.updateChildren(childUpdates).addOnSuccessListener(((Activity) context), aVoid
                                -> listener.onChangesSuccess("User Preferences Updated",
                                "Your Preferences has been updated")).addOnFailureListener(((Activity) context), e
                                -> listener.onChangesFailure("Saving Error", e.getMessage()));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onSavingTextFailure("Database Error", databaseError.getMessage());
                }
            });
        } else {
            listener.onSavingTextFailure("Network Error", "Please check your internet connection.");
        }
    }

    @Override
    public void signOut(SignOutListener listener) {
        if (isNetworkAvailable(context)) {
            FirebaseDatabase
                    .getInstance().getReference(STUDENTS)
                    .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                    .child("tokenId").removeValue();

            FirebaseAuth.getInstance().signOut();
            listener.onSignOutSuccess(
                    "User Logged Out",
                    "You have been logged out for effective implementation of your changes. Please press the button to go back to the Login Page.");
        } else {
            listener.onSignOutFailure(
                    "User Logged Out Failure",
                    "You are attempting to log out without Internet Connectivity. Please check your internet connection.");
        }
    }
}

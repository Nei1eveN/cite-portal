package cp2.citeportalv1.presenters.profile.faculty.editprofile;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Faculty;

import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.FACULTY_USERS;
import static cp2.citeportalv1.utils.Constants.PNG;
import static cp2.citeportalv1.utils.Constants.PROFILE_IMAGES;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

class FacultyEditInteractorImpl implements FacultyEditInteractor {
    private Context context;

    FacultyEditInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserDetails(UserDetailsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                    if (faculty.getContactNumber() == null) {
                        faculty.setContactNumber("(Not yet settled.)");
                    }

                    listener.onDetailsSuccess(faculty);
                } else {
                    listener.onDetailsFailure("Detail Failure", "Your details might not be existing. Please raise this concern by sending details and screenshot of errors to neilpotot@gmail.com");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onDetailsFailure(
                        "Database Error",
                        "Code:\n" + databaseError.getCode() +
                                "\n\nMessage:\n" + databaseError.getMessage() +
                                "\n\nDetails:\n" + databaseError.getDetails());
            }
        });
    }

    @Override
    public void getUserDepartment(UserDepartmentListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<String> programStrings = new ArrayList<>();
                    Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                    switch (faculty.getDepartment()) {
                        case "Computer Science":
                            programStrings.add("Computer Science");
                            programStrings.add("Information Systems");
                            programStrings.add("Information Technology");
                            break;
                        case "Information Systems":
                            programStrings.add("Information Systems");
                            programStrings.add("Information Technology");
                            programStrings.add("Computer Science");
                            break;
                        default:
                            programStrings.add("Information Technology");
                            programStrings.add("Computer Science");
                            programStrings.add("Information Systems");
                            break;
                    }
                    listener.onDeptDetailSuccess(programStrings);

                } else {
                    listener.onDeptDetailFailure("Department Not Existing", "Have you forgotten where your department is? Please raise this concern by sending details and screenshot of errors to neilpotot@gmail.com");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onDeptDetailFailure(
                        "Database Error",
                        "Code:\n" + databaseError.getCode() +
                                "\n\nMessage:\n" + databaseError.getMessage() +
                                "\n\nDetails:\n" + databaseError.getDetails());
            }
        });
    }

    @Override
    public void getCredentials(byte[] thumbData, Uri ImageURI, String email, String firstName, String middleName, String lastName, String contactNumber, String employeeID, String selectedDepartment, SaveCredentialsListener listener) {
        if (isNetworkAvailable(context)) {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            databaseReference.keepSynced(true);

            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
            StorageReference storageReference = firebaseStorage.getReference();

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    StorageReference rootReference = storageReference.child(FACULTY_USERS).child(PROFILE_IMAGES)
                            .child(faculty.facultyId);

                    if (faculty.getEmail().equals(email)) {
                        rootReference.putBytes(thumbData).addOnCompleteListener(((Activity) context), task -> {
                            if (task.isSuccessful()) {
                                rootReference.getDownloadUrl().addOnSuccessListener(((Activity) context), uri -> {
                                    Map<String, Object> childUpdates = new HashMap<>();
                                    childUpdates.put("contactNumber", contactNumber);
                                    childUpdates.put("department", selectedDepartment);
                                    childUpdates.put("email", email);
                                    childUpdates.put("employeeID", employeeID);
                                    childUpdates.put("fName", firstName);
                                    childUpdates.put("facultyImageURL", uri.toString());
                                    childUpdates.put("lastName", lastName);
                                    childUpdates.put("midName", middleName);
                                    databaseReference.updateChildren(childUpdates).addOnSuccessListener(((Activity) context), aVoid -> listener.onSavingSuccess("Preference Saved", "Your changes has been saved. Heading back to your Profile page.")).addOnFailureListener(((Activity) context), e -> listener.onSavingFailure("Saving Error", e.getMessage()));
                                });
                            } else {
                                listener.onSavingFailure("Saving Error", Objects.requireNonNull(task.getException()).getMessage());
                            }
                        });

                    } else {

                        rootReference.putBytes(thumbData).addOnCompleteListener(((Activity) context), task -> {
                            if (task.isSuccessful()) {
                                rootReference.getDownloadUrl().addOnSuccessListener(((Activity) context), uri -> {
                                    Map<String, Object> childUpdates = new HashMap<>();
                                    childUpdates.put("contactNumber", contactNumber);
                                    childUpdates.put("department", selectedDepartment);
                                    childUpdates.put("email", email);
                                    childUpdates.put("employeeID", employeeID);
                                    childUpdates.put("fName", firstName);
                                    childUpdates.put("facultyImageURL", uri.toString());
                                    childUpdates.put("lastName", lastName);
                                    childUpdates.put("midName", middleName);
                                    databaseReference.updateChildren(childUpdates).addOnSuccessListener(((Activity) context), aVoid -> listener.onChangesSuccess("Preference Changed", "Your Preferences has been changed. You will be logged out for taking effect of changes immediately.")).addOnFailureListener(((Activity) context), e
                                            -> listener.onChangesFailure("Saving Error", e.getMessage()));
                                });
                            } else {
                                listener.onSavingFailure("Saving Error", Objects.requireNonNull(task.getException()).getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onSavingFailure("Database Error", databaseError.getMessage());
                }
            });


        } else {
            listener.onSavingFailure("Network Error", "Please check your internet Connection.");
        }
    }

    @Override
    public void getAuthenticationCredentials(String email, String password, byte[] thumbData, Uri ImageURI, String changingEmail, String firstName, String middleName, String lastName, String contactNumber, String employeeID, String selectedDepartment, ReAuthenticationListener listener) {
        if (isNetworkAvailable(context)) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            AuthCredential credential = EmailAuthProvider.getCredential(email, password);

            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            databaseReference.keepSynced(true);

            Query emailQuery = databaseReference.orderByChild("email").equalTo(changingEmail);
            emailQuery.keepSynced(true);

            emailQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        listener.onAuthenticationFailure("Existing Email", "Your proposed email already exists.");
                    } else {
                        Objects.requireNonNull(user).reauthenticate(credential).addOnSuccessListener(((Activity) context), (Void aVoid) -> {
                            if (email.equals(changingEmail)) {
                                listener.onAuthenticationSuccess(thumbData, ImageURI,
                                        email, firstName, middleName, lastName, contactNumber,
                                        employeeID, selectedDepartment);
                            } else {
                                user.updateEmail(changingEmail).addOnCompleteListener(((Activity) context), task -> {
                                    if (task.isSuccessful()) {
                                        user.sendEmailVerification().addOnSuccessListener(((Activity) context), aVoid1
                                                -> listener.onAuthenticationSuccess(thumbData, ImageURI,
                                                changingEmail, firstName, middleName, lastName, contactNumber,
                                                employeeID, selectedDepartment)).addOnFailureListener(((Activity) context), e
                                                -> listener.onAuthenticationFailure("Email Verification Error", e.getMessage()));
                                    } else {
                                        listener.onAuthenticationFailure("Saving Error", Objects.requireNonNull(task.getException()).getMessage());
                                    }
                                });
                            }
                        }).addOnFailureListener(((Activity) context),
                                e -> listener.onAuthenticationFailure("Error", e.getMessage()));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } else {
            listener.onAuthenticationFailure("Network Error", "Please check your internet connection");
        }
    }

    @Override
    public void getTextCredentials(String email, String firstName, String middleName, String lastName, String contactNumber,
                                   String employeeID, String selectedDepartment, SaveTextCredentialsListener listener) {

        if (isNetworkAvailable(context)) {

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("contactNumber", contactNumber);
            childUpdates.put("department", selectedDepartment);
            childUpdates.put("email", email);
            childUpdates.put("employeeID", employeeID);
            childUpdates.put("fName", firstName);
            childUpdates.put("lastName", lastName);
            childUpdates.put("midName", middleName);

            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            databaseReference.keepSynced(true);

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    if (faculty.getEmail().equals(email)) {
                        databaseReference.updateChildren(childUpdates).addOnSuccessListener(((Activity) context), aVoid ->
                                listener.onSavingTextSuccess(
                                        "User Preferences Updated",
                                        "Your Preferences has been updated"))
                                .addOnFailureListener(((Activity) context), e
                                        -> listener.onSavingTextFailure(
                                        "Saving Error", e.getMessage()));
                    } else {
                        databaseReference.updateChildren(childUpdates).addOnSuccessListener(((Activity) context), aVoid
                                -> listener.onChangesSuccess("User Preferences Updated",
                                "Your Preferences has been updated")).addOnFailureListener(((Activity) context), e
                                -> listener.onChangesFailure("Saving Error", e.getMessage()));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onSavingTextFailure("Database Error", databaseError.getMessage());
                }
            });
        } else {
            listener.onSavingTextFailure("Network Error", "Please check your internet connection.");
        }


    }

    @Override
    public void signOut(SignOutListener listener) {
        if (isNetworkAvailable(context)) {
            FirebaseDatabase
                    .getInstance().getReference(FACULTY)
                    .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                    .child("tokenId").removeValue();

            FirebaseAuth.getInstance().signOut();
            listener.onSignOutSuccess(
                    "User Logged Out",
                    "You have been logged out for effective implementation of your changes. Please press the button to go back to the Login Page.");
        } else {
            listener.onSignOutFailure(
                    "User Logged Out Failure",
                    "You are attempting to log out without Internet Connectivity. Please check your internet connection.");
        }
    }
}

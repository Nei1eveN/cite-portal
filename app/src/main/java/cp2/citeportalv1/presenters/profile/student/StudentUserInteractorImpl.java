package cp2.citeportalv1.presenters.profile.student;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.views.student.appointment.StudentDayAppointmentFragment;
import cp2.citeportalv1.views.student.classmate.ClassmateFragment;
import cp2.citeportalv1.views.student.profile.StudentUserDetailFragment;

import static cp2.citeportalv1.utils.Constants.STUDENTS;

class StudentUserInteractorImpl implements StudentUserInteractor {
    private Context context;

    StudentUserInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getStudentUserDetails(StudentUserListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    listener.onStudentUserSuccess(student);
                }
                else {
                    listener.onStudentUserFailure("Student Detail Error","Student detail not found.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onStudentUserFailure(
                        "Database Error",
                        "Code:\n"+databaseError.getCode()+
                                "\n\nMessage:\n"+databaseError.getMessage()+
                                "\n\nDetails:\n"+databaseError.getDetails());
            }
        });
    }

    @Override
    public void getPages(StudentPagesListener listener) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(((FragmentActivity) context).getSupportFragmentManager());
        viewPagerAdapter.addFragment(new StudentUserDetailFragment(), "User Info");
        viewPagerAdapter.addFragment(new ClassmateFragment(), "Schoolmates");
        viewPagerAdapter.addFragment(new StudentDayAppointmentFragment(), "Appointments");

        listener.onPagesSuccess(viewPagerAdapter, 3);
    }
}

package cp2.citeportalv1.presenters.profile.student.signature.edit_signature;

public interface StudentUserEditSignatureInteractor {
    interface UserEditSignatureListener {
        void onEditSuccess(String title, String message);
        void onEditError(String errorTitle, String errorMessage);
    }
    void getUserSignature(byte[] thumbData, UserEditSignatureListener listener);
}

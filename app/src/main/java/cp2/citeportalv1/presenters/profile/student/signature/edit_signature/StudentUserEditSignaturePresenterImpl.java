package cp2.citeportalv1.presenters.profile.student.signature.edit_signature;

import android.content.Context;
import android.graphics.Bitmap;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.ByteArrayOutputStream;

public class StudentUserEditSignaturePresenterImpl implements StudentUserEditSignaturePresenter, StudentUserEditSignatureInteractor.UserEditSignatureListener {
    private StudentUserEditSignaturePresenter.View view;
    private StudentUserEditSignatureInteractor interactor;

    public StudentUserEditSignaturePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentUserEditSignatureInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.initViews();
        }
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void submitUserSignature(SignaturePad signaturePad) {
        if (signaturePad.isEmpty()) {
            view.showErrorMessage("Empty Signature", "You cannot upload an empty signature.");
            view.showSnackMessage("Please write your signature.");
        } else {
            Bitmap bitmap = signaturePad.getTransparentSignatureBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, baos);
            byte[] data = baos.toByteArray();
            if (view != null) {
                view.showProgress("Saving Your Signature", "Please wait...");
            }
            interactor.getUserSignature(data, this);
        }
    }

    @Override
    public void onEditSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showSnackMessage(message);
            view.showExitMessage(title, message);
        }
    }

    @Override
    public void onEditError(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorMessage(errorTitle, errorMessage);
        }
    }
}

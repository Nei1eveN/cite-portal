package cp2.citeportalv1.presenters.profile.faculty;

import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Faculty;

public interface FacultyUserPresenter {
    interface View {
        void setPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
        void setUserInfo(Faculty faculty);
        void setExitDialog(String title, String message);
    }
    void onStart();
    void onDestroy();
    void requestUserInfo();
    void requestPages();
}

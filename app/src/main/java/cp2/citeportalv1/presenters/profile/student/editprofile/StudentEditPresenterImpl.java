package cp2.citeportalv1.presenters.profile.student.editprofile;

import android.content.Context;
import android.net.Uri;

import java.util.List;

import cp2.citeportalv1.models.Student;

import static cp2.citeportalv1.utils.Constants.digitCasePattern;

public class StudentEditPresenterImpl implements StudentEditPresenter, StudentEditInteractor.UserDetailsListener,
        StudentEditInteractor.UserProgramListener, StudentEditInteractor.UserYearLevelListener,
        StudentEditInteractor.ReAuthenticationListener, StudentEditInteractor.SaveCredentialsListener,
        StudentEditInteractor.SignOutListener, StudentEditInteractor.SaveTextCredentialsListener {

    private StudentEditPresenter.View view;
    private StudentEditInteractor interactor;

    public StudentEditPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentEditInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getUserDetails(this);
        interactor.getUserProgram(this);
        interactor.getUserYearLevel(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestAuthentication(String email, String password, byte[] thumbData, Uri imageURI, String changingEmail, String firstName, String middleName, String lastName, String contactNumber, String studentId, String selectedProgram, String selectedYearLevel) {
        if (email.isEmpty()) {
            view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
        } else if (password.isEmpty()) {
            view.showErrorDialog("Password Field Empty", "Please fill in the required field");
        } else if (thumbData == null) {
            if (view != null) {
                view.showProgress("Authentication Credentials", "Submitting Credentials. Please wait...");
            }
            interactor.getAuthenticationCredentials(email, password, null, imageURI, changingEmail, firstName, middleName, lastName, contactNumber, studentId, selectedProgram, selectedYearLevel, this);
        } else if (imageURI == null) {
            if (view != null) {
                view.showProgress("Authentication Credentials", "Submitting Credentials. Please wait...");
            }
            interactor.getAuthenticationCredentials(email, password, thumbData, null, changingEmail, firstName, middleName, lastName, contactNumber, studentId, selectedProgram, selectedYearLevel, this);
        } else if (changingEmail.isEmpty()) {
            view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
        } else if (firstName.isEmpty()) {
            view.showErrorDialog("First Name Field Empty", "Please fill in the required field.");
        } else if (middleName.isEmpty()) {
            view.showErrorDialog("Middle Name Field Empty", "Please fill in the required field.\n\nIf you do not have a Middle Name, just type (none)");
        } else if (lastName.isEmpty()) {
            view.showErrorDialog("Last Name Field Empty", "Please fill in the required field.");
        } else if (digitCasePattern.matcher(firstName).find()) {
            view.showErrorDialog("First Name Field Error", "First Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(middleName).find()) {
            view.showErrorDialog("Middle Name Field Error", "Middle Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(lastName).find()) {
            view.showErrorDialog("Last Name Field Error", "Last Name Field must not have numbers.");
        }
        else if (contactNumber.isEmpty()) {
            if (view != null) {
                view.showProgress("Authentication Credentials", "Submitting Credentials. Please wait...");
            }
            interactor.getAuthenticationCredentials(email, password, thumbData, imageURI, changingEmail, firstName, middleName, lastName, "(Not yet settled)", studentId, selectedProgram, selectedYearLevel, this);
        } else if (studentId.isEmpty()) {
            view.showErrorDialog("Student ID Field Empty", "Please fill in the required field.");
        } else if (selectedProgram.isEmpty()) {
            view.showErrorDialog("Program Field Empty", "Please fill in the required field.");
        } else if (selectedYearLevel.isEmpty()) {
            view.showErrorDialog("Year Level Field Empty", "Please fill in the required field.");
        }else {
            if (view != null) {
                view.showProgress("Authentication Credentials", "Submitting Credentials. Please wait...");
            }
            interactor.getAuthenticationCredentials(email, password, thumbData, imageURI, changingEmail, firstName, middleName, lastName, contactNumber, studentId, selectedProgram, selectedYearLevel, this);
        }
    }

    @Override
    public void submitCredentials(byte[] thumbData, Uri imageURI, String email, String firstName, String middleName, String lastName, String contactNumber, String studentId, String selectedProgram, String selectedYearLevel) {
        if (view != null) {
            if (thumbData == null) {
                view.setUserReAuthenticate();
            } else if (imageURI == null) {
                view.setUserReAuthenticate();
            } else if (email.isEmpty()) {
                view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
            } else if (firstName.isEmpty()) {
                view.showErrorDialog("First Name Field Empty", "Please fill in the required field.");
            } else if (middleName.isEmpty()) {
                view.showErrorDialog("Middle Name Field Empty", "Please fill in the required field.\n\nIf you don't have Middle Name, just type '(none)'");
            } else if (lastName.isEmpty()) {
                view.showErrorDialog("Last Name Field Empty", "Please fill in the required field.");
            } else if (digitCasePattern.matcher(firstName).find()) {
                view.showErrorDialog("First Name Field Error", "First Name Field must not have numbers.");
            } else if (digitCasePattern.matcher(middleName).find()) {
                view.showErrorDialog("Middle Name Field Error", "Middle Name Field must not have numbers.");
            } else if (digitCasePattern.matcher(lastName).find()) {
                view.showErrorDialog("Last Name Field Error", "Last Name Field must not have numbers.");
            }
            else if (contactNumber.isEmpty()) {
                view.showErrorDialog("Contact Field Empty", "Please fill in the required field.");
            } else if (studentId.isEmpty()) {
                view.showErrorDialog("Student ID Field Empty", "Please fill in the required field.");
            } else if (selectedProgram.isEmpty()) {
                view.showErrorDialog("Selecting Program Field Empty", "Please fill in the required field.");
            } else if (selectedYearLevel.isEmpty()) {
                view.showErrorDialog("Selecting Year Level Field Empty", "Please fill in the required field.");
            }
            else {
                view.setUserReAuthenticate();
            }
        }
    }

    @Override
    public void submitCredentialsWithoutFiles(String email, String firstName, String middleName, String lastName, String contactNumber, String studentId, String selectedProgram, String selectedYearLevel) {
        if (email.isEmpty()) {
            view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
        } else if (firstName.isEmpty()) {
            view.showErrorDialog("First Name Field Empty", "Please fill in the required field.");
        } else if (middleName.isEmpty()) {
            view.showErrorDialog("Middle Name Field Empty", "Please fill in the required field.\n\nIf you don't have Middle Name, just type '(none)'");
        } else if (lastName.isEmpty()) {
            view.showErrorDialog("Last Name Field Empty", "Please fill in the required field.");
        } else if (digitCasePattern.matcher(firstName).find()) {
            view.showErrorDialog("First Name Field Error", "First Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(middleName).find()) {
            view.showErrorDialog("Middle Name Field Error", "Middle Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(lastName).find()) {
            view.showErrorDialog("Last Name Field Error", "Last Name Field must not have numbers.");
        }
        else if (contactNumber.isEmpty()) {
            view.showErrorDialog("Contact Field Empty", "Please fill in the required field.");
        } else if (studentId.isEmpty()) {
            view.showErrorDialog("Student ID Field Empty", "Please fill in the required field.");
        } else if (selectedProgram.isEmpty()) {
            view.showErrorDialog("Selecting Department Field Empty", "Please fill in the required field.");
        } else if (selectedYearLevel.isEmpty()) {
            view.showErrorDialog("Selecting Year Level Field Empty", "Please fill in the required field.");
        }
        else {
            view.setUserReAuthenticate();
        }
    }

    @Override
    public void signOutUser() {
        if (view != null) {
            view.showProgress("Signing Out", "User Signing Out. Please wait...");
        }
        interactor.signOut(this);
    }

    @Override
    public void onSignOutSuccess(String signOutTitle, String signOutMessage) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(signOutTitle, signOutMessage);
        }
    }

    @Override
    public void onSignOutFailure(String signOutTitle, String signOutMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(signOutTitle, signOutMessage);
        }
    }

    @Override
    public void onSavingSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showDialog(title, message);
        }
    }

    @Override
    public void onSavingFailure(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(title, message);
        }
    }

    @Override
    public void onSavingTextSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showDialog(title, message);
        }
    }

    @Override
    public void onSavingTextFailure(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(title, message);
        }
    }

    @Override
    public void onChangesSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.changedEmailDialog(title, message);
        }
    }

    @Override
    public void onChangesFailure(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.notChangedEmailDialog(title, message);
        }
    }

    @Override
    public void onAuthenticationSuccess(byte[] thumbData, Uri imageURI, String email, String firstName, String middleName, String lastName, String contactNumber, String studentId, String selectedProgram, String selectedYearLevel) {
        if (view != null) {
            view.hideProgress();
            if (thumbData == null) {
                view.showProgress("Saving Changes", "Saving User Preferences. Please wait...");
                interactor.getTextCredentials(email, firstName, middleName, lastName, contactNumber, studentId, selectedProgram, selectedYearLevel, this);
            } else if (imageURI == null) {
                view.showProgress("Saving Changes", "Saving User Preferences. Please wait...");
                interactor.getTextCredentials(email, firstName, middleName, lastName, contactNumber, studentId, selectedProgram, selectedYearLevel, this);
            } else if (email.isEmpty()) {
                view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
            } else if (firstName.isEmpty()) {
                view.showErrorDialog("First Name Field Empty", "Please fill in the required field.");
            } else if (middleName.isEmpty()) {
                view.showErrorDialog("Middle Name Field Empty", "Please fill in the required field.\n\nIf you don't have Middle Name, just type '(none)'");
            } else if (lastName.isEmpty()) {
                view.showErrorDialog("Last Name Field Empty", "Please fill in the required field.");
            } else if (digitCasePattern.matcher(firstName).find()) {
                view.showErrorDialog("First Name Field Error", "First Name Field must not have numbers.");
            } else if (digitCasePattern.matcher(middleName).find()) {
                view.showErrorDialog("Middle Name Field Error", "Middle Name Field must not have numbers.");
            } else if (digitCasePattern.matcher(lastName).find()) {
                view.showErrorDialog("Last Name Field Error", "Last Name Field must not have numbers.");
            }
            else if (contactNumber.isEmpty()) {
                view.showErrorDialog("Contact Field Empty", "Please fill in the required field.");
            } else if (studentId.isEmpty()) {
                view.showErrorDialog("Student ID Field Empty", "Please fill in the required field.");
            } else if (selectedProgram.isEmpty()) {
                view.showErrorDialog("Selecting Program Field Empty", "Please fill in the required field.");
            } else if (selectedYearLevel.isEmpty()) {
                view.showErrorDialog("Selecting Year Level Field Empty", "Please fill in the required field.");
            }
            else {
                view.showProgress("Saving Preferences", "Saving User Preferences. Please wait...");
                interactor.getCredentials(thumbData, imageURI, email, firstName, middleName, lastName, contactNumber, studentId, selectedProgram, selectedYearLevel, this);
            }
        }
    }

    @Override
    public void onAuthenticationFailure(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(title, message);
        }
    }

    @Override
    public void onDetailsSuccess(Student student) {
        if (view != null) {
            view.setUserInfo(student);
        }
    }

    @Override
    public void onDetailsFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onProgramsDetailSuccess(List<String> programs) {
        if (view != null) {
            view.setSpinnerForPrograms(programs);
        }
    }

    @Override
    public void onProgramsDetailFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onYearLevelDetailSuccess(List<String> yearLevel) {
        if (view != null) {
            view.setSpinnerForYearLevel(yearLevel);
        }
    }

    @Override
    public void onYearLevelDetailFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }
}

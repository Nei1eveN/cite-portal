package cp2.citeportalv1.presenters.profile.student.signature;

public interface StudentUserSignaturePresenter {
    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void setUserSignature(String userSignature);

        void setNoSignature(String emptySignature);
    }
    void onStart();
    void onDestroy();
}

package cp2.citeportalv1.presenters.profile.faculty.editprofile;

import android.content.Context;
import android.net.Uri;

import java.util.List;

import cp2.citeportalv1.models.Faculty;

import static cp2.citeportalv1.utils.Constants.digitCasePattern;

public class FacultyEditPresenterImpl implements FacultyEditPresenter,
        FacultyEditInteractor.UserDetailsListener, FacultyEditInteractor.UserDepartmentListener,
        FacultyEditInteractor.ReAuthenticationListener, FacultyEditInteractor.SaveCredentialsListener,
        FacultyEditInteractor.SignOutListener, FacultyEditInteractor.SaveTextCredentialsListener {
    private FacultyEditPresenter.View view;
    private FacultyEditInteractor interactor;

    public FacultyEditPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyEditInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getUserDetails(this);
        interactor.getUserDepartment(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestAuthentication(String email, String password,
                                      byte[] thumbData, Uri ImageURI, String changingEmail,
                                      String firstName, String middleName, String lastName, String contactNumber,
                                      String employeeID, String selectedDepartment) {
        if (email.isEmpty()) {
            view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
        } else if (password.isEmpty()) {
            view.showErrorDialog("Password Field Empty", "Please fill in the required field");
        } else if (thumbData == null) {
            if (view != null) {
                view.showProgress("Authentication Credentials", "Submitting Credentials. Please wait...");
            }
            interactor.getAuthenticationCredentials(email, password, null, ImageURI, changingEmail, firstName, middleName, lastName, contactNumber, employeeID, selectedDepartment, this);
        } else if (ImageURI == null) {
            if (view != null) {
                view.showProgress("Authentication Credentials", "Submitting Credentials. Please wait...");
            }
            interactor.getAuthenticationCredentials(email, password, thumbData, null, changingEmail, firstName, middleName, lastName, contactNumber, employeeID, selectedDepartment, this);
        } else if (changingEmail.isEmpty()) {
            view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
        } else if (firstName.isEmpty()) {
            view.showErrorDialog("First Name Field Empty", "Please fill in the required field.");
        } else if (middleName.isEmpty()) {
            view.showErrorDialog("Middle Name Field Empty", "Please fill in the required field.\n\nIf you do not have a Middle Name, just type (none)");
        } else if (lastName.isEmpty()) {
            view.showErrorDialog("Last Name Field Empty", "Please fill in the required field.");
        } else if (digitCasePattern.matcher(firstName).find()) {
            view.showErrorDialog("First Name Field Error", "First Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(middleName).find()) {
            view.showErrorDialog("Middle Name Field Error", "Middle Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(lastName).find()) {
            view.showErrorDialog("Last Name Field Error", "Last Name Field must not have numbers.");
        }
        else if (contactNumber.isEmpty()) {
            if (view != null) {
                view.showProgress("Authentication Credentials", "Submitting Credentials. Please wait...");
            }
            interactor.getAuthenticationCredentials(email, password, thumbData, ImageURI, changingEmail, firstName, middleName, lastName, "(Not yet settled)", employeeID, selectedDepartment, this);
        } else if (employeeID.isEmpty()) {
            view.showErrorDialog("Employee ID Field Empty", "Please fill in the required field.");
        } else if (!employeeID.contains("Q-")) {
            view.showErrorDialog("Employee ID Error", "Employee ID must contain Q- before the number");
        } else if (!employeeID.startsWith("Q-")) {
            view.showErrorDialog("Employee ID Error", "Employee ID must start with Q- before the number");
        }
        else if (selectedDepartment.isEmpty()) {
            view.showErrorDialog("Department Field Empty", "Please fill in the required field.");
        } else {
            if (view != null) {
                view.showProgress("Authentication Credentials", "Updating "+employeeID+"\n\nPlease wait...");
            }
            interactor.getAuthenticationCredentials(email, password, thumbData, ImageURI, changingEmail, firstName, middleName, lastName, contactNumber, employeeID, selectedDepartment, this);
        }
    }

    @Override
    public void submitCredentials(byte[] thumbData, Uri ImageURI, String email, String firstName, String middleName, String lastName, String contactNumber, String employeeID, String selectedDepartment) {
        if (view != null) {
            if (thumbData == null) {
                view.setUserReAuthenticate();
            } else if (ImageURI == null) {
                view.setUserReAuthenticate();
            } else if (email.isEmpty()) {
                view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
            } else if (firstName.isEmpty()) {
                view.showErrorDialog("First Name Field Empty", "Please fill in the required field.");
            } else if (middleName.isEmpty()) {
                view.showErrorDialog("Middle Name Field Empty", "Please fill in the required field.\n\nIf you don't have Middle Name, just type '(none)'");
            } else if (lastName.isEmpty()) {
                view.showErrorDialog("Last Name Field Empty", "Please fill in the required field.");
            } else if (digitCasePattern.matcher(firstName).find()) {
                view.showErrorDialog("First Name Field Error", "First Name Field must not have numbers.");
            } else if (digitCasePattern.matcher(middleName).find()) {
                view.showErrorDialog("Middle Name Field Error", "Middle Name Field must not have numbers.");
            } else if (digitCasePattern.matcher(lastName).find()) {
                view.showErrorDialog("Last Name Field Error", "Last Name Field must not have numbers.");
            }
            else if (contactNumber.isEmpty()) {
                view.showErrorDialog("Contact Field Empty", "Please fill in the required field.");
            } else if (employeeID.isEmpty()) {
                view.showErrorDialog("Employee ID Field Empty", "Please fill in the required field.");
            } else if (!employeeID.contains("Q-")) {
                view.showErrorDialog("Employee ID Error", "Employee ID must contain Q- before the number");
            } else if (!employeeID.startsWith("Q-")) {
                view.showErrorDialog("Employee ID Error", "Employee ID must start with Q- before the number");
            } else if (selectedDepartment.isEmpty()) {
                view.showErrorDialog("Selecting Department Field Empty", "Please fill in the required field.");
            } else {
                view.setUserReAuthenticate();
            }
        }
    }

    @Override
    public void submitCredentialsWithoutFiles(String email, String firstName, String middleName, String lastName, String contactNumber, String employeeID, String selectedDepartment) {
        if (email.isEmpty()) {
            view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
        } else if (firstName.isEmpty()) {
            view.showErrorDialog("First Name Field Empty", "Please fill in the required field.");
        } else if (middleName.isEmpty()) {
            view.showErrorDialog("Middle Name Field Empty", "Please fill in the required field.\n\nIf you don't have Middle Name, just type '(none)'");
        } else if (lastName.isEmpty()) {
            view.showErrorDialog("Last Name Field Empty", "Please fill in the required field.");
        } else if (digitCasePattern.matcher(firstName).find()) {
            view.showErrorDialog("First Name Field Error", "First Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(middleName).find()) {
            view.showErrorDialog("Middle Name Field Error", "Middle Name Field must not have numbers.");
        } else if (digitCasePattern.matcher(lastName).find()) {
            view.showErrorDialog("Last Name Field Error", "Last Name Field must not have numbers.");
        }
        else if (contactNumber.isEmpty()) {
            view.showErrorDialog("Contact Field Empty", "Please fill in the required field.");
        } else if (employeeID.isEmpty()) {
            view.showErrorDialog("Employee ID Field Empty", "Please fill in the required field.");
        } else if (!employeeID.contains("Q-")) {
            view.showErrorDialog("Employee ID Error", "Employee ID must contain Q- before the number");
        } else if (!employeeID.startsWith("Q-")) {
            view.showErrorDialog("Employee ID Error", "Employee ID must start with Q- before the number");
        } else if (selectedDepartment.isEmpty()) {
            view.showErrorDialog("Selecting Department Field Empty", "Please fill in the required field.");
        } else {
            view.setUserReAuthenticate();
        }
    }

    @Override
    public void signOutUser() {
        if (view != null) {
            view.showProgress("Signing Out", "User Signing Out. Please wait...");
        }
        interactor.signOut(this);
    }

    @Override
    public void onDetailsSuccess(Faculty faculty) {
        if (view != null) {
            view.setUserInfo(faculty);
        }
    }

    @Override
    public void onDetailsFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onDeptDetailSuccess(List<String> programs) {
        if (view != null) {
            view.setSpinnerItems(programs);
        }
    }

    @Override
    public void onDeptDetailFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onAuthenticationSuccess(byte[] thumbData, Uri ImageURI,
                                        String email,
                                        String firstName, String middleName, String lastName, String contactNumber,
                                        String employeeID, String selectedDepartment) {
        if (view != null) {
            view.hideProgress();
            if (thumbData == null) {
                view.showProgress("Saving Changes", "Saving User Preferences. Please wait...");
                interactor.getTextCredentials(email, firstName, middleName, lastName, contactNumber, employeeID, selectedDepartment, this);
            } else if (ImageURI == null) {
                view.showProgress("Saving Changes", "Saving User Preferences. Please wait...");
                interactor.getTextCredentials(email, firstName, middleName, lastName, contactNumber, employeeID, selectedDepartment, this);
            } else if (email.isEmpty()) {
                view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
            } else if (firstName.isEmpty()) {
                view.showErrorDialog("First Name Field Empty", "Please fill in the required field.");
            } else if (middleName.isEmpty()) {
                view.showErrorDialog("Middle Name Field Empty", "Please fill in the required field.\n\nIf you don't have Middle Name, just type '(none)'");
            } else if (lastName.isEmpty()) {
                view.showErrorDialog("Last Name Field Empty", "Please fill in the required field.");
            } else if (digitCasePattern.matcher(firstName).find()) {
                view.showErrorDialog("First Name Field Error", "First Name Field must not have numbers.");
            } else if (digitCasePattern.matcher(middleName).find()) {
                view.showErrorDialog("Middle Name Field Error", "Middle Name Field must not have numbers.");
            } else if (digitCasePattern.matcher(lastName).find()) {
                view.showErrorDialog("Last Name Field Error", "Last Name Field must not have numbers.");
            }
            else if (contactNumber.isEmpty()) {
                view.showErrorDialog("Contact Field Empty", "Please fill in the required field.");
            } else if (employeeID.isEmpty()) {
                view.showErrorDialog("Employee ID Field Empty", "Please fill in the required field.");
            } else if (!employeeID.contains("Q-")) {
                view.showErrorDialog("Employee ID Error", "Employee ID must contain Q- before the number");
            } else if (!employeeID.startsWith("Q-")) {
                view.showErrorDialog("Employee ID Error", "Employee ID must start with Q- before the number");
            } else if (selectedDepartment.isEmpty()) {
                view.showErrorDialog("Selecting Department Field Empty", "Please fill in the required field.");
            } else {
                view.showProgress("Saving Preferences", "Saving User Preferences. Please wait...");
                interactor.getCredentials(thumbData, ImageURI, email, firstName, middleName, lastName, contactNumber, employeeID, selectedDepartment, this);
            }
        }
    }

    @Override
    public void onAuthenticationFailure(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(title, message);
        }
    }

    @Override
    public void onSavingSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showDialog(title, message);
        }
    }

    @Override
    public void onSavingFailure(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(title, message);
        }
    }

    @Override
    public void onChangesSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.changedEmailDialog(title, message);
        }
    }

    @Override
    public void onChangesFailure(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.notChangedEmailDialog(title, message);
        }

    }

    @Override
    public void onSignOutSuccess(String signOutTitle, String signOutMessage) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(signOutTitle, signOutMessage);
        }
    }

    @Override
    public void onSignOutFailure(String signOutTitle, String signOutMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(signOutTitle, signOutMessage);
        }
    }

    @Override
    public void onSavingTextSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showDialog(title, message);
        }
    }

    @Override
    public void onSavingTextFailure(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(title, message);
        }
    }
}

package cp2.citeportalv1.presenters.profile.faculty;

import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Faculty;

public interface FacultyUserInteractor {
    interface FacultyUserListener {
        void onUserSuccess(Faculty faculty);
        void onUserFailure(String errorTitle, String errorMessage);
    }

    interface FacultyPagesListener {
        void onPagesSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }

    void getUserDetails(FacultyUserListener listener);

    void getPages(FacultyPagesListener listener);
}

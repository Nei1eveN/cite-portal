package cp2.citeportalv1.presenters.profile.student.signature;

public interface StudentUserSignatureInteractor {
    interface UserSignatureListener {
        void onSignatureFound(String userSignature);

        void onSignatureNotFound(String errorMessage);
    }

    void getUserSignature(UserSignatureListener listener);
}

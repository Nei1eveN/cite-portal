package cp2.citeportalv1.presenters.profile.faculty.detail;

import android.content.Context;

import cp2.citeportalv1.models.Faculty;

public class FacultyDetailPresenterImpl implements FacultyDetailPresenter,
        FacultyDetailInteractor.FacultyDetailListener, FacultyDetailInteractor.UserSignatureListener {
    private FacultyDetailPresenter.View view;
    private FacultyDetailInteractor interactor;

    public FacultyDetailPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyDetailInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        interactor.getUserDetails(this);
        interactor.getUserSignature(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void requestUserInfo() {
        if (view != null){
            view.showProgress();
        }
        interactor.getUserDetails(this);
    }

    @Override
    public void onDetailSuccess(Faculty faculty) {
        if (view != null){
            view.hideProgress();
            view.setUserInfo(faculty);
        }
    }

    @Override
    public void onDetailFailure(String errorTitle, String errorMessage) {
        if (view != null){
            view.hideProgress();
            view.setExitDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onSignatureFound(String userSignature) {
        if (view != null) {
            view.hideProgress();
            view.setUserSignature(userSignature);
        }
    }

    @Override
    public void onSignatureNotFound(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setNoSignatureFound(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.profile.faculty;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import cp2.citeportalv1.views.faculty.FacultyDayAppointmentFragment;
import cp2.citeportalv1.views.faculty.FacultyUserDetailFragment;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Faculty;

import static cp2.citeportalv1.utils.Constants.FACULTY;

class FacultyUserInteractorImpl implements FacultyUserInteractor {
    private Context context;

    FacultyUserInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserDetails(FacultyUserListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Faculty student = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    listener.onUserSuccess(student);
                }
                else {
                    listener.onUserFailure("Student Detail Error","Student detail not found.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onUserFailure(
                        "Database Error",
                        "Code:\n"+databaseError.getCode()+
                                "\n\nMessage:\n"+databaseError.getMessage()+
                                "\n\nDetails:\n"+databaseError.getDetails());
            }
        });
    }

    @Override
    public void getPages(FacultyPagesListener listener) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(((FragmentActivity) context).getSupportFragmentManager());

        viewPagerAdapter.addFragment(new FacultyUserDetailFragment(), "User Profile");
        viewPagerAdapter.addFragment(new FacultyDayAppointmentFragment(), "Upcoming Appointments");

        listener.onPagesSuccess(viewPagerAdapter, 2);
    }
}

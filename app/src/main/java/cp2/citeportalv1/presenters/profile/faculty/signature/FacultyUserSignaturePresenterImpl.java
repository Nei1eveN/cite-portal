package cp2.citeportalv1.presenters.profile.faculty.signature;

import android.content.Context;

public class FacultyUserSignaturePresenterImpl implements FacultyUserSignaturePresenter, FacultyUserSignatureInteractor.UserSignatureListener {
    private View view;
    private FacultyUserSignatureInteractor interactor;

    public FacultyUserSignaturePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyUserSignatureInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.showProgress("User Signature Loading", "Loading your signature. Please wait...");
        }
        interactor.getUserSignature(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void onSignatureFound(String userSignature) {
        if (view != null) {
            view.hideProgress();
            view.setUserSignature(userSignature);
        }
    }

    @Override
    public void onSignatureNotFound(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setNoSignature(errorMessage);
        }
    }
}

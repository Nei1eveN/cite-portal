package cp2.citeportalv1.presenters.profile.faculty;

import android.content.Context;

import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Faculty;

public class FacultyUserPresenterImpl implements FacultyUserPresenter, FacultyUserInteractor.FacultyUserListener,
        FacultyUserInteractor.FacultyPagesListener {
    private FacultyUserPresenter.View view;
    private FacultyUserInteractor interactor;

    public FacultyUserPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyUserInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getUserDetails(this);
        interactor.getPages(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestUserInfo() {
        interactor.getUserDetails(this);
    }

    @Override
    public void requestPages() {
        interactor.getPages(this);
    }

    @Override
    public void onUserSuccess(Faculty faculty) {
        if (view != null){
            view.setUserInfo(faculty);
        }
    }

    @Override
    public void onUserFailure(String errorTitle, String errorMessage) {
        if (view != null){
            view.setExitDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onPagesSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null){
            view.setPages(viewPagerAdapter, numberOfPages);
        }
    }
}

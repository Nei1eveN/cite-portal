package cp2.citeportalv1.presenters.profile.faculty.detail;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Faculty;

import static cp2.citeportalv1.utils.Constants.FACULTY;

class FacultyDetailInteractorImpl implements FacultyDetailInteractor {
    private Context context;

    FacultyDetailInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserDetails(FacultyDetailListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Faculty faculty = Objects.requireNonNull(dataSnapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                    if (faculty.getContactNumber() == null) {
                        faculty.setContactNumber("Not yet settled.");
                    }

                    listener.onDetailSuccess(faculty);
                } else {
                    listener.onDetailFailure("Detail Problem", "Details might not be synced for a while");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onDetailFailure(
                        "Database Error",
                        "Code: " + databaseError.getCode() +
                                "\n\nMessage:\n" + databaseError.getMessage() +
                                "\n\nDetails:\n" + databaseError.getDetails());
            }
        });
    }

    @Override
    public void getUserSignature(UserSignatureListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.child("userSignature").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String userSignature = dataSnapshot.getValue(String.class);
                    listener.onSignatureFound(userSignature);
                } else {
                    listener.onSignatureNotFound("You don't have a signature. Click this text to create.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onSignatureNotFound(databaseError.getMessage());
            }
        });
    }
}

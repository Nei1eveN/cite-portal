package cp2.citeportalv1.presenters.profile.faculty.signature.edit_signature;

import com.github.gcacace.signaturepad.views.SignaturePad;

public interface FacultyUserEditSignaturePresenter {
    interface View {
        void initViews();
        void showProgress(String title, String message);
        void hideProgress();
        void showSnackMessage(String snackMessage);
        void showErrorMessage(String title, String message);
        void showExitMessage(String title, String message);
    }
    void onStart();
    void onDestroy();
    void submitUserSignature(SignaturePad signaturePad);
}

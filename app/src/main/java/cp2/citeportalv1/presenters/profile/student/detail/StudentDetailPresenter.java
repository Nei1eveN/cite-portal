package cp2.citeportalv1.presenters.profile.student.detail;

import cp2.citeportalv1.models.Student;

public interface StudentDetailPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setUserInfo(Student student);
        void setUserSignature(String userSignature);
        void setNoSignatureFound(String message);
        void setExitDialog(String title, String message);
        void setSnackMessage(String snackMessage);
    }
    void onStart();
    void onDestroy();
    void requestStudentUserInfo();
}

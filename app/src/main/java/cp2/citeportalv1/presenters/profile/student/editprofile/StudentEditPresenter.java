package cp2.citeportalv1.presenters.profile.student.editprofile;

import android.net.Uri;

import java.util.List;

import cp2.citeportalv1.models.Student;

public interface StudentEditPresenter {
    interface View {
        void showProgress(String title, String message);

        void hideProgress();

        void showDialog(String title, String caption);

        void showErrorDialog(String errorTitle, String errorCaption);

        void changedEmailDialog(String title, String message);

        void notChangedEmailDialog(String exitTitle, String exitMessage);

        void showExitDialog(String title, String message);

        void setUserInfo(Student student);

        void setSpinnerForPrograms(List<String> programs);

        void setSpinnerForYearLevel(List<String> yearLevel);

        void setUserReAuthenticate();
    }
    void onStart();
    void onDestroy();

    void requestAuthentication(String email, String password,
                               byte[] thumbData, Uri imageURI,
                               String changingEmail,
                               String firstName, String middleName, String lastName, String contactNumber,
                               String studentId, String selectedProgram, String selectedYearLevel);

    void submitCredentials(byte[] thumbData, Uri imageURI,
                           String email,
                           String firstName, String middleName, String lastName, String contactNumber,
                           String studentId, String selectedProgram, String selectedYearLevel);

    void submitCredentialsWithoutFiles(String email,
                                       String firstName, String middleName, String lastName, String contactNumber,
                                       String studentId, String selectedProgram, String selectedYearLevel);

    void signOutUser();
}


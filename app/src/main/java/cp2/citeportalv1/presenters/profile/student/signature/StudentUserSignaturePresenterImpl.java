package cp2.citeportalv1.presenters.profile.student.signature;

import android.content.Context;

public class StudentUserSignaturePresenterImpl implements StudentUserSignaturePresenter, StudentUserSignatureInteractor.UserSignatureListener {
    private StudentUserSignaturePresenter.View view;
    private StudentUserSignatureInteractor interactor;

    public StudentUserSignaturePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentUserSignatureInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.showProgress("User Signature Loading", "Loading your signature. Please wait...");
        }
        interactor.getUserSignature(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void onSignatureFound(String userSignature) {
        if (view != null) {
            view.hideProgress();
            view.setUserSignature(userSignature);
        }
    }

    @Override
    public void onSignatureNotFound(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setNoSignature(errorMessage);
        }
    }
}

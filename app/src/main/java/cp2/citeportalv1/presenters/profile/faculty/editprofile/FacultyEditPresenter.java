package cp2.citeportalv1.presenters.profile.faculty.editprofile;

import android.net.Uri;

import java.util.List;

import cp2.citeportalv1.models.Faculty;

public interface FacultyEditPresenter {
    interface View {
        void showProgress(String title, String message);
        void hideProgress();
        void showDialog(String title, String caption);
        void showErrorDialog(String errorTitle, String errorCaption);
        void changedEmailDialog(String title, String message);
        void notChangedEmailDialog(String exitTitle, String exitMessage);
        void showExitDialog(String title, String message);
        void setUserInfo(Faculty faculty);
        void setSpinnerItems(List<String> spinnerItems);
        void setUserReAuthenticate();
    }
    void onStart();
    void onDestroy();

    void requestAuthentication(String email, String password, byte[] thumbData, Uri ImageURI, String changingEmail, String firstName, String middleName, String lastName, String contactNumber,
                               String employeeID, String selectedDepartment);

    void submitCredentials(byte[] thumbData, Uri ImageURI,
                           String email,
                           String firstName, String middleName, String lastName, String contactNumber,
                           String employeeID, String selectedDepartment);

    void submitCredentialsWithoutFiles(String email,
                                       String firstName, String middleName, String lastName, String contactNumber,
                                       String employeeID, String selectedDepartment);

    void signOutUser();
}

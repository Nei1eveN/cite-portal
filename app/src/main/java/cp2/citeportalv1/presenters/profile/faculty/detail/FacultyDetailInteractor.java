package cp2.citeportalv1.presenters.profile.faculty.detail;

import cp2.citeportalv1.models.Faculty;

public interface FacultyDetailInteractor {
    interface FacultyDetailListener {
        void onDetailSuccess(Faculty faculty);
        void onDetailFailure(String errorTitle, String errorMessage);
    }
    void getUserDetails(FacultyDetailListener listener);

    interface UserSignatureListener {
        void onSignatureFound(String userSignature);
        void onSignatureNotFound(String errorMessage);
    }

    void getUserSignature(UserSignatureListener listener);
}

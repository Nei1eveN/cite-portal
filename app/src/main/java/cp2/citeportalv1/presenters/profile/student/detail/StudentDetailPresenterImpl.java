package cp2.citeportalv1.presenters.profile.student.detail;

import android.content.Context;

import cp2.citeportalv1.models.Student;

public class StudentDetailPresenterImpl implements StudentDetailPresenter,
        StudentDetailInteractor.StudentDetailListener, StudentDetailInteractor.UserSignatureListener {
    private StudentDetailPresenter.View view;
    private StudentDetailInteractor interactor;

    public StudentDetailPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentDetailInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        interactor.getStudentUserDetails(this);
        interactor.getUserSignature(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void requestStudentUserInfo() {
        if (view != null){
            view.showProgress();
        }
        interactor.getStudentUserDetails(this);
    }

    @Override
    public void onStudentDetailSuccess(Student student) {
        if (view != null){
            view.hideProgress();
            view.setUserInfo(student);
        }
    }

    @Override
    public void onStudentDetailFailure(String errorTitle, String errorMessage) {
        if (view != null){
            view.hideProgress();
            view.setExitDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onSignatureFound(String userSignature) {
        if (view != null) {
            view.hideProgress();
            view.setUserSignature(userSignature);
        }
    }

    @Override
    public void onSignatureNotFound(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setNoSignatureFound(errorMessage);
        }
    }
}

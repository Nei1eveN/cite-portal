package cp2.citeportalv1.presenters.profile.student.signature;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;

import static cp2.citeportalv1.utils.Constants.STUDENTS;

class StudentUserSignatureInteractorImpl implements StudentUserSignatureInteractor {
    private Context context;

    public StudentUserSignatureInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserSignature(UserSignatureListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference signatureReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());

        signatureReference.child("userSignature").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String userSignature = dataSnapshot.getValue(String.class);
                    listener.onSignatureFound(userSignature);
                } else {
                    listener.onSignatureNotFound("It seems you haven't created your signature yet. Click this text to create.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onSignatureNotFound(databaseError.getMessage());
            }
        });
    }
}

package cp2.citeportalv1.presenters.profile.student.detail;

import cp2.citeportalv1.models.Student;

public interface StudentDetailInteractor {
    interface StudentDetailListener {
        void onStudentDetailSuccess(Student student);
        void onStudentDetailFailure(String errorTitle, String errorMessage);
    }
    void getStudentUserDetails(StudentDetailListener listener);

    interface UserSignatureListener {
        void onSignatureFound(String userSignature);
        void onSignatureNotFound(String errorMessage);
    }

    void getUserSignature(UserSignatureListener listener);
}

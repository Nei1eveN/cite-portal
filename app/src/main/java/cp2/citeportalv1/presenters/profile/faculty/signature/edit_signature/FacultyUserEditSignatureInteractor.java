package cp2.citeportalv1.presenters.profile.faculty.signature.edit_signature;

public interface FacultyUserEditSignatureInteractor {
    interface UserEditSignatureListener {
        void onEditSuccess(String title, String message);
        void onEditError(String errorTitle, String errorMessage);
    }
    void getUserSignature(byte[] thumbData, UserEditSignatureListener listener);
}

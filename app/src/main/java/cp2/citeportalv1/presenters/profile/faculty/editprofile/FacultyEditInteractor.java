package cp2.citeportalv1.presenters.profile.faculty.editprofile;

import android.net.Uri;

import java.util.List;

import cp2.citeportalv1.models.Faculty;

public interface FacultyEditInteractor {
    interface SignOutListener {
        void onSignOutSuccess(String signOutTitle, String signOutMessage);
        void onSignOutFailure(String signOutTitle, String signOutMessage);
    }

    interface SaveCredentialsListener {
        void onSavingSuccess(String title, String message);
        void onSavingFailure(String title, String message);
        void onChangesSuccess(String title, String message);
        void onChangesFailure(String title, String message);
    }

    interface ReAuthenticationListener {
        void onAuthenticationSuccess(byte[] thumbData, Uri ImageURI,
                                     String email,
                                     String firstName, String middleName, String lastName, String contactNumber,
                                     String employeeID, String selectedDepartment);
        void onAuthenticationFailure(String title, String message);
    }

    interface UserDetailsListener {
        void onDetailsSuccess(Faculty faculty);
        void onDetailsFailure(String errorTitle, String errorMessage);
    }

    interface UserDepartmentListener {
        void onDeptDetailSuccess(List<String> programs);
        void onDeptDetailFailure(String errorTitle, String errorMessage);
    }

    interface SaveTextCredentialsListener {
        void onSavingTextSuccess(String title, String message);
        void onSavingTextFailure(String title, String message);
        void onChangesSuccess(String title, String message);
        void onChangesFailure(String title, String message);
    }

    void getUserDetails(UserDetailsListener listener);

    void getUserDepartment(UserDepartmentListener listener);

    void getCredentials(byte[] thumbData, Uri ImageURI,
                        String email,
                        String firstName, String middleName, String lastName, String contactNumber,
                        String employeeID, String selectedDepartment, SaveCredentialsListener listener);

    void getAuthenticationCredentials(String email, String password, byte[] thumbData, Uri ImageURI, String changingEmail, String firstName, String middleName, String lastName, String contactNumber,
                                      String employeeID, String selectedDepartment, ReAuthenticationListener listener);

    void getTextCredentials(String email, String firstName, String middleName, String lastName, String contactNumber,
                            String employeeID, String selectedDepartment, SaveTextCredentialsListener listener);

    void signOut(SignOutListener listener);
}

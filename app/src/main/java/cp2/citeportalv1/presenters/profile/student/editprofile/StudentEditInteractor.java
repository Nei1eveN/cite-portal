package cp2.citeportalv1.presenters.profile.student.editprofile;

import android.net.Uri;

import java.util.List;

import cp2.citeportalv1.models.Student;

public interface StudentEditInteractor {
    interface SignOutListener {
        void onSignOutSuccess(String signOutTitle, String signOutMessage);
        void onSignOutFailure(String signOutTitle, String signOutMessage);
    }

    interface SaveCredentialsListener {
        void onSavingSuccess(String title, String message);
        void onSavingFailure(String title, String message);
        void onChangesSuccess(String title, String message);
        void onChangesFailure(String title, String message);
    }

    interface ReAuthenticationListener {
        void onAuthenticationSuccess(byte[] thumbData, Uri imageURI,
                                     String email,
                                     String firstName, String middleName, String lastName, String contactNumber,
                                     String studentId, String selectedProgram, String selectedYearLevel);
        void onAuthenticationFailure(String title, String message);
    }

    interface UserDetailsListener {
        void onDetailsSuccess(Student student);
        void onDetailsFailure(String errorTitle, String errorMessage);
    }

    interface UserProgramListener {
        void onProgramsDetailSuccess(List<String> programs);
        void onProgramsDetailFailure(String errorTitle, String errorMessage);
    }

    interface UserYearLevelListener {
        void onYearLevelDetailSuccess(List<String> yearLevel);
        void onYearLevelDetailFailure(String errorTitle, String errorMessage);
    }

    interface SaveTextCredentialsListener {
        void onSavingTextSuccess(String title, String message);
        void onSavingTextFailure(String title, String message);
        void onChangesSuccess(String title, String message);
        void onChangesFailure(String title, String message);
    }

    void getUserDetails(UserDetailsListener listener);

    void getUserProgram(UserProgramListener listener);

    void getUserYearLevel(UserYearLevelListener listener);

    void getCredentials(byte[] thumbData, Uri imageURI,
                        String email,
                        String firstName, String middleName, String lastName, String contactNumber,
                        String studentId, String selectedProgram, String selectedYearLevel,
                        SaveCredentialsListener listener);

    void getAuthenticationCredentials(String email, String password,
                                      byte[] thumbData, Uri imageURI,
                                      String changingEmail,
                                      String firstName, String middleName, String lastName, String contactNumber,
                                      String studentId, String selectedProgram, String selectedYearLevel,
                                      ReAuthenticationListener listener);

    void getTextCredentials(String email,
                            String firstName, String middleName, String lastName, String contactNumber,
                            String studentId, String selectedProgram, String selectedYearLevel,
                            SaveTextCredentialsListener listener);

    void signOut(SignOutListener listener);
}

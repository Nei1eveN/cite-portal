package cp2.citeportalv1.presenters.profile.faculty.signature;

public interface FacultyUserSignatureInteractor {
    interface UserSignatureListener {
        void onSignatureFound(String userSignature);

        void onSignatureNotFound(String errorMessage);
    }

    void getUserSignature(UserSignatureListener listener);
}

package cp2.citeportalv1.presenters.profile.faculty.detail;

import cp2.citeportalv1.models.Faculty;

public interface FacultyDetailPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setUserInfo(Faculty faculty);
        void setUserSignature(String userSignature);
        void setNoSignatureFound(String message);
        void setExitDialog(String title, String message);
        void setSnackMessage(String snackMessage);
    }
    void onStart();
    void onDestroy();
    void requestUserInfo();
}

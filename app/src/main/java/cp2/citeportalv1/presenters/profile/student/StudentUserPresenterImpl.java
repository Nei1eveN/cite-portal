package cp2.citeportalv1.presenters.profile.student;

import android.content.Context;

import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Student;

public class StudentUserPresenterImpl implements StudentUserPresenter, StudentUserInteractor.StudentUserListener, StudentUserInteractor.StudentPagesListener {
    private StudentUserPresenter.View view;
    private StudentUserInteractor interactor;

    public StudentUserPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentUserInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getStudentUserDetails(this);
        interactor.getPages(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void requestStudentUserInfo() {
        interactor.getStudentUserDetails(this);
    }

    @Override
    public void requestPages() {
        interactor.getPages(this);
    }

    @Override
    public void onStudentUserSuccess(Student student) {
        if (view != null){
            view.setStudentUserInfo(student);
        }
    }

    @Override
    public void onStudentUserFailure(String errorTitle, String errorMessage) {
        if (view != null){
            view.setExitDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onPagesSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null){
            view.setPages(viewPagerAdapter, numberOfPages);
        }
    }
}

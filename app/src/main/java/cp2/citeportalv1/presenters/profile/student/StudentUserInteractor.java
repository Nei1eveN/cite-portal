package cp2.citeportalv1.presenters.profile.student;

import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Student;

public interface StudentUserInteractor {
    interface StudentUserListener {
        void onStudentUserSuccess(Student student);
        void onStudentUserFailure(String errorTitle, String errorMessage);
    }

    interface StudentPagesListener {
        void onPagesSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }

    void getStudentUserDetails(StudentUserListener listener);

    void getPages(StudentPagesListener listener);
}

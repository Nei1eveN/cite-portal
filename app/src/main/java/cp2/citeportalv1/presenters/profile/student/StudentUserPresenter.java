package cp2.citeportalv1.presenters.profile.student;

import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.models.Student;

public interface StudentUserPresenter {
    interface View {
        void setPages(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
        void setStudentUserInfo(Student studentUserInfo);
        void setExitDialog(String title, String message);
    }
    void onStart();
    void onDestroy();
    void requestStudentUserInfo();
    void requestPages();
}

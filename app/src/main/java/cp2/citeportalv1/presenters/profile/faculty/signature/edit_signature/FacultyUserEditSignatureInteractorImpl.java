package cp2.citeportalv1.presenters.profile.faculty.signature.edit_signature;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.FACULTY_USERS;
import static cp2.citeportalv1.utils.Constants.SIGNATURES;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

class FacultyUserEditSignatureInteractorImpl implements FacultyUserEditSignatureInteractor {
    private Context context;

    FacultyUserEditSignatureInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserSignature(byte[] thumbData, UserEditSignatureListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onEditError("Network Error", "Please check your network connection");
        } else {
            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
            StorageReference storageReference = firebaseStorage.getReference(FACULTY_USERS).child(SIGNATURES);

            StorageReference rootReference = storageReference.child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            rootReference.putBytes(thumbData).addOnSuccessListener(((Activity) context), taskSnapshot -> rootReference.getDownloadUrl().addOnSuccessListener(((Activity) context), uri -> {
                Map<String, Object> signatureMap = new HashMap<>();
                signatureMap.put("userSignature", uri.toString());

                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                DatabaseReference savingSignatureReference = firebaseDatabase.getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
                savingSignatureReference.keepSynced(true);

                savingSignatureReference.updateChildren(signatureMap).addOnSuccessListener(((Activity) context), aVoid -> listener.onEditSuccess("Signature Saved", "Your signature has been saved.")).addOnFailureListener(((Activity) context), e -> listener.onEditError("Saving Error", e.getMessage()));


            })
            .addOnFailureListener(((Activity) context), e -> listener.onEditError("URL Error", e.getMessage()))).addOnFailureListener((Activity) context, e -> listener.onEditError("Saving Error", e.getMessage()));

        }
    }
}

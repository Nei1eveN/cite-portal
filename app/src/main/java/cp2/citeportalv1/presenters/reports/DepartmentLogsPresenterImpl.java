package cp2.citeportalv1.presenters.reports;

import android.content.Context;

import cp2.citeportalv1.adapters.DepartmentLogsAdapter;

public class DepartmentLogsPresenterImpl implements DepartmentLogsPresenter, DepartmentLogsInteractor.OnDepartmentSyncListener {
    private DepartmentLogsPresenter.View view;
    private DepartmentLogsInteractor interactor;

    public DepartmentLogsPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new DepartmentLogsInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.setProgress();
        }
        interactor.getList(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestFacultyEmployees() {
        if (view != null){
            view.setProgress();
        }
        interactor.getList(this);
    }

    @Override
    public void onSuccess(DepartmentLogsAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setFacultyEmployees(adapter);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.reports.faculty_report_logs.log_detail;

import cp2.citeportalv1.models.ConsultationLog;

public interface LogDetailInteractor {
    void getNotificationDetail(String facultyUid, String notificationId, ConsultationLogDetailListener listener);
    interface ConsultationLogDetailListener {
        void getNotificationDetail(ConsultationLog consultationLog);
        void getDetailError(String errorTitle, String errorMessage);
    }
}

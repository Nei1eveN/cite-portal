package cp2.citeportalv1.presenters.reports.faculty_report_logs;

import android.content.Context;

import cp2.citeportalv1.adapters.DepartmentInstructorLogAdapter;

public class InstructorLogPresenterImpl implements InstructorLogPresenter, InstructorLogInteractor.FacultyConsultationLogListener {
    private InstructorLogPresenter.View view;
    private InstructorLogInteractor interactor;

    public InstructorLogPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new InstructorLogInteractorImpl(context);
    }

    @Override
    public void onStart(String facultyUid) {
        if (view != null){
            view.showProgress();
        }
        interactor.getDetails(facultyUid,this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void OnResponseSuccess(DepartmentInstructorLogAdapter adapter) {
        if (view != null) {
            view.hideProgress();
            view.setAppointments(adapter);
        }
    }

    @Override
    public void OnResponseFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyAppointment(errorMessage);
            view.showSnackMessage(errorMessage);
        }
    }
}

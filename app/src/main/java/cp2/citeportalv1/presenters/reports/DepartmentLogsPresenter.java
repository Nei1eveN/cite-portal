package cp2.citeportalv1.presenters.reports;

import cp2.citeportalv1.adapters.DepartmentLogsAdapter;

public interface DepartmentLogsPresenter {
    interface View {
        void setProgress();
        void hideProgress();
        void setSnackMessage(String message);
        void setEmptyState(String emptyStateMessage);
        void setFacultyEmployees(DepartmentLogsAdapter adapter);
    }
    void onStart();
    void onDestroy();
    void requestFacultyEmployees();
}

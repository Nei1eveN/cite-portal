package cp2.citeportalv1.presenters.reports.faculty_report_logs.log_detail;

import cp2.citeportalv1.models.ConsultationLog;

public interface LogDetailPresenter {
    interface View {
        void showProgress(String title, String caption);
        void hideProgress();
        void setApprovedDetail(ConsultationLog consultationLog);
        void showExitDialog(String title, String message);
        void setSnackMessage(String snackMessage);
    }
    void onStart(String facultyUid, String notificationId);
    void onDestroy();
}

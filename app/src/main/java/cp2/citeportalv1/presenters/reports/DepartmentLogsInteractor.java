package cp2.citeportalv1.presenters.reports;

import cp2.citeportalv1.adapters.DepartmentLogsAdapter;

public interface DepartmentLogsInteractor {
    void getList(OnDepartmentSyncListener listener);
    interface OnDepartmentSyncListener {
        void onSuccess(DepartmentLogsAdapter adapter);
        void onFailure(String errorMessage);
    }
}

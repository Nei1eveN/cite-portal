package cp2.citeportalv1.presenters.reports.faculty_report_logs;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.adapters.DepartmentInstructorLogAdapter;
import cp2.citeportalv1.models.ConsultationLog;

import static cp2.citeportalv1.utils.Constants.CONSULTATION_LOG;
import static cp2.citeportalv1.utils.Constants.FACULTY;

class InstructorLogInteractorImpl implements InstructorLogInteractor {
    private Context context;

    public InstructorLogInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getDetails(String facultyUid, FacultyConsultationLogListener detailsListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY)
                .child(facultyUid).child(CONSULTATION_LOG);
        databaseReference.keepSynced(true);

        databaseReference.orderByChild("requestedTimeStart").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<ConsultationLog> appointments = new ArrayList<>();
                    appointments.clear();

                    for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                        ConsultationLog consultationLog = Objects.requireNonNull(snapshot.getValue(ConsultationLog.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                        appointments.add(consultationLog);
                    }

                    DepartmentInstructorLogAdapter adapter = new DepartmentInstructorLogAdapter(context, appointments);
                    detailsListener.OnResponseSuccess(adapter);

                } else {
                    detailsListener.OnResponseFailure("This user has no items in Consultation Log.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                detailsListener.OnResponseFailure(databaseError.getMessage());
            }
        });
    }
}

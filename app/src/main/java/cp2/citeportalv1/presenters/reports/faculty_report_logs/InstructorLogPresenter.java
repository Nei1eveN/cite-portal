package cp2.citeportalv1.presenters.reports.faculty_report_logs;

import cp2.citeportalv1.adapters.DepartmentInstructorLogAdapter;

public interface InstructorLogPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void showSnackMessage(String message);
        void setAppointments(DepartmentInstructorLogAdapter adapter);
        void setEmptyAppointment(String emptyMessage);
    }
    void onStart(String facultyUid);
    void onDestroy();
}

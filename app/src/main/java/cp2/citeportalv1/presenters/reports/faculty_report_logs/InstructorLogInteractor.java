package cp2.citeportalv1.presenters.reports.faculty_report_logs;

import cp2.citeportalv1.adapters.DepartmentInstructorLogAdapter;

public interface InstructorLogInteractor {
    interface FacultyConsultationLogListener {
        void OnResponseSuccess(DepartmentInstructorLogAdapter adapter);

        void OnResponseFailure(String errorMessage);
    }

    void getDetails(String facultyUid, FacultyConsultationLogListener detailsListener);
}

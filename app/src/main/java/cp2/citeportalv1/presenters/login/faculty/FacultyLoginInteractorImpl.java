package cp2.citeportalv1.presenters.login.faculty;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Accounts;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.Result;
import cp2.citeportalv1.services.APIService;
import cp2.citeportalv1.utils.APIClient;
import cp2.citeportalv1.utils.Constants;
import cp2.citeportalv1.views.admin.AccountsActivity;
import cp2.citeportalv1.views.faculty.FacultyLoginActivity;
import cp2.citeportalv1.views.faculty.FacultyHomeActivity;
import cp2.citeportalv1.views.secretary.StudentAccountsActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cp2.citeportalv1.utils.Constants.ACCOUNTS;
import static cp2.citeportalv1.utils.Constants.ACCOUNT_ACCESS_LEVEL;
import static cp2.citeportalv1.utils.Constants.EMAIL;
import static cp2.citeportalv1.utils.Constants.FACULTY;
import static cp2.citeportalv1.utils.Constants.SECRETARY;
import static cp2.citeportalv1.utils.Constants.TOKEN_ID;
import static cp2.citeportalv1.utils.Constants.VALID;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

public class FacultyLoginInteractorImpl implements FacultyLoginInteractor {

    private Context context;

    FacultyLoginInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void checkNetwork(String email, String password, LoginListener listener) {
        if (Constants.isNetworkAvailable(context)) {
            Log.d("interactor-checkNetwork", "getLoginCredentials");
            getLoginCredentials(email, password, listener);
        } else {
            Log.d("interactor-checkNetwork", "network not available");
            listener.onFailure("Please connect to the internet");
        }
    }

    @Override
    public void getLoginCredentials(String email, String password, LoginListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference();
        databaseReference.keepSynced(true);

        /**LOGIN**/
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnSuccessListener(authResult -> {
            /**ADMIN  / SECRETARY LOGIN CHECKER**/
            DatabaseReference accountsReference = FirebaseDatabase.getInstance().getReference(ACCOUNTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
            accountsReference.keepSynced(true);

            accountsReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        if (FirebaseAuth.getInstance().getCurrentUser().isEmailVerified()) {
                            String accessLevel = dataSnapshot.child(ACCOUNT_ACCESS_LEVEL).getValue(String.class);
                            if (Objects.requireNonNull(accessLevel).equals(SECRETARY)) {
                                listener.onSuccess("Welcome back, " + FirebaseAuth.getInstance().getCurrentUser().getEmail());
                                context.startActivity(new Intent(context, StudentAccountsActivity.class));
                                ((android.app.Activity) context).finish();
                            } else {
                                /**TO LEAVE NOTE ON CREATING ADMIN ACCOUNT PANEL**/
                                listener.onSuccess("Welcome back, " + FirebaseAuth.getInstance().getCurrentUser().getEmail());
                                context.startActivity(new Intent(context, AccountsActivity.class));
                                ((android.app.Activity) context).finish();
                            }
                        } else {
                            FirebaseAuth.getInstance().signOut();
                            listener.onFailure("Your email is not verified. Please verify your email first.");
                            context.startActivity(new Intent(context, FacultyLoginActivity.class));
                            ((android.app.Activity) context).finish();
                            ((android.app.Activity) context).overridePendingTransition(0, 0);
                        }
                    } else {
                        /**CHECK DEPARTMENT VALIDATED REFERENCE**/
                        DatabaseReference department_validated_reference = FirebaseDatabase.getInstance().getReference(FACULTY).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
                        department_validated_reference.keepSynced(true);

                        Query query = databaseReference.child(FACULTY).orderByChild(EMAIL).equalTo(email);
                        query.keepSynced(true);
                        query.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    department_validated_reference.child(VALID).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                Boolean value = (Boolean) dataSnapshot.getValue();
                                                if (Objects.requireNonNull(value).equals(false)) {
                                                    FirebaseAuth.getInstance().signOut();
                                                    listener.onFailure("You cannot login further. You may ask your Department Secretary to validate your account");
                                                    context.startActivity(new Intent(context, FacultyLoginActivity.class));
                                                    ((android.app.Activity) context).overridePendingTransition(0, 0);
                                                } else {
                                                    if (Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).isEmailVerified()) {

                                                        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(((android.app.Activity) context),
                                                                instanceIdResult -> {
                                                                    String tokenId = instanceIdResult.getToken();
                                                                    String currentId = FirebaseAuth.getInstance().getCurrentUser().getUid();

                                                                    DatabaseReference tokenReference = firebaseDatabase.getReference(FACULTY).child(currentId).child(TOKEN_ID);
                                                                    tokenReference.keepSynced(true);

                                                                    tokenReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                            if (dataSnapshot.exists()) {
                                                                                FirebaseAuth.getInstance().signOut();
                                                                                listener.onFailure("Your account must be currently logged on to other device. To be able to log in, please sign out your account from other device.");
                                                                                context.startActivity(new Intent(context, FacultyLoginActivity.class));
                                                                                ((android.app.Activity) context).finish();
                                                                                ((android.app.Activity) context).overridePendingTransition(0, 0);
                                                                            } else {
                                                                                FirebaseDatabase.getInstance().getReference(FACULTY)
                                                                                        .child(currentId)
                                                                                        .child(TOKEN_ID).setValue(tokenId)
                                                                                        .addOnCompleteListener(((android.app.Activity) context), task -> {
                                                                                            if (task.isSuccessful()) {
                                                                                                try {
                                                                                                    listener.onSuccess("Welcome back, " + FirebaseAuth.getInstance().getCurrentUser().getEmail());
                                                                                                    context.startActivity(new Intent(context, FacultyHomeActivity.class));
                                                                                                    ((android.app.Activity) context).finish();
                                                                                                } catch (NullPointerException e) {
                                                                                                    FirebaseAuth.getInstance().signOut();
                                                                                                    context.startActivity(new Intent(context, FacultyLoginActivity.class));
                                                                                                    ((android.app.Activity) context).finish();
                                                                                                    ((android.app.Activity) context).overridePendingTransition(0, 0);
                                                                                                }
                                                                                            }
                                                                                        });
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                            FirebaseAuth.getInstance().signOut();
                                                                            listener.onFailure(databaseError.getMessage());
                                                                            context.startActivity(new Intent(context, FacultyLoginActivity.class));
                                                                            ((android.app.Activity) context).finish();
                                                                            ((android.app.Activity) context).overridePendingTransition(0, 0);
                                                                        }
                                                                    });
                                                                });

                                                    } else {
                                                        FirebaseAuth.getInstance().signOut();
                                                        listener.onFailure("You cannot login further. Please validate your email.");
                                                        context.startActivity(new Intent(context, FacultyLoginActivity.class));
                                                        ((android.app.Activity) context).finish();
                                                        ((android.app.Activity) context).overridePendingTransition(0, 0);
                                                    }

                                                }
                                            } else {
                                                FirebaseAuth.getInstance().signOut();
                                                listener.onFailure("You cannot login further. You may ask your secretary to validate your account");
                                                context.startActivity(new Intent(context, FacultyLoginActivity.class));
                                                ((android.app.Activity) context).overridePendingTransition(0, 0);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            FirebaseAuth.getInstance().signOut();
                                            listener.onFailure(databaseError.getMessage());
                                        }
                                    });
                                } else {
                                    FirebaseAuth.getInstance().signOut();
                                    listener.onFailure("Your email does not exist");
                                    context.startActivity(new Intent(context, FacultyLoginActivity.class));
                                    ((android.app.Activity) context).overridePendingTransition(0, 0);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.onFailure(databaseError.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onFailure(databaseError.getMessage());
                }
            });

        }).addOnFailureListener(e -> listener.onFailure(e.getMessage()));

    }

    @Override
    public void getUser(LoginListener listener) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            if (user.isEmailVerified()) {
                context.startActivity(new Intent(context, FacultyHomeActivity.class));
                ((android.app.Activity) context).finish();
                Toast.makeText(context, "Welcome back, " + user.getEmail(), Toast.LENGTH_SHORT).show();
            } else {
                firebaseAuth.signOut();
                context.startActivity(new Intent(context, FacultyLoginActivity.class));
                ((android.app.Activity) context).finish();
                ((android.app.Activity) context).overridePendingTransition(0, 0);
                Toast.makeText(context, "Please verify your email", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void sendEmailVerification(String email, EmailVerificationListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onVerificationFailure("Network Error", "Please check your network connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY);
            databaseReference.keepSynced(true);

            Query facultyQuery = databaseReference.orderByChild(EMAIL).equalTo(email);
            facultyQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Faculty faculty = Objects.requireNonNull(snapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                            APIService emailVerificationService = APIClient.getClient(context).create(APIService.class);
                            Call<Result> resultCall = emailVerificationService.sendVerification(faculty.facultyId);
                            resultCall.enqueue(new Callback<Result>() {
                                @Override
                                public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                                    Result result = response.body();

                                    if (!Objects.requireNonNull(result).getStatus()) {
                                        listener.onVerificationFailure("Email Verification Failure", "Cannot send email verification to this user. Please try again later.");
                                    } else {
                                        listener.onVerificationSuccess("User Validation Success", "Email Verification Link has been sent to " + faculty.getEmail());
                                    }
                                }

                                @Override
                                public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                                    listener.onVerificationFailure("Validation Error", t.getMessage());
                                }
                            });
                        }
                    } else {
                        DatabaseReference databaseReference = firebaseDatabase.getReference(ACCOUNTS);
                        databaseReference.keepSynced(true);

                        Query accountQuery = databaseReference.orderByChild(EMAIL).equalTo(email);
                        accountQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                        Accounts account = Objects.requireNonNull(snapshot.getValue(Accounts.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                                        APIService emailVerificationService = APIClient.getClient(context).create(APIService.class);
                                        Call<Result> resultCall = emailVerificationService.sendVerification(account.accountsId);
                                        resultCall.enqueue(new Callback<Result>() {
                                            @Override
                                            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                                                Result result = response.body();

                                                if (!Objects.requireNonNull(result).getStatus()) {
                                                    listener.onVerificationFailure("Email Verification Failure", "Cannot send email verification to this user. Please try again later.");
                                                } else {
                                                    listener.onVerificationSuccess("User Validation Success", "Email Verification Link has been sent to " + account.getEmail());
                                                }
                                            }

                                            @Override
                                            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                                                listener.onVerificationFailure("Validation Error", t.getMessage());
                                            }
                                        });
                                    }
                                } else {
                                    listener.onVerificationFailure("Not Existing Detail", "Email does not exist in our database.");

                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.onVerificationFailure("Database Error", databaseError.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onVerificationFailure("Database Error", databaseError.getMessage());
                }
            });
        }
    }

    @Override
    public void sendPasswordReset(String email, PasswordResetListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onResetFailure("Network Error", "Please check your network connection.");
        } else {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference(FACULTY);
            databaseReference.keepSynced(true);

            Query facultyQuery = databaseReference.orderByChild(EMAIL).equalTo(email);
            facultyQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Faculty faculty = Objects.requireNonNull(snapshot.getValue(Faculty.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                            FirebaseAuth.getInstance().sendPasswordResetEmail(faculty.getEmail()).addOnSuccessListener(((android.app.Activity) context), aVoid -> listener.onResetSuccess("Password Reset", "Password Reset Link has been sent to " + faculty.getEmail() + ".")).addOnFailureListener(((android.app.Activity) context), e -> listener.onResetFailure("Password Reset Error", e.getMessage()));
                        }
                    } else {
                        DatabaseReference databaseReference = firebaseDatabase.getReference(ACCOUNTS);
                        databaseReference.keepSynced(true);

                        Query accountQuery = databaseReference.orderByChild(EMAIL).equalTo(email);
                        accountQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                        Accounts account = Objects.requireNonNull(snapshot.getValue(Accounts.class)).withId(Objects.requireNonNull(snapshot.getKey()));
                                        FirebaseAuth.getInstance().sendPasswordResetEmail(account.getEmail()).addOnSuccessListener(((android.app.Activity) context), aVoid -> listener.onResetSuccess("Password Reset", "Password Reset Link has been sent to " + account.getEmail() + ".")).addOnFailureListener(((android.app.Activity) context), e -> listener.onResetFailure("Password Reset Error", e.getMessage()));
                                    }
                                } else {
                                    listener.onResetFailure("Account Detail Error", "This user does not exist in the database.");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                listener.onResetFailure("Database Error", databaseError.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onResetFailure("Database Error", databaseError.getMessage());
                }
            });
        }
    }
}

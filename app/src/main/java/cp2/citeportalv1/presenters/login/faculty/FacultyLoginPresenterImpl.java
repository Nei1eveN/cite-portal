package cp2.citeportalv1.presenters.login.faculty;

import android.content.Context;
import android.util.Log;

import java.util.Objects;

public class FacultyLoginPresenterImpl implements FacultyLoginPresenter,
        FacultyLoginInteractor.LoginListener, FacultyLoginInteractor.PasswordResetListener, FacultyLoginInteractor.EmailVerificationListener {

    private View view;
    private FacultyLoginInteractor interactor;

    public FacultyLoginPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new FacultyLoginInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getUser(this);
    }

    @Override
    public void onDestroy() {
        if(view != null) {
            view = null;
        }
    }

    @Override
    public void requestLogin(String userID, String password) {
        if (userID.isEmpty()){
            view.setEmptyUserId();
        } else if (password.isEmpty()){
            view.setEmptyPass();
        } else {
            if (view != null) {
                view.showProgress();
                Objects.requireNonNull(view).setNullWrapper();
            }
            interactor.checkNetwork(userID, password, this);
        }
    }

    @Override
    public void sendPasswordResetLink(String email) {
        if (email.isEmpty()) {
            view.showAlertDialog("Empty Email", "You cannot submit an empty email");
            view.toastMessage("You cannot submit an empty email");
        } else {
            if (view != null) {
                view.showProgress();
            }
            interactor.sendPasswordReset(email, this);
        }

    }

    @Override
    public void sendEmailVerificationLink(String email) {
        if (email.isEmpty()) {
            view.showAlertDialog("Empty Email", "You cannot submit an empty email");
            view.toastMessage("You cannot submit an empty email");
        } else {
            if (view != null) {
                view.showProgress();
            }
            interactor.sendEmailVerification(email, this);
        }
    }

    @Override
    public void onSuccess(String successMessage) {
        Log.d("LoginListenerSuccess", "interactor Firebase Login");
        if (view != null) {
            view.hideProgress();
            assert view != null;
            view.toastMessage(successMessage);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        Log.d("onSyncListenerFailed", "interactor Firebase Login");
        if (view != null) {
            view.hideProgress();
            assert view != null;
            view.toastMessage(errorMessage);
        }
    }

    @Override
    public void onResetSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showAlertDialog(title, message);
        }
    }

    @Override
    public void onResetFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showAlertDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onVerificationSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showAlertDialog(title, message);
        }
    }

    @Override
    public void onVerificationFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showAlertDialog(errorTitle, errorMessage);
        }
    }
}

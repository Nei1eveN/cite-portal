package cp2.citeportalv1.presenters.login.student;

public interface StudentLoginInteractor {
    void checkNetwork(String userID, String password, OnSyncListener listener);

    void validateOnlineCredentials(String userID, String password, OnSyncListener listener);

    //    void validateOfflineCredentials(String studentID, String userID, String password, OnSyncListener listener);
    void getCurrentUser();

    interface OnSyncListener {
        void onSuccess(String successMessage);

        void onFailure(String errorMessage);
    }

    interface EmailVerificationListener {
        void onVerificationSuccess(String title, String message);

        void onVerificationFailure(String errorTitle, String errorMessage);
    }

    void sendEmailVerification(String email, EmailVerificationListener listener);

    interface PasswordResetListener {
        void onResetSuccess(String title, String message);

        void onResetFailure(String errorTitle, String errorMessage);
    }

    void sendPasswordReset(String email, PasswordResetListener listener);
}

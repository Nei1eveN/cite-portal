package cp2.citeportalv1.presenters.login.student;

public interface StudentLoginPresenter {
    interface StudentLoginView{
        void showProgress();
        void hideProgress();
        void toastMessage(String message);
        void setEmptyUserId();
        void setEmptyPass();
        void setNullWrapper();
        void showAlertDialog(String title, String message);
    }
    void onStart();
    void onDestroy();
    void requestLogin(String userID, String password);
    void sendPasswordResetLink(String email);
    void sendEmailVerificationLink(String email);
}

package cp2.citeportalv1.presenters.login.student;

import android.content.Context;
import android.util.Log;

import java.util.Objects;

public class StudentLoginPresenterImpl implements StudentLoginPresenter, StudentLoginInteractor.OnSyncListener, StudentLoginInteractor.EmailVerificationListener, StudentLoginInteractor.PasswordResetListener {
    private StudentLoginView view;
    private StudentLoginInteractor interactor;

    public StudentLoginPresenterImpl(StudentLoginView view, Context context) {
        this.view = view;
        this.interactor = new StudentLoginInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getCurrentUser();
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestLogin(String userID, String password) {
        if (userID.isEmpty()){
            view.setEmptyUserId();
        } else if (password.isEmpty()){
            view.setEmptyPass();
        } else {
            if (view != null) {
                view.showProgress();
            }
            Objects.requireNonNull(view).setNullWrapper();
            interactor.checkNetwork(userID, password, this);
        }
    }

    @Override
    public void sendPasswordResetLink(String email) {
        if (email.isEmpty()) {
            view.showAlertDialog("Empty Email", "You cannot submit an empty email");
            view.toastMessage("You cannot submit an empty email");
        } else {
            if (view != null) {
                view.showProgress();
            }
            interactor.sendPasswordReset(email, this);
        }

    }

    @Override
    public void sendEmailVerificationLink(String email) {
        if (email.isEmpty()) {
            view.showAlertDialog("Empty Email", "You cannot submit an empty email");
            view.toastMessage("You cannot submit an empty email");
        } else {
            if (view != null) {
                view.showProgress();
            }
            interactor.sendEmailVerification(email, this);
        }
    }

    @Override
    public void onSuccess(String successMessage) {
        if (view != null) {
            view.hideProgress();
            view.toastMessage(successMessage);
        }
        Log.d("onSyncListenerSuccess", "interactor Firebase Login");
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.toastMessage(errorMessage);
        }
        Log.d("onSyncListenerFailure", "interactor Firebase Login");
    }

    @Override
    public void onResetSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showAlertDialog(title, message);
        }
    }

    @Override
    public void onResetFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showAlertDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onVerificationSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showAlertDialog(title, message);
        }
    }

    @Override
    public void onVerificationFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showAlertDialog(errorTitle, errorMessage);
        }
    }
}

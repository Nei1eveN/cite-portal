package cp2.citeportalv1.presenters.login.faculty;

public interface FacultyLoginInteractor {
    interface LoginListener {
        void onSuccess(String successMessage);

        void onFailure(String errorMessage);
    }

    void checkNetwork(String email, String password, LoginListener listener);

    void getLoginCredentials(String email, String password, LoginListener listener);

    void getUser(LoginListener listener);

    interface EmailVerificationListener {
        void onVerificationSuccess(String title, String message);

        void onVerificationFailure(String errorTitle, String errorMessage);
    }

    void sendEmailVerification(String email, EmailVerificationListener listener);

    interface PasswordResetListener {
        void onResetSuccess(String title, String message);

        void onResetFailure(String errorTitle, String errorMessage);
    }

    void sendPasswordReset(String email, PasswordResetListener listener);
}

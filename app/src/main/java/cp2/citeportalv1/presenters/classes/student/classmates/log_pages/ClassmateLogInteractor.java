package cp2.citeportalv1.presenters.classes.student.classmates.log_pages;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface ClassmateLogInteractor {
    void getFragmentPages(LoadPagesListener listener);
    interface LoadPagesListener {
        void onSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }
}

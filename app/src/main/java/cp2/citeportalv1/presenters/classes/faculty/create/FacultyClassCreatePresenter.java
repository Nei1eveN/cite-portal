package cp2.citeportalv1.presenters.classes.faculty.create;

import java.util.ArrayList;
import java.util.List;

import cp2.citeportalv1.models.Course;

public interface FacultyClassCreatePresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void loadCoursesToSpinner(ArrayList<String> courseList);
        void loadCourseDescriptionToField(String courseDescription);
        void setEmptySectionWrapper(String emptySectionMessage);
        void setEmptyCourseCodeWrapper(String emptyCourseCodeMessage);
        void setEmptyCourseDescWrapper(String emptyCourseDescMessage);
        void setNullWrapper();
        void setSnackMessage(String message);
        void setToastMessage(String message);
    }
    void onDestroy();
    void requestCourses();
    void requestCourseDescription(String courseCode);
    void submitClassCredentials(String section, String courseCode, String courseDescription);
}

package cp2.citeportalv1.presenters.classes.student.search;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.Class;

public class StudClassSearchPresenterImpl implements StudClassSearchPresenter, StudClassSearchInteractor.SearchListener {
    private StudClassSearchPresenter.SearchView searchView;
    private StudClassSearchInteractor searchInteractor;

    public StudClassSearchPresenterImpl(SearchView searchView, Context context) {
        this.searchView = searchView;
        this.searchInteractor = new StudClassSearchInteractorImpl(context);
    }

    @Override
    public void onStart() {
        searchView.initViews();
        searchView.setEmptyState("Start searching a class by typing its Course Code.");
    }

    @Override
    public void onDestroy() {
        searchView = null;
    }

    @Override
    public void submitQuery(String stringQuery) {
        if (stringQuery.isEmpty()){
            searchView.setEmptyState("Start searching a class by typing its Course Code.");
        }
        else {
            searchInteractor.getDataFromQuery(stringQuery, this);
        }
    }

    @Override
    public void onSuccess(List<Class> classList) {
        searchView.setClassResult(classList);
    }

    @Override
    public void onFailure(String errorMessage) {
        searchView.setClassNotFound(errorMessage);
    }
}

package cp2.citeportalv1.presenters.classes.student.classmates;

import java.util.List;

import cp2.citeportalv1.models.Classmate;

public interface ClassmateListPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setClassmateFound(List<Classmate> studentList);
        void setNoClassmateFound(String noClassmatesMessage);
        void setSnackMessage(String snackMessage);
    }
    void onStart();
    void onDestroy();
    void findClassmates();
}

package cp2.citeportalv1.presenters.classes.student.user_pending_classmate_request;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.ClassmateRequest;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.views.student.profile.StudentProfileActivity;

import static cp2.citeportalv1.utils.Constants.FRIEND_REQUESTS;
import static cp2.citeportalv1.utils.Constants.STUDENTS;

class PendingClassmateRequestInteractorImpl implements PendingClassmateRequestInteractor {
    private Context context;

    PendingClassmateRequestInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getClassmateRequests(OnUserPendingClassmateRequestListener requestListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(FRIEND_REQUESTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.orderByChild("Request_Type").equalTo("sent").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<ClassmateRequest> requests = new ArrayList<>();
                    requests.clear();
                    Iterable<DataSnapshot> snapshots = dataSnapshot.getChildren();
                    for (DataSnapshot snapshot : snapshots){
                        DatabaseReference studentReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(snapshot.getKey()));
                        studentReference.keepSynced(true);

                        String requestType = snapshot.child("Request_Type").getValue(String.class);
                        Log.d("request Type", requestType);

                        studentReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                                    ClassmateRequest request = new ClassmateRequest(student.studentUId, requestType, student.getStudentImageURL(), student.getStudentID(), student.getfName(), student.getMidName(), student.getLastName(), student.getProgram(), student.getYearLvl()).withId(student.studentUId);
                                    requests.add(request);
                                    requestListener.onUserPendingSuccess(requests);
                                } else {
                                    databaseReference.child(Objects.requireNonNull(dataSnapshot.getKey())).removeValue().addOnCompleteListener(((Activity) context), task -> {
                                        if (task.isSuccessful()) {
                                            DatabaseReference deleteNonExistingUserReference = firebaseDatabase.getReference(FRIEND_REQUESTS).child(dataSnapshot.getKey());
                                            deleteNonExistingUserReference.removeValue().addOnCompleteListener(((Activity) context), task1 -> {
                                                if (task1.isSuccessful()) {
                                                    requestListener.onUserPendingFailure("The user you have sent a request on does not exists. The user might have deleted the account and created a new one. Page will be refreshed in a while.\n\nSorry for inconvenience.");
                                                    context.startActivity(new Intent(context, StudentProfileActivity.class));
                                                } else {
                                                    requestListener.onUserPendingFailure(Objects.requireNonNull(task1.getException()).getMessage());
                                                    context.startActivity(new Intent(context, StudentProfileActivity.class));
                                                }
                                            });

                                        } else {
                                            requestListener.onUserPendingFailure(Objects.requireNonNull(task.getException()).getMessage());
                                            context.startActivity(new Intent(context, StudentProfileActivity.class));
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                requestListener.onUserPendingFailure(databaseError.getMessage());
                            }
                        });
                    }
                }
                else {
                    requestListener.onUserPendingFailure("You don't have Pending Requests to your classmates. Click the icon above and search them.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                requestListener.onUserPendingFailure(databaseError.getMessage());
            }
        });
    }
}

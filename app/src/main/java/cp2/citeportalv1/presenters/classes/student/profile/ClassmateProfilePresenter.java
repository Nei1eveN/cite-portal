package cp2.citeportalv1.presenters.classes.student.profile;

import android.graphics.drawable.Drawable;

import cp2.citeportalv1.models.Student;

public interface ClassmateProfilePresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setToastMessage(String toastMessage);
        void setSnackMessage(String snackMessage);
        void setStudentProfile(Student student);
        void setButtonStatus(String buttonStatus, Drawable buttonDrawable);
        void setStudentVerifiedStatus();
        void setStudentNotVerifiedStatus();
        void hideAddButtonToSameOwner();
        void hideConfidentialOptionsToOtherUser();
    }
    void onStart(String toReceiverUid);
    void onDestroy();
    void refreshStudentInfo(String toReceiverUid);
    void submitClassmateRequest(String toReceiverUid);
}

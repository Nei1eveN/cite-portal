package cp2.citeportalv1.presenters.classes.student.profile;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.R;
import cp2.citeportalv1.models.Classmate;
import cp2.citeportalv1.models.ClassmateRequest;
import cp2.citeportalv1.models.Student;

import static cp2.citeportalv1.utils.Constants.STUDENTS;
import static cp2.citeportalv1.utils.Constants.combinedFormat;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

class ClassmateProfileInteractorImpl implements ClassmateProfileInteractor {
    private Context context;

    private String currentState; // = "not_friends"

    ClassmateProfileInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getStudentDetails(String toReceiverUid, StudentProfileListener profileListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(toReceiverUid);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));
                    profileListener.onProfileSuccess(student);
                } else {
                    profileListener.onProfileFailure("Student Profile does not exist");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                profileListener.onProfileFailure(databaseError.getMessage());
            }
        });
    }

    @Override
    public void getRequestUid(String toReceiverUid, ClassmateRequestListener requestListener) {

        if (!isNetworkAvailable(context)) {
            requestListener.onRequestFailure("Please check your internet connection");
        } else {
            String currentUserUid = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();

            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference("Friend_Request");
            databaseReference.keepSynced(true);

            DatabaseReference classmateReference = firebaseDatabase.getReference(STUDENTS).child(currentUserUid).child("Classmates");
            classmateReference.keepSynced(true);

            DatabaseReference acceptedClassmateReference = firebaseDatabase.getReference(STUDENTS).child(toReceiverUid).child("Classmates");
            acceptedClassmateReference.keepSynced(true);

            classmateReference.child(toReceiverUid).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {

                        /**
                         * FRIENDS STATE
                         * **/

                        classmateReference.child(toReceiverUid).removeValue().addOnSuccessListener(((Activity) context), aVoid ->
                                acceptedClassmateReference.child(currentUserUid).removeValue().addOnSuccessListener(((Activity) context), aVoid1 ->
                                        classmateReference.child(toReceiverUid).removeValue().addOnSuccessListener(((Activity) context), aVoid11 -> {

                                            DatabaseReference addClassmateRequestReference = firebaseDatabase.getReference(STUDENTS).child(toReceiverUid).child("ClassmateRequest").child(currentUserUid);
                                            addClassmateRequestReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    if (dataSnapshot.exists()) {
                                                        addClassmateRequestReference.removeValue().addOnSuccessListener(((Activity) context), aVoid2 -> {
                                                            currentState = "not_friends";
                                                            requestListener.onRequestSuccess("Classmate Removed", "Add as Classmate", context.getResources().getDrawable(R.drawable.ic_person_add));
                                                        }).addOnFailureListener(((Activity) context), e -> requestListener.onRequestFailure(e.getMessage()));
                                                    }
                                                    else {
                                                        currentState = "not_friends";
                                                        requestListener.onRequestSuccess("Classmate Removed", "Add as Classmate", context.getResources().getDrawable(R.drawable.ic_person_add));
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    requestListener.onRequestFailure(databaseError.getMessage());
                                                }
                                            });
                                        })))
                                .addOnFailureListener(((Activity) context), e ->
                                        requestListener.onRequestFailure(e.getMessage()));
                    } else {
                        databaseReference
                                .child(currentUserUid)
                                .child(toReceiverUid)
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            currentState = dataSnapshot.child("Request_Type").getValue(String.class);

                                            /**
                                             * CANCEL REQUEST STATE
                                             * **/

                                            if (Objects.requireNonNull(currentState).equals("sent")) {
                                                databaseReference.child(currentUserUid).child(toReceiverUid).removeValue().addOnSuccessListener(((Activity) context), aVoid ->
                                                        databaseReference.child(toReceiverUid).child(currentUserUid).removeValue().addOnSuccessListener(((Activity) context), aVoid14 -> {

                                                            DatabaseReference addClassmateRequestReference = firebaseDatabase.getReference(STUDENTS).child(toReceiverUid).child("ClassmateRequest").child(currentUserUid);
                                                            addClassmateRequestReference.keepSynced(true);

                                                            addClassmateRequestReference.removeValue().addOnSuccessListener(((Activity) context), aVoid18 -> {
                                                                currentState = "not_friends";
                                                                requestListener.onRequestSuccess("Request Cancelled", "Add Classmate", context.getResources().getDrawable(R.drawable.ic_person_add));
                                                            }).addOnFailureListener(((Activity) context), e -> requestListener.onRequestFailure(e.getMessage()));

                                                        }));
                                            } else if (currentState.equals("received")) {

                                                DatabaseReference acceptedProfilePreference = firebaseDatabase.getReference(STUDENTS).child(toReceiverUid);
                                                acceptedProfilePreference.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        if (dataSnapshot.exists()) {
                                                            Student student = dataSnapshot.getValue(Student.class);

                                                            Classmate acceptedClassmate = new Classmate(Objects.requireNonNull(student).getStudentImageURL(),
                                                                    student.getStudentID(), student.getfName(), student.getMidName(), student.getLastName(),
                                                                    student.getProgram(), student.getYearLvl(), student.getEmail(),
                                                                    combinedFormat.format(new Date()));

                                                            DatabaseReference acceptedProfilePreference = firebaseDatabase.getReference(STUDENTS).child(currentUserUid);
                                                            acceptedProfilePreference.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                    Student acceptingStudent = dataSnapshot.getValue(Student.class);

                                                                    Classmate acceptingClassmate = new Classmate(Objects.requireNonNull(acceptingStudent).getStudentImageURL(),
                                                                            acceptingStudent.getStudentID(), acceptingStudent.getfName(), acceptingStudent.getMidName(), acceptingStudent.getLastName(),
                                                                            acceptingStudent.getProgram(), acceptingStudent.getYearLvl(), acceptingStudent.getEmail(),
                                                                            combinedFormat.format(new Date()));

                                                                    classmateReference.child(toReceiverUid).setValue(acceptedClassmate).addOnSuccessListener(((Activity) context), aVoid ->
                                                                            acceptedClassmateReference.child(currentUserUid).setValue(acceptingClassmate).addOnSuccessListener(((Activity) context), aVoid15 ->
                                                                                    databaseReference.child(currentUserUid).child(toReceiverUid).setValue(null).addOnSuccessListener(((Activity) context), aVoid13 ->
                                                                                            databaseReference.child(toReceiverUid).child(currentUserUid).setValue(null).addOnSuccessListener(((Activity) context), aVoid12 -> {
                                                                                                DatabaseReference addClassmateRequestReference = firebaseDatabase.getReference(STUDENTS).child(currentUserUid).child("ClassmateRequest").child(toReceiverUid);
                                                                                                addClassmateRequestReference.keepSynced(true);

                                                                                                addClassmateRequestReference.setValue(null).addOnSuccessListener(((Activity) context), aVoid17 -> {
                                                                                                    currentState = "friends";
                                                                                                    requestListener.onRequestSuccess(
                                                                                                            "Request Accepted",
                                                                                                            "Remove Classmate",
                                                                                                            context.getResources().getDrawable(R.drawable.ic_cancel));
                                                                                                }).addOnFailureListener(((Activity) context), e -> requestListener.onRequestFailure(e.getMessage()));
                                                                                            }))))
                                                                            .addOnFailureListener(((Activity) context), e ->
                                                                                    requestListener.onRequestFailure(e.getMessage()));
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                    requestListener.onRequestFailure(databaseError.getMessage());
                                                                }
                                                            });
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        requestListener.onRequestFailure(databaseError.getMessage());
                                                    }
                                                });
                                            }
                                        } else {
                                            Log.d("requestNotExisting", "falling condition");

                                            /**
                                             * NOT FRIENDS STATE
                                             * **/

                                            databaseReference.child(currentUserUid).child(toReceiverUid).child("Request_Type").setValue("sent").addOnCompleteListener(((Activity) context), task -> {
                                                if (task.isSuccessful()) {
                                                    databaseReference.child(toReceiverUid).child(currentUserUid).child("Request_Type").setValue("received").addOnSuccessListener(((Activity) context), aVoid -> {

                                                        DatabaseReference addClassmateRequestReference = firebaseDatabase.getReference(STUDENTS).child(currentUserUid);
                                                        addClassmateRequestReference.keepSynced(true);

                                                        addClassmateRequestReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                Student student = Objects.requireNonNull(dataSnapshot.getValue(Student.class)).withId(Objects.requireNonNull(dataSnapshot.getKey()));

                                                                ClassmateRequest classmateRequest = new ClassmateRequest(student.studentUId, "received", student.getStudentImageURL(), student.getStudentID(), student.getfName(), student.getMidName(), student.getLastName(), student.getProgram(), student.getYearLvl());

                                                                DatabaseReference addDetailReference = firebaseDatabase.getReference(STUDENTS).child(toReceiverUid).child("ClassmateRequest");
                                                                addDetailReference.keepSynced(true);

                                                                addDetailReference.child(currentUserUid).setValue(classmateRequest).addOnSuccessListener(((Activity) context), aVoid16 -> {
                                                                    currentState = "sent";

                                                                    requestListener.onRequestSuccess(
                                                                            "Request Sent",
                                                                            "Cancel Request",
                                                                            context.getResources().getDrawable(R.drawable.ic_cancel));
                                                                }).addOnFailureListener(((Activity) context), e -> requestListener.onRequestFailure(e.getMessage()));
                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                requestListener.onRequestFailure(databaseError.getMessage());
                                                            }
                                                        });
                                                    });
                                                } else {
                                                    requestListener.onRequestFailure("Request Error");
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        Log.d("databaseRequestError", databaseError.getMessage());
                                    }
                                });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    requestListener.onRequestFailure(databaseError.getMessage());
                }
            });
        }

    }

    @Override
    public void compareReceiverUid(String toReceiverUid, OnCompareClassmatesListener compareClassmatesListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.d("ownerUid", dataSnapshot.getKey());
                    Log.d("studentUid", toReceiverUid);
                    String ownerUid = dataSnapshot.getKey();
                    if (toReceiverUid.equals(ownerUid)) {
                        Log.d("owner==Viewer", "viewer has the same uid to profile");
                        compareClassmatesListener.profileSameUid();
                    } else {
                        Log.d("Viewer!=owner", "viewer does not have the same uid to profile");

                        compareClassmatesListener.profileNotSameUid();

                        DatabaseReference classmateReference = databaseReference.child("Classmates");
                        classmateReference.keepSynced(true);

                        classmateReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    if (dataSnapshot.hasChild(toReceiverUid)) {
                                        compareClassmatesListener.ifClassmates(
                                                "Remove Classmate",
                                                context.getResources().getDrawable(R.drawable.ic_cancel));
                                    } else {
                                        DatabaseReference friendRequest = firebaseDatabase.getReference("Friend_Request");
                                        friendRequest.keepSynced(true);
                                        friendRequest
                                                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser().getUid()))
                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        if (dataSnapshot.hasChild(toReceiverUid)) {
                                                            String request_type =
                                                                    dataSnapshot.child(toReceiverUid).child("Request_Type").getValue(String.class);
                                                            assert request_type != null;
                                                            if (request_type.equals("received")) {
                                                                compareClassmatesListener.ifNotClassmates(
                                                                        "Accept Request",
                                                                        context.getResources().getDrawable(R.drawable.ic_person_add));
                                                            } else if (request_type.equals("sent")) {
                                                                DatabaseReference addClassmateRequestReference = firebaseDatabase.getReference(STUDENTS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("ClassmateRequest").child(toReceiverUid);
                                                                addClassmateRequestReference.keepSynced(true);

                                                                addClassmateRequestReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        if (dataSnapshot.exists()){
                                                                            if (isNetworkAvailable(context)){
                                                                                addClassmateRequestReference.setValue(null).addOnCompleteListener(((Activity) context), new OnCompleteListener<Void>() {
                                                                                    @Override
                                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                                        if (task.isSuccessful()){
                                                                                            compareClassmatesListener.ifNotClassmates(
                                                                                                    "Cancel Request",
                                                                                                    context.getResources().getDrawable(R.drawable.ic_cancel));
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }
                                                                            else {
                                                                                compareClassmatesListener.ifNotClassmates(
                                                                                        "Cancel Request",
                                                                                        context.getResources().getDrawable(R.drawable.ic_cancel));
                                                                            }
                                                                        }
                                                                        else {
                                                                            compareClassmatesListener.ifNotClassmates(
                                                                                    "Cancel Request",
                                                                                    context.getResources().getDrawable(R.drawable.ic_cancel));
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                        Log.d("Database Error", "Code: "+databaseError.getCode()+"\nMessage: "+databaseError.getMessage()+"\nDetails: "+databaseError.getDetails());
                                                                    }
                                                                });
                                                            }
                                                        } else {
                                                            friendRequest
                                                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                            if (dataSnapshot.hasChild(toReceiverUid)) {
                                                                                currentState = "friends";
                                                                                compareClassmatesListener.ifClassmates(
                                                                                        "Remove Classmate",
                                                                                        context.getResources().getDrawable(R.drawable.ic_cancel));
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                            Log.d("friendCheckingError", databaseError.getMessage());
                                                                        }
                                                                    });
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        Log.d("comparisonError", databaseError.getMessage());
                                                    }
                                                });
                                    }
                                } else {
                                    DatabaseReference friendRequest = firebaseDatabase.getReference("Friend_Request");
                                    friendRequest.keepSynced(true);
                                    friendRequest
                                            .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser().getUid()))
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    if (dataSnapshot.hasChild(toReceiverUid)) {
                                                        String request_type =
                                                                dataSnapshot
                                                                        .child(toReceiverUid)
                                                                        .child("Request_Type")
                                                                        .getValue(String.class);
                                                        assert request_type != null;
                                                        if (request_type.equals("received")) {
                                                            compareClassmatesListener.ifNotClassmates(
                                                                    "Accept Request",
                                                                    context.getResources().getDrawable(R.drawable.ic_person_add));
                                                        } else if (request_type.equals("sent")) {
                                                            compareClassmatesListener.ifNotClassmates(
                                                                    "Cancel Request",
                                                                    context.getResources().getDrawable(R.drawable.ic_cancel));
                                                        }
                                                    } else {
                                                        friendRequest
                                                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        if (dataSnapshot.hasChild(toReceiverUid)) {
                                                                            currentState = "friends";
                                                                            compareClassmatesListener.ifClassmates(
                                                                                    "Remove Classmate",
                                                                                    context.getResources().getDrawable(R.drawable.ic_cancel));
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                        Log.d("friendCheckingError", databaseError.getMessage());
                                                                    }
                                                                });
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    Log.d("comparisonError", databaseError.getMessage());
                                                }
                                            });
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Log.d("classmatesError", databaseError.getMessage());
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("databaseErrorCompareUid", databaseError.getMessage());
            }
        });
    }

    @Override
    public void verifyReceiverUid(String toReceiverUid, OnEmailVerificationCheckListener verificationCheckListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(toReceiverUid).child("valid");
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.d("snapshotValue", String.valueOf(dataSnapshot.getValue()));
                    boolean valid = Boolean.valueOf(String.valueOf(dataSnapshot.getValue()));
                    if (valid) {
                        Log.d("emailVerification", "email is valid");
                        verificationCheckListener.isVerified();
                    } else {
                        Log.d("emailVerification", "email is not valid");
                        verificationCheckListener.isNotVerified();
                    }
                } else {
                    Log.d("snapshotNotExist", "valid value does not exist");
                    verificationCheckListener.isNotVerified();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                verificationCheckListener.onEmailVerificationError(databaseError.getMessage());
            }
        });
    }
}

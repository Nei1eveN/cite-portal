package cp2.citeportalv1.presenters.classes.faculty.create;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cp2.citeportalv1.R;
import cp2.citeportalv1.utils.Constants;
import cp2.citeportalv1.views.faculty.FacultyClassesActivity;

import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

public class FacultyClassCreateInteractorImpl implements FacultyClassCreateInteractor {
    private Context context;
    private ArrayList<String> stringArrayList = new ArrayList<>();

    FacultyClassCreateInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void loadCourses(OnLoadingCourseListener onLoadingCourseListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(Constants.COURSES);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    String courseCodes = data.child("courseCode").getValue(String.class);
                    Log.d("courseCodes", courseCodes);
                    stringArrayList.add(courseCodes);
                }
                try {
                    onLoadingCourseListener.onLoadingSuccess(stringArrayList);
                } catch (NullPointerException e) {
//                    onLoadingCourseListener.onLoadingFailure(e.getMessage());
                    Toast.makeText(context, "Changes applied, you will be redirected to Class List", Toast.LENGTH_SHORT).show();
                    context.startActivity(new Intent(context, FacultyClassesActivity.class));
                    ((Activity) context).finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                onLoadingCourseListener.onLoadingFailure(databaseError.getMessage());
            }
        });
    }

    @Override
    public void getSelectedCourseCode(String courseCode, OnSelectedCourseListener onSelectedCourseListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(Constants.COURSES);
        databaseReference.orderByChild("courseCode").equalTo(courseCode).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        String courseDescription = data.child("courseDesc").getValue(String.class);
                        String courseCode = data.child("courseCode").getValue(String.class);
                        Log.d("courseCode", courseCode);
                        Log.d("courseDesc", courseDescription);
                        Log.d("course", courseCode + " " + courseDescription);
                        onSelectedCourseListener.onSelectedCourseSuccess(courseDescription);
                    }
                } else {
                    onSelectedCourseListener.onSelectedCourseFailure("Something went wrong");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                onSelectedCourseListener.onSelectedCourseFailure(databaseError.getMessage());
            }
        });
    }

    @Override
    public void registerClassCredentials(String section, String courseCode, String courseDescription, OnClassCreateListener onClassCreateListener) {
        Log.d("interactor--section", section);
        Log.d("interactor--courseCode", courseCode);
        Log.d("interactor--courseDesc", courseDescription);

        if (isNetworkAvailable(context)){
            Map<String, String> courseMap = new HashMap<>();
            courseMap.put("course", courseCode+" "+courseDescription);
            courseMap.put("courseCode", courseCode);
            courseMap.put("courseDescription", courseDescription);
            courseMap.put("createdBy", FirebaseAuth.getInstance().getCurrentUser().getUid());
            courseMap.put("section", section);

            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseDatabase.getReference("Classes");

            databaseReference.child(courseCode+" - "+section)
                    .setValue(courseMap).addOnCompleteListener(((Activity) context), task -> {
                if (task.isSuccessful()){
                    onClassCreateListener.onCreateSuccess(courseCode+" - "+section+" Class Created");
                    context.startActivity(new Intent(context, FacultyClassesActivity.class));
                    ((Activity) context).finish();
                    ((Activity) context).overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
                else {
                    onClassCreateListener.onCreateFailure("Something went wrong");
                }
            });
        }
        else {
            onClassCreateListener.onCreateFailure("Please check your internet connection");
        }

    }
}

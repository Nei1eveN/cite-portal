package cp2.citeportalv1.presenters.classes.student.classmates.log_pages;

import android.content.Context;

import cp2.citeportalv1.adapters.ViewPagerAdapter;


public class ClassmateLogPresenterImpl implements ClassmateLogPresenter, ClassmateLogInteractor.LoadPagesListener {
    private ClassmateLogPresenter.View view;
    private ClassmateLogInteractor interactor;

    public ClassmateLogPresenterImpl(ClassmateLogPresenter.View view, Context context) {
        this.view = view;
        this.interactor = new ClassmateLogInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getFragmentPages(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void onSuccess(ViewPagerAdapter viewPagerAdapter, int numberOfPages) {
        if (view != null) {
            view.initializeTabs(viewPagerAdapter, numberOfPages);
        }
    }
}

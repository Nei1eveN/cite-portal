package cp2.citeportalv1.presenters.classes.student.classmate_request;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.ClassmateRequest;

public class ClassmateRequestPresenterImpl implements ClassmateRequestPresenter, ClassmateRequestInteractor.OnClassmateRequestListener {
    private ClassmateRequestPresenter.View view;
    private ClassmateRequestInteractor interactor;

    public ClassmateRequestPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new ClassmateRequestInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        interactor.getClassmateRequests(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void findRequests() {
        if (view != null){
            view.showProgress();
        }
        interactor.getClassmateRequests(this);
    }

    @Override
    public void onRequestSuccess(List<ClassmateRequest> classmates) {
        if (view != null){
            view.hideProgress();
            view.setClassmateRequests(classmates);
        }
    }

    @Override
    public void onRequestFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

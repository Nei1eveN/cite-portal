package cp2.citeportalv1.presenters.classes.student.search;

import android.content.Context;
import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cp2.citeportalv1.models.Class;

public class StudClassSearchInteractorImpl implements StudClassSearchInteractor {
    private Context context;

    StudClassSearchInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getDataFromQuery(String stringQuery, SearchListener searchListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference("Classes");
        Query query = databaseReference.orderByChild("courseCode").startAt(stringQuery).endAt(stringQuery+"\uf8ff");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<Class> aClasses = new ArrayList<>();
                    aClasses.clear();
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                        Class facultyClass = dataSnapshot1.getValue(Class.class).withId(databaseReference.push().getKey());
                        aClasses.add(facultyClass);
                        searchListener.onSuccess(aClasses);
                    }
                }
                else {
                    searchListener.onFailure(" Class for "+stringQuery+" is not found.\n\nMaybe, the Class Section for that Course you are looking for is not yet created.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

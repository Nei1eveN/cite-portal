package cp2.citeportalv1.presenters.classes.faculty.display;

import java.util.List;

import cp2.citeportalv1.models.Class;

public interface FacultyClassInteractor {
    void getData(FacultyClassListener listener);

    interface FacultyClassListener {
        void onFailure(String errorMessage);

        void onSuccess(List<Class> createdByFacultyUid);
    }
}

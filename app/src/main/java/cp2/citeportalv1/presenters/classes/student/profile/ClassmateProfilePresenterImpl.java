package cp2.citeportalv1.presenters.classes.student.profile;

import android.content.Context;
import android.graphics.drawable.Drawable;

import cp2.citeportalv1.models.Student;

public class ClassmateProfilePresenterImpl implements
        ClassmateProfilePresenter, ClassmateProfileInteractor.ClassmateRequestListener,
        ClassmateProfileInteractor.OnCompareClassmatesListener, ClassmateProfileInteractor.OnEmailVerificationCheckListener,
        ClassmateProfileInteractor.StudentProfileListener {
    private ClassmateProfilePresenter.View view;
    private ClassmateProfileInteractor profileInteractor;

    public ClassmateProfilePresenterImpl(View view, Context context) {
        this.view = view;
        this.profileInteractor = new ClassmateProfileInteractorImpl(context);
    }

    @Override
    public void onStart(String toReceiverUid) {
        if (view != null){
            view.showProgress();
        }
        profileInteractor.compareReceiverUid(toReceiverUid, this);
        profileInteractor.verifyReceiverUid(toReceiverUid, this);
        profileInteractor.getStudentDetails(toReceiverUid, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void refreshStudentInfo(String toReceiverUid) {
        if (view != null){
            view.showProgress();
        }
        profileInteractor.compareReceiverUid(toReceiverUid, this);
        profileInteractor.verifyReceiverUid(toReceiverUid, this);
        profileInteractor.getStudentDetails(toReceiverUid, this);
    }

    @Override
    public void submitClassmateRequest(String toReceiverUid) {
        if (view != null) {
            view.showProgress();
        }
        profileInteractor.getRequestUid(toReceiverUid, this);
    }

    @Override
    public void onRequestSuccess(String successMessage, String changeButtonStatus, Drawable changeButtonDrawable) {
        if (view != null) {
            view.hideProgress();
            view.setSnackMessage(successMessage);
            view.setButtonStatus(changeButtonStatus, changeButtonDrawable);
        }

    }

    @Override
    public void onRequestFailure(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setSnackMessage(errorMessage);
        }
    }

    @Override
    public void profileSameUid() {
        if (view != null) {
            view.hideProgress();
            view.hideAddButtonToSameOwner();
        }
    }

    @Override
    public void profileNotSameUid() {
        if (view != null) {
            view.hideProgress();
            view.hideConfidentialOptionsToOtherUser();
        }
    }

    @Override
    public void ifClassmates(String changeButtonStatus, Drawable changeButtonDrawable) {
        if (view != null) {
            view.hideProgress();
            view.setButtonStatus(changeButtonStatus, changeButtonDrawable);
        }
    }

    @Override
    public void ifNotClassmates(String doNotChangeButtonStatus, Drawable doNotChangeButtonDrawable) {
        if (view != null) {
            view.hideProgress();
            view.setButtonStatus(doNotChangeButtonStatus, doNotChangeButtonDrawable);
        }
    }

    @Override
    public void isVerified() {
        if (view != null) {
            view.hideProgress();
            view.setStudentVerifiedStatus();
        }
    }

    @Override
    public void isNotVerified() {
        if (view != null) {
            view.hideProgress();
            view.setStudentNotVerifiedStatus();
        }
    }

    @Override
    public void onEmailVerificationError(String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.setSnackMessage(errorMessage);
        }
    }

    @Override
    public void onProfileSuccess(Student student) {
        if (view != null) {
            view.hideProgress();
            view.setStudentProfile(student);
        }
    }

    @Override
    public void onProfileFailure(String errorMessage) {
        if (view != null){
            view.hideProgress();
            view.setSnackMessage(errorMessage);
        }
    }
}

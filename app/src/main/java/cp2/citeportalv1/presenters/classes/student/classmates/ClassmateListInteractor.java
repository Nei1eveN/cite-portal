package cp2.citeportalv1.presenters.classes.student.classmates;

import java.util.List;

import cp2.citeportalv1.models.Classmate;

public interface ClassmateListInteractor {

    void getClassmates(ClassmateListsListener listener);

    interface ClassmateListsListener {
        void onSuccess(List<Classmate> classmates);
        void onFailure(String errorMessage);
    }
}

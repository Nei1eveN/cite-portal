package cp2.citeportalv1.presenters.classes.faculty.create;

import android.content.Context;

import java.util.ArrayList;

public class FacultyClassCreatePresenterImpl implements FacultyClassCreatePresenter,
        FacultyClassCreateInteractor.OnLoadingCourseListener,
        FacultyClassCreateInteractor.OnSelectedCourseListener, FacultyClassCreateInteractor.OnClassCreateListener {
    private FacultyClassCreatePresenter.View view;
    private FacultyClassCreateInteractor facultyClassCreateInteractor;

    public FacultyClassCreatePresenterImpl(View view, Context context) {
        this.view = view;
        this.facultyClassCreateInteractor = new FacultyClassCreateInteractorImpl(context);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void requestCourses() {
        view.showProgress();
        facultyClassCreateInteractor.loadCourses(this);
    }

    @Override
    public void requestCourseDescription(String courseCode) {
//        view.showProgress();
        facultyClassCreateInteractor.getSelectedCourseCode(courseCode, this);
    }

    @Override
    public void submitClassCredentials(String section, String courseCode, String courseDescription) {
        if (section.isEmpty()) {
            view.setEmptySectionWrapper("Required Field");
            view.setSnackMessage("Please fill in the required fields");
        }
//        else if (!".*[ACTCSISIT]*.".matches(section)) {
//            view.showSnackMessage("Section must contain ACT, CS, IS, or IT");
//        }
//        else if (!section.contains("CS")){
//            view.showSnackMessage("Section must contain ACT, CS, IS, or IT");
//        }
//        else if (!section.contains("IS")){
//            view.showSnackMessage("Section must contain ACT, CS, IS, or IT");
//        }
//        else if (!section.contains("ACT")){
//            view.showSnackMessage("Section must contain ACT, CS, IS, or IT");
//        }
        else if (courseCode.isEmpty()) {
            view.setEmptyCourseCodeWrapper("Please select a Course Code");
        } else if (courseDescription.isEmpty()) {
            view.setEmptyCourseDescWrapper("To fill this field, please select a Course Code");
        }
        else {
            view.showProgress();
            view.setNullWrapper();
            facultyClassCreateInteractor.registerClassCredentials(section, courseCode, courseDescription, this);
        }
    }

    @Override
    public void onLoadingSuccess(ArrayList<String> courseList) {
        view.hideProgress();
        view.loadCoursesToSpinner(courseList);
    }

    @Override
    public void onLoadingFailure(String errorMessage) {
        view.hideProgress();
        view.setSnackMessage(errorMessage);
    }

    @Override
    public void onSelectedCourseSuccess(String courseDescription) {
//        view.hideProgress();
        view.loadCourseDescriptionToField(courseDescription);
//        view.showSnackMessage(courseDescription);
    }

    @Override
    public void onSelectedCourseFailure(String errorMessage) {
//        view.hideProgress();
        view.setSnackMessage(errorMessage);
    }

    @Override
    public void onCreateSuccess(String successMessage) {
        view.hideProgress();
        view.setToastMessage(successMessage);
    }

    @Override
    public void onCreateFailure(String errorMessage) {
        view.hideProgress();
        view.setSnackMessage(errorMessage);
    }
}

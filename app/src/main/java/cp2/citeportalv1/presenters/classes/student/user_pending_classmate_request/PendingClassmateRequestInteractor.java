package cp2.citeportalv1.presenters.classes.student.user_pending_classmate_request;

import java.util.List;

import cp2.citeportalv1.models.ClassmateRequest;

public interface PendingClassmateRequestInteractor {
    void getClassmateRequests(OnUserPendingClassmateRequestListener requestListener);

    interface OnUserPendingClassmateRequestListener {
        void onUserPendingSuccess(List<ClassmateRequest> students);
        void onUserPendingFailure(String errorMessage);
    }
}

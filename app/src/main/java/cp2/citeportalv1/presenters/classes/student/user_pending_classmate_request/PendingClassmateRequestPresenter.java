package cp2.citeportalv1.presenters.classes.student.user_pending_classmate_request;

import java.util.List;

import cp2.citeportalv1.models.ClassmateRequest;
import cp2.citeportalv1.models.Student;

public interface PendingClassmateRequestPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setUserPendingRequests(List<ClassmateRequest> classmateRequests);
        void setEmptyState(String emptyStateMessage);
        void setSnackMessage(String snackMessage);
    }
    void onStart();
    void onDestroy();
    void findUserPendingRequests();
}

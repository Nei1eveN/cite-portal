package cp2.citeportalv1.presenters.classes.student.search.classmate;

import java.util.List;

import cp2.citeportalv1.models.Student;

public interface ClassmateSearchPresenter {
    interface View {
        void initViews();
        void setEmptyState(String emptyStateMessage);
        void setClassmateFound(List<Student> studentList);
        void setNoClassmateFound(String noClassmateFoundMessage);
    }
    void onStart();
    void onDestroy();
    void submitQuery(String query);
}

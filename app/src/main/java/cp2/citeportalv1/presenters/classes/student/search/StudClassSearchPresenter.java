package cp2.citeportalv1.presenters.classes.student.search;

import java.util.List;

import cp2.citeportalv1.models.Class;

public interface StudClassSearchPresenter {
    interface SearchView {
        void initViews();

        void showProgress();

        void hideProgress();

        void setEmptyState(String emptyCaption);

        void setClassResult(List<Class> classList);

        void setClassNotFound(String notFoundMessage);
    }

    void onStart();

    void onDestroy();

    void submitQuery(String stringQuery);
}

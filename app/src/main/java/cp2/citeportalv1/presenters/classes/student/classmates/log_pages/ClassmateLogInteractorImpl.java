package cp2.citeportalv1.presenters.classes.student.classmates.log_pages;

import android.content.Context;

import androidx.fragment.app.FragmentActivity;
import cp2.citeportalv1.adapters.ViewPagerAdapter;
import cp2.citeportalv1.views.student.classmate.ClassmateRequestFragment;
import cp2.citeportalv1.views.student.classmate.ClassmateUserPendingRequestFragment;
import cp2.citeportalv1.views.student.classmate.ClassmateFragment;

class ClassmateLogInteractorImpl implements ClassmateLogInteractor {
    private Context context;

    ClassmateLogInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getFragmentPages(LoadPagesListener listener) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(((FragmentActivity) context).getSupportFragmentManager());

        viewPagerAdapter.addFragment(new ClassmateFragment(), "Schoolmates");
        viewPagerAdapter.addFragment(new ClassmateRequestFragment(), "Received Requests");
        viewPagerAdapter.addFragment(new ClassmateUserPendingRequestFragment(), "Sent Requests");

        listener.onSuccess(viewPagerAdapter, 3);
    }
}

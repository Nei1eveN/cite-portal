package cp2.citeportalv1.presenters.classes.student.classmates.log_pages;

import cp2.citeportalv1.adapters.ViewPagerAdapter;

public interface ClassmateLogPresenter {
    interface View {
        void initializeTabs(ViewPagerAdapter viewPagerAdapter, int numberOfPages);
    }
    void onStart();
    void onDestroy();
}

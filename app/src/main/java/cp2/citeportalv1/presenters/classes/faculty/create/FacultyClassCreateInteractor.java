package cp2.citeportalv1.presenters.classes.faculty.create;

import java.util.ArrayList;

public interface FacultyClassCreateInteractor {
    void loadCourses(OnLoadingCourseListener onLoadingCourseListener);
    void getSelectedCourseCode(String courseCode, OnSelectedCourseListener onSelectedCourseListener);
    void registerClassCredentials(String section, String courseCode, String courseDescription, OnClassCreateListener onClassCreateListener);

    interface OnLoadingCourseListener {
        void onLoadingSuccess(ArrayList<String> courseCodes);
        void onLoadingFailure(String errorMessage);
    }
    interface OnSelectedCourseListener {
        void onSelectedCourseSuccess(String courseDescription);
        void onSelectedCourseFailure(String errorMessage);
    }
    interface OnClassCreateListener {
        void onCreateSuccess(String successMessage);
        void onCreateFailure(String errorMessage);
    }
}

package cp2.citeportalv1.presenters.classes.faculty.display;

import java.util.List;

import cp2.citeportalv1.models.Class;

public interface FacultyClassPresenter {
    interface FacultyClassView {
        void setUpFabAddClass();

        void setUpEmptyImageAddClass();

        void setErrorState(String errorMessage);

        void setClassesCreatedByFaculty(List<Class> createdByFaculty);
    }

    void onStart();

    void onDestroy();
}

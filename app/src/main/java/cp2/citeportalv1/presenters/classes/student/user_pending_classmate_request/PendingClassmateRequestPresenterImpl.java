package cp2.citeportalv1.presenters.classes.student.user_pending_classmate_request;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.ClassmateRequest;

public class PendingClassmateRequestPresenterImpl implements PendingClassmateRequestPresenter, PendingClassmateRequestInteractor.OnUserPendingClassmateRequestListener {
    private PendingClassmateRequestPresenter.View view;
    private PendingClassmateRequestInteractor interactor;

    public PendingClassmateRequestPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new PendingClassmateRequestInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        interactor.getClassmateRequests(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void findUserPendingRequests() {
        if (view != null){
            view.showProgress();
        }
        interactor.getClassmateRequests(this);
    }

    @Override
    public void onUserPendingSuccess(List<ClassmateRequest> students) {
        if (view != null){
            view.hideProgress();
            view.setUserPendingRequests(students);
        }
    }

    @Override
    public void onUserPendingFailure(String errorMessage) {
        if (view != null){
            view.hideProgress();
            view.setEmptyState(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

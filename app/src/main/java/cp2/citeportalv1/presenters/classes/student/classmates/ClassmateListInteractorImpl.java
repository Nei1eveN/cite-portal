package cp2.citeportalv1.presenters.classes.student.classmates;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Classmate;

import static cp2.citeportalv1.utils.Constants.STUDENTS;

class ClassmateListInteractorImpl implements ClassmateListInteractor {
    private Context context;

    ClassmateListInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getClassmates(ClassmateListsListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference classmateReference = firebaseDatabase
                .getReference(STUDENTS)
                .child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                .child("Classmates");

        classmateReference.keepSynced(true);

        classmateReference.orderByChild("lastName").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<Classmate> classmates = new ArrayList<>();
                    classmates.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Log.d("classmateKey", snapshot.getKey());
                        String classmate_since = snapshot.child("classmate_since").getValue(String.class);
                        Log.d("accepted_date", classmate_since);

                        Classmate classmate = Objects.requireNonNull(snapshot.getValue(Classmate.class)).withId(snapshot.getKey());
                        classmates.add(classmate);
                    }
                    listener.onSuccess(classmates);

                }
                else {
                    listener.onFailure("It seems like you have not added your classmates yet. Click the icon above this text to search your classmates.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

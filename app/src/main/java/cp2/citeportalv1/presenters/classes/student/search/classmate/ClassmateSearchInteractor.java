package cp2.citeportalv1.presenters.classes.student.search.classmate;

import java.util.List;

import cp2.citeportalv1.models.Student;

public interface ClassmateSearchInteractor {
    void getDataFromQuery(String stringQuery, ClassmateSearchListener searchListener);

    interface ClassmateSearchListener {
        void onSuccess(List<Student> studentList);

        void onFailure(String errorMessage);
    }
}

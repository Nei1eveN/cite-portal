package cp2.citeportalv1.presenters.classes.student.classmates;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.Classmate;

public class ClassmateListPresenterImpl implements ClassmateListPresenter, ClassmateListInteractor.ClassmateListsListener {
    private ClassmateListPresenter.View view;
    private ClassmateListInteractor interactor;

    public ClassmateListPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new ClassmateListInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null){
            view.showProgress();
        }
        interactor.getClassmates(this);
    }

    @Override
    public void onDestroy() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void findClassmates() {
        if (view != null){
            view.showProgress();
        }
        interactor.getClassmates(this);
    }

    @Override
    public void onSuccess(List<Classmate> classmates) {
        if (view != null){
            view.hideProgress();
            view.setClassmateFound(classmates);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        if (view != null){
            view.hideProgress();
            view.setNoClassmateFound(errorMessage);
            view.setSnackMessage(errorMessage);
        }
    }
}

package cp2.citeportalv1.presenters.classes.student.classmate_request;

import java.util.List;

import cp2.citeportalv1.models.ClassmateRequest;

public interface ClassmateRequestPresenter {
    interface View {
        void showProgress();
        void hideProgress();
        void setClassmateRequests(List<ClassmateRequest> classmateRequests);
        void setEmptyState(String emptyStateMessage);
        void setSnackMessage(String snackMessage);
    }
    void onStart();
    void onDestroy();
    void findRequests();
}

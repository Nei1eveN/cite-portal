package cp2.citeportalv1.presenters.classes.student.classmate_request;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.ClassmateRequest;

import static cp2.citeportalv1.utils.Constants.CLASSMATE_REQUESTS;
import static cp2.citeportalv1.utils.Constants.STUDENTS;

class ClassmateRequestInteractorImpl implements ClassmateRequestInteractor {
    private Context context;

    ClassmateRequestInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getClassmateRequests(OnClassmateRequestListener requestListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS).child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child(CLASSMATE_REQUESTS);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    List<ClassmateRequest> requests = new ArrayList<>();
                    requests.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                        ClassmateRequest classmateRequest = Objects.requireNonNull(snapshot.getValue(ClassmateRequest.class)).withId(snapshot.getKey());
                        requests.add(classmateRequest);
                        requestListener.onRequestSuccess(requests);
                    }
                }
                else {
                    requestListener.onRequestFailure("There are no Requests so far. Click the icon above to find your classmates.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                requestListener.onRequestFailure(databaseError.getMessage());
            }
        });
    }
}

package cp2.citeportalv1.presenters.classes.student.search.classmate;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import cp2.citeportalv1.models.Student;

import static cp2.citeportalv1.utils.Constants.STUDENTS;

public class ClassmateSearchInteractorImpl implements ClassmateSearchInteractor {
    private Context context;

    ClassmateSearchInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getDataFromQuery(String stringQuery, ClassmateSearchListener searchListener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(STUDENTS);
        databaseReference.keepSynced(true);
        Query query = databaseReference.orderByChild("lastName").startAt(stringQuery).endAt(stringQuery+"\uf8ff"); //.equalTo(stringQuery)
        query.keepSynced(true);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Log.d("key", dataSnapshot.getKey());
                    List<Student> studentList = new ArrayList<>();
                    studentList.clear();
                    Iterable<DataSnapshot> snapshots = dataSnapshot.getChildren();
                    for (DataSnapshot snapshot : snapshots){
                        Log.d("studentUid", snapshot.getKey());
                        String studentUid = snapshot.getKey();
                        assert studentUid != null;
                        Student student = Objects.requireNonNull(snapshot.getValue(Student.class)).withId(studentUid);
                        studentList.add(student);
                        searchListener.onSuccess(studentList);
                    }
                }
                else {
                    searchListener.onFailure("Classmate "+stringQuery+" is not found. Maybe, your classmate is not yet registered.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                searchListener.onFailure(databaseError.getMessage());
            }
        });
    }
}

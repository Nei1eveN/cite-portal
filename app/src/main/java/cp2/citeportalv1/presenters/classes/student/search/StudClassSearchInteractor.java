package cp2.citeportalv1.presenters.classes.student.search;

import java.util.List;

import cp2.citeportalv1.models.Class;

public interface StudClassSearchInteractor {
    void getDataFromQuery(String stringQuery, SearchListener searchListener);

    interface SearchListener {
        void onSuccess(List<Class> classList);

        void onFailure(String errorMessage);
    }
}

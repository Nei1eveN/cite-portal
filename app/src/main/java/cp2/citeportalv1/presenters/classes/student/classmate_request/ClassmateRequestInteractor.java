package cp2.citeportalv1.presenters.classes.student.classmate_request;

import java.util.List;

import cp2.citeportalv1.models.ClassmateRequest;

public interface ClassmateRequestInteractor {
    void getClassmateRequests(OnClassmateRequestListener requestListener);

    interface OnClassmateRequestListener {
        void onRequestSuccess(List<ClassmateRequest> classmates);
        void onRequestFailure(String errorMessage);
    }

}

package cp2.citeportalv1.presenters.classes.faculty.display;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.Class;

public class FacultyClassPresenterImpl implements FacultyClassPresenter, FacultyClassInteractor.FacultyClassListener {
    private FacultyClassPresenter.FacultyClassView facultyClassView;
    private FacultyClassInteractor facultyClassInteractor;

    public FacultyClassPresenterImpl(FacultyClassView facultyClassView, Context context) {
        this.facultyClassView = facultyClassView;
        this.facultyClassInteractor = new FacultyClassInteractorImpl(context);
    }

    @Override
    public void onStart() {
        facultyClassView.setUpFabAddClass();
        facultyClassView.setUpEmptyImageAddClass();
        facultyClassInteractor.getData(this);
    }

    @Override
    public void onDestroy() {
        facultyClassView = null;
    }

    @Override
    public void onFailure(String errorMessage) {
        facultyClassView.setErrorState(errorMessage);
    }

    @Override
    public void onSuccess(List<Class> createdByFacultyUid) {
        facultyClassView.setClassesCreatedByFaculty(createdByFacultyUid);
    }
}

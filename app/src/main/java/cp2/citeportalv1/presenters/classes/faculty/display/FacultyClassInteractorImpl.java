package cp2.citeportalv1.presenters.classes.faculty.display;

import android.content.Context;
import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cp2.citeportalv1.models.Class;

public class FacultyClassInteractorImpl implements FacultyClassInteractor {
    private Context context;
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    FacultyClassInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getData(FacultyClassListener listener) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference("Class");
        databaseReference.keepSynced(true);
        databaseReference.orderByChild("createdBy").equalTo(firebaseAuth.getCurrentUser().getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.exists()){
                            listener.onFailure("You haven't created a Class. Click the icon above to make one");
                        }
                        else {
                            List<Class> aClasses = new ArrayList<>();
                            aClasses.clear();
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                String classIds = dataSnapshot1.getKey();

                                Class aClass = dataSnapshot1.getValue(Class.class).withId(classIds);
                                aClasses.add(aClass);
                                listener.onSuccess(aClasses);

                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        listener.onFailure(databaseError.getMessage());
                    }
                });
    }
}

package cp2.citeportalv1.presenters.classes.student.profile;

import android.graphics.drawable.Drawable;

import cp2.citeportalv1.models.Student;

public interface ClassmateProfileInteractor {
    void getStudentDetails(String toReceiverUid, StudentProfileListener profileListener);

    void getRequestUid(String toReceiverUid, ClassmateRequestListener requestListener);

    void compareReceiverUid(String toReceiverUid, OnCompareClassmatesListener compareClassmatesListener);

    void verifyReceiverUid(String toReceiverUid, OnEmailVerificationCheckListener verificationCheckListener);

    interface StudentProfileListener {
        void onProfileSuccess(Student student);
        void onProfileFailure(String errorMessage);
    }

    interface ClassmateRequestListener {
        void onRequestSuccess(String successMessage, String changeButtonStatus, Drawable changeButtonDrawable);

        void onRequestFailure(String errorMessage);
    }

    interface OnCompareClassmatesListener {
        void profileSameUid();

        void profileNotSameUid();

        void ifClassmates(String changeButtonStatus, Drawable changeButtonDrawable);

        void ifNotClassmates(String doNotChangeButtonStatus, Drawable doNotChangeButtonDrawable);
    }

    interface OnEmailVerificationCheckListener {
        void isVerified();

        void isNotVerified();

        void onEmailVerificationError(String errorMessage);
    }
}

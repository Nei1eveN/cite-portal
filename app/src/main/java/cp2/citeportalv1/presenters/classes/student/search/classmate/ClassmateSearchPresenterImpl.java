package cp2.citeportalv1.presenters.classes.student.search.classmate;

import android.content.Context;

import java.util.List;

import cp2.citeportalv1.models.Student;

public class ClassmateSearchPresenterImpl implements ClassmateSearchPresenter, ClassmateSearchInteractor.ClassmateSearchListener {
    private ClassmateSearchPresenter.View view;
    private ClassmateSearchInteractor searchInteractor;

    public ClassmateSearchPresenterImpl(View view, Context context) {
        this.view = view;
        this.searchInteractor = new ClassmateSearchInteractorImpl(context);
    }

    @Override
    public void onStart() {
        view.initViews();
        view.setEmptyState("Search your classmate by their Last Name / Surname");
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void submitQuery(String query) {
        if (query.isEmpty()){
            view.setEmptyState("Search your classmate by their Last Name / Surname");
        }
        else {
            searchInteractor.getDataFromQuery(query, this);
        }
    }

    @Override
    public void onSuccess(List<Student> studentList) {
        view.setClassmateFound(studentList);
    }

    @Override
    public void onFailure(String errorMessage) {
        view.setNoClassmateFound(errorMessage);
    }
}

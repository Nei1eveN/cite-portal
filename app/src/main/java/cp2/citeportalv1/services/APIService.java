package cp2.citeportalv1.services;


import cp2.citeportalv1.models.Result;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {
    @GET("/api/send-email-verification")
    Call<Result> sendVerification(@Query("uid") String userUid);
}

package cp2.citeportalv1.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Objects;

import androidx.core.app.NotificationCompat;
import cp2.citeportalv1.R;

import static cp2.citeportalv1.utils.Constants.getBitmapFromURL;
import static cp2.citeportalv1.utils.Constants.getCircleBitmap;

public class ConsultationResponseService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("FCM Token", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String title = remoteMessage.getData().get("title");
        String body = remoteMessage.getData().get("body");
        String click_action = remoteMessage.getData().get("click_action");

        String messageTitle = remoteMessage.getData().get("messageTitle");
        String messageBody = remoteMessage.getData().get("messageBody");

        String from_user_image = remoteMessage.getData().get("from_user_image");

        String notificationId = remoteMessage.getData().get("notificationId");

        sendNotification(title, body, click_action, messageTitle, messageBody, from_user_image, notificationId);
    }

    private void sendNotification(String title, String body, String click_action,
                                  String messageTitle, String messageBody,
                                  String from_user_image,
                                  String notificationId) {

        if (from_user_image != null) {
            Log.d("ConsultResp imageURL", from_user_image);
        }

        NotificationCompat.Builder mBuilder;

        if (from_user_image != null) {

            mBuilder = new NotificationCompat
                    .Builder(this, "Consultation Response")
                    .setSmallIcon(R.drawable.cite_logo300px)
                    .setLargeIcon(getCircleBitmap(Objects.requireNonNull(getBitmapFromURL(from_user_image))))
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setColor(getResources().getColor(R.color.fullBluePrimary))
//                .setStyle(new NotificationCompat.BigTextStyle()
//                        .bigText("Course:\n"+messageTitle+"\n\nConcern Details:\n"+messageBody))
                    .setPriority(Notification.PRIORITY_MAX | NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL);

        } else {
            mBuilder = new NotificationCompat
                    .Builder(this, "Consultation Response")
                    .setSmallIcon(R.drawable.cite_logo300px)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setColor(getResources().getColor(R.color.fullBluePrimary))
//                .setStyle(new NotificationCompat.BigTextStyle()
//                        .bigText("Course:\n"+messageTitle+"\n\nConcern Details:\n"+messageBody))
                    .setPriority(Notification.PRIORITY_MAX | NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL);
        }

        Intent resultIntent = new Intent(click_action);

        resultIntent.putExtra("notificationId", notificationId);

        PendingIntent resultPendingIntent = PendingIntent
                .getActivity(this,0,resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(resultPendingIntent);

        int mNotificationId = (int) System.currentTimeMillis();

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(getString(R.string.default_notification_channel_id),
                    "Consultation Response",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotifyMgr.createNotificationChannel(channel);
        }

        mNotifyMgr.notify(mNotificationId, mBuilder.build());

    }
}

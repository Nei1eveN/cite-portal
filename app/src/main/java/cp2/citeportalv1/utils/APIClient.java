package cp2.citeportalv1.utils;

import android.content.Context;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static cp2.citeportalv1.utils.Constants.DATABASE_URL;
import static cp2.citeportalv1.utils.Constants.cacheSize;
import static cp2.citeportalv1.utils.Constants.isNetworkAvailable;

public class APIClient {
    public static Retrofit getClient(final Context context) {
        Cache cache = new Cache(context.getCacheDir(), cacheSize);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    if (!isNetworkAvailable(context)) {
                        int maxStale = 60 * 60 * 24 * 28;
                        request = request
                                .newBuilder()
                                .header("cache-control", "public, only-if-cached,max-stale=" + maxStale)
                                .build();
                    }
                    return chain.proceed(request);
                }).build();

        return new Retrofit.Builder()
                .baseUrl(DATABASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}

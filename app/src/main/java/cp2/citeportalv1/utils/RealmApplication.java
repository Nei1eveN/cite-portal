package cp2.citeportalv1.utils;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.firebase.database.FirebaseDatabase;
import com.jakewharton.threetenabp.AndroidThreeTen;

import androidx.multidex.MultiDex;
import cp2.citeportalv1.BuildConfig;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        MultiDex.install(base);
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(realmConfiguration);

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        Log.d("versionName", BuildConfig.VERSION_NAME);

        AndroidThreeTen.init(this);
    }
}

package cp2.citeportalv1.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.wdullaer.materialdatetimepicker.time.Timepoint;

import org.joda.time.DateTime;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.regex.Pattern;

import androidx.appcompat.app.AlertDialog;
import cp2.citeportalv1.BuildConfig;
import cp2.citeportalv1.models.Appointments;
import cp2.citeportalv1.models.ConsultationLog;
import cp2.citeportalv1.models.ConsultationRequest;
import cp2.citeportalv1.models.ConsultationResponse;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.models.Interval;
import cp2.citeportalv1.models.Schedule;

public class Constants {

    //Cache Size
    public static int cacheSize = 10 * 1024 * 1024;

    public static String DATABASE_URL = "http://citeportal2018.x10.mx";

    public static int TOTAL_ITEMS_TO_LOAD = 10;

    public static final String ACCOUNTS = "Accounts";

    public static final String ACCOUNT_UID = "accountUid";

    public static final String APPOINTMENTS = "Appointments";

    public static final String CLASS = "Class";
    public static final String CLASSES = "Class";
    public static final String CLASSMATES = "Classmates";
    public static final String CLASSMATE_REQUESTS = "ClassmateRequest";

    public static final String CONSULTATION_REQUESTS = "ConsultationRequests";
    public static final String CONSULTATION_RESPONSES = "ConsultationResponse";
    public static final String CONSULTATION_SCHEDULES = "ConsultationSchedules";
    public static final String CONSULTATION_LOG = "ConsultationLog";

    public static final String COURSES = "Courses";

    public static final String FACULTY = "Faculty";
    public static final String FACULTY_USERS = "FacultyUsers";

    public static final String FRIEND_REQUESTS = "Friend_Request";

    public static final int PICK_IMAGE_REQUEST = 1;

    public static final int WRITE_EXTERNAL_STORAGE = 11;

    public static final String PNG = "png";

    public static final String PROFILE_IMAGES = "ProfileImages";

    public static final int ACTIVITY_REQUEST_CODE = 0;

    public static final String SCHEDULE_DAY = "day";

    public static final String STUDENT_USERS = "StudentUsers";
    public static final String STUDENTS = "Student";

    public static final String SIGNATURES = "Signatures";

    public static final String SENDER_SIGNATURE = "senderSignature";
    public static final String RECEIVER_SIGNATURE = "receiverSignature";

    public static final String UF8FF = "\uf8ff";

    public static final String FACULTY_UID = "facultyUid";
    public static final String STUDENT_UID = "studentUid";
    public static final String NOTIFICATION_ID = "notificationId";

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM d, yyyy", /*Locale.getDefault()*/Locale.ENGLISH);
    public static final SimpleDateFormat timeFormat12hr = new SimpleDateFormat("h:mm aa", Locale.ENGLISH);
    public static final SimpleDateFormat hourMinuteSecondFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
    public static final SimpleDateFormat combinedFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
    public static final SimpleDateFormat fixedFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH); //'00'.'000'
    public static final SimpleDateFormat fixedTimeFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

    public static final SimpleDateFormat hourFormat = new SimpleDateFormat("HH", Locale.ENGLISH);
    public static final SimpleDateFormat minuteFormat = new SimpleDateFormat("mm", Locale.ENGLISH);
    public static final SimpleDateFormat secondsFormat = new SimpleDateFormat("ss", Locale.ENGLISH);

    public static final SimpleDateFormat yearDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    public static final SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.ENGLISH);
    public static final SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM", Locale.ENGLISH);

    public static final SimpleDateFormat dayMonthYearFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    public static final SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);

    /**CONSULTATION REQUEST STRING VARIABLES**/
    public static final String SENDER_ID = "senderId";
    public static final String RECEIVER_ID = "receiverId";

    public static final String SENDER_CC = "senderCc";

    public static final String SENDER_FIRST_NAME = "senderFirstName";
    public static final String SENDER_IMAGE_URL = "senderImageURL";
    public static final String SENDER_LAST_NAME = "senderLastName";
    public static final String SENDER_MIDDLE_NAME = "senderMiddleName";
    public static final String SENDER_PROGRAM = "senderProgram";
    public static final String SENDER_YEAR_LEVEL = "senderYearLevel";

    public static final String MESSAGE_BODY = "messageBody";
    public static final String MESSAGE_TITLE = "messageTitle";
    public static final String MESSAGE_STATUS = "messageStatus";

    public static final String TIMESTAMP = "timeStamp";

    public static final String REQUEST_CODE = "requestCode";
    public static final String REQUESTED_DATE = "requestedDate";
    public static final String REQUESTED_DAY = "requestedDay";
    public static final String REQUESTED_SCHEDULE = "requestedSchedule";
    public static final String REQUESTED_TIME_END = "requestedTimeEnd";
    public static final String REQUESTED_TIME_START = "requestedTimeStart";

    public static final String VENUE = "venue";

    /**APPOINTMENT STRING VARIABLES**/
    public static final String APPOINTMENT_CC = "appointmentCc";
    public static final String APPOINTMENT_CODE = "appointmentCode";
    public static final String MESSAGE_REMARKS = "messageRemarks";
    public static final String MESSAGE_SIDE_NOTE = "messageSideNote";
    public static final String SENDER_DEPARTMENT = "senderDepartment";

    /**CONSULTATION LOG STRING VARIABLES**/
    public static final String APPOINTMENT_FEEDBACK = "appointmentFeedback";

    /**CONSULTATION RESPONSE STRING VARIABLES**/
    public static final String RESPONSE_CODE = "responseCode";

    /**REGISTRATION STRING VARIABLES**/
    public static final String DEPARTMENT = "department";
    public static final String EMAIL = "email";
    public static final String EMPLOYEE_ID = "employeeID";
    public static final String FIRST_NAME = "fName";
    public static final String FACULTY_IMAGE_URL = "facultyImageURL";
    public static final String LAST_NAME = "lastName";
    public static final String MID_NAME = "midName";
    public static final String TOKEN_ID = "tokenId";
    public static final String PROGRAM = "program";
    public static final String STUDENT_ID = "studentID";
    public static final String STUDENT_IMAGE_URL = "studentImageURL";
    public static final String YEAR_LVL = "yearLvl";
    public static final String VALID = "valid";

    /**MODERATOR ACCOUNT STRING VARIABLES**/
    public static final String ACCOUNT_ACCESS_LEVEL = "accessLevel";
    public static final String ACCOUNT_ID = "accountID";
    public static final String ACCOUNT_FIRST_NAME = "firstName";
    public static final String ACCOUNT_MIDDLE_NAME = "middleName";
    public static final String ACCOUNT_LAST_NAME = "lastName";

    public static final String ADMIN = "ADMIN";
    public static final String SECRETARY = "SECRETARY";


    /**Pattern for Password**/
    public static Pattern specialCharPatten = Pattern.compile("[^a-z0-9]", Pattern.CASE_INSENSITIVE);
    public static Pattern upperCasePattern = Pattern.compile("[A-Z]");
    public static Pattern lowerCasePattern = Pattern.compile("[a-z]");
    public static Pattern digitCasePattern = Pattern.compile("[0-9]");

    /**
     * Calendar Instance
     **/
    public static Calendar calendar = Calendar.getInstance();
    public static int hour = calendar.get(Calendar.HOUR_OF_DAY);
    public static int minute = calendar.get(Calendar.MINUTE);
    public static int second = calendar.get(Calendar.SECOND);

    public static int month = calendar.get(Calendar.MONTH);
    public static int day = calendar.get(Calendar.DAY_OF_MONTH);
    public static int year = calendar.get(Calendar.YEAR);

    public static void showExitDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want to exit?");
        builder.setPositiveButton("yes", (dialogInterface, i) -> {
            ((Activity) context).finishAffinity();
        });
        builder.setNegativeButton("no", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public int getRandomColor() {
        Random rand = new Random();
        return Color.argb(100, rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
    }

    public static Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection)
                    url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    public static void whiteNotificationBar(View view, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            ((Activity) context).getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    public static boolean isConflict(DateTime s1, DateTime e1, DateTime s2, DateTime e2) {
        return (s1.isBefore(e2) && (e1.isAfter(s2)));
    }

    /**
     * Consultation Request Grouping to LinkedHashMap <String, List<ConsultationRequest>
     * **/

    public static LinkedHashMap<String, List<ConsultationRequest>> groupRequestDataIntoHashMap(List<ConsultationRequest> requests) {
        LinkedHashMap<String, List<ConsultationRequest>> groupedHashMap = new LinkedHashMap<>();

        for (ConsultationRequest request : requests) {
            String hashMapKey = request.getTimeStamp(); //String.valueOf(new Date(Long.parseLong(response.getTimeStamp())*1000))
            if (groupedHashMap.containsKey(hashMapKey)) {
                groupedHashMap.get(hashMapKey).add(request);
            } else {
                List<ConsultationRequest> requestList = new ArrayList<>();
                requestList.add(request);
                groupedHashMap.put(hashMapKey, requestList);
            }
        }

        return groupedHashMap;
    }

    /**
     * Consultation Response Grouping to LinkedHashMap
     * **/

    public static void groupResponseDataIntoHashMap(List<ConsultationResponse> responses) {
        LinkedHashMap<String, Set<ConsultationResponse>> groupedHashMap = new LinkedHashMap<>();

        Set<ConsultationResponse> responseSet;
        for (ConsultationResponse response : responses) {
            String hashMapKey = response.getTimeStamp(); //String.valueOf(new Date(Long.parseLong(response.getTimeStamp())*1000))
            if (groupedHashMap.containsKey(hashMapKey)) {
                groupedHashMap.get(hashMapKey).add(response);
            } else {
                responseSet = new LinkedHashSet<>();
                responseSet.add(response);
                groupedHashMap.put(hashMapKey, responseSet);
            }
        }
    }

    public static LinkedHashMap<String, Set<ConsultationResponse>> groupApprovedConsultationResponseToAppointmentsIntoHashMap(List<ConsultationResponse> responses) {
        LinkedHashMap<String, Set<ConsultationResponse>> groupedHashMap = new LinkedHashMap<>();

        Set<ConsultationResponse> responseSet;
        for (ConsultationResponse response : responses) {
            /**
             * TO BE IMPROVED
             *
             * TO REPLACE requestedDay to requestedDate later
             * **/

            try {
                Date dayDate = dayFormat.parse(response.getRequestedDay());
                String hashMapKey = String.valueOf(dayDate.getTime()); //String.valueOf(new Date(Long.parseLong(response.getTimeStamp())*1000))

                if (groupedHashMap.containsKey(hashMapKey)) {
                    groupedHashMap.get(hashMapKey).add(response);
                } else {
                    responseSet = new LinkedHashSet<>();
                    responseSet.add(response);
                    groupedHashMap.put(hashMapKey, responseSet);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return groupedHashMap;
    }

    /**
     * Schedules Grouping to HashMap
     * **/

    public static LinkedHashMap<String, Set<Schedule>> groupScheduleDataIntoHashMap(List<Schedule> schedules){
        LinkedHashMap<String, Set<Schedule>> groupedHashMap = new LinkedHashMap<>();
        Set<Schedule> scheduleSet;
        for (Schedule schedule : schedules){
            try {
                Date dayDate = dayFormat.parse(schedule.getDay());
                String hashMapKey = String.valueOf(dayDate.getTime());

                Log.d("timeStamps", schedule.getTimeStart());

                if (groupedHashMap.containsKey(hashMapKey)){
                    Objects.requireNonNull(groupedHashMap.get(hashMapKey)).add(schedule);
                }
                else {
                    scheduleSet = new LinkedHashSet<>();
                    scheduleSet.add(schedule);
                    groupedHashMap.put(hashMapKey, scheduleSet);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        return groupedHashMap;
    }

    public static LinkedHashMap<String, Set<Appointments>> groupAppointmentDataIntoHashMap(List<Appointments> appointments) {
        LinkedHashMap<String, Set<Appointments>> groupedHashMap = new LinkedHashMap<>();
        Set<Appointments> appointmentsSet;
        for (Appointments appointment : appointments) {
            try {
                Date dayDate = yearDateFormat.parse(appointment.getRequestedDate());
                String hashMapKey = String.valueOf(dayDate.getTime());

                if (groupedHashMap.containsKey(hashMapKey)){
                    Objects.requireNonNull(groupedHashMap.get(hashMapKey)).add(appointment);
                } else {
                    appointmentsSet = new LinkedHashSet<>();
                    appointmentsSet.add(appointment);
                    groupedHashMap.put(hashMapKey, appointmentsSet);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return groupedHashMap;
    }

    public static LinkedHashMap<String, Set<ConsultationLog>> groupConsultationLogDataIntoHashMap(List<ConsultationLog> appointments) {
        LinkedHashMap<String, Set<ConsultationLog>> groupedHashMap = new LinkedHashMap<>();
        Set<ConsultationLog> appointmentsSet;
        for (ConsultationLog appointment : appointments) {
            try {
                Date dayDate = yearDateFormat.parse(appointment.getRequestedDate());
                String hashMapKey = String.valueOf(dayDate.getTime());

                if (groupedHashMap.containsKey(hashMapKey)){
                    Objects.requireNonNull(groupedHashMap.get(hashMapKey)).add(appointment);
                } else {
                    appointmentsSet = new LinkedHashSet<>();
                    appointmentsSet.add(appointment);
                    groupedHashMap.put(hashMapKey, appointmentsSet);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return groupedHashMap;
    }

    public static LinkedHashMap<String, Set<String>> groupDayStringIntoHashMap(List<String> stringList) {
        LinkedHashMap<String, Set<String>> groupedHashMap = new LinkedHashMap<>();
        Set<String> stringSet;
        for (String day : stringList) {
            try {
                Date dayDate = dayFormat.parse(day);
                String hashMapKey = String.valueOf(dayDate.getTime());

                if (groupedHashMap.containsKey(hashMapKey)){
                    Objects.requireNonNull(groupedHashMap.get(hashMapKey)).add(day);
                } else {
                    stringSet = new LinkedHashSet<>();
                    stringSet.add(day);
                    groupedHashMap.put(hashMapKey, stringSet);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return groupedHashMap;
    }

    public static LinkedHashMap<String, List<Schedule>> groupScheduleDataPerDayIntoHashMap(List<Schedule> schedules){
        LinkedHashMap<String, List<Schedule>> groupedHashMap = new LinkedHashMap<>();

        for (Schedule schedule : schedules){
            try {
                Date dayDate = dayFormat.parse(schedule.getDay());
                String hashMapKey = String.valueOf(dayDate.getTime());

                Log.d("timeStamps", schedule.getTimeStart());

                if (groupedHashMap.containsKey(hashMapKey)){
                    Objects.requireNonNull(groupedHashMap.get(hashMapKey)).add(schedule);
                }
                else {
                    List<Schedule> scheduleList = new ArrayList<>();
                    scheduleList.add(schedule);
                    groupedHashMap.put(hashMapKey, scheduleList);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        return groupedHashMap;
    }

    public static LinkedHashMap<String, List<Faculty>> groupFacultyDataIntoHashMap(List<Faculty> facultyList) {
        LinkedHashMap<String, List<Faculty>> groupedHashMap = new LinkedHashMap<>();

        for (Faculty faculty : facultyList) {
            String hashMapKey = faculty.getDepartment();
            if (groupedHashMap.containsKey(hashMapKey)) {
                Objects.requireNonNull(groupedHashMap.get(hashMapKey)).add(faculty);
            } else {
                List<Faculty> faculties = new ArrayList<>();
                faculties.add(faculty);
                groupedHashMap.put(hashMapKey, faculties);
            }
        }
        return groupedHashMap;
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    public static String getApplicationVersion(){
        return BuildConfig.VERSION_NAME;
    }

    public static Comparator<String> dateComparator = (s1, s2) -> {
        try{
            SimpleDateFormat format = new SimpleDateFormat("EEE", Locale.ENGLISH);
            Date d1 = format.parse(s1);
            Date d2 = format.parse(s2);
            if(d1.equals(d2)){
                return s1.substring(s1.indexOf(" ") + 1).compareTo(s2.substring(s2.indexOf(" ") + 1));
            }else{
                Calendar cal1 = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                cal1.setTime(d1);
                cal2.setTime(d2);
                return cal1.get(Calendar.DAY_OF_WEEK) - cal2.get(Calendar.DAY_OF_WEEK);
            }
        }catch(ParseException pe){
            throw new RuntimeException(pe);
        }
    };

    /**GENERATING TIMEPOINTS FROM MINUTE-INTERVAL**/
    public static Timepoint[] generateTimepoints(int minutesInterval) {
        List<Timepoint> timepoints = new ArrayList<>();

        for (int minute = 0; minute<=1440; minute += minutesInterval) {
            int currentHour = minute / 60;
            int currentMinute = minute - (currentHour > 0 ? (currentHour * 60) : 0);
            if (currentHour == 24)
                continue;
            timepoints.add(new Timepoint(currentHour, currentMinute));
        }
        return timepoints.toArray(new Timepoint[timepoints.size()]);
    }

    /**
     * @param context
     * @return the screen height in pixels
     */
    public static int getScreenHeight(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    /**
     * @param context
     * @return the screen width in pixels
     */
    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static List<Interval> findIntervalsThatOverlap(List<Interval> intervals) {

        // Keeps unique intervals.
        Set<Interval> set = new HashSet<Interval>();

        // Sort the intervals by starting time.
        Collections.sort(intervals, (x, y) -> Integer.compare(x.start, y.start));

        // Keep track of the interval that has the latest end time.
        Interval intervalWithLatestEnd = null;

        for (Interval interval : intervals) {

            if (intervalWithLatestEnd != null &&
                    interval.start < intervalWithLatestEnd.end) {

                // Overlap occurred.
                // Add the current interval and the interval it overlapped with.
                set.add(interval);
                set.add(intervalWithLatestEnd);

                System.out.println(interval + " overlaps with " +
                        intervalWithLatestEnd);
            }

            // Update the interval with latest end.
            if (intervalWithLatestEnd == null ||
                    intervalWithLatestEnd.end < interval.end) {

                intervalWithLatestEnd = interval;
            }
        }
        // Convert the Set to a List.
        return new ArrayList<Interval>(set);
    }

}

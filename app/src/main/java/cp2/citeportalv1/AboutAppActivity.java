package cp2.citeportalv1;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;

import static cp2.citeportalv1.utils.Constants.getApplicationName;
import static cp2.citeportalv1.utils.Constants.getApplicationVersion;

public class AboutAppActivity extends AppCompatActivity {

    TextView appName;
    TextView appVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);

        appName = findViewById(R.id.tvAppName);
        appVersion = findViewById(R.id.tvVersionName);

        appName.setText(getApplicationName(this));
        appVersion.setText(getApplicationVersion());

        Objects.requireNonNull(getSupportActionBar()).setTitle("About");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

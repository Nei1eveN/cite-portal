package cp2.citeportalv1.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.citeportalv1.AboutAppActivity;
import cp2.citeportalv1.views.secretary.FacultyAccountsActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.views.secretary.SecretaryConsultationLogsActivity;
import cp2.citeportalv1.views.secretary.StudentAccountsActivity;
import cp2.citeportalv1.models.Accounts;
import cp2.citeportalv1.presenters.base.secretary.SecretaryBasePresenter;
import cp2.citeportalv1.presenters.base.secretary.SecretaryBasePresenterImpl;

public abstract class SecretaryBaseActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, SecretaryBasePresenter.View {

    Unbinder unbinder;
    @BindView(R.id.baseToolbarFaculty) protected Toolbar toolbar;
    @BindView(R.id.base_drawer_faculty) protected DrawerLayout drawer;
    @BindView(R.id.base_navigation_viewFaculty) protected NavigationView navigationView;
    @BindView(R.id.view_stubFaculty) protected FrameLayout viewStub;
    protected ActionBarDrawerToggle toggle;

    protected View headerView;
    protected TextView facultyName;
    protected TextView facultyDepartment;
    protected ImageView facultyProfileImage;

    ProgressDialog progressDialog;

    SecretaryBasePresenter presenter;

    String department;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.basedrawer_secretary);
        setSupportActionBar(toolbar);

        unbinder = ButterKnife.bind(this);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        headerView = navigationView.getHeaderView(0);
        facultyName = headerView.findViewById(R.id.tvNameFaculty);
        facultyDepartment = headerView.findViewById(R.id.tvDepartmentFaculty);
        facultyProfileImage = headerView.findViewById(R.id.profile_imageFaculty);

        navigationView.setNavigationItemSelectedListener(this);

        progressDialog = new ProgressDialog(this);
        presenter = new SecretaryBasePresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setContentView(int layoutResID) {
        if (viewStub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, viewStub, false);
            viewStub.addView(stubView, lp);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.student_accounts:
                navigationView.setCheckedItem(R.id.student_accounts);
                drawer.closeDrawer(GravityCompat.START);
                toolbar.setTitle("For Verification");
                toolbar.setSubtitle("Student Accounts");
                if (getClass() == StudentAccountsActivity.class){
                    return true;
                }
                else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), StudentAccountsActivity.class));
                        finish();
                        overridePendingTransition(0,0);
                    }, 500);
                }
                break;
            case R.id.faculty_accounts:
                navigationView.setCheckedItem(R.id.faculty_accounts);
                drawer.closeDrawer(GravityCompat.START);
                toolbar.setTitle("For Verification");
                toolbar.setSubtitle("Faculty Accounts");
                if (getClass() == FacultyAccountsActivity.class){
                    return true;
                }
                else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), FacultyAccountsActivity.class));
                        finish();
                        overridePendingTransition(0,0);
                    }, 500);
                }
                break;
            case R.id.consultation_logs:
                navigationView.setCheckedItem(R.id.consultation_logs);
                drawer.closeDrawer(GravityCompat.START);
                toolbar.setTitle("Consultation Logs");
                toolbar.setSubtitle(department);
                if (getClass() == SecretaryConsultationLogsActivity.class){
                    return true;
                }
                else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), SecretaryConsultationLogsActivity.class));
                        finish();
                        overridePendingTransition(0,0);
                    }, 500);
                }
                break;
            case R.id.about:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(() ->
                                startActivity(new Intent(getApplicationContext(), AboutAppActivity.class)),
                        500);
                break;
            case R.id.sign_out:
                navigationView.setCheckedItem(R.id.signOut);
                presenter.requestLogout();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you want to exit?");
            builder.setPositiveButton("YES", (dialogInterface, i) -> finishAffinity());
            builder.setNegativeButton("NO", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setUserInfo(Accounts account) {
        facultyName.setText(String.format("%s, %s %s", account.getLastName(), account.getFirstName(), account.getMiddleName()));
        facultyDepartment.setText(String.format("%s Department | %s", account.getDepartment(), account.getAccessLevel()));
        department = account.getDepartment();
    }

    @Override
    public void setToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}

package cp2.citeportalv1.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.citeportalv1.AboutAppActivity;
import cp2.citeportalv1.views.faculty.FacultyAppointmentsActivity;
import cp2.citeportalv1.views.faculty.FacultyConsultationLogActivity;
import cp2.citeportalv1.views.faculty.FacultyProfileActivity;
import cp2.citeportalv1.views.faculty.ConsultationHoursActivity;
import cp2.citeportalv1.models.Faculty;
import cp2.citeportalv1.views.faculty.FacultyConsultationRequestsLogActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.presenters.base.faculty.FacultyBasePresenter;
import cp2.citeportalv1.presenters.base.faculty.FacultyBasePresenterImpl;
import cp2.citeportalv1.views.faculty.FacultyHomeActivity;


public abstract class FacultyBaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FacultyBasePresenter.FacultyBaseView {

    Unbinder unbinder;
    @BindView(R.id.baseToolbarFaculty) protected Toolbar toolbar;
    @BindView(R.id.base_drawer_faculty) protected DrawerLayout drawer;
    @BindView(R.id.base_navigation_viewFaculty) protected NavigationView navigationView;
    @BindView(R.id.view_stubFaculty) protected FrameLayout viewStub;
    protected ActionBarDrawerToggle toggle;

    protected View headerView;
    protected TextView facultyName;
    protected TextView facultyDepartment;
    protected ImageView facultyProfileImage;

    FacultyBasePresenter facultyBasePresenter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.basedrawer_faculty);
        setSupportActionBar(toolbar);
        unbinder = ButterKnife.bind(this);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        headerView = navigationView.getHeaderView(0);
        facultyName = headerView.findViewById(R.id.tvNameFaculty);
        facultyDepartment = headerView.findViewById(R.id.tvDepartmentFaculty);
        facultyProfileImage = headerView.findViewById(R.id.profile_imageFaculty);

        navigationView.setNavigationItemSelectedListener(this);

        facultyBasePresenter = new FacultyBasePresenterImpl(this, this);

        progressDialog = new ProgressDialog(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        facultyBasePresenter.onStart();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setContentView(int layoutResID) {
        if (viewStub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, viewStub, false);
            viewStub.addView(stubView, lp);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.facHomeMenu:
                navigationView.setCheckedItem(R.id.facHomeMenu);
                drawer.closeDrawer(GravityCompat.START);
                toolbar.setTitle("Home");
                toolbar.setSubtitle(null);
                if (getClass() == FacultyHomeActivity.class){
                    return true;
                }
                else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), FacultyHomeActivity.class));
                        finish();
                    },500);

                }
                break;
            case R.id.appointments:
                navigationView.setCheckedItem(R.id.appointments);
                drawer.closeDrawer(GravityCompat.START);
                toolbar.setTitle("Appointments");
                toolbar.setSubtitle(null);
                if (getClass() == FacultyAppointmentsActivity.class) {
                    return true;
                } else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), FacultyAppointmentsActivity.class));
                        finish();
                    },500);
                }
                break;
//            case R.id.classes:
//                navigationView.setCheckedItem(R.id.classes);
//                drawer.closeDrawer(GravityCompat.START);
//                toolbar.setTitle("Class");
//                toolbar.setSubtitle(null);
//                if (getClass() == FacultyClassesActivity.class) {
//                    return true;
//                }
//                else {
//                    new Handler().postDelayed(() -> {
//                        startActivity(new Intent(getApplicationContext(), FacultyClassesActivity.class));
//                        finish();
//                    },500);
//                }
//                break;
//            case R.id.stud_incl_requests:
//                navigationView.setCheckedItem(R.id.stud_incl_requests);
//                drawer.closeDrawer(GravityCompat.START);
//                toolbar.setTitle("Requests");
//                toolbar.setSubtitle("Include Student to Class");
//                break;
            case R.id.consultation_hours:
                navigationView.setCheckedItem(R.id.consultation_hours);
                drawer.closeDrawer(GravityCompat.START);
                toolbar.setTitle("Consultation Schedules");
                toolbar.setSubtitle(null);
                if (getClass() == ConsultationHoursActivity.class){
                    return true;
                }
                else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), ConsultationHoursActivity.class));
                        finish();
                    }, 500);
                }
                break;
            case R.id.consultation_requests:
                navigationView.setCheckedItem(R.id.consultation_requests);
                drawer.closeDrawer(GravityCompat.START);
                toolbar.setTitle("Consultation Requests");
                toolbar.setSubtitle(null);
                if (getClass() == FacultyConsultationRequestsLogActivity.class){
                    return true;
                }
                else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), FacultyConsultationRequestsLogActivity.class));
                        finish();
                    }, 500);
                }
                break;
            case R.id.consultation_history:
                navigationView.setCheckedItem(R.id.consultation_history);
                drawer.closeDrawer(GravityCompat.START);
                toolbar.setTitle("Consultation Log");
                toolbar.setSubtitle(null);
                if (getClass() == FacultyConsultationLogActivity.class){
                    return true;
                }
                else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), FacultyConsultationLogActivity.class));
                        finish();
                    }, 500);
                }
                break;
            case R.id.about:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(() ->
                                startActivity(new Intent(getApplicationContext(), AboutAppActivity.class)),
                        500);
                break;
            case R.id.sign_out_faculty:
                navigationView.setCheckedItem(R.id.sign_out_faculty);
                facultyBasePresenter.requestLogout();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you want to exit?");
            builder.setPositiveButton("YES", (dialogInterface, i) -> finishAffinity());
            builder.setNegativeButton("NO", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        facultyBasePresenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setUserInfo(Faculty faculty) {
        facultyName.setText(String.format("%s, %s %s", faculty.getLastName(), faculty.getfName(), faculty.getMidName()));
        facultyDepartment.setText(String.format("%s Department", faculty.getDepartment()));
        Glide.with(this).load(faculty.getFacultyImageURL()).apply(new RequestOptions().circleCrop().placeholder(R.drawable.cite_logo300px).error(R.drawable.cite_logo300px)).into(facultyProfileImage);

        facultyProfileImage.setOnClickListener(v -> {
            Intent profileIntent = new Intent(getApplicationContext(), FacultyProfileActivity.class);
            startActivity(profileIntent);
        });
    }

    @Override
    public void setToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}

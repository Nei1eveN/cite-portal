package cp2.citeportalv1.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cp2.citeportalv1.AboutAppActivity;
import cp2.citeportalv1.R;
import cp2.citeportalv1.views.student.consultation.logs.StudentConsultationLogActivity;
import cp2.citeportalv1.views.student.consultation.logs.StudentConsultationResponseLogActivity;
import cp2.citeportalv1.views.student.consultation.logs.StudentPendingRequestsLogActivity;
import cp2.citeportalv1.models.Student;
import cp2.citeportalv1.presenters.base.student.StudentBasePresenter;
import cp2.citeportalv1.presenters.base.student.StudentBasePresenterImpl;
import cp2.citeportalv1.views.student.appointment.StudentAppointmentsActivity;
import cp2.citeportalv1.views.student.classmate.ClassmateLogActivity;
import cp2.citeportalv1.views.student.classmate.ClassmateSearchActivity;
import cp2.citeportalv1.views.student.consultation.ConsultDepartmentsActivity;
import cp2.citeportalv1.views.student.home.StudHomeActivity;
import cp2.citeportalv1.views.student.profile.StudentProfileActivity;

public abstract class StudentBaseActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, StudentBasePresenter.View { //, MenuItem.OnMenuItemClickListener

    Unbinder unbinder;
    @BindView(R.id.baseToolbar) protected Toolbar toolbar;
    @BindView(R.id.base_drawer_layout) protected DrawerLayout drawer;
    @BindView(R.id.base_navigation_view) protected NavigationView navigationView;
    @BindView(R.id.view_stub) protected FrameLayout viewStub;
    protected ActionBarDrawerToggle toggle;

    View headerView;
    TextView studName;
    TextView studProg;
    ImageView studProfileImage;

    protected StudentBasePresenter presenter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.basedrawer_student);
        setSupportActionBar(toolbar);
        unbinder = ButterKnife.bind(this);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        headerView = navigationView.getHeaderView(0);
        studName = headerView.findViewById(R.id.tvName);
        studProg = headerView.findViewById(R.id.tvProg);
        studProfileImage = headerView.findViewById(R.id.profile_image);

        presenter = new StudentBasePresenterImpl(this, this);

        navigationView.setNavigationItemSelectedListener(this);

        progressDialog = new ProgressDialog(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void showProgress() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void setUserInfo(Student student) {
        studName.setText(String.format("%s, %s %s", student.getLastName(), student.getfName(), student.getMidName()));
        studProg.setText(String.format("%s\n%s Year", student.getProgram(), student.getYearLvl()));
        Glide.with(this).load(student.getStudentImageURL()).apply(new RequestOptions().circleCrop()).into(studProfileImage);

        studProfileImage.setOnClickListener(v -> {
            Intent profileIntent = new Intent(getApplicationContext(), StudentProfileActivity.class);
            startActivity(profileIntent);
        });
    }

    @Override
    public void setToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setContentView(int layoutResID) {
        if (viewStub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, viewStub, false);
            viewStub.addView(stubView, lp);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.studHomeMenu:
                navigationView.setCheckedItem(R.id.studHomeMenu);
                drawer.closeDrawer(GravityCompat.START);
                if (getClass() == StudHomeActivity.class) {
                    return true;
                } else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), StudHomeActivity.class));
//                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                    }, 500);
                }
                break;
            case R.id.appointments:
                navigationView.setCheckedItem(R.id.appointments);
                drawer.closeDrawer(GravityCompat.START);
                if (getClass() == StudentAppointmentsActivity.class) {
                    return true;
                } else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), StudentAppointmentsActivity.class));
//                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                    }, 500);
                }
                break;
            case R.id.classmates:
                navigationView.setCheckedItem(R.id.classmates);
                drawer.closeDrawer(GravityCompat.START);
                if (getClass() == ClassmateLogActivity.class) {
                    return true;
                } else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), ClassmateLogActivity.class));
//                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                    }, 500);
                }
                break;
            case R.id.pending_requests:
                navigationView.setCheckedItem(R.id.pending_requests);
                drawer.closeDrawer(GravityCompat.START);
                if (getClass() == StudentPendingRequestsLogActivity.class) {
                    return true;
                } else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), StudentPendingRequestsLogActivity.class));
//                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                    }, 500);
                }
                break;
            case R.id.consultation_responses:
                navigationView.setCheckedItem(R.id.consultation_responses);
                drawer.closeDrawer(GravityCompat.START);
                if (getClass() == StudentConsultationResponseLogActivity.class) {
                    return true;
                } else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), StudentConsultationResponseLogActivity.class));
//                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                    }, 500);
                }

                break;
//            case R.id.announcements:
//                navigationView.setCheckedItem(R.id.announcements);
//                drawer.closeDrawer(GravityCompat.START);
//                if (getClass() == StudNewsFeedActivity.class) {
//                    return true;
//                } else {
//                    new Handler().postDelayed(() -> {
//                        startActivity(new Intent(getApplicationContext(), StudNewsFeedActivity.class));
////                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
//                    }, 500);
//                }
//                break;
//            case R.id.lectures:
//                navigationView.setCheckedItem(R.id.lectures);
//                drawer.closeDrawer(GravityCompat.START);
//                if (getClass() == StudLectureActivity.class) {
//                    return true;
//                } else {
//                    new Handler().postDelayed(() -> {
//                        startActivity(new Intent(getApplicationContext(), StudLectureActivity.class));
////                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
//                    }, 500);
//
//                }
//                break;
            case R.id.consult:
                navigationView.setCheckedItem(R.id.consult);
                drawer.closeDrawer(GravityCompat.START);
                if (getClass() == ConsultDepartmentsActivity.class) {
                    return true;
                } else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), ConsultDepartmentsActivity.class));
//                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                    }, 500);
                }
                break;
            case R.id.stud_consultation_history:
                navigationView.setCheckedItem(R.id.stud_consultation_history);
                drawer.closeDrawer(GravityCompat.START);
                if (getClass() == StudentConsultationLogActivity.class){
                    return true;
                }
                else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), StudentConsultationLogActivity.class));
                        }, 500);
                }
                break;
//            case R.id.checkClass:
//                navigationView.setCheckedItem(R.id.checkClass);
//                drawer.closeDrawer(GravityCompat.START);
////                new Handler().postDelayed(new Runnable() {
////                    @Override
////                    public void run() {
////
////                    }
////                }, 500);
//                break;
//            case R.id.search:
//                navigationView.setCheckedItem(R.id.search);
//                drawer.closeDrawer(GravityCompat.START);
//                navigationView.setCheckedItem(R.id.search);
//                if (getClass() == SearchClassActivity.class) {
//                    return true;
//                } else {
//                    new Handler().postDelayed(() -> {
//                        startActivity(new Intent(getApplicationContext(), SearchClassActivity.class));
////                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
//                    }, 500);
//                }
//                break;
            case R.id.searchClassmate:
                navigationView.setCheckedItem(R.id.searchClassmate);
                drawer.closeDrawer(GravityCompat.START);
                if (getClass() == ClassmateSearchActivity.class) {
                    return true;
                }
                else {
                    new Handler().postDelayed(() -> {
                        startActivity(new Intent(getApplicationContext(), ClassmateSearchActivity.class));
//                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left);
                    }, 500);
                }
                break;
            case R.id.about:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(() ->
                                startActivity(new Intent(getApplicationContext(), AboutAppActivity.class)),
                        500);
                break;
            case R.id.signOut:
                navigationView.setCheckedItem(R.id.signOut);
                presenter.requestLogout();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you want to exit?");
            builder.setPositiveButton("YES", (dialogInterface, i) -> finishAffinity());
            builder.setNegativeButton("NO", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.onDestroy();
        progressDialog.dismiss();
    }
}

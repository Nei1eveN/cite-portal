package cp2.citeportalv1.models;

public class ClassmateRequest extends ClassmateRequestKeyId {
    private String senderUid, requestType;

    private String studentImageURL;

    private Long studentId;

    private String firstName;

    private String middleName;

    private String lastName;

    private String program;

    private String yearLvl;

    public ClassmateRequest() {
    }

    public ClassmateRequest(String senderUid, String requestType, String studentImageURL, Long studentId, String firstName, String middleName, String lastName, String program, String yearLvl) {
        this.senderUid = senderUid;
        this.requestType = requestType;
        this.studentImageURL = studentImageURL;
        this.studentId = studentId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.program = program;
        this.yearLvl = yearLvl;
    }

    public String getSenderUid() {
        return senderUid;
    }

    public void setSenderUid(String senderUid) {
        this.senderUid = senderUid;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getStudentImageURL() {
        return studentImageURL;
    }

    public void setStudentImageURL(String studentImageURL) {
        this.studentImageURL = studentImageURL;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getYearLvl() {
        return yearLvl;
    }

    public void setYearLvl(String yearLvl) {
        this.yearLvl = yearLvl;
    }
}

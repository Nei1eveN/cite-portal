package cp2.citeportalv1.models;

import java.util.Locale;

import androidx.annotation.NonNull;

public class Interval {
    public int start;
    public int end;
    public Interval(int s, int e) {
        start = s; end = e;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format(Locale.getDefault(),"[%3d, %3d]", start, end);
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + start;
        result = prime * result + end;
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Interval other = (Interval) obj;
        if (start != other.start)
            return false;
        if (end != other.end)
            return false;
        return true;
    }
}
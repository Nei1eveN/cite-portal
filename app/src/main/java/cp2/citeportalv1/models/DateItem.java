package cp2.citeportalv1.models;

public class DateItem extends ListItem {

    private String date;
    private String day;

    private String department;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public int getType() {
        return TYPE_DATE;
    }
}

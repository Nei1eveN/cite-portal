package cp2.citeportalv1.models;

import androidx.annotation.NonNull;

public class AppointmentId {
    public String appointmentId;

    public <T extends AppointmentId> T withId(@NonNull final String appointmentId){
        this.appointmentId = appointmentId;
        return (T) this;
    }
}

package cp2.citeportalv1.models;

import io.reactivex.annotations.NonNull;

public class ClassmateRequestKeyId {
    public String classmateRequestKeyId;

    public <T extends ClassmateRequestKeyId> T withId(@NonNull final String classmateRequestKeyId){
        this.classmateRequestKeyId = classmateRequestKeyId;
        return (T) this;
    }
}

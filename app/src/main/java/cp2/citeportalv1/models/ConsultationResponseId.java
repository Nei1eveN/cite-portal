package cp2.citeportalv1.models;

import androidx.annotation.NonNull;

public class ConsultationResponseId {
    public String notificationId;

    public <T extends ConsultationResponseId> T withId(@NonNull final String notificationId){
        this.notificationId = notificationId;
        return (T) this;
    }
}

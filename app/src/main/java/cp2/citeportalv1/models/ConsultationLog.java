package cp2.citeportalv1.models;

import java.util.ArrayList;
import java.util.List;

public class ConsultationLog extends ConsultationLogId {

    private String receiverId;
    private String senderId;

    private String senderFirstName;
    private String senderMiddleName;
    private String senderLastName;
    private String senderImageURL;
    private String senderProgram;
    private String senderYearLevel;

    private String senderDepartment;

    private String messageTitle;
    private String messageBody;
    private String messageStatus;
    private String messageRemarks;
    private String messageSideNote;

    private String requestedDay;
    private String requestedDate;
    private String requestedSchedule;
    private String requestedTimeStart;
    private String requestedTimeEnd;
    private String venue;

    private String timeStamp;

    private String appointmentCode;

    private String appointmentFeedback;

    private List<String> senderCc = new ArrayList<>();

    private String senderSignature;
    private String receiverSignature;

    public ConsultationLog() {
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderFirstName() {
        return senderFirstName;
    }

    public void setSenderFirstName(String senderFirstName) {
        this.senderFirstName = senderFirstName;
    }

    public String getSenderMiddleName() {
        return senderMiddleName;
    }

    public void setSenderMiddleName(String senderMiddleName) {
        this.senderMiddleName = senderMiddleName;
    }

    public String getSenderLastName() {
        return senderLastName;
    }

    public void setSenderLastName(String senderLastName) {
        this.senderLastName = senderLastName;
    }

    public String getSenderImageURL() {
        return senderImageURL;
    }

    public void setSenderImageURL(String senderImageURL) {
        this.senderImageURL = senderImageURL;
    }

    public String getSenderProgram() {
        return senderProgram;
    }

    public void setSenderProgram(String senderProgram) {
        this.senderProgram = senderProgram;
    }

    public String getSenderYearLevel() {
        return senderYearLevel;
    }

    public void setSenderYearLevel(String senderYearLevel) {
        this.senderYearLevel = senderYearLevel;
    }

    public String getSenderDepartment() {
        return senderDepartment;
    }

    public void setSenderDepartment(String senderDepartment) {
        this.senderDepartment = senderDepartment;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getMessageRemarks() {
        return messageRemarks;
    }

    public void setMessageRemarks(String messageRemarks) {
        this.messageRemarks = messageRemarks;
    }

    public String getMessageSideNote() {
        return messageSideNote;
    }

    public void setMessageSideNote(String messageSideNote) {
        this.messageSideNote = messageSideNote;
    }

    public String getRequestedDay() {
        return requestedDay;
    }

    public void setRequestedDay(String requestedDay) {
        this.requestedDay = requestedDay;
    }

    public String getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getRequestedSchedule() {
        return requestedSchedule;
    }

    public void setRequestedSchedule(String requestedSchedule) {
        this.requestedSchedule = requestedSchedule;
    }

    public String getRequestedTimeStart() {
        return requestedTimeStart;
    }

    public void setRequestedTimeStart(String requestedTimeStart) {
        this.requestedTimeStart = requestedTimeStart;
    }

    public String getRequestedTimeEnd() {
        return requestedTimeEnd;
    }

    public void setRequestedTimeEnd(String requestedTimeEnd) {
        this.requestedTimeEnd = requestedTimeEnd;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getAppointmentCode() {
        return appointmentCode;
    }

    public void setAppointmentCode(String appointmentCode) {
        this.appointmentCode = appointmentCode;
    }

    public String getAppointmentFeedback() {
        return appointmentFeedback;
    }

    public void setAppointmentFeedback(String appointmentFeedback) {
        this.appointmentFeedback = appointmentFeedback;
    }

    public List<String> getSenderCc() {
        return senderCc;
    }

    public void setSenderCc(List<String> senderCc) {
        this.senderCc = senderCc;
    }

    public String getSenderSignature() {
        return senderSignature;
    }

    public void setSenderSignature(String senderSignature) {
        this.senderSignature = senderSignature;
    }

    public String getReceiverSignature() {
        return receiverSignature;
    }

    public void setReceiverSignature(String receiverSignature) {
        this.receiverSignature = receiverSignature;
    }
}

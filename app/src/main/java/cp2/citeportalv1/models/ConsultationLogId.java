package cp2.citeportalv1.models;

import androidx.annotation.NonNull;

public class ConsultationLogId {
    public String consultationLogId;

    public <T extends ConsultationLogId> T withId(@NonNull final String consultationLogId){
        this.consultationLogId = consultationLogId;
        return (T) this;
    }
}

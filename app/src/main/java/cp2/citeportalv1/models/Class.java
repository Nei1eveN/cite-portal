package cp2.citeportalv1.models;

import java.util.HashMap;
import java.util.Map;

public class Class extends ClassId {

//    private String classId;
    private String course;
    private String courseCode;
    private String courseDescription;
    private String section;
    private String createdBy;

    public Class() {
    }

//    public String getClassId() {
//        return classId;
//    }
//
//    public void setClassId(String classId) {
//        this.classId = classId;
//    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Map<String, Object> toMap(){
        HashMap<String, Object> courseMap = new HashMap<>();
        courseMap.put("Section", this.section);
        courseMap.put("Course Code", this.courseCode);
        courseMap.put("Course Description", this.courseDescription);
        courseMap.put("Instructor", this.createdBy);
        return courseMap;
    }
}

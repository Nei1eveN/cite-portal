package cp2.citeportalv1.models;

import io.realm.RealmObject;

public class Course extends RealmObject {
    private String courseCode;
    private String courseDescription;

    public Course() {
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }
}

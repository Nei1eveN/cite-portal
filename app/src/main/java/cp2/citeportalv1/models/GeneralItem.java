package cp2.citeportalv1.models;

public class GeneralItem extends ListItem {

    private ConsultationResponse response;
    private ConsultationRequest request;
    private Faculty faculty;

    private Schedule schedule;
    private Appointments appointments;

    private ConsultationLog consultationLog;

    public ConsultationResponse getResponse() {
        return response;
    }

    public void setResponse(ConsultationResponse response) {
        this.response = response;
    }

    public ConsultationRequest getRequest() {
        return request;
    }

    public void setRequest(ConsultationRequest request) {
        this.request = request;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Appointments getAppointments() {
        return appointments;
    }

    public void setAppointments(Appointments appointments) {
        this.appointments = appointments;
    }

    public ConsultationLog getConsultationLog() {
        return consultationLog;
    }

    public void setConsultationLog(ConsultationLog consultationLog) {
        this.consultationLog = consultationLog;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    @Override
    public int getType() {
        return TYPE_GENERAL;
    }
}

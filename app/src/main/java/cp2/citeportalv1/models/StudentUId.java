package cp2.citeportalv1.models;

import androidx.annotation.NonNull;

public abstract class StudentUId {
    public String studentUId;

    public <T extends StudentUId> T withId(@NonNull final String studentUId) {
        this.studentUId = studentUId;
        return (T) this;
    }
}

package cp2.citeportalv1.models;

import io.reactivex.annotations.NonNull;

public class ClassId {
    public String facultyClassId;

    public <T extends ClassId> T withId(@NonNull final String facultyClassId){
        this.facultyClassId = facultyClassId;
        return (T) this;
    }
}

package cp2.citeportalv1.models;

import androidx.annotation.NonNull;

public class FacultyId {
    public String facultyId;

    public <T extends FacultyId> T withId(@NonNull final String facultyId){
        this.facultyId = facultyId;
        return (T) this;
    }
}

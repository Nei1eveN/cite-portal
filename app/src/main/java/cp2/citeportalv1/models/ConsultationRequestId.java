package cp2.citeportalv1.models;

import androidx.annotation.NonNull;

public class ConsultationRequestId {
    public String notificationId;

    public <T extends ConsultationRequestId> T withId(@NonNull final String notificationId){
        this.notificationId = notificationId;
        return (T) this;
    }
}

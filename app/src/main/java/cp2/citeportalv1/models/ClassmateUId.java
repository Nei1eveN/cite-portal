package cp2.citeportalv1.models;

import io.reactivex.annotations.NonNull;

public class ClassmateUId {
    public String classmateUId;

    public <T extends ClassmateUId> T withId(@NonNull final String classmateUId){
        this.classmateUId = classmateUId;
        return (T) this;
    }
}

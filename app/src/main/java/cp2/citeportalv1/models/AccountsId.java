package cp2.citeportalv1.models;

import androidx.annotation.NonNull;

public class AccountsId {
    public String accountsId;

    public <T extends AccountsId> T withId(@NonNull final String accountsId){
        this.accountsId = accountsId;
        return (T) this;
    }
}

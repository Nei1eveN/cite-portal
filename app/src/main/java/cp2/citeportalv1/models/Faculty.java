package cp2.citeportalv1.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Faculty extends FacultyId {

    private String facultyImageURL;

//    private Integer employeeID;

    @SerializedName("employeeID")
    @Expose
    private String employeeID;

    private String fName;

    private String midName;

    private String lastName;

    private String department;

    private String email;

    private String contactNumber;

    private String userSignature;

    public Faculty() {
    }

    public String getFacultyImageURL() {
        return facultyImageURL;
    }

    public void setFacultyImageURL(String facultyImageURL) {
        this.facultyImageURL = facultyImageURL;
    }

//    public Integer getEmployeeID() {
//        return employeeID;
//    }
//
//    public void setEmployeeID(Integer employeeID) {
//        this.employeeID = employeeID;
//    }

    public String getEmployeeID() {
        return employeeID;
    }

    @NonNull
    @Override
    public String toString() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getMidName() {
        return midName;
    }

    public void setMidName(String midName) {
        this.midName = midName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getUserSignature() {
        return userSignature;
    }

    public void setUserSignature(String userSignature) {
        this.userSignature = userSignature;
    }
}

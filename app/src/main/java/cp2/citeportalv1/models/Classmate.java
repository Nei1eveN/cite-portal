package cp2.citeportalv1.models;

public class Classmate extends ClassmateUId {
    private String studentImageURL;

    private Long studentId;

    private String firstName;

    private String middleName;

    private String lastName;

    private String program;

    private String yearLvl;

    private String email;

    private String classmate_since;

    public Classmate() {
    }

    public Classmate(String studentImageURL, Long studentId, String firstName, String middleName, String lastName, String program, String yearLvl, String email, String classmate_since) {
        this.studentImageURL = studentImageURL;
        this.studentId = studentId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.program = program;
        this.yearLvl = yearLvl;
        this.email = email;
        this.classmate_since = classmate_since;
    }

    public String getStudentImageURL() {
        return studentImageURL;
    }

    public void setStudentImageURL(String studentImageURL) {
        this.studentImageURL = studentImageURL;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getYearLvl() {
        return yearLvl;
    }

    public void setYearLvl(String yearLvl) {
        this.yearLvl = yearLvl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClassmate_since() {
        return classmate_since;
    }

    public void setClassmate_since(String classmate_since) {
        this.classmate_since = classmate_since;
    }
}

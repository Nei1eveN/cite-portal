package cp2.citeportalv1.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Student extends StudentUId {

    private String studentImageURL;

    @SerializedName("studentID")
    @Expose
    private Long studentID;

    private String fName;

    private String midName;

    private String lastName;

    private String program;

    private String yearLvl;

    private String email;

    private String contactNumber;

    private String userSignature;

    public Student() {
    }

    public String getStudentImageURL() {
        return studentImageURL;
    }

    public void setStudentImageURL(String studentImageURL) {
        this.studentImageURL = studentImageURL;
    }

//    @Exclude
//    @Override
    public Long getStudentID() {
        return studentID;
    }

//    @Exclude
//    @Override
    public void setStudentID(Long studentID) {
        this.studentID = studentID;
    }

//    @NonNull
//    @Override
//    public String toString() {
//        return studentID;
//    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getMidName() {
        return midName;
    }

    public void setMidName(String midName) {
        this.midName = midName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getYearLvl() {
        return yearLvl;
    }

    public void setYearLvl(String yearLvl) {
        this.yearLvl = yearLvl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getUserSignature() {
        return userSignature;
    }

    public void setUserSignature(String userSignature) {
        this.userSignature = userSignature;
    }
}

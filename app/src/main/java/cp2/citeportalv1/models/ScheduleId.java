package cp2.citeportalv1.models;

import androidx.annotation.NonNull;

public class ScheduleId {
    public String scheduleId;

    public <T extends ScheduleId> T withId(@NonNull final String scheduleId){
        this.scheduleId = scheduleId;
        return (T) this;
    }
}
